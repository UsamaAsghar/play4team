import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,Slider,KeyboardAvoidingView,ToastAndroid,
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import DateTimePicker from 'react-native-datepicker'
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { observer } from 'mobx-react';
import Store from '../Stores';
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';
import closeImgLight from "../img/multiply.png";
const homePlace = {description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = {description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};
const DARK_COLOR = "#18171C";
const PLACEHOLDER_COLOR = "rgba(255,255,255,0.2)";
const LIGHT_COLOR = "#FFF";
const API_KEY='AIzaSyAmnk3Obj7VDY1GfuZ_A6ep8voAGqJfayE';
const NORTH_AMERICA = ['CA', 'MX', 'US']
export default class SignUp extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      checked: false,
      f_name: '',
      about_me: '',
      email: '',
      phone: '',
      postalCode: '',
      dob: '',
      gender: 1,
      fitness: 0,
      password: '',
      address:'',
      latitude: '',
      longitude: '',
      cca2:'US',
      callingCode:'',
      value: false,
    }
  }
  getPredictions(postalCode){
    let { orderStore } = Store;
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    if(postalCode.length!==0){
      fetch('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + postalCode + '&key=' + api_key )
          .then((response) => response.json())
          .then((responseJson) => {
          console.log('Predictions', responseJson);
          if (responseJson.status === 'OK') {
              orderStore.predictions = responseJson.predictions;
              for(var i=0;i<orderStore.predictions.length;i++){
                orderStore.predictions[i].added = false;
                orderStore.predictions[i].checkStatus = false;
              }
              this.setState({loading: false})
           }
     })
    }else{
        orderStore.predictions=[];
        this.setState({loading: false})
    }
  }
  getLatLong(item){
    let { orderStore } = Store;
    this.setState({address: item.description})
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + item.description + '&key=' + api_key )
          .then((response) => response.json())
          .then((responseJson) => {
          console.warn('latLong response', responseJson);
          if (responseJson.status === 'OK') {
              this.setState({
                 latitude: responseJson.results[0].geometry.location.lat,
                 longitude: responseJson.results[0].geometry.location.lng,
                //  address: responseJson.results[0].formatted_address,
              })
              orderStore.predictions = [];
              this.setState({loading: false})
           }
     })
  }
  register(){
    let { orderStore } = Store;
    var {params} = this.props.navigation.state;
    if ( this.state.email.length === 0 || this.state.f_name.length === 0 || this.state.checked=== false || this.state.email.length === 0 || this.state.address.length === 0) {
        ToastAndroid.show('Please enter required fields and make sure you have checked the terms and conditions', ToastAndroid.LONG);
    }
      else {
        // this.get();
        this.setState({loading: true})
      var url = 'http://play4team.com/AdminApi/api/Players?'+'&First_Name='+this.state.f_name+'&password='+this.state.password+'&About=iamgood'+'&Email='+this.state.email+'&Phone='+this.state.callingCode+'&address='+this.state.address+'&Dates='+this.state.dob+'&gender='+this.state.gender+'&Rate='+this.state.fitness+
      '&Status=0&player_latitude='+this.state.latitude+'&player_longitude='+this.state.longitude;
       console.log('url=',url);
        fetch('http://play4team.com/AdminApi/api/Players?'+'&First_Name='+this.state.f_name+'&password='+this.state.password+'&About=iamgood'+'&Email='+this.state.email+'&Phone='+this.state.callingCode+'&address='+this.state.address+'&Dates='+this.state.dob+'&gender='+this.state.gender+'&Rate='+this.state.fitness+
        '&Status=0&player_latitude='+this.state.latitude+'&player_longitude='+this.state.longitude, {
         method: 'POST',
           headers: {
             'Content-Type': 'application/json',
             'Accept': 'application/json',
           }
          }).then((response) => response.json())
                .then((responseJson) => {
                    this.setState({loading: false})
                  if (responseJson.status === 'True') {
                      orderStore.RES.register = true;
                      orderStore.RES.email = this.state.email;
                      orderStore.RES.password = this.state.password;
                      ToastAndroid.show('You have successfuly sign up as a player', ToastAndroid.LONG);
                      this.props.navigation.navigate('Login',{type:'0'});
                  }else {
                    this.setState({loading: false})
                    ToastAndroid.show('This email is already used please enter another email', ToastAndroid.LONG);
                  }
                }).catch((error)=>{
                    this.setState({loading: false})
                    ToastAndroid.show('There is a network issue please,Try again', ToastAndroid.LONG);
                });
      }
    }
    componentWillUnmount(){
      let { orderStore } = Store;
      orderStore.predictions = [];
    }
    termsCondition(checked){
      this.setState({checked: checked})
      if (checked===true) {
          this.props.navigation.navigate('Lisence')
      }
    }
    static navigationOptions = {
          header: null
      }
  render() {
    let { orderStore } = Store;
    var image = <Image style={{height:height(2),width:width(4),resizeMode:'contain'}} source={require('../img/multiply.png')} />;
    if(this.state.loading == true){
        return(
            <ImageBackground style={{height: height(100), width: width(100), flex:1}} source={require('../img/splash.jpg')} >
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </ImageBackground>
        );
    }
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../img/bg_img.jpg')} style={{flex:1}}>
          <ScrollView
            bounces={true}
            bouncesZoom={true}
            indicatorStyle='white'
            pinchGestureEnabled={true}
            ref={ref => this.scrollView = ref}
            onContentSizeChange={()=>{        
                this.scrollView.scrollToEnd({animated: true});
            }}>
            <View style={{flex:1,alignItems:'center',backgroundColor:'rgba(29,37,86,0.8)'}}>
              <View style={{height:height(8),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2),color:'white'}}>SIGN UP</Text>
              </View>
              <View style={{height:height(10),width:width(90)}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/first_name.png')} style={{height:height(2.5),width:width(3.5),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Player Name (required)</Text>
                </View>
                <View style={{height:height(6),width:width(90),justifyContent:'center',alignItems:'center',borderRadius:2}}>
                  <TextInput
                      onChangeText={(value) => this.setState({f_name: value})}
                      underlineColorAndroid='transparent'
                      placeholder='Player Name'
                      autoFocus={true}
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={{height:height(6),width:width(90),fontSize:totalSize(1.5),textAlign :'center',marginTop:0,color:'black',backgroundColor:'white'}}
                      />
                </View>
              </View>
              <View style={{height:height(10),width:width(90)}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/mail_icon.png')} style={{height:height(2.5),width:width(4.5),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Email (required)</Text>
                </View>
                <View style={{height:height(6),width:width(90),justifyContent:'center',alignItems:'center',borderRadius:2}}>
                  <TextInput
                      onChangeText={(value) => this.setState({email: value})}
                      underlineColorAndroid='transparent'
                      placeholder='Email'
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={{height:height(6),width:width(90),fontSize:totalSize(1.5),textAlign :'center',color:'black',marginTop:0,backgroundColor:'white'}}
                      />
                </View>
              </View>
              <View style={{height:height(10),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/call.png')} style={{height:height(3),width:width(4),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Phone</Text>
                </View>
                <View style={{height:height(6),width:width(90),flexDirection:'row',borderRadius:2,backgroundColor:'white'}}>
                  <View style={{height:height(6),width:width(10),justifyContent:'center'}}>
                    <CountryPicker
                       filterPlaceholderTextColor={PLACEHOLDER_COLOR}
                      //  countryList={NORTH_AMERICA}
                       closeable={true}
                       flagType='flat'
                       filterable={true}
                       filterPlaceholder='Search your country'
                       filterPlaceholderTextColor='black'
                       autoFocusFilter={true}
                       transparent={true}
                       showCallingCode={true}
                      //  closeButtonImage={closeImgLight}
                       renderImageFlag={closeImgLight}
                       imgStyle={{height:height(4),width:width(10),resizeMode:'contain'}}
                       onChange={(value)=>{this.setState({ cca2: value.cca2, callingCode: value.callingCode })}}
                       styles={darkTheme}
                       cca2='US'
                       translation="eng"
                      //  {...props}
                      />
                  </View>
                  <View style={{height:height(6),width:width(25),justifyContent:'center'}}>
                    <Text style={styles.textSimple}>+</Text>
                  </View>
                  <TextInput
                      onChangeText={(value) => this.setState({callingCode: value})}
                      onSubmitEditing={(value)=>this.setState({phone: this.state.callingCode})}
                      // mask='+[00] ([000]) [000] [00] [00]'
                      underlineColorAndroid='transparent'
                      editable={true}
                      autoFocus={this.state.callingCode.length===0? false:true}
                      placeholder='Phone Number'
                      value={this.state.callingCode}
                      placeholderTextColor='gray'
                      textContentType='telephoneNumber'
                      dataDetectorTypes='phoneNumber'
                      keyboardType='numeric'
                      underlineColorAndroid='transparent'
                      autoCorrect={this.state.callingCode.length>0? true:false}
                      style={{height:height(6),width:width(55),fontSize:totalSize(1.5),textAlign :'left',marginTop:0,backgroundColor:'white'}}
                      />
                </View>
              </View>
              <View style={{height:height(10),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/lock_icon.png')} style={{height:height(3),width:width(3.8),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Postal/Zip Code</Text>
                </View>
                <View style={{height:height(6),width:width(90),borderRadius:2}}>
                  <TextInput
                       onChangeText={(value) => this.getPredictions(value)}
                       underlineColorAndroid='transparent'
                       placeholder='Postal/Zip Code'
                       value = {this.state.address.length>0?this.state.address:null}
                       onFocus={()=>{ this.setState({address: ''}) }}
                       placeholderTextColor='gray'
                       keyboardType = 'email-address'
                      underlineColorAndroid='transparent'
                      autoCorrect={true}
                      style={{height:height(6),width:width(90),textAlign :'center',fontSize:totalSize(1.5),marginTop:0,backgroundColor:'white',paddingLeft:10}}
                      />
                </View>
              </View>
              {
              orderStore.predictions.length > 0?
                <View style={{width:width(90),backgroundColor:'white',elevation:5}}>
                  {
                    orderStore.predictions.map((item,key)=>{
                      return(
                        <TouchableOpacity key={key} style={{marginHorizontal:5,borderBottomWidth:0.4,borderColor:'gray',elevation:0}}
                          onPress={()=>{
                            this.getLatLong(item)
                          }}
                        >
                          <Text style={{fontSize:totalSize(1.6),color:'black',marginVertical:10}}>{ item.description }</Text>
                        </TouchableOpacity>
                      )
                    })
                  }
                </View>
              :
              null
            }
              <View style={{height:height(10),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/calender.png')} style={{height:height(2.5),width:width(5),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Date Of Birth</Text>
                </View>
                <View style={{height:height(6),width:width(90),borderRadius:2,justifyContent:'center',flexDirection:'row'}}>
                  <TouchableOpacity style={{height:height(5.5),width:width(10),backgroundColor:'blue',justifyContent:'center',alignItems:'center',backgroundColor:'white'}}>
                    <DateTimePicker
                      style={{width:width(6),height:height(4),backgroundColor:'transparent'}}
                      date={this.state.dob}
                      // time={this.state.time}
                      mode="date"
                      placeholder="Event Start"
                      format="DD-MM-YYYY"
                      minDate="01-01-1990"
                      maxDate="01-01-2090"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      showIcon= {true}
                      hideText='false'
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                           right: 0,
                           top: 0,
                           marginBottom: 0,
                           marginRight: 0,
                           height:height(4),
                           width:width(6)
                        },
                        dateInput: {
                          marginLeft: 0
                        }
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(date) => {this.setState({dob: date})}}
                    />
                  </TouchableOpacity>
                  <TextInput
                      onChangeText={(value) => this.setState({dob: value})}
                      underlineColorAndroid='transparent'
                      placeholder='DD/MM/YYYY'
                      value={this.state.dob}
                      keyboardType='phone-pad'
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={{height:height(5.5),width:width(80),marginRight:10,fontSize:totalSize(1.5),backgroundColor:'white',textAlign:'center',paddingRight:30}}
                      />
                </View>
              </View>
              <View style={{height:height(10),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                  <Image source={require('../img/lock_icon.png')} style={{height:height(3.2),width:width(3.8),margin:3,resizeMode:'contain'}}/>
                  <Text style={{fontSize:totalSize(1.5),color:'white',margin:3}}>Password (required)</Text>
                </View>
                <View style={{height:height(6),width:width(90),borderRadius:2}}>
                  <TextInput
                      onChangeText={(value) => this.setState({password: value})}
                      underlineColorAndroid='transparent'
                      placeholder='password'
                      secureTextEntry = { true }
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={{height:height(5.5),width:width(90),textAlign :'center',marginTop:0,backgroundColor:'white',fontSize:totalSize(1.5),paddingLeft:10}}
                      />
                </View>
              </View>
              <View style={{height:height(10),width:width(90),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(3),width:width(90),justifyContent:'center',alignItems:'flex-start'}}>
                  <Text style={{fontSize:totalSize(1.5),color:'white'}}>Gender</Text>
                </View>
                <View style={{height:height(5),width:width(90),borderRadius:2,justifyContent:'center'}}>
                  <View style={{height:height(4.2),width:width(44.5),borderRadius:2,flexDirection:'row',justifyContent:'center',borderWidth:0.5,borderColor:'#27dbac'}}>
                      <TouchableOpacity style={this.state.gender === 1? styles.TxtViewColor : styles.TxtView}
                        onPress={()=>{
                          this.setState({
                            gender: 1
                          })
                        }}>
                        <Text style={{fontSize:totalSize(1.5),color:'white'}}>Male</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={this.state.gender === 0? styles.TxtViewColor:styles.TxtView}
                        onPress={()=>{
                          this.setState({
                            gender: 0
                          })
                        }}>
                        <Text style={{fontSize:totalSize(1.5),color:'white'}}>Female</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={this.state.gender===2? styles.TxtViewColor:styles.TxtView}
                        onPress={()=>{
                          this.setState({
                            gender: 2
                          })
                        }}>
                        <Text style={{fontSize:totalSize(1.5),color:'white'}}>Other</Text>
                      </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{height:height(8),width:width(90)}}>
                <View style={{height:height(3),flexDirection:'row'}}>
                  <View style={{width:width(60)}}>
                    <Text style={{fontSize:totalSize(1.4),color:'white'}}>How would you rate your general fitness</Text>
                  </View>
                  <View style={{width:width(30),alignItems:'flex-end'}}>
                    <Text style={{fontSize:totalSize(1.4),color:'white'}}>Your rate:{parseInt(this.state.fitness)}</Text>
                  </View>
                </View>
                <Slider
                  value={this.state.fitness}
                  onValueChange={ value => this.setState({fitness: parseInt(value) })}
                  maximumValue={10}
                  minimumValue={0}
                  thumbTintColor='#29d9a4'
                  maximumTrackTintColor='#1fefd5'
                  minimumTrackTintColor='#1fefd5'
                  style={{backgroundColor:'white',height:height(4)}}
                />
              </View>
              {/*
                <View style={{height:height(6),width:width(90),backgroundColor:'#37da7c',justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:totalSize(2.5),color:'black'}}>SAVE</Text>
                </View>
                */}
                <View style={{height:height(6),width:width(90),flexDirection:'row'}}>
                  <View style={{width:width(10),justifyContent:'center',alignItems:'center'}}>
                    <CircleCheckBox
                      checked={this.state.checked}
                      onToggle={(checked) => this.termsCondition(checked)}
                      onPress={()=>alert('alert')}
                      outerColor  = {'#5b5b7e'}
                      innerColor  = {'#45e059'}
                      style={{height:height(2.5),width:width(4),resizeMode:'contain'}}
                      />
                  </View>
                  <View style={{width:width(80),justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={{fontSize:totalSize(1.8),color:'white'}}>I accept the terms and conditions</Text>
                  </View>
                </View>
                <TouchableOpacity style={{height:height(10),width:width(92),justifyContent:'center',alignItems:'center'}}
                  onPress = {()=> {
                    this.register()
                  }}
                >
                  <ImageBackground source={require('../img/btn.png')} style={{height:height(6),width:width(90),justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:totalSize(2.2),color:'black'}}>Sign Up</Text>
                  </ImageBackground>
                </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const darkTheme = StyleSheet.create({
 modalContainer: {
    backgroundColor: 'white'
  },
  contentContainer: {
    backgroundColor: 'white'
  },
  header: {
    backgroundColor: 'white'
  },
  itemCountryName: {
    borderBottomWidth: 0
  },
  countryName: {
    color: 'black'
  },
  letterText: {
    color: 'red'
  },
  input: {
    color: 'black',
    borderBottomWidth: 1,
    borderColor: 'black'
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textSimple:{
    fontSize:totalSize(1.8),
    color:'black',
    alignSelf:'flex-end'
  },
  textCode:{
    fontSize:totalSize(1.7),
    color:'black',
    alignSelf:'flex-end'
  },
  TxtView:{
    height:height(4.2),
    width:width(15),
    borderRadius:2,
    justifyContent:'center',
    alignItems:'center',
    borderRightWidth:0.8,
    borderColor:'#27dbac'
  },
  TxtViewColor:{
    height:height(4.2),
    width:width(14.5),
    borderRadius:2,
    justifyContent:'center',
    alignItems:'center',
    borderRightWidth:0.5,
    backgroundColor:'#27dbac',
  }

});
