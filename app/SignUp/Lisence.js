import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,Slider,ToastAndroid,Picker
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { observer } from 'mobx-react';
import Store from '../Stores';
@observer export default class SignUpTeam extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
    }
  }

  static navigationOptions = {
        headerTitle:'Terms & Conditions Agreement',
        headerTintColor:'white',
        headerStyle:{
          backgroundColor:'#1d1d4f'
        },
        headerTintStyle: {
          fontSize:totalSize(1.8)
        }
    }
  render() {
    if(this.state.loading == true){
        return(
            <ImageBackground style={{height: height(100), width: width(100), flex:1}} source={require('../img/splash.jpg')} >
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </ImageBackground>
        );
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{flex:1,margin:10}}>
            <Text style={{fontSize:totalSize(2),color:'black',fontWeight:'bold'}}>Terms and Conditions Agreement</Text>
            <Text style={{fontSize:totalSize(2),color:'black',fontWeight:'bold'}}>Play4Team</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginVertical:5}}>ONCE YOU ACCEPT THE TERMS AND CONDITIONS IT IS CONFIRMATION OF A LEGAL AGREEMENT. PLEASE READ IT AS IT CONFIRMS THE OBLIGATIONS UPON YOU AND IF YOU DO NOT UNDERSTAND ANY OF ITS TERMS PLEASE SEEK ADVICE FROM A LEGAL PROFESSIONAL.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>A) Establishment</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.1  This application is provided to you by Play4Team group. We are solely responsible for the Application and the content thereof.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.2  The Play4Team Application is only made available for personal non-commercial use. You may not download, install or use the Play4Team Application other than for personal non-commercial use.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.3  You may use Play4Team Application only as permitted under these Terms and Conditions.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.4  By downloading, installing and accepting the Terms and Conditions upon registration of the Play4Team Application, you accept the responsibility bestowed upon you within the Terms and Conditions.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.5  Any updates, upgrades or new versions of the Play4Team Application we provide to you shall be deemed to be part of the Play4Team Application and subject to the terms and conditions.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.6  No correspondence between you and us, any marketing or advertising material made available by us to you or similar materials shall form part of the contractual relationship between you and us. You are not able to rely upon such correspondence or materials as they do not form part of the agreement between you and us relating to your downloading, installation and use of the Play4Team Application.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.7  You are responsible for making all arrangements necessary for you to download, install and use the Play4Team Application. You are responsible for ensuring that any devices or software you use in connection with the Play4Team Application meet all relevant technical specifications required download, install or use the Play4Team Application (as appropriate). We shall not be liable to you for any loss, cost, expense or damage arising as a result of any technical incompatibility between the Play4Team Application and any of your devices or software.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.8  You are also responsible for ensuring that all persons who access the Play4Team Application through any device upon which you install the Play4Team Application are aware of the terms and conditions which comply with the Application.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>A.9  We make no representations, warranties or guarantees that the Play4Team Application is appropriate or available for downloading, installation or use in all locations. Where you download, install or use the Play4Team Application you do so at your own risk and are responsible for compliance with all laws applicable to such location.</Text>
  
            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>B) Minimum Age</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>B.1  You must be aged 18 or over</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>C) Terms of Use</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.1  This application is provided to you by Play4Team group. We are solely responsible for the Application and the content thereof.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.2  We grant you no rights to download, install or use the Play4Team Application other than as stated in these Terms and Conditions. We reserve all rights in and to the Play4Team Application not expressly granted to you under these Terms and Conditions. Nothing contained should be construed as granting by implication, estoppel, personal bar or otherwise any right to download, install or use the Play4Team Application without our express written permission. The Play4Team Application not sold to you.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.3  Your right to download, install and / or use the Play4Team Application is dependant upon the provider (Apple, Andriod IOS).</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.4  Your right to download, install and use the Play4Team Application is limited to a non-transferable right to download, install and use the Play4Team Application on any device compatible.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.5  You are not permitted to download, install or use the Play4Team Application on any device that you do not own or control.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.6  You must not distribute or make the Play4Team Application available over a network where it could be used by multiple devices at the same time.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.7  You must not decompile, reverse engineer, disassemble, attempt to derive the source code of, modify, translate, merge, adapt, vary, alter, or create derivative works based upon the Play4Team Application.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.8  You must not use the Play4Team Application to distribute material which is malicious or technologically harmful; defamatory of any person; obscene, offensive, hateful or inflammatory; which promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation or age; which discloses the name, address, telephone, mobile or fax number, e-mail address or any other personal data in respect of any individual other than where you have the express written permission of that individual to disclose such information; which may be in contempt of court; or which is likely to harass, upset, embarrass, alarm or annoy any other person.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.9  You must not use the Play4Team Application to impersonate any person, or misrepresent your identity or affiliation with any person.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.10 You must not use the Play4Team Application to advocate, promote, incite any third party to commit, or assist any unlawful or criminal act.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.11 You must not use the Play4Team Application to distribute any material which you know or believe, or have reasonable grounds for believing, is or is likely to be construed as a direct or indirect encouragement or other inducement to the commission, preparation or instigation of acts of terrorism.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.12 You must not use the Play4Team Application to attempt to gain unauthorised access to any server, computer or database.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.13 You must not use the Play4Team Application in a manner that imposes an unreasonable or disproportionately large load on our infrastructure, or that of any third party.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.14 You must not use the Play4Team Application to seek to infringe rights held by third parties.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.15 You must not use the Play4Team Application to breach any legal duty owned to any third party.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.16 You must not use the Play4Team Application for any purposes prohibited by the law of the country of you location applicable in your place of downloading, installation or use of the Play4Team Application</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.17 You must not use the Play4Team Application in pursuit of the development, design, manufacture or production of nuclear, missiles, or chemical or biological weapons.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>C.18  If you breach any of the Right to Use clauses we have the right to report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use the Play4Team Application will cease immediately.</Text>
        
            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>D) Data</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>D.1  You acknowledge that where the Play4Team Application is used by us to collect personal data from you, we will notify you that we are collecting this personal data. You acknowledge that for the purposes of data protection legislation we shall be the data controller of this personal data. In such an event you agree that we may use this personal data to operate and maintain the Play4Team Application, to provide you with information on products or services which may be of interest to you and to allow us to properly correspond with you. We may also share this personal data with our business partners for marketing purposes. By providing us with this personal data, you consent to being contacted in this manner by us and our business partners.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>D.2  You acknowledge that in all other events, where you input personal data through the Play4Team Application, the Play4Team Application is being used by you to transmit this personal data to third parties through telecommunications networks and services provided by third parties, and that we will have no access to or control over such personal data. We shall not be controller or processor of this personal data for the purposes of data protection legislation.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>D.3  You acknowledge that in all events any information or data we gather from you may be transmitted to and stored by our service providers. These service providers may provide a lesser standard of protection in respect of your information and data than we do. Whilst we will use reasonable endeavours to ensure that our service providers provide a reasonable standard of protection concerning your personal data we make no guarantees in this respect.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>E) Information you provide through the Play4Team Application</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>E.1  You warrant that any material submitted by you through the Play4Team Application shall be accurate.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>E.2  You warrant that any material submitted by you through the Play4Team Application shall be genuinely held.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>E.3  You warrant that any material submitted by you through the Play4Team Application shall comply with all applicable laws in any country from which it is posted, through which is may be transmitted, and in which it may be read.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>F) Personal Security</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>F.1  If you choose, or you are provided with, a user identification code, login, password or any other piece of information enabling access to or use of the Play4Team Application, you must treat such information as confidential, and you must not disclose it to any third party.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>F.2  We reserve the right to disable any user identification code, login, password, or any other piece of information enabling access to or use of the Play4Team Application at any time where we believe you are in breach or likely to breach the Terms and Conditions.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>F.3  You must immediately notify us if you have reason to believe any user identification code, login, password, or any other piece of information enabling access to or use of the Play4Team Application provided by us to you has become known to any third party.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>G) Breach</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>G.1  Any breach of the terms and conditios is a violation of our rights and (if applicable) those of our service providers and third parties. You may incur liability (both criminal and civil) and become subject to court action.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>G.2  If we have reason to believe that you have breached the Terms and Conditions of the Play4Team Application or are likely to breach we may take action to protect ourselves, our service providers and third parties from liability, including but not limited to:</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:20,marginVertical:15}}>i) contacting relevant third parties and disclosing information collected from you or concerning you</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:20,marginVertical:15}}>ii) changing, suspending, removing, or disabling access to the Play4Team Application or any Services without notice.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:20,marginVertical:15}}>iii) imposing limits on your use of or access to certain parts of the Play4Team Application or any Services</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>G.3  Where you are in breach of the Terms and Conditions you shall Compensate and keep us compensated against any loss, damager or expense arising as a result of the breach.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>H) Personal Support and Maintenance</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>H.1  We do not undertake to maintain or support the Play4Team Application</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>H.2  Both we and you acknowledge that the Application service provider has no obligation whatsoever to furnish any maintenance and support services with respect to the Play4Team Application.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>I) Free to Use Application</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>I.1  The Play4Team Application is a free to use Application any payments requested must be notified to the Play4Team group</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>J) Restrictions</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.1  We shall use reasonable endeavours to ensure that the Play4Team Application is of satisfactory quality and operates in accordance with its documentation. In the event of any failure of the Play4Team Application to comply we do not hold responsibility of the failure.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.2  We do not guarantee that any information or data made available through the Play4Team Application is accurate, complete or error free. You undertake not to rely upon such information or data, nor to provide such information or data to any third party with a view to that third party relying upon such information or data. Any reliance you or any third party places upon such information or data is entirely at your (or their) own risk and you undertake not to hold us liable for any loss, cost, damage or expense incurred as a result of such reliance.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.3  We do not guarantee that the Play4Team Application will be free of errors or defects or operate in an uninterrupted manner.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.4  We do not guarantee that the Play4Team Application will not interfere with the performance or operation of the device upon which it is installed or other software installed thereon</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.5  We do not guarantee that the Play4Team Application will be free of viruses or other malicious code. You acknowledge that you are solely responsible for making the necessary arrangements to protect your device and peripherals from infection from viruses and other forms of malicious code.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.6  We do not guarantee that the Play4Team Application is suitable for any particular purpose or that it will meet your requirements, irrespective of whether that purpose or those requirements are known to us.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>J.7  Other than as expressly stated hereunder we exclude all guarantees, warranties, conditions and representations whether express or implied to the extent permissible by law.</Text>

            <Text style={{fontSize:totalSize(1.8),color:'black',fontWeight:'bold',marginVertical:10}}>K) Liability</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.1  In no event shall we be liable to you.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.2  We shall not be liable to you for death or personal injury.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.3  We shall not be liable to you for any loss, cost, damage or expense not considered likely to arise at the date of your agreeing to the Terms and Conditions of the Play4Team Application</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.4  We shall not be liable to you for any incidental, special, indirect or consequential loss, cost, damage or expense.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.5  We shall not be liable to you for loss of profits, loss of data, business interruption or any other commercial damages or losses.</Text>
            <Text style={{fontSize:totalSize(1.8),color:'black',marginLeft:15,marginVertical:5}}>K.6  We shall not be liable to you regardless of the basis for such liability, whether under contract, delict, tort or otherwise, and even if we have been advised of the possibility of such liability.</Text>

            {/*<TouchableOpacity style={{height:height(5),width:width(30),marginVertical:15,elevation:5,backgroundColor:'#1d1d4f',justifyContent:'center',alignItems:'center',alignSelf:'center'}}
              onPress={()=>{
                this.props.navigation.goBack(),
                orderStore.agree = true;
              }}
            >
              <Text style={{fontSize:totalSize(1.8),color:'white',fontWeight:'bold'}}>I Agree</Text>
            </TouchableOpacity>*/}
          </View>
        </ScrollView>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#f9f9f9'
  },
  TxtView:{
    height:height(4.2),
    width:width(15),
    borderRadius:2,
    justifyContent:'center',
    alignItems:'center',
    borderRightWidth:0.8,
    borderColor:'#27dbac'
  },
  TxtViewColor:{
    height:height(4.2),
    width:width(14.5),
    borderRadius:2,
    justifyContent:'center',
    alignItems:'center',
    borderRightWidth:0.5,
    backgroundColor:'#27dbac',
  }

});
