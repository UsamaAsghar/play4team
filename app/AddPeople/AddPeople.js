import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import FlipToggle from 'react-native-flip-toggle-button'
import PopupDialog, {slideAnimation,DialogTitle,FadeAnimation} from 'react-native-popup-dialog';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox'
import AddPeopleComp from './AddPeopleComp';

class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>ABC</Text>
          </View>
        </View>
    );
  }
}
export default class AddPeople extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white',
    headerRight: (
      <View style={{height:height(7),width:width(10),justifyContent:'center'}}>
        <Image source={require('../img/opt_btn.png')} />
      </View>
    )
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,alignItems:'center'}}>
          <View style={{height:height(7),width:width(100),marginBottom:5,flexDirection:'row',backgroundColor:'#1d1d4f',justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5.5),width:width(80),backgroundColor:'#33327e',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/s_btn.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
              </TouchableOpacity>
              <View style={{height:height(7),width:width(70)}}>
                <TextInput
                  onChangeText={(value) => this.validate(value,'mobNo')}
                  placeholder={"Search"}
                  placeholderTextColor='white'
                  style={{fontSize:totalSize(2),color:'white',backgroundColor:'#33327e',width:width(65)}}
                  underlineColorAndroid="transparent"
                  />
              </View>
            </View>
            <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}>
              <Image source={require('../img/c_btn.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
          </View>

          <AddPeopleComp />

        </View>
        <TouchableOpacity style={{height:height(8),width:width(100)}}>
          <Image source={require('../img/add_pep.png')} style={{height:height(8),width:width(100)}} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
