import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import AddComp from './AddComp';
export default class AddFriends extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      findFriend: false,
      nearby: false,
      starter: true,
    };
  }
  static navigationOptions = {
    headerTitle : 'Add Friends',
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white',

  }

  render() {

    return(
      <View style={styles.container}>
        <View style={{height:height(8),flexDirection:'row',backgroundColor:'#33327e'}}>
          <TouchableOpacity style={{width:width(50),alignItems:'center'}}
            onPress={()=> this.setState({
              findFriend: true,
              nearby: false,
              starter: false,
            })}
          >
            <Image source={require('../img/friend_icon.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Find Friends</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:width(50),alignItems:'center'}}
            onPress={()=> this.setState({
              findFriend: false,
              nearby: true,
              starter: false,
            })}
          >
            <Image source={require('../img/nearby_user.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Nearby Users</Text>
          </TouchableOpacity>
        </View>

        <View style={{height:height(7),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(5.5),width:width(85),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/search_friends.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
            <View style={{height:height(7),width:width(70)}}>
              <TextInput
                onChangeText={(value) => this.validate(value,'mobNo')}
                placeholder={"Search"}
                style={{fontSize:totalSize(2),color:'white',backgroundColor:'white',width:width(65)}}
                underlineColorAndroid="transparent"
                />
            </View>
          </View>
          <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}>
            <Image source={require('../img/cross.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
          </TouchableOpacity>
        </View>
        {
          this.state.starter === true ?
            <View style={{flex:1}}>
              <View style={{height:height(5),backgroundColor:'#eaedf2',justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'#282c34'}}>Sync Your Contacts</Text>
              </View>
            </View>
            :
            <View style={{flex:1}}>
              {
              this.state.findFriend === true ?
                <View style={{flex:1,backgroundColor:'#f3f3f3'}}>
                  <View style={{height:height(5),backgroundColor:'#eaedf2',justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={{fontSize: totalSize(1.8),color:'#282c34',marginLeft:20}}>Already on Find a Player</Text>
                  </View>
                  <View style={{height:height(9),justifyContent:'center',alignItems:'center'}}>
                    <AddComp />
                  </View>
                  <View style={{height:height(6),backgroundColor:'#eaedf2',justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={{fontSize: totalSize(1.8),color:'#282c34',marginLeft:20}}>Invite From Your Book Phone</Text>
                  </View>
                  <View style={{height:height(52),justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:height(49),width:width(92)}}>
                      <ScrollView>
                        <AddComp />
                        <AddComp />
                        <AddComp />
                        <AddComp />
                        <AddComp />
                        <AddComp />
                        <AddComp />
                      </ScrollView>
                    </View>
                  </View>
                </View>
                :
                <View style={{flex:1,alignItems:'center'}}>
                  <View style={{height:height(2)}}></View>
                  <ScrollView>
                    <AddComp />
                  </ScrollView>  
                </View>
              }
            </View>
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
