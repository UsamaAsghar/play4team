/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  AppState,
  View,StatusBar,Alert
} from 'react-native';
import MainRoute from './Routes/MainRoute';
import store from './Stores/orderStore';
import firebase from 'react-native-firebase'
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType, NotificationActionType, NotificationActionOption, NotificationCategoryOption} from "react-native-fcm";
export default class App extends Component<Props> {
  // componentDidMount () {
  //   firebase.messaging().getToken().then(token => {
  //     console.warn("TOKEN (getFCMToken)", token);
  //   });

  //   this.notificationListener = firebase.notifications().onNotification(notification => {
  //     //firebase.notifications().displayNotification(notification);
  //     this.alert(notification)
  //     console.warn('This is Notification==>',notification)
  //   })
  // }
  // alert(notif){
  //   Alert.alert(
  //     notif.title,
  //     notif.body,
  //     [
  //       {text: 'OK', onPress: () => console.log('OK Pressed')},
  //     ],
  //     {cancelable: false},
  //   );
  // }
  // componentWillMount(){
  //   FCM.getFCMToken().then(token => {
  //     store.FireBaseNotif = token;
  //    console.log("TOKEN (getFCMToken):", store.FireBaseNotif);
  //  });
  //  FCM.on(FCMEvent.Notification, notif => {
  //     this.alert(notif)
  //     // console.log('notif===>>>',notif);
  //  }); 
  // }
  // // async componentDidMount(){
  // //   try {
  // //     let result = await FCM.requestPermissions({
  // //       badge: true,
  // //       sound: true,
  // //       alert: true
  // //     });
  // //     console.log("Notification requestPermissions : => ", result)
  // //   } catch (e) {
  // //     console.error(e);
  // //   }
  // //   // await FCM.requestPermissions({ badge: true, sound: true, alert: true })
  // //   FCM.getInitialNotification().then(notif => {
  // //     console.log("getInitialNotification Notification : => ", notif);

  // //     // if notif.targetScreen is details screen then it will redirect to details screen directly!
  // //     // if (notif && notif.targetScreen === "detail") {
  // //     //   setTimeout(() => {
  // //     //     this.props.navigation.navigate("Detail");
  // //     //   }, 500);
  // //     // }
  // //   });
  // //   FCM.getFCMToken().then(token => {
  // //     store.FireBaseNotif = token;
  // //    console.log("TOKEN (getFCMToken):", store.FireBaseNotif);
  // //   });
  // //   FCM.on(FCMEvent.Notification, async(notif) => {
  // //     console.log("Notification", notif);
  // //     if (!notif.opened_from_tray ) {
  // //       await this.showLocalNotification(notif);
  // //      }
  // //    if (AppState.currentState === 'background' ) {
  // //       console.warn('backgroundState');
  // //       // this.setState({ badgeNo: this.state.badgeNo + 1 })
  // //       // await FCM.setBadgeNumber(this.state.badgeNo)
  // //    }else{
  // //       // this.setState({ badgeNo: this.state.badgeNo - 1 })
  // //       // await FCM.setBadgeNumber(this.state.badgeNo)
  // //    }
  // //     // alert(notif.fcm.body)
  // //     // this.alert(notif)
  // //   }) 

  // //   // FCM.enableDirectChannel();
  // //   // FCM.on(FCMEvent.DirectChannelConnectionChanged, (data) => {
  // //   //   console.log('direct channel connected' + data);
  // //   // });
  // //   // setTimeout(function() {
  // //   //   FCM.isDirectChannelEstablished().then(d => console.log(d));
  // //   // }, 1000);
  // // }
  
  // showLocalNotification(notif) {
  //   FCM.presentLocalNotification({
  //     id: new Date().valueOf().toString(),         // (optional for instant notification)
  //     // title: notif.title,      // as FCM payload
  //     // body: notif.body,                // as FCM payload (required)
  //     show_in_foreground: true    ,
  //     title: notif.fcm.title,
  //     body: notif.fcm.body,
  //     priority: "high",
  //     badge: 1,
  //     number: 1,
  //     ticker: "My Notification Ticker",
  //     auto_cancel: true,
  //     // big_text: "Show when notification is expanded",
  //     // sub_text: "This is a subText",
  //     wake_screen: true,
  //     group: "group",
  //     icon: "ic_launcher",
  //     ongoing: true,
  //     my_custom_data: "my_custom_field_value",
  //     lights: true,
  //     show_in_foreground: true                 // notification when app is in foreground (local & remote)
  //   });
  // }
  render() {
    return (
      <View style={{flex:1}}>
        <StatusBar 
         backgroundColor="#1d1d4f"
         barStyle="light-content"
         animated={true}
        />
        <MainRoute />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
