
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer export default class ForgetPassword extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      email: '',
     };
  }
  forget(){
    let {orderStore} = Store;
          if (this.state.email.length === 0) {
              ToastAndroid.show('Please enter an email address', ToastAndroid.LONG);
          }else {
              this.setState({loading: true})
            fetch('http://play4team.com/AdminApi/api/forgetPassword?email='+this.state.email+'&type='+orderStore.playerLoginRes.Status, {
             method: 'POST',
              }).then((response) => response.json())
                    .then((responseJson) => {
                        // console.warn('response=',responseJson);
                        this.setState({loading: false})
                        if (responseJson.status === 'True') {
                            ToastAndroid.show('Email send successfully', ToastAndroid.LONG);
                        }else {
                          this.setState({loading: false})
                          ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                        }
                    }).catch((error)=>{
                        this.setState({loading: false})
                        ToastAndroid.show('Please enter your valid email address', ToastAndroid.LONG);
                    });
          }
      }
  static navigationOptions = {
     title : "Forget Password",
     headerStyle:{
       backgroundColor:'#03031d'
     },
     headerTintColor: 'white',
     headerTintStyle: {
       fontSize:totalSize(1.8)
     }
    }
  render() {
      if(this.state.loading == true){
          return(
              <ImageBackground style={styles.preloader} source={require('../img/splash.jpg')} >
                  <OrientationLoadingOverlay
                      visible={true}
                      color="white"
                      indicatorSize="small"
                      messageFontSize={18}
                      message="Loading..."
                  />
              </ImageBackground>
          );
    }
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../img/bg_img.jpg')} style={styles.backgroungImage}>
         <ScrollView>
            <View style={styles.subCon}>
              <View style={{height:height(6),marginTop: 50,width:width(90),justifyContent:'center',alignItems:'center',borderRadius:2}}>
                <TextInput
                    onChangeText={(value) => this.setState({email: value})}
                    underlineColorAndroid='transparent'
                    placeholder='Enter Recovery Email'
                    placeholderTextColor='gray'
                    underlineColorAndroid='transparent'
                    autoCorrect={false}

                    style={{height:height(6),width:width(90),textAlign :'center',marginTop:0,color:'black',backgroundColor:'white'}}
                    />
              </View>
              <TouchableOpacity style={{height:height(6),marginVertical: 15,width:width(90),backgroundColor:'#03031d',justifyContent:'center',alignItems:'center',borderRadius:2}}
                onPress={()=>{
                  this.forget()
                }}
              >
                <Text style={{fontSize:totalSize(2),color:'white'}}>Send</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroungImage: {
    height:height(100)
  },
  icon: {
    width:width(13),
    borderWidth:0.7,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center'
  },
  iconCon: {
    height:height(8),
    width:width(90),
    justifyContent:'center',
    flexDirection:'row',
    marginTop:5
  },
  searchCon: {
    height:height(6.5),
    width:width(90),
    backgroundColor:'white',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  search: {
    height:height(6.5),
    width:width(90),
    fontSize:totalSize(1.5),
    textAlign :'center',
    marginTop:0
  },
  logo: {
    height:height(20),
    width:width(90),
    alignItems:'center'
  },
  logoImg: {
    height:height(18),
    width:width(30)
  },
  textInputCon: {
    height:height(6),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  textInput: {
    height:height(6),
    width:width(90),
    textAlign :'center',
    fontSize:totalSize(1.5),
    marginTop:0,
    backgroundColor:'white'
  },
  subCon: {
    height:height(100),
    alignItems:'center',
    backgroundColor:'rgba(29,37,86,0.8)'
  },
  preloader: {
    height: height(100),
    width: width(100),
    flex:1
  },
  email: {
    height:height(9),
    width:width(90),
    marginBottom:5
  },
  emailSub: {
    height:height(4),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  emailIcon: {
    height:height(2.5),
    width:width(4.5),
    margin:3
  },
  text: {
    fontSize:totalSize(1.7),
    color:'white',
    margin:3
  },
  otherText: {
    fontSize:totalSize(1.5),
    color:'white',
    margin:3
  },
  password: {
    height:height(10),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  passLogo: {
    height:height(4),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  passIcon: {
    height:height(2.8),
    width:width(3.6),
    margin:3
  },
  empty: {
    height:height(5),
    width:width(90)
  },
  loginCon: {
    height:height(8),
    width:width(90),
    borderRadius:2,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#52e63a'
  },
  fbIcon: {
    height:height(3),
    width:width(2)
  },
  gIcon: {
    height:height(3),
    width:width(5)
  },
  loginBtn: {
    height:height(8),
    width:width(90)
  },
  otherCon: {
    height:height(6),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  other: {
    height:height(3),
    width:width(22),
    justifyContent:'center',
    alignItems:'center',
    borderBottomWidth:0.5,
    borderBottomColor:'white'
  },
  forgetCon: {
    height:height(5),
    width:width(90),
    justifyContent:'flex-end',
    alignItems:'center'
  },
  forgetSubcon: {
    height:height(3),
    width:width(28),
    justifyContent:'center',
    alignItems:'center',
    borderBottomWidth:0.5,
    borderBottomColor:'white'
  },
  forgetText: {
    fontSize:totalSize(1.7),
    color:'white'
  },
  title: {
    height:height(7),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  titleText: {
    fontSize:totalSize(2),
    color:'white'
  },
});
