import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';


class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Event i am Attending</Text>
          </View>
        </View>
    );
  }
}
export default class AttendingEvents extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white',

  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,alignItems:'center'}}>
          <View style={{height:height(8),width:width(100),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5.5),width:width(80),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/search_btn.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
              </TouchableOpacity>
              <View style={{height:height(7),width:width(70)}}>
                <TextInput
                  onChangeText={(value) => this.validate(value,'mobNo')}
                  placeholder={"Search"}
                  placeholderTextColor='gray'
                  style={{fontSize:totalSize(1.8),color:'white',backgroundColor:'white',width:width(65)}}
                  underlineColorAndroid="transparent"
                  />
              </View>
            </View>
            <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}>
              <Image source={require('../img/cross-icon.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
          </View>
          <View style={{height:height(5),width:width(100),backgroundColor:'#1d1d4f',justifyContent:'center'}}>
            <Text style={{fontSize: totalSize(1.8),color:'white',marginLeft:25}}>Cricket</Text>
          </View>

          <View style={{height:height(10),width:width(90),marginTop:15,backgroundColor:'#ffffff',flexDirection:'row',borderRadius:5}}>
            <View style={{width:width(15),backgroundColor:'#1d1d4f',alignItems:'center',borderLeftWidth:5,borderColor:'#41de6b'}}>
              <Text style={{fontSize: totalSize(1.8),color:'white'}}>THU</Text>
              <Text style={{fontSize: totalSize(1.8),color:'white'}}>12:00</Text>
              <Text style={{fontSize: totalSize(1.8),color:'white'}}>AM</Text>
            </View>
            <View style={{width:width(57),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(1.8),color:'#2b2c59',marginLeft:5}}>XYZ</Text>
              <Text style={{fontSize: totalSize(1.2),color:'gray',marginLeft:5}}>02 April 2018</Text>
              <Text style={{fontSize: totalSize(1.5),color:'#777777',marginLeft:5}}>Lahore,Punjab</Text>
            </View>
            <View style={{width:width(18),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/cricket.png')} style={{height:height(8),width:width(13)}} />
            </View>
          </View>

        </View>
        <View style={{height:height(9),width:width(100)}}>
          <Image source={require('../img/next.png')} style={{height:height(9),width:width(100)}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#f6f6f6',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
