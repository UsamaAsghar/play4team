
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,Keyboard,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,Alert,BackHandler,AsyncStorage
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import LatLon from 'mt-latlon';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import FindPlayers from '../FindPlayers/FindPlayers'
import { observer } from 'mobx-react';
import Store from '../Stores';
@observer export default class Home extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            latitude: null,
            longitude: null,
            mapRegion: null,
            error:null,
            address: [],
            distances: [],
            markers: [],
            overlay: false,
            refresh: false
        };
    }
    static navigationOptions = {
        header: null
    }
    backPressed = () => {
        Alert.alert(
            'Logout',
            'Do you want to logout and exit?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Yes', onPress: () => this.clearStorage() },
            ],
            { cancelable: false });
        return true;
    }
    clearStorage= async() => {
        this.setState({ loading: true })
        await  AsyncStorage.removeItem('async_email');
        await  AsyncStorage.removeItem('async_password');
        this.setState({ loading: false })
        await BackHandler.exitApp()
    }
    logout(){
        let { orderStore } = Store;
        this.setState({loading: true})
        fetch('http://play4team.com/AdminApi/api/logout?id='+orderStore.playerLoginRes.id+'&type='+orderStore.playerLoginRes.Status, {
            method: 'POST',
        }).then((response) => response.json())
            .then((responseJson) => {
                // console.log('data=',responseJson);
                this.setState({loading: false})
                if (responseJson.status === 'true') {
                    ToastAndroid.show('logout and Exiting...', ToastAndroid.LONG);
                    this.clearStorage()
                    // console.warn('address',this.state.address);
                }else {
                    this.setState({loading: false})
                    // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                }
            }).catch((error)=>{
            this.setState({loading: false})
            // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        });
    }
    getLatLong(item){
        let { orderStore } = Store;
        let api_key =  'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';//'AIzaSyAGLQfdF7kjd7w8x-7TRqr48hFWV1Kh1tg';//'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + item.address + '&key=' + api_key )
            .then((response) => response.json())
            .then((responseJson) => {
                //  console.log('latLong', responseJson);
                if (responseJson.status === 'OK') {
                    var lat1 = responseJson.results[0].geometry.location.lat;
                    var long1 = responseJson.results[0].geometry.location.lng;
                    var p1 = new LatLon(lat1 , long1);
                    var p2 = new LatLon(this.state.latitude, this.state.longitude);
                    var dist = p1.distanceTo(p2);
                    if (dist<50) {
                        this.state.markers.push({...responseJson.results[0], Team_Name: item.Team_Name});
                    }
                    this.setState({ refresh: false })
                    // console.warn('distance', dist);
                    // this.state.markers.push(responseJson.results[0]);
                    console.warn('distance', this.state.markers);
                }
            })
    }
    componentWillMount=async()=>{
        Keyboard.dismiss();
        let { orderStore } = Store;
        await this.notifications()
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
        this.setState({overlay: false , loading: true})
        // if (orderStore.playerLoginRes.Status===1) {
        var url='http://play4team.com/AdminApi/api/teamsList?teamId='+orderStore.playerLoginRes.id;
        // console.log('team======>>>>',url);
        var methode = 'POST';
        // }else {
        //     var url='http://play4team.com/AdminApi/api/Players';
        //     var methode = 'GET';
        // }
        fetch(url , {
            method: methode,
        }).then((response) => response.json())
            .then(func=async(responseJson) => {
                orderStore.players = responseJson.data;
                console.log('players/Teams are=',responseJson);
                this.setState({loading: false})
                if (responseJson.status === 'True') {
                    for (var i = 0; i < orderStore.players.length; i++) {
                        await this.getLatLong(orderStore.players[i]);
                        orderStore.players[i].added = false;
                        orderStore.players[i].checkStatus = false;
                        // this.state.address.push(responseJson.data[i].Address);
                    }
                    // console.warn('address',this.state.address);
                }else {
                    this.setState({loading: false})
                    // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                }
            }).catch((error)=>{
            this.setState({loading: false})
            ToastAndroid.show('error', ToastAndroid.LONG);
        });
    }
    notifications(){
        let { orderStore } = Store;
        var url = 'http://play4team.com/AdminApi/api/getNotifications?id='+orderStore.playerLoginRes.id+'&type='+orderStore.playerLoginRes.Status;
        console.log('invitations url=',url);
        this.setState({loading: true})
        fetch(url , {
            method: 'POST',
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('notification Data = from home', responseJson);
                this.setState({loading: false})
                orderStore.notifications = responseJson;
                if (responseJson.status === 'True') {
                    // ToastAndroid.show('Done', ToastAndroid.LONG);
                }
            }).catch((error)=>{
            this.setState({loading: false})
            // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
        });
    }
    alert(){
        Alert.alert(
            'Alert',
            'Dear user you can not create an event, for creating event please login as a team',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }

    //  watchID: ?number = null
    async componentDidMount() {
        let { orderStore } = Store;
        navigator.geolocation.getCurrentPosition(
            func = async(position) => {
                //  console.warn('getCurrentPosition=',position);
                orderStore.currentLocation.latitude = position.coords.latitude;
                orderStore.currentLocation.longitude = position.coords.longitude;
                await this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
                //    let region = {
                //     latitude:       position.coords.latitude,
                //     longitude:      position.coords.longitude,
                //     latitudeDelta:  0.00922*1.5,
                //     longitudeDelta: 0.00421*1.5
                // }
                //      await this.onRegionChange(region, region.latitude, region.longitude);

                //    },
                //    (error) => this.setState({ error: error.message }),
                //    { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 });

                //    var watchID =  navigator.geolocation.watchPosition(func=async(position) => {
                // //     // console.warn('watchPosition',position);
                //         orderStore.currentLocation.latitude = position.coords.latitude;
                //         orderStore.currentLocation.longitude = position.coords.longitude;
                //         await this.setState({
                //           latitude: position.coords.latitude,
                //           longitude: position.coords.longitude,
                //           error: null,
                //         });
                //     let region = {
                //          latitude:       position.coords.latitude,
                //          longitude:      position.coords.longitude,
                //          latitudeDelta:  0.00922*1.5,
                //          longitudeDelta: 0.00421*1.5
                //      }
                //     this.onRegionChange(region, region.latitude, region.longitude);
            });
    }
    onRegionChange=async(region, latitude, longitude)=>{
        await this.setState({
            mapRegion: region,
            // If there are no new values set use the the current ones
            latitude: latitude || this.state.latitude,
            longitude: longitude || this.state.longitude,
        });
    }
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
        navigator.geolocation.clearWatch(this.watchID);
    }
    overlayController = async() => {
        let { orderStore } = Store;
        orderStore.overlay = false;
        this.setState({ overlay: false })
    }
    render() {
        let { orderStore } = Store;
        if(this.state.loading == true){
            return(
                <View style={{height: height(100), width: width(100), flex:1}}>
                    <OrientationLoadingOverlay
                        visible={true}
                        color="white"
                        indicatorSize="small"
                        messageFontSize={18}
                        message="Loading..."
                    />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <View style={{height:height(88)}}>
                    <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
                        <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
                                          onPress={()=>{
                                              // this.props.navigation.navigate('Setting')
                                              this.backPressed()
                                          }}
                        >
                            <Image source={require('../img/logoutNew.png')} style={{height:height(4),width:width(10),resizeMode:'contain'}} />
                        </TouchableOpacity>
                        <View style={{width:width(68),justifyContent:'center',alignItems:'center',flexDirection:'row',marginRight:10}}>
                            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(14),resizeMode:'contain'}} />
                            <Text style={{fontSize: totalSize(2.5),color:'white',paddingRight:5}}>Home</Text>
                        </View>
                        <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
                                          onPress={()=>{
                                              if (orderStore.playerLoginRes.Status ===  0) {
                                                  this.props.navigation.navigate('Profile')
                                              }else {
                                                  this.props.navigation.navigate('Profile')
                                              }
                                          }}
                        >
                            <Image source={require('../img/userProfile.png')} style={{height:height(2.5),width:width(5),resizeMode:'contain'}} />
                        </TouchableOpacity>
                    </View>
                    <View style={{height:height(85), width: width(100)}}>
                        <MapView
                            zoomEnabled={true}
                            zoomControlEnabled={true}
                            toolbarEnabled={true}
                            showsCompass={true}
                            showsBuildings={true}
                            showsIndoors={true}
                            provider={PROVIDER_GOOGLE}
                            showsMyLocationButton={true}
                            showsUserLocation={true}
                            followsUserLocation={true}
                            minZoomLevel={5}
                            maxZoomLevel = {20}
                            mapType = {"standard"}
                            loadingEnabled = {true}
                            loadingIndicatorColor = {'white'}
                            loadingBackgroundColor = {'gray'}
                            moveOnMarkerPress={true}
                            style={styles.map}
                            region={{
                                latitude:       this.state.latitude || 31.179,
                                longitude:      this.state.longitude || 70.435,
                                latitudeDelta:  0.0922,
                                longitudeDelta: 0.0421
                            }}
                            // onRegionChange={this.onRegionChange.bind(this)}
                        >
                            {
                                this.state.latitude === null && this.state.longitude === null ?
                                    null
                                    :
                                    <MapView.Marker
                                        coordinate={
                                            { latitude: this.state.latitude, longitude: this.state.longitude }
                                        }
                                        title={'My Location'}
                                        description={orderStore.playerLoginRes.address}
                                        // onPress={()=>alert('player')}
                                        pinColor={'#3edc6d'}
                                        // image={require('../img/friend_img.png')}
                                    >

                                    </MapView.Marker>
                            }
                            {
                                this.state.markers !== [] ?
                                    this.state.markers.map((item,key)=>{
                                        return(
                                            <MapView.Marker key={key}
                                                            coordinate={
                                                                { latitude: item.geometry.location.lat, longitude: item.geometry.location.lng }
                                                            }
                                                            title={item.Team_Name}
                                                            description={item.formatted_address}
                                                // onPress={()=>alert('player')}
                                                            pinColor={'red'}
                                            />
                                        );
                                    })
                                    :
                                    null
                            }
                        </MapView>
                        <View style={{height:height(80),width:width(100)}}>
                            <View style={{height:height(65)}}>
                                {/*<View style={{height:height(10),justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:totalSize(2),color:'red'}}>{this.state.latitude} + {this.state.longitude}</Text>
                  </View>*/}
                            </View>
                            <View style={{height:height(8),width:width(100),flexDirection:'row',justifyContent:'center',alignItems:'flex-start'}}>
                                <TouchableOpacity style={{width:width(45),margin:1,height:height(7),justifyContent:'center',alignItems:'center'}}
                                                  onPress={()=>{
                                                      orderStore.overlay = true;
                                                      this.setState({overlay: true})
                                                  }}
                                >
                                    <Image source={require('../img/find-btn.png')} style={{height:height(7),width:width(45)}} />
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:width(45),height:height(7),margin:1,justifyContent:'center',alignItems:'center',backgroundColor:'#1d1d4f'}}
                                                  onPress={()=>{
                                                      if (orderStore.playerLoginRes.Status === 1) {
                                                          this.props.navigation.push('CreateEvent')
                                                      }else {
                                                          this.alert()
                                                      }
                                                  }}
                                >
                                    <Text style={{fontSize: totalSize(2),color:'white'}}>CREATE EVENTS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {
                            orderStore.overlay === true ?
                                <View style={{height:height(85),width:width(100),margin:0,marginTop:0,position: 'absolute',zIndex:2}} >
                                    <FindPlayers navigation={this.props.navigation} _overlayController={ this.overlayController } />
                                </View>
                                :
                                null
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        height:height(85),
        width:width(100),
        zIndex : -10,
        position: 'absolute',
    },
});
