import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';

class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(15),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Cencal</Text>
          </View>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Send Feedback</Text>
          </View>
          <View style={{width:width(15),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Send</Text>
          </View>
        </View>
    );
  }
}
export default class FeedBack extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      checked : false,
    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{height:height(30),width:width(90),justifyContent:'center'}}>
          <TouchableOpacity style={styles.stripTop} >
            <View style={{width:width(20),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(2),color:'gray',marginLeft:10}}>To: </Text>
            </View>
            <View style={{width:width(61),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(1.8),color:'#282c34'}}>feedBack@gmail.com</Text>
            </View>
            <View style={{width:width(9),justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity style={{width:width(5),height:height(3),justifyContent:'center',alignItems:'center',borderRadius:3,borderColor:'gray',borderWidth:0.5}}>
                <Text style={{fontSize: totalSize(2),color:'gray'}}> + </Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(20),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(2),color:'gray',marginLeft:10}}>Cc/Bcc: </Text>
            </View>
            <View style={{width:width(61),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(1.8),color:'#282c34'}}></Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.stripLast}>
            <View style={{width:width(20),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(2),color:'gray',marginLeft:10}}>Subject: </Text>
            </View>
            <View style={{width:width(61),justifyContent:'center'}}>
              <Text style={{fontSize: totalSize(1.8),color:'#282c34'}}>send feedback</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{height:height(15),width:width(90),justifyContent:'center'}}>
          <Text style={{fontSize: totalSize(2.2),color:'#282c34'}}>Send From My Mobile</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    alignItems:'center',
  },
  stripTop: {
    height:height(8),
    backgroundColor:'#ffffff',
    flexDirection:'row',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomWidth:0.3,
    marginBottom:0.3,
    borderColor:'gray',
  },
  stripLast: {
    height:height(8),
    backgroundColor:'#ffffff',
    flexDirection:'row',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    borderBottomWidth:0.3,
    marginBottom:0.3,
    borderColor:'gray',
    flexDirection:'row',
  },
});
