/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,Slider
} from 'react-native';
import PopupDialog, {slideAnimation,DialogTitle} from 'react-native-popup-dialog';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';

class LogoTitle extends React.Component {
  constructor(props){
    super(props)
    this.state = {

      value: 0
    }
  }
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(100),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Hamza</Text>
          </View>
        </View>
    );
  }
}
export default class GeneralEventSearch extends Component<Props> {

  constructor(props){
      super(props);
      this.state = {
        date:"",
        time:"",
      }
    }
  static navigationOptions = {
        header: null,

    }
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{height:height(5),width:width(100),backgroundColor:'#e9e9e9',justifyContent:'center',alignItems:'flex-end'}}>
            <Text style={{fontSize:totalSize(1.5),color:'black',marginRight:15}}>Show All</Text>
          </View>
          <View style={{height:height(3),width:width(100),justifyContent:'center',alignItems:'center'}}>
          </View>
          <View style={{height:height(20),width:width(93),borderRadius:5,backgroundColor:'#ffffff',margin:15,marginTop:0,marginBottom:10,justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5),width:width(93),flexDirection:'row'}}>
              <View style={{width:width(46),justifyContent:'center',alignItems:'flex-end'}}>
                <Text style={{fontSize:totalSize(1.5),color:'black'}}>EventType: </Text>
              </View>
              <View style={{width:width(46),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.5),color:'gray'}}> Cricket</Text>
              </View>
            </View>
            <View style={{height:height(15),width:width(93),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <ScrollView horizontal={true}>


              </ScrollView>
            </View>
          </View>
          <View style={{height:height(1)}}>
          </View>
          <View style={{height:height(18),margin:15,marginTop:10,marginBottom:10,backgroundColor:'#ffffff',borderRadius:5}}>
            <View style={{height:height(5),justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:totalSize(1.8),color:'#444473'}}>Search</Text>
            </View>
            <View style={{height:height(13),flexDirection:'row'}}>
              <TouchableOpacity style={{width:width(30),alignItems:'center'}}>
                <ImageBackground source={require('../img/calenderr.png')} style={{height:height(9),width:width(15)}}>
                  <DateTimePicker
                    style={{width:width(20),height:height(9),backgroundColor:'transparent'}}
                    date={this.state.date}
                    time={this.state.time}
                    mode="datetime"
                    placeholder="Event Start"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon= {false}
                    hideText='false'
                    iconComponent={<Image source={require('../img/calenderr.png')} style={{height:height(9),width:width(15)}} />}
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                         right: 0,
                         top: 20,
                         marginBottom: 5,
                         marginLeft: 0,
                         flex: 1
                      },
                      dateInput: {
                        marginLeft: 0
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {this.setState({date: date})}}
                  />
                </ImageBackground>
                <View style={{height:height(4),width:width(14),alignItems:'center'}}>
                  <Text style={{fontSize:totalSize(1.4),color:'#444473',alignItems:'center'}}>When?</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{height:height(13),width:width(30),alignItems:'center'}}
                  onPress={()=>{
                    this.popupDialog.show();
                  }}
                >
                <Image source={require('../img/distance.png')} style={{height:height(9),width:width(15)}} />
                <View style={{height:height(4),width:width(17),alignItems:'center',flexWrap:'wrap'}}>
                  <Text style={{fontSize:totalSize(1.3),color:'#444473',textAlign:'center'}}>Upto 20 miles from here</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity style={{width:width(30),alignItems:'center'}}>
                <Image source={require('../img/All.png')} style={{height:height(9),width:width(15)}} />
                <View style={{height:height(4),width:width(14),alignItems:'center'}}>
                  <Text style={{fontSize:totalSize(1.4),color:'#444473',alignItems:'center'}}>All</Text>
                </View>
              </TouchableOpacity>

            </View>

          </View>
        </View>
        <PopupDialog
          ref={(popupDialog) => { this.popupDialog = popupDialog; }}
          dialogAnimation={slideAnimation}
          containerStyle = {{ height:height(100)}}
          dialogStyle ={{ height:height(45),marginTop:200,marginBottom:0}}
          slideFrom = 'bottom'
          animationDuration = {150}
          dismissOnTouchOutside = { false }
        >
          <View style={{flex:1,backgroundColor:'#ffffff'}}>
            <View style={{height:height(7),backgroundColor:'#1d1d4f',flexDirection:'row'}}>
              <TouchableOpacity style={{width:width(10),justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  this.popupDialog.dismiss()
                }}
              >
                <Image source={require('../img/x_btn.png')} style={{height:height(5),width:width(6)}} />
              </TouchableOpacity>
              <View style={{width:width(80),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2),color:'white'}}>Distance</Text>
              </View>
              <TouchableOpacity style={{width:width(10),justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  this.popupDialog.dismiss()
                }}
              >
                <Image source={require('../img/tick_btn.png')} style={{height:height(5),width:width(6)}} />
              </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={{height:height(2)}}>
              </View>
              <TouchableOpacity style={{height:height(4),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2),color:'#333360'}}>Up to {parseInt(this.state.value)}mi from my location</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:height(4),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2),color:'#333360'}}>Up to {parseInt(this.state.value)}mi from my postcode</Text>
              </TouchableOpacity>
              <View style={{height:height(10),justifyContent:'center',alignItems:'center'}}>
                <View style={{height:height(7),width:width(43),justifyContent:'center',alignItems:'center',backgroundColor:'#d7d7d7',borderRadius:7}}>
                  <Text style={{fontSize:totalSize(2),color:'#333360'}}>e.gG1 3ST</Text>
                </View>
              </View>
              <View style={{height:height(8),justifyContent:'center',backgroundColor:'white'}}>

                <Slider
                  value={this.state.value}
                  onValueChange={value => this.setState({ value })}
                  maximumValue={25}
                  minimumValue={1}
                  thumbTintColor='#29d9a4'
                  maximumTrackTintColor='#1fefd5'
                  minimumTrackTintColor='#1fefd5'
                  style={{backgroundColor:'white',height:height(8)}}
                />
              </View>
            </ScrollView>
          </View>
        </PopupDialog>
        <TouchableOpacity style={{height:height(10),backgroundColor:'#212157',justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:totalSize(2),color:'white'}}>Find Players</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#f6f6f6',
  },

});
