/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';

class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(100),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Hamza</Text>
          </View>
        </View>
    );
  }
}
export default class SearchEventByName extends Component<Props> {

  constructor(props){
      super(props);
      this.state = {
        date:"",
        time:"",
        username:''
      }
    }
  static navigationOptions = {
        header: null,

    }
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{height:height(3),margin:15,marginBottom:5,justifyContent:'center',alignItems:'flex-start'}}>
            <Text style={{fontSize:totalSize(1.5),color:'black',marginLeft:5}}>Name</Text>
          </View>
          <View style={{height:height(7),margin:15,marginTop:0,justifyContent:'center',alignItems:'flex-start'}}>
            <TextInput
                onChangeText={(value) => this.setState({username: value})}
                underlineColorAndroid='transparent'
                placeholder='Name'
                placeholderTextColor='gray'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(92.5),backgroundColor:'white',borderRadius:7,fontSize:totalSize(1.5),textAlign :'left',marginTop:0}}
                />
          </View>
        </View>
        <TouchableOpacity style={{height:height(10),backgroundColor:'#212157',justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:totalSize(2),color:'white'}}>Find Player</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#f6f6f6',
  },

});
