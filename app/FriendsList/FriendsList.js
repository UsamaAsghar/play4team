import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ToastAndroid, TextInput, Image, ScrollView, TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import FriendComponent from './FriendComponent';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(75), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>{orderStore.playerLoginRes.Status === 0 ? 'Friends' : 'Favorites '}</Text>
        </View>
      </View>
    );
  }
}
export default class FriendsList extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      friends: [],
      searchFriends: []
    };
  }
  static navigationOptions = ({ navigate }) => ({
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  })
  componentDidMount () {
    this._onFocusListener = this.props.navigation.addListener('didFocus', async(payload) => {
      // console.warn('Focus');
      await this.getList()
      // refresh the component here
      // or update based on your requirements
    });
  }
  searchFrienfs = (name) => {
    const { friends, searchFriends } = this.state;
      if (friends.length && name.length) {
        friends.forEach((item) => {
          console.log('item----->>>>>',item)
          if (item.Team_Name.includes(name)) {
            searchFriends.push(item);
          }
        })
      } else {
        this.setState({ searchFriends: [] })
        ToastAndroid.show('No Record Found!s', ToastAndroid.LONG);
      }
  }
  getList = async() => {
    this.setState({ loading: true })
    let { orderStore } = Store;
    var url = 'http://play4team.com/AdminApi/api/getFriendsList?teamId=' + orderStore.playerLoginRes.id;
    console.log('url=',url);
    await fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then(func=async(responseJson) => {
        this.setState({ loading: false })
        console.log('friend list=', responseJson);
        if (responseJson.status === 'True') {
          for (var i = 0; i < responseJson.friends.length; i++) {
            responseJson.friends[i].status = "teams";
          }
          await this.setState({
            friends: responseJson.friends,
          })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        this.setState({ loading: false })
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1 }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    if ( this.state.searchFriends.length > 0 ) {
      var friendsList = this.state.searchFriends;
    } else {
      var friendsList = this.state.friends;
    }
    return (
      <View style={styles.container}>
         <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                // this.props.navigation.navigate('Setting')
                this.backPressed()
              }}
            >
              <Image source={require('../img/logoutNew.png')} style={{height:height(4),width:width(10),resizeMode:'contain'}} />
            </TouchableOpacity>
            <View style={{width:width(68),justifyContent:'center',alignItems:'center',flexDirection:'row',marginRight:10}}>
              <Image source={require('../img/logo.png')} style={{height:height(8),width:width(14),resizeMode:'contain'}} />
              <Text style={{fontSize: totalSize(2.5),color:'white',paddingRight:5}}>Friends</Text>
            </View>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                if (orderStore.playerLoginRes.Status ===  0) {
                    this.props.navigation.navigate('Profile')
                }else {
                    this.props.navigation.navigate('Profile')
                }
              }}
             >
              <Image source={require('../img/userProfile.png')} style={{height:height(2.5),width:width(5),resizeMode:'contain'}} />
            </TouchableOpacity>
          </View>

            <View style={{ flex: 1 }}>
              <View style={{ height: height(12), width: width(100), backgroundColor: '#33327e', justifyContent: 'flex-end' }}>
                <Text style={{ fontSize: totalSize(1.7), color: 'white', marginHorizontal: 5, alignSelf: 'center', marginVertical: 5 }}>Invite Friends To Find A Player</Text>
                <View style={{ height: height(8), width: width(100), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ height: height(6), width: width(45), borderRadius: 5, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginHorizontal: 5, alignSelf: 'center' }}>
                    <TextInput
                      onChangeText={(text) => this.searchFrienfs(text) }
                      placeholder='Search'
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      keyboardType='default'
                      style={{ alignSelf: 'stretch', width: width(35), fontSize: totalSize(1.6), color: '#1c1c4e' }}
                    />
                    <TouchableOpacity>
                      <Image source={require('../img/search1.png')} style={{ height: height(3.2), width: width(7), resizeMode: 'contain' }} />
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={{ height: height(6), width: width(45), borderRadius: 5, alignItems: 'center', justifyContent: 'center', backgroundColor: '#3fde6f', flexDirection: 'row', marginHorizontal: 5, alignSelf: 'center' }}
                    onPress={() => { this.props.navigation.navigate('ContactList') }}
                  >
                    <Image source={require('../img/follow.png')} style={{ height: height(3.5), width: width(7), resizeMode: 'contain' }} />
                    <Text style={{ fontSize: totalSize(1.7), color: '#1c1c4e', marginHorizontal: 5 }}>Invite Friends</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

        <View style={{ marginTop: 25 }} />
        <View style={{ height: height(65), width: width(93), margin: 15, marginBottom: 5, marginTop: 0 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {
              friendsList.length > 0 ?
                  friendsList.map((item, key) => {
                  return (
                    <FriendComponent key={key} navigation={this.props.navigation} item={item} />
                  );
                })
                :
                <View style={{ height: height(65) , justifyContent:'center',alignItems:'center' }}>
                  <Text style={{ fontSize: totalSize(3), marginHorizontal: 20, textAlign: 'center', color: '#1c1c4e' }}>Search your friends to build your sports network</Text>
                </View>
            }
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },

});
