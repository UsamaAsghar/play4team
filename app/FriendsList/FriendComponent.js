import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, Image, TouchableOpacity
} from 'react-native';
import { observer } from 'mobx-react'
import { Avatar, Icon } from 'react-native-elements';
import Store from '../Stores';
import { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
export default class FriendComponent extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }
  static navigationOptions = {
    header: null,
  }
  removeTeam() {
    let { orderStore } = Store;
    var url = 'http://play4team.com/AdminApi/api/removeFriend?playerId=' + orderStore.playerLoginRes.id + '&friendId=' + this.props.item.id;
    console.log('remove url===>>>', url);
    this.setState({ loading: true })
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({ loading: false })
        console.warn('responseJson=', responseJson);
        if (responseJson.status === 'True') {
          ToastAndroid.show('This team is remove from you favourite Teams List', ToastAndroid.LONG);
          this.props.navigation.goBack('Home')
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1 }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }

    return (
      <TouchableOpacity style={{ elevation: 2, width: width(90), backgroundColor: '#ffffff', flexDirection: 'row', marginHorizontal: 5, marginVertical: 5, borderRadius: 5, borderColor: 'gray' }}
        onPress={() => {
          if (this.props.item.Status === 1) {
            this.props.navigation.navigate('playerDetail', { item: this.props.item }),
              orderStore.tag = true;
          }
        }}
      >
        <View style={{ width: width(15), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          {
            this.props.item.Path.length !== 0 ?
              <Avatar
                medium
                rounded
                source={{
                  uri: this.props.item.Path,
                }}
              /> :
              <Image source={require('../img/userprofileImag.png')} style={{ height: height(6), width: width(14), resizeMode: 'contain' }} />
          }
        </View>
        <View style={{ width: width(78), flexDirection: 'row', marginBottom: 7 }}>
          <View style={{ width: width(53), justifyContent: 'center' }}>
            <View style={{ height: height(4), justifyContent: 'flex-end', alignItems: 'flex-start' }}>
              <Text style={{ fontSize: totalSize(2.2), color: 'black' }}>{this.props.item.Team_Name}</Text>
            </View>
            <View style={{ height: height(4), justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontSize: totalSize(1.3), color: 'gray' }}>Address: {this.props.item.address}</Text>
            </View>
          </View>
          <View style={{ width: width(20) }}>
            <View style={{ height: height(5), width: width(18), justifyContent: 'flex-start', alignItems: 'flex-end' }}>
              <Text style={{ fontSize: totalSize(1.3), color: 'black', marginTop: 5 }}>{this.props.item.visiblity === 1 ? "Active" : "Ofline"}</Text>
            </View>
            <TouchableOpacity style={{ width: width(18), justifyContent: 'flex-end', alignItems: 'flex-end' }} onPress={() => { this.removeTeam() }}>
              <Text style={{ fontSize: totalSize(1.2), color: 'black', marginBottom: 5 }}>Remove</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
