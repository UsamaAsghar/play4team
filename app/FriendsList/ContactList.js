import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, PermissionsAndroid,ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Contacts from 'react-native-contacts';
import SendSMS from 'react-native-sms'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
export default class ContactList extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      contacts: [],
      searchText: '',
      searchContacts: []
    };
  }
  static navigationOptions = {
    title: 'Invite Friends',
    headerStyle: {
      backgroundColor: '#1d1d4f',

    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontSize: totalSize(2),
      fontWeight: 'normal'
    }
  }
  componentWillMount = async () => {
    await this.getContacts();
  }
  getContacts() {
    let { orderStore } = Store;
    if (Platform.OS == 'android') {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'Describing why I need to access contact information.'
        }
      )
        .then(granted => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.setState({loading: true})
             Contacts.getAll((err, contacts) => {
               console.log('contactList===>>>',contacts)
               orderStore.CONTACT_LIST = contacts;
              this.setState({
                contacts: contacts
              });
              this.setState({loading: false})
              // console.log('contact func=', this.state.contacts);
            });
          }
          else {
            // Handle
          }
        })
        .catch(err => {
          console.log('PermissionsAndroid', err)
        })
    }
  }
  search(value) {
    let { orderStore } = Store;
    if (orderStore.CONTACT_LIST.length !== 0) {
      orderStore.searchContacts = []
      for (let i = 0; i < orderStore.CONTACT_LIST.length; i++) {
        if (orderStore.CONTACT_LIST[i].givenName.includes(value)) {
              orderStore.searchContacts.push(orderStore.CONTACT_LIST[i])
              this.setState({loading: false})
          }
        }
    } else {
      orderStore.searchContacts = [];
      ToastAndroid.show('No Record found!', ToastAndroid.LONG);
    }
  }
  sendInvitation(number) {
    SendSMS.send({
      body: 'Send invitation to join Play4team',
      recipients: [number],
      successTypes: ['sent', 'queued'],
      allowAndroidSendWithoutReadPermission: true
    }, (completed, cancelled, error) => {

      console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);

    });
  }
  render() {
    let { orderStore } = Store;
    if (orderStore.searchContacts.length !== 0) {
         var contacts = orderStore.searchContacts;
    } else {
      var contacts = orderStore.CONTACT_LIST;
    }
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1 }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(10), width: width(100), backgroundColor: '#33327e', justifyContent:'center', alignItems:'center' }}>
          <View style={{height:height(7),width:width(90), borderRadius:5, backgroundColor:'white', justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <TextInput
              onChangeText={(value) => {
                this.search(value)
                // this.setState({searchText: value})
              }}
              placeholder='Search'
              placeholderTextColor='gray'
              underlineColorAndroid='transparent'
              keyboardType='default'
              style={{ alignSelf: 'stretch', width: width(80), fontSize: totalSize(1.6), color: '#1c1c4e' }}
            />
            <TouchableOpacity>
              <Image source={this.state.searchContacts.length === 0? require('../img/search1.png'):require('../img/cancel.png')} style={{height:height(2.3),width:width(5),resizeMode:'contain',marginHorizontal:5}} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
    <View styles={{ height: 20, width: '100%' }} />
    {
            contacts.map((item, key) => {
              return (
                <TouchableOpacity key={key} style={{ height: height(8), width: width(92), marginVertical: 7, elevation: 2, showOpacity: 0.2,alignSelf: 'center', marginTop: 5, backgroundColor: '#ffffff', flexDirection: 'row' }}
                  onPress={() => { this.sendInvitation(item.phoneNumbers[0].number) }}
                >
                 {/* <View style={{ width: width(20), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    {
                      item.hasThumbnail === false ?
                        <Image source={require('../img/friend_img.png')} style={{ height: height(6), width: width(12) }} />
                        :
                        <Image source={{ uri: item.thumbnailPath }} style={{ height: height(7), width: width(12), borderRadius: 100 }} />
                    }
                  </View>*/}
                  <View style={{ width: width(90), flexDirection: 'row' }}>
                    <View style={{ width: width(73), justifyContent: 'center' }}>
                      <View style={{ height: height(3), width: width(70),justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: totalSize(1.8), color: 'black', marginHorizontal: 15 }}>{item.givenName}</Text>
                      </View>
                      <View style={{  justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}></Text>
                      </View>
                    </View>
                    <View style={{ width: width(15), justifyContent: 'center', alignItems: 'flex-end' }}>
                      <Image source={require('../img/add_btn.png')} style={{ height: height(5), width: width(8), resizeMode:'contain' }} />
                    </View>

                  </View>
                </TouchableOpacity>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },

});
