import {observable} from 'mobx'

class orderStore {
    @observable baseURL= 'http://play4team.com/AdminApi/api/';
    @observable getFriendReguest= null;
    @observable FireBaseNotif = null;
    @observable playerLoginRes= {};
    @observable players = [];
    @observable sports = [];
    @observable sportsTeam = [];
    @observable sportEvent = [];
    @observable overlay = false;
    @observable searchResult = [];
    @observable sport_id = 0;
    @observable teamMembers = [];
    @observable selectedSports = [];
    @observable selectedRates = [];
    @observable playerProfile = [];
    @observable eventResponse = {};
    @observable search = '';
    @observable searchData = [];
    @observable searchEvents = [];
    @observable searchContacts = [];
    @observable eventStatus = '';
    @observable eventLife = '';
    @observable placeId = 0;
    @observable events = {};
    @observable teamMembers = [];
    @observable checkToken = '';
    @observable chatList = [];
    @observable chatStatus = false;
    @observable CONTACT_LIST = [];
    @observable chat = [];
    @observable predictions = [];
    @observable tag = false;
    @observable editLocation = {
      latitude: null,
      longitude: null,
      status: false,
      address: '',
      name: '',
    };
    @observable RES = {
      register: false,
      email: null ,
      password: null
    };
    @observable managingEvent = [];
    @observable sportEventLoc = [];
    @observable eventInvitNotify = [];
    @observable notifications = {};
    @observable searchTeams = [];
    @observable currentLocation = {
        latitude: null,
        longitude: null
    }
    @observable newMessage = false;
}

const store = new orderStore();

export default store;
