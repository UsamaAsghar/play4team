import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import FlipToggle from 'react-native-flip-toggle-button'


class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Settings</Text>
          </View>
        </View>
    );
  }
}
export default class Setting extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f',

    },
    headerTintColor :'white',
    headerRight: null
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,alignItems:'center'}}>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(70),justifyContent:'center'}}>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>Share App</Text>
              </View>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.2),color:'gray'}}>Share find a Player</Text>
              </View>
            </View>
            <View style={{width:width(5),justifyContent:'center'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(70),justifyContent:'center'}}>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>Currency</Text>
              </View>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.2),color:'gray'}}>USD</Text>
              </View>
            </View>
            <View style={{width:width(5),justifyContent:'center'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(70),justifyContent:'center'}}>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>FeedBack</Text>
              </View>
              <View style={{height:height(3),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.2),color:'gray'}}>Send FeedBack</Text>
              </View>
            </View>
            <View style={{width:width(5),justifyContent:'center'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(70),justifyContent:'center'}}>
              <View style={{justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>Enable Location Services</Text>
              </View>
            </View>
            <View style={{width:width(5),justifyContent:'center'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(55),justifyContent:'center'}}>
              <View style={{width:width(50),justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>Opt out of Marketing</Text>
              </View>
            </View>
            <View style={{width:width(20),justifyContent:'center',alignItems:'center'}}>
              <FlipToggle
                value = { this.state.isActive }
                onToggle = {(value)=>{this.setState({isActive : value})}}
                buttonWidth={60}
                buttonHeight={33}
                buttonRadius={50}
                buttonOnColor = {'#757575'}
                buttonOffColor = {'#cccccc'}
                sliderWidth={31}
                sliderHeight={32}
                sliderRadius={50}
                labelStyle={{ color: 'black' }}
                sliderOnColor={'#2bd89c'}
                sliderOffColor = {'#f0f0f0'}
                margin = {10}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.strip}>
            <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center',backgroundColor:'#2bd89c',borderRadius:50,borderBottomWidth:2,borderLeftWidth:0.5,borderRightWidth:0.5,borderColor:'gray'}}>
                <Text style={{fontSize: totalSize(1.7),color:'black'}}>0</Text>
              </View>
            </View>
            <View style={{width:width(70),justifyContent:'center'}}>
              <View style={{justifyContent:'center'}}>
                <Text style={{fontSize: totalSize(1.8),color:'black'}}>Rate Find a Player</Text>
              </View>
            </View>
            <View style={{width:width(5),justifyContent:'center'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
          <View style={{height:height(15),width:width(85),alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/follow.png')} style={{height:height(8),width:width(40),margin:5,borderRadius:5}} />
            <Image source={require('../img/fb_like.png')} style={{height:height(8),width:width(40),margin:5,borderRadius:5}} />
          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    margin: 5,
    flexDirection:'row',
    borderRadius:5
  },
});
