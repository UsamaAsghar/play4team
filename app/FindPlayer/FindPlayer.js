/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,ToastAndroid,Image,ScrollView,TouchableOpacity
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import  { width, height, totalSize } from 'react-native-dimension';
import { TabNavigator } from 'react-navigation';
import SearchByName  from './SearchByName';
import GeneralSearch from './GeneralSearch';
import { observer } from 'mobx-react';
import Store from '../Stores';
class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
        <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
          <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
          {/* <Text style={{fontSize: totalSize(2.5),color:'white'}}>Find Player</Text> */}
          <Text style={{fontSize:totalSize(2),color:'white'}}>Find {orderStore.search == 'Team' ? 'Players':'Events'}</Text>
        </View>
      </View>
    );
  }
}
 export default class FindPlayer extends Component<Props> {
   constructor(props){
       super(props);
       this.state = {
        g_search: true,
        n_search: false
       }
     }

  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle:{
      backgroundColor:'#1d1d4f'
    },
    headerTintColor: 'white',

    }
    componentWillMount=async()=>{
      // await this.sport()
    }
    sport () {
      fetch('http://play4team.com/AdminApi/api/Sport', {
         method: 'GET',
          }).then((response) => response.json())
                .then((responseJson) => {
                  // console.warn('Response',responseJson);
                  // orderStore.token = responseJson.token;
                    orderStore.sportEvent = responseJson.data;
                    // this.setState({loading: false})
                  if (responseJson.status === 'True') {
                        for (var i = 0; i < orderStore.sportEvent.length; i++) {
                            orderStore.sportEvent[i].added = false;
                            orderStore.sportEvent[i].checkStatus = false;
                        }
                        // console.log('Response',orderStore.sportEvent);
                  }else {
                    this.setState({loading: false})
                  }
                }).catch((error)=>{
                    this.setState({loading: false})
                    ToastAndroid.show('error', ToastAndroid.SHORT);
                });
    }
    sport () {
      fetch('http://play4team.com/AdminApi/api/Sport', {
         method: 'GET',
          }).then((response) => response.json())
                .then((responseJson) => {
                  // console.warn('Response',responseJson);
                  // orderStore.token = responseJson.token;
                    orderStore.sportEvent = responseJson.data;
                    // this.setState({loading: false})
                  if (responseJson.status === 'True') {
                        for (var i = 0; i < orderStore.sportEvent.length; i++) {
                            orderStore.sportEvent[i].added = false;
                            orderStore.sportEvent[i].checkStatus = false;
                        }
                        // console.log('Response',orderStore.sportEvent);
                  }else {
                    this.setState({loading: false})
                  }
                }).catch((error)=>{
                    this.setState({loading: false})
                    ToastAndroid.show('error', ToastAndroid.SHORT);
                });
    }
  render() {
    let { orderStore } = Store;
    if(this.state.loading == true){
        return(
            <View style={{height: height(100), width: width(100), flex:1}}>
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </View>
        );
    }
    return (
      <View style={styles.container}>
        <View style={{height:height(7),width:width(100),backgroundColor:'#33327e',flexDirection:'row'}}>
          <TouchableOpacity style={ this.state.g_search === false? styles.unSelect : styles.selected }
            onPress={()=>{
              this.setState({
                g_search: true,
                n_search: false,
              })
            }}
          >
            {
              this.state.g_search === true?
                <Text style={{fontSize:totalSize(2),color:'white'}}>General Search</Text>
                :
                <Text style={{fontSize:totalSize(1.8),color:'white'}}>General Search</Text>
            }
          </TouchableOpacity>
          <TouchableOpacity style={ this.state.n_search === false? styles.unSelect : styles.selected }
            onPress={()=>{
              this.setState({
                g_search: false,
                n_search: true,
              })
            }}
          >
            {
              this.state.n_search === true ?
                <Text style={{fontSize:totalSize(2),color:'white'}}>Search by Name</Text>
                :
                <Text style={{fontSize:totalSize(1.8),color:'white'}}>Search by Name</Text>
            }
          </TouchableOpacity>
        </View>
        <View style={{flex:1}}>
          {
            this.state.g_search === true ?
              <View style={{flex:1}}>
                <GeneralSearch navigation = { this.props.navigation } />
              </View>
              :
              <View style={{flex:1}}>
                <SearchByName navigation = { this.props.navigation } />
              </View>
          }
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#f6f6f6',
  },
  unSelect: {
    height:height(7),
    width:width(50),
    justifyContent:'center',
    alignItems:'center'
  },
  selected: {
    height:height(7),
    width:width(50),
    justifyContent:'center',
    alignItems:'center',
    borderBottomWidth: 3,
    borderColor: 'white',
  },

});
