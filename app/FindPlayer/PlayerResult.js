import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, CheckBox, ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { Avatar } from 'react-native-elements';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(75), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Result</Text>
        </View>
      </View>
    );
  }
}
export default class PlayerResult extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      checked: false,
    };
  }
  componentWillMount() {
    var { params } = this.props.navigation.state;
    // console.warn('data: ', params.list);
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.searchData.length; i++) {
      orderStore.searchData[i].status = 'PlayerResult';
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  }
  _strip = (item, key) => {
    let { orderStore } = Store;
    // console.warn('item:',item);
    return (
      item.id !== orderStore.playerLoginRes.id || item.player_id !== orderStore.playerLoginRes.id ?
        <View key={key} style={{ height: height(8), width: width(90), borderRadius: 5, elevation: 5, marginHorizontal: 5, marginVertical: 5, borderColor: 'gray', marginTop: 1, backgroundColor: '#ffffff', flexDirection: 'row' }}>
          <View style={{ width: width(15), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
            {
              item.Path && item.Path.length > 0 ?
                <Avatar
                  medium
                  rounded
                  source={{
                    uri: item.Path
                  }}
                  containerStyle={{ marginLeft: 5 }}
                />
                // <Image source={{ uri: item.Path }} style={{ height: height(5), width: width(15), resizeMode: 'contain' }} />
                :
                <Image source={require('../img/userprofileImag.png')} style={{ height: height(5), width: width(15), resizeMode: 'contain' }} />
            }
          </View>
          <View style={{ width: width(75), flexDirection: 'row' }}>
            <View style={{ width: width(58), justifyContent: 'center' }}>
              <View style={{ height: height(2), justifyContent: 'center', alignItems: 'flex-start' }}>
                {
                  orderStore.search === 'Player' ?
                    <Text style={{ fontSize: totalSize(2), color: '#1d1d4f' }}>{item.Name}</Text>
                    :
                    <Text style={{ fontSize: totalSize(2), color: '#1d1d4f' }}>{item.Team_Name}</Text>
                }
              </View>
              <View style={{ height: height(2), justifyContent: 'center', alignItems: 'flex-start' }}>
                {
                  item.visiblity === 0 ?
                    <Text style={{ fontSize: totalSize(1.5), color: '#1d1d4f' }}>Busy</Text>
                    :
                    <Text style={{ fontSize: totalSize(1.5), color: '#1d1d4f' }}>Available</Text>
                }
              </View>
            </View>
            <TouchableOpacity style={{ width: width(15), justifyContent: 'center', alignItems: 'flex-end' }}
              onPress={() => {
                this.addFavorit(item)
              }}>
              {
                orderStore.playerLoginRes.Status === 0 && orderStore.search === 'Player' ?
                  <Text style={{ fontSize: totalSize(1.7), color: '#1d1d4e' }}>View</Text>
                  :
                  <Image source={require('../img/add_btn.png')} style={{ height: height(5), width: width(8), resizeMode: 'contain' }} />
              }
            </TouchableOpacity>
          </View>
        </View>
        :
        null
    );
  }
  _event = (item, key) => {
    let { orderStore } = Store;
    return (
      <TouchableOpacity key={key} style={{ height: height(13), flexDirection: 'row', marginBottom: 0.3, borderBottomWidth: 0.5, borderColor: 'black' }}
        onPress={() => {
          this.props.navigation.navigate('detailEvent', { eventItem: item })
        }}
      >
        <View style={{ width: width(25), justifyContent: 'center', alignItems: 'center' }}>
          <Image source={{ uri: 'http://play4team.com/AdminApi/public/img/' + item.sport_image }} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(1.5), color: 'gray', marginTop: 3 }}>{item.sport_name}</Text>
        </View>
        <View style={{ width: width(70), justifyContent: 'center' }}>
          <Text style={{ fontSize: totalSize(2.5), height: height(4), color: 'black' }}>{item.Event_Name}</Text>
          <Text style={{ fontSize: totalSize(1.7), height: height(3), color: 'gray' }}>From {item.Start_Date} to {item.End_Date}</Text>
          <View style={{ height: height(5), flexDirection: 'row' }}>
            <Image source={require('../img/Addresslocation.png')} style={{ height: height(2), width: width(2.5), marginRight: 3 }} />
            <Text style={{ fontSize: totalSize(1.6), height: height(5), textAlign: 'center', color: 'gray' }}>{item.Address}</Text>
          </View>
        </View>
        <View style={{ width: width(5), justifyContent: 'center' }}>
          <Image source={require('../img/ic_arrow_rit.png')} />
        </View>
      </TouchableOpacity>
    );
  }
  addFavorit(item) {
    let { orderStore } = Store;
    var url = 'http://play4team.com/AdminApi/api/addTeamMemberRequest?team_id=' + orderStore.playerLoginRes.id + '&friend_team_id=' + item.id;
    console.log('urlFavTeam===>>>', url);
    this.setState({ loading: true })
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        console.warn('add favorit team==', responseJson);
        this.setState({ loading: false })
        if (responseJson.status === 'True') {
          ToastAndroid.show('Your friend request has been sent successfully.', ToastAndroid.LONG);
        } else {
          this.setState({ loading: false })
          ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('Try again', ToastAndroid.LONG);
      });
  }
  addMembers(item) {
    // console.log('player id=',item.id);
    let { orderStore } = Store;
    if (item.Status === 1) {
      var url = 'http://play4team.com/AdminApi/api/playerRequestToTeam?player_id=' + orderStore.playerLoginRes.id + '&team_id=' + item.id;
      //  console.log('url=',url);
    }
    else {
      var url = 'http://play4team.com/AdminApi/api/TeamRequestToplayer?team_id=' + orderStore.playerLoginRes.id + '&player_id=' + item.id;
      // console.log('url=',url);
    }
    this.setState({ loading: true })
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        // console.log('Resquest Api==',responseJson);
        this.setState({ loading: false })
        if (responseJson.status === 'True') {
          if (item.Status === 1) {
            ToastAndroid.show('Team add request sent.', ToastAndroid.LONG);
          } else {
            ToastAndroid.show('Team joining request sent to this player', ToastAndroid.LONG);
          }
        } else {
          this.setState({ loading: false })
          ToastAndroid.show('Your request already sent.', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Try again', ToastAndroid.LONG);
      });
  }
  componentWillUnmount() {
    let { orderStore } = Store;
    orderStore.searchData = [];
    orderStore.searchEvents = [];
  }
  render() {
    let { orderStore } = Store;
    var { params } = this.props.navigation.state;
    // console.log('PlayerResult==>>>', orderStore.searchData);
    // console.log('list=',params.list);
    if (this.state.loading == true) {
      return (
        <View style={styles.preloader} >
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(7), width: width(100), marginBottom: 10, justifyContent: 'center', backgroundColor: '#eaedf2' }}>
          <Text style={{ fontSize: totalSize(1.7), color: 'black', marginLeft: 20 }}>Public Profile Result</Text>
        </View>
        {
          orderStore.search == 'Team' ?
            orderStore.searchData.length > 0 ?
              <View style={{ height: height(80), width: width(95), alignItems: 'center' }}>
                <ScrollView>
                  {
                    orderStore.searchData.map((item, key) => {
                      return (
                        this._strip(item, key)
                      );
                    })
                  }
                </ScrollView>
              </View>
              :
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: totalSize(2.5), color: 'black', marginHorizontal: 15, textAlign: 'center' }}>There is no player found in this range.</Text>
              </View>
            :
            orderStore.searchEvents.length > 0 ?
              <View style={{ height: height(80), width: width(95), alignItems: 'center' }}>
                <ScrollView>
                  {
                    orderStore.searchEvents.map((item, key) => {
                      return (
                        this._event(item, key)
                      );
                    })
                  }
                </ScrollView>
              </View>
              :
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: totalSize(2.5), color: 'black', marginHorizontal: 15, textAlign: 'center' }}>No event found.</Text>
              </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },

});
