/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { observer } from 'mobx-react';
import Store from '../Stores';
export default class SearchByName extends Component<Props> {
  constructor(props){
      super(props);
      this.state = {
        loading: false,
        name:'',
        result: [],
      }
    }
  static navigationOptions = {
        header: null,
    }
    searchPlayer(){
      if ( this.state.name.length === 0 ) {
        ToastAndroid.show('Please enter a player name', ToastAndroid.LONG);
      }
        else {
          // this.get();
          let { orderStore } = Store;
          this.setState({loading: true})
          var url = 'http://play4team.com/AdminApi/api/searchPlayerByName?name=' + this.state.name+'&id='+orderStore.playerLoginRes.id+'&type='+orderStore.playerLoginRes.Status;
          console.log('url=',url);

          fetch(url, {
             method: 'POST',
             headers: {
               'Content-Type': 'application/x-www-form-urlencoded',
               // 'Accept': 'application/json',
              }
            }).then((response) => response.json())
                  .then((responseJson) => {
                    // console.log('search by name finding============>>>>>',responseJson.data);
                    if (responseJson.status === 'True') {
                         orderStore.searchData = responseJson.data;
                         // console.log('searchPlayerby Name= ',orderStore.searchData);
                         this.setState({
                           result: responseJson.data
                         })
                         // console.warn('player list',this.state.result);
                         this.setState({loading: false})
                         this.props.navigation.navigate('PlayerResult',{list: this.state.result});

                    }else {
                      this.setState({loading: false})
                      ToastAndroid.show('There is no player exist with this name', ToastAndroid.LONG);
                    }
                  }).catch((error)=>{
                    console.log('error',error);

                    this.setState({loading: false})
                  })
        }
      }
      searchTeam(){
        let { orderStore } = Store;
        if ( this.state.name.length === 0 ) {
          ToastAndroid.show('Please enter a team name', ToastAndroid.LONG);
        }
          else {
            // this.get();
            this.setState({loading: true})
            let url = 'http://play4team.com/AdminApi/api/searchteamByName?name=' + this.state.name +'&id='+orderStore.playerLoginRes.id +'&type=1';
            console.log('searchByName URL====:', url);
            fetch('http://play4team.com/AdminApi/api/searchteamByName?name=' + this.state.name+'&id='+orderStore.playerLoginRes.id +'&type=1', {
               method: 'POST',
               headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json',
                }
              }).then((response) => response.json())
                    .then((responseJson) => {
                      // console.log('search by name finding============>>>>>',responseJson.data);
                      if (responseJson.status === 'True') {
                           orderStore.searchData = responseJson.data;
                           this.setState({
                             result: responseJson.data
                           })
                          //  console.warn('Team list',this.state.result);
                           this.setState({loading: false})
                           this.props.navigation.navigate('PlayerResult',{list: this.state.result});
                      }else {
                        this.setState({loading: false})

                          ToastAndroid.show('There is no team exist with this name', ToastAndroid.LONG);

                      }
                    })
          }
        }
        searchEvents(){
          if ( this.state.name.length === 0 ) {
            ToastAndroid.show('Please enter a Event name', ToastAndroid.LONG);
          }
            else {
              // this.get();
              let { orderStore } = Store;
              this.setState({loading: true})
              fetch('http://play4team.com/AdminApi/api/searchEventByName?eventName=' + this.state.name, {
                 method: 'POST',
                }).then((response) => response.json())
                      .then((responseJson) => {
                          this.setState({loading: false})
                          // console.log('events=',responseJson);
                        if (responseJson.status === 'True') {
                             orderStore.searchEvents = responseJson.eventData;
                             this.props.navigation.navigate('detailEvent',{eventItem: responseJson.eventData});
                        }else {
                          this.setState({loading: false})
                          ToastAndroid.show('There is no event found with this name.', ToastAndroid.LONG);
                        }
                      })
            }
          }
        searchByName(){
          let { orderStore } = Store;

          if (orderStore.search === 'Player') {
              this.searchPlayer()
          }else {
            if (orderStore.search === 'Team') {
                this.searchTeam()
            }else {
                this.searchEvents()
            }
          }
        }

  render() {
    let { orderStore } = Store;
    if(this.state.loading == true){
        return(
            <View style={{height: height(100), width: width(100), flex:1}} >
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </View>
        );
    }

    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <View style={{height:height(3),margin:15,marginBottom:5,justifyContent:'center',alignItems:'flex-start'}}>
            <Text style={{fontSize:totalSize(1.5),color:'black',marginLeft:5}}>Name</Text>
          </View>
          <View style={{height:height(7),margin:15,marginTop:0,justifyContent:'center',alignItems:'flex-start'}}>
            <TextInput
                onChangeText={(value) => this.setState({name: value})}
                underlineColorAndroid='transparent'
                placeholder='Name'
                placeholderTextColor='gray'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(92.5),backgroundColor:'white',borderRadius:7,fontSize:totalSize(1.5),textAlign :'left',marginTop:0}}
                />
          </View>
        </View>
        <TouchableOpacity style={{height:height(10),backgroundColor:'#212157',justifyContent:'center',alignItems:'center'}}
          onPress={()=>{
            this.searchByName();
          }}
        >
          <Text style={{fontSize:totalSize(2),color:'white'}}>Find {orderStore.search == 'Team' ? 'Players':'Events'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#f6f6f6',
  },

});
