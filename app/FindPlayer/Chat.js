// import React, { Component } from 'react';
// import {
//   Platform,
//   StyleSheet,
//   Text,
//   View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput,alert
// } from 'react-native';
// import  { width, height, totalSize } from 'react-native-dimension';
// import { observer } from 'mobx-react';
// import Store from '../Stores';
// import { GiftedChat } from 'react-native-gifted-chat';
// const CHATKIT_TOKEN_PROVIDER_ENDPOINT = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/0c434a7d-636b-44ac-ba8e-32a3e2064c97/token";
// const CHATKIT_INSTANCE_LOCATOR = "v1:us1:0c434a7d-636b-44ac-ba8e-32a3e2064c97";
// const CHATKIT_SECRIT_KEY = "ac3f89bc-187d-4d1c-9266-8a3fa7b291d7:l00A6dnAJ1bJmDXqt5iqWRdDTO9rD6Bwtl0nBTDeyhU=";
// const CHATKIT_ROOM_ID = 15931313;
// const CHATKIT_USER_NAME = "test2"; // Let's chat as "Dave" for this tutorial
// export default class Chat extends Component<Props> {
//   constructor(props) {
//     super(props);
//     this.state = {
//        messages: [],
//     };
//   }
//   static navigationOptions = {
//        title: 'Chat',
//        headerStyle: {
//          backgroundColor:'#1d1d4f',
//        },
//        headerTintColor:'white',
//        headerTintStyle: {
//          fontSize: totalSize(2.2)
//        }
//     }
//   componentWillMount(){
//     let { orderStore } = Store;
//     console.log('sender info=',orderStore.playerLoginRes);
//     const chatkit = new Chatkit.default({
//         instanceLocator: CHATKIT_INSTANCE_LOCATOR,
//         key: CHATKIT_SECRIT_KEY
//       })
//       chatkit.createUser({
//         id: orderStore.playerLoginRes.id,
//         name: orderStore.playerLoginRes.Name,
//       })
//     // currentUser.createRoom({
//     //     name: 'general',
//     //     private: true,
//     //     addUserIds: ['craig', 'kate']
//     //   }).then(room => {
//     //     console.log(`Created room called ${room.name}`)
//     //   })
//     //   .catch(err => {
//     //     console.log(`Error creating room ${err}`)
//     //   })
//   }
//   componentDidMount(){
//     // This will create a `tokenProvider` object. This object will be later used to make a Chatkit Manager instance.
//        const tokenProvider = new Chatkit.TokenProvider({
//          url: CHATKIT_TOKEN_PROVIDER_ENDPOINT
//        });

//        // This will instantiate a `chatManager` object. This object can be used to subscribe to any number of rooms and users and corresponding messages.
//        // For the purpose of this example we will use single room-user pair.
//        const chatManager = new Chatkit.ChatManager({
//          instanceLocator: CHATKIT_INSTANCE_LOCATOR,
//          userId: CHATKIT_USER_NAME,
//          tokenProvider: tokenProvider
//        });

//        // In order to subscribe to the messages this user is receiving in this room, we need to `connect()` the `chatManager` and have a hook on `onNewMessage`. There are several other hooks that you can use for various scenarios. A comprehensive list can be found [here](https://docs.pusher.com/chatkit/reference/javascript#connection-hooks).
//        chatManager.connect().then(currentUser => {
//          this.currentUser = currentUser;
//          this.currentUser.subscribeToRoom({
//            roomId: CHATKIT_ROOM_ID,
//            hooks: {
//              onNewMessage:  this.onReceive.bind(this)
//            }
//          });
//        })
//        .catch(error => {
//       console.error("error:", error);
//     })
//     // DeviceEventEmitter.addListener('sms_onDelivery', (msg) => {
//     //   console.log(msg);
//     // });
//   }
//   onReceive(data) {
//       console.warn('data=',data);
//       const { id, senderId, text, createdAt } = data;
//       const incomingMessage = {
//         _id: id,
//         text: text,
//         createdAt: new Date(createdAt),
//         user: {
//           _id: senderId,
//           name: senderId,
//           avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmXGGuS_PrRhQt73sGzdZvnkQrPXvtA-9cjcPxJLhLo8rW-sVA"
//         }
//       };

//       this.setState(previousState => ({
//         messages: GiftedChat.append(previousState.messages, incomingMessage)
//       }));
//     }
//     onSend([message]) {
//     this.currentUser.sendMessage({
//       text: message.text,
//       roomId: CHATKIT_ROOM_ID
//     });
//   }
//   render() {
//     return(
//       <GiftedChat
//         messages={this.state.messages}
//         onSend={messages => this.onSend(messages)}
//         user={{
//           _id: CHATKIT_USER_NAME,
//         }}
//       />

//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 10,
//     backgroundColor: '#f6f6f6',
//   },
//   subContainer: {
//     flex:1,
//     margin:20,

//     marginBottom:5
//   },
//   strip: {
//     height:height(8),
//     backgroundColor:'#ffffff',
//     marginBottom:5,
//     flexDirection:'row',
//     borderRadius:5
//   },
// });
