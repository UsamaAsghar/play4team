import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, Image, ScrollView, TouchableOpacity, Slider, ToastAndroid
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import PopupDialog, { slideAnimation, DialogTitle } from 'react-native-popup-dialog';
import { Avatar, Icon } from 'react-native-elements';
import DateTimePicker from 'react-native-datepicker'
import Check from 'react-native-vector-icons/Entypo';
import Cross from 'react-native-vector-icons/Entypo';
import { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import api from '../ApiController/api';
import { observer } from 'mobx-react';
import Store from '../Stores';
@observer export default class GeneralSearch extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      date: "",
      time: "",
      result: [],
      distance: 0,
      sport_id: [],
      sportName: '',
    }
  }
  static navigationOptions = {
    header: null,
  }
  componentWillMount = async () => {
    let { orderStore } = Store;
    this.setState({ loading: true })
    let response = await api.get('Sport');
    orderStore.sportEvent = response.data;
    // console.log('sportAPi=',response.data);
    this.setState({ loading: false })
    if (response.status === 'True') {
      for (var i = 0; i < orderStore.sportEvent.length; i++) {
        orderStore.sportEvent[i].added = false;
        orderStore.sportEvent[i].checkStatus = false;
      }
      // console.log('Response',orderStore.sportEvent);
    } else {
      this.setState({ loading: false })
    }
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      orderStore.sportEvent[0].added = true;
      orderStore.sportEvent[0].checkStatus = true;
      orderStore.sport_id = orderStore.sportEvent[0].id;
      if (i === 0) {
        this.state.sport_id.push(orderStore.sportEvent[0].id);
      }
      this.setState({ sportName: orderStore.sportEvent[0].Name })
    }
  }
  selectSport(sport) {
    //** Selecting sport by clicking
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      if (sport.id === orderStore.sportEvent[i].id) {
        orderStore.sportEvent[i].added = true;
        orderStore.sportEvent[i].checkStatus = true;
        orderStore.sport_id = orderStore.sportEvent[i].id;
        this.setState({
          loading: false
        })
      } else {
        //** other sports marked as unSelect
        orderStore.sportEvent[i].added = false;
        orderStore.sportEvent[i].checkStatus = false;
      }
    }
  }
  sportsEvent(sport) {
    //** Selecting sport by clicking
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      if (sport.id === orderStore.sportEvent[i].id) {
        orderStore.sportEvent[i].added = true;
        orderStore.sportEvent[i].checkStatus = true;
        this.state.sport_id.push(orderStore.sportEvent[i].id);
        // console.log('sports are=',this.state.sport_id);
        this.setState({
          loading: false
        })
      }
    }
  }
  unSelectSports(sport, state) {
    //** Unselecting sport by clicking
    let { orderStore } = Store;
    if (state === false) {
      var index = 0;
      for (var i = 0; i < this.state.sport_id.length; i++) {
        if (this.state.sport_id[i] === sport.id) {
          index = i;
          break;
        }
      }
      if (this.state.sport_id.length > 1) {
        this.state.sport_id.splice(index, 1);
        sport.added = false;
        sport.checkStatus = false;
      }
      this.setState({
        loading: false
      })
      // console.log('remove list=',this.state.sport_id);
    }
  }
  generalSearch() {
    let { orderStore } = Store;
    if (orderStore.search === 'Player') {
      var url = 'http://play4team.com/AdminApi/api/playerGeneralSearch?sportId=' + orderStore.sport_id + '&latitude=' + orderStore.currentLocation.latitude + '&longitude=' + orderStore.currentLocation.longitude + '&radius=' + parseInt(this.state.distance * 160934) + '&id=' + orderStore.playerLoginRes.id + '&type=' + orderStore.playerLoginRes.Status;
       console.log('playerGeneralSearch=',url);
    } else {
      if (orderStore.search === 'Events') {
        var url = 'http://play4team.com/AdminApi/api/eventGeneralSearch?sportId=' + this.state.sport_id + '&latitude=' + orderStore.currentLocation.latitude + '&longitude=' + orderStore.currentLocation.longitude + '&radius=' + parseInt(this.state.distance * 160934);
      } else {
        var url = 'http://play4team.com/AdminApi/api/teamGeneralSearch?sportId=' + orderStore.sport_id + '&latitude=' + orderStore.currentLocation.latitude + '&longitude=' + orderStore.currentLocation.longitude + '&radius=' + parseInt(this.state.distance * 160934) + '&id=' + orderStore.playerLoginRes.id + '&type=' + orderStore.playerLoginRes.Status;
        console.log('playerGeneralSearch=',url);
      }
    }
    // console.log('url=',url);
    this.setState({ loading: true })
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log('player list general search',responseJson);
        if (responseJson.status === 'True' && orderStore.search === 'Team') {
          orderStore.searchData = responseJson.data;
          // if (orderStore.search === 'Team') {
          //   orderStore.searchData.forEach((item) => {
          //     item.id = item.Team_id;
          //     item.Status = 1;
          //   })
          // }
          this.setState({
            result: responseJson.data
          })
          // orderStore.searchData.forEach((item) => {
          //   item.id = item.player_id;
          //   item.Status = item.status;
          // })
          // console.log('player list',orderStore.searchData); //Team_id

          this.setState({ loading: false })
          this.props.navigation.navigate('PlayerResult');
        } else {
          this.setState({ loading: false })
          orderStore.searchEvents = responseJson.data;
          // console.log('event list',orderStore.searchEvents);
          this.props.navigation.navigate('PlayerResult');
          // ToastAndroid.show('Please choose any sport and give range in meters', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('There is a network issue please,Try again', ToastAndroid.LONG);
      });
  }
  componentWillUnmount() {
    let { orderStore } = Store;
    orderStore.sport_id = 0;
    this.setState({ distance: 0 })
    orderStore.searchEvents = [];
    // **** reset the sport store
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      orderStore.sportEvent[i].added = false;
      orderStore.sportEvent[i].checkStatus = false;
    }
  }
  _renderItem({ item, index }){
    return (
      <View style={{ alignItems:'center' }}>
        <Avatar
          large
          rounded
          source={{
            uri: item.Path,
          }}
        />
        <Text style={{ marginHorizontal: 10, color: 'black', fontSize: 12, textAlign:'center' }}>{item.Name}</Text>
      </View>
    );
  }
  render() {
    let { orderStore } = Store;
    if (this.state.loading == true) {
      return (
        <View style={styles.preloader} >
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(5), width: width(100), backgroundColor: '#e9e9e9', justifyContent: 'center', alignItems: 'flex-end' }}>
            <Text style={{ fontSize: totalSize(1.5), color: 'black', marginRight: 15 }}>Show All</Text>
          </View>
          <View style={{ height: height(3), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
          </View>
          <View style={{ height: height(22), width: width(93), borderRadius: 5, backgroundColor: '#ffffff', margin: 15, marginTop: 0, marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
            {/* <View style={{ height: height(5), width: width(93), flexDirection: 'row' }}>
              <View style={{ width: width(46), justifyContent: 'center', alignItems: 'flex-end' }}>
                <Text style={{ fontSize: totalSize(1.5), color: 'black' }}>EventType: </Text>
              </View>
              <View style={{ width: width(46), justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>{this.state.sportName}</Text>
              </View>
            </View> */}
            <View style={{ height: height(17), width: width(93), alignItems: 'center' }}>
              <View style={{ height: height(17), width: width(80), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <Carousel
                  ref={(c) => { this._carousel = c; }}
                  data={orderStore.sportEvent}
                  renderItem={this._renderItem}
                  // activeAnimationType='timing' //spring
                  inactiveSlideOpacity={0.5}
                  inactiveSlideScale={0.6}
                  sliderWidth={width(80)}
                  itemWidth={width(20)}
                  onSnapToItem={(index) =>{
                    orderStore.sport_id = index
                  }}
                />
              </View>
            </View>
          </View>
          <View style={{ height: height(1) }}>
          </View>
          <View style={{ height: height(18), margin: 15, marginTop: 10, marginBottom: 10, backgroundColor: '#ffffff', borderRadius: 5 }}>
            <View style={{ height: height(5), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: totalSize(1.8), color: '#444473' }}>Search</Text>
            </View>
            <View style={{ height: height(13), flexDirection: 'row' }}>
              <TouchableOpacity style={{ width: width(40), alignItems: 'flex-end' }}>
                <Icon
                  reverse
                  name='calendar'
                  type='entypo'
                  color='#1d1d4f'
                  containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
                />
                {/* <ImageBackground source={require('../img/calenderr.png')} style={{height:height(9),width:width(16.3)}}/> */}
                <DateTimePicker
                  style={{ width: width(20), height: height(9), position: 'absolute', backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}
                  date={this.state.date}
                  time={this.state.time}
                  mode="datetime"
                  placeholder="Event Start"
                  format="YYYY-MM-DD"
                  minDate="2018-8-01"
                  maxDate="2020-8-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  hideText='false'
                  iconComponent={<Image source={require('../img/calenderr.png')} style={{ height: height(9), width: width(16) }} />}
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: 0,
                      top: 0,
                      marginBottom: 0,
                      marginRight: 0,
                      height: height(9),
                      width: width(16)
                    },
                    dateInput: {
                      marginLeft: 0
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => { this.setState({ date: date }) }}
                />
                <View style={{ height: height(4), width: width(14), alignItems: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.4), color: '#444473', alignItems: 'center' }}>When?</Text>
                </View>
              </TouchableOpacity>
              <View style={{ width: width(13) }}>
              </View>
              <TouchableOpacity style={{ height: height(13), width: width(40), alignItems: 'flex-start' }}
                onPress={() => {
                  this.popupDialog.show();
                }}
              >
                <Icon
                  reverse
                  name='golf-course'
                  type='material'
                  color='#1d1d4f'
                  containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}
                />
                {/* <Image source={require('../img/distance.png')} style={{height:height(9),width:width(16),resizeMode:'contain'}} /> */}
                <View style={{ height: height(4), width: width(17), alignItems: 'center', flexWrap: 'wrap' }}>
                  <Text style={{ fontSize: totalSize(1.3), color: '#444473', textAlign: 'center' }}>Upto {parseInt(this.state.distance)} miles from here</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <PopupDialog
          ref={(popupDialog) => { this.popupDialog = popupDialog; }}
          dialogAnimation={slideAnimation}
          containerStyle={{ height: height(100) }}
          dialogStyle={{ height: height(30), marginTop: 200, marginBottom: 0 }}
          slideFrom='bottom'
          animationDuration={150}
          dismissOnTouchOutside={false}
        >
          <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
            <View style={{ height: height(7), backgroundColor: '#1d1d4f', flexDirection: 'row' }}>
              <TouchableOpacity style={{ width: width(10), justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                  this.popupDialog.dismiss()
                }}
              >
                <Cross name='cross' size={26} color='#fff' />
                {/* <Image source={require('../img/x_btn.png')} style={{height:height(5),width:width(6)}} /> */}
              </TouchableOpacity>
              <View style={{ width: width(80), justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: totalSize(2), color: 'white' }}>Distance</Text>
              </View>
              <TouchableOpacity style={{ width: width(10), justifyContent: 'center', alignItems: 'center' }}
                onPress={() => {
                  this.popupDialog.dismiss()
                }}
              >
                <Check name='check' size={26} color='#fff' />
                {/* <Image source={require('../img/tick_btn.png')} style={{height:height(5),width:width(6)}} /> */}
              </TouchableOpacity>
            </View>
            <View style={{ height: height(2) }}>
            </View>
            <TouchableOpacity style={{ height: height(4), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: totalSize(2), color: '#333360' }}>Up to {parseInt(this.state.distance)}mi from my location</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={{height:height(4),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2),color:'#333360'}}>Up to {parseInt(this.state.distance)}mi from my postcode</Text>
              </TouchableOpacity> */}
            {/* <View style={{height:height(10),justifyContent:'center',alignItems:'center'}}>
                 <View style={{height:height(7),width:width(43),justifyContent:'center',alignItems:'center',backgroundColor:'#d7d7d7',borderRadius:7}}>
                  <Text style={{fontSize:totalSize(2),color:'#333360'}}>e.gG1 3ST</Text>
                </View>
              </View> */}
            <View style={{ height: height(8), justifyContent: 'center', backgroundColor: 'white' }}>
              <Slider
                value={this.state.distance}
                onValueChange={value => this.setState({ distance: value })}
                maximumValue={25}
                minimumValue={1}
                thumbTintColor='#29d9a4'
                maximumTrackTintColor='#1fefd5'
                minimumTrackTintColor='#1fefd5'
                style={{ backgroundColor: 'white', height: height(8) }}
              />
            </View>
          </View>
        </PopupDialog>
        <TouchableOpacity style={{ height: height(9), backgroundColor: '#212157', justifyContent: 'center', alignItems: 'center' }}
          onPress={() => {
            if (orderStore.sport_id !== 0 && this.state.distance !== 0) {
              this.generalSearch()
            } else {
              ToastAndroid.show('Please choose any sport or give distance', ToastAndroid.LONG);
            }
          }}
        >
          <Text style={{fontSize:totalSize(2),color:'white'}}>Find {orderStore.search == 'Team' ? 'Players':'Events'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },
  unSelect: {
    height: height(10),
    width: width(15),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selected: {
    height: height(13),
    width: width(20),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },

});
// {/* <ScrollView
// horizontal={true}
// showsHorizontalScrollIndicator = {false}
// scrollToEnd = {true}
// >
// {
//   orderStore.sportEvent.map((item,key)=>{
//     return(
//         <TouchableOpacity key={key} style={item.added === true? styles.selected : styles.unSelect}
//           onPress={()=>{
//             if (orderStore.search !== 'Events') {
//               this.selectSport(item)
//               this.setState({sportName: item.Name})
//             }else {
//                 if (orderStore.search === 'Events' && item.added === false) {
//                   this.sportsEvent(item)
//                   this.setState({sportName: item.Name})
//                 }else {
//                    this.unSelectSports(item,false)
//                 }
//             }
//           }}
//         >
//           {
//             item.added === true?
//               <View style={{flex:1,alignItems:'center',marginHorizontal:5}}>
//                 <Avatar
//                   size="large"
//                   // rounded
//                   source={{
//                     uri: item.Path,
//                   }}
//                 />
//                 {/* <Image source={{uri: item.Path}} style={{height:height(10),width:width(20),borderRadius:100,resizeMode:'contain'}}/> */}
// <Text style={{ fontSize: totalSize(1.6), color: 'black', textAlign: 'center', marginBottom: 3 }}>{item.Name}</Text>
//               </View >
//               :
// <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 5, marginVertical: 0 }}>
//   <Avatar
//     size="medium"
//     // rounded
//     source={{
//       uri: item.Path,
//     }}
//   />
//   {/* <Image source={{uri: item.Path}} style={{height:height(8),width:width(14),resizeMode:'contain',borderRadius:100}}/> */}
//   <Text style={{ fontSize: totalSize(1.2), color: 'black', marginBottom: 3, marginHorizontal: 2 }}>{item.Name}</Text>
// </View>
//           }
//         </TouchableOpacity >
//     );
//   })
// }
// </ScrollView > * /}
