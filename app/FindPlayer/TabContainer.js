/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';

import FindPlayer  from './FindPlayer';

class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(80),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Find Players</Text>
          </View>
        </View>
    );
  }
}
export default class TabContainer extends Component<Props> {

  constructor(props){
      super(props);
      this.state = {
        date:"",
        time:"",
      }
    }
  static navigationOptions = {
        headerTitle: <LogoTitle />,
        headerStyle:{
          backgroundColor:'#1d1d4f'
        },
        headerTintColor: 'white',


    }
  render() {
    return (
      <View style={styles.container}>
        <FindPlayer navigation = { this.props.navigation } />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
