import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,ImageBackground,Image,TouchableOpacity, AsyncStorage
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import PopupDialog, {slideAnimation,DialogTitle,FadeAnimation} from 'react-native-popup-dialog';
export default class MainScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      login: false,
      register: false,
    };
  }
  static navigationOptions = {
        header: null
  }
  componentWillMount = async() => {
    let mail =  await  AsyncStorage.getItem('async_email') || null;
    let pass = await AsyncStorage.getItem('async_password') || null;
    if (mail && pass) {
      this.setState({ loading: false })
      this.props.navigation.navigate('Login',{type:'1'});
    } else {
      this.setState({ loading: false })
    }
  }
  render() {
    if(this.state.loading){
      return(
          <ImageBackground style={styles.preloader} source={require('../img/splash.jpg')} >
              <OrientationLoadingOverlay
                  visible={true}
                  color="white"
                  indicatorSize="small"
                  messageFontSize={18}
                  message="Loading..."
              />
          </ImageBackground>
      );
}
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../img/bg_img.jpg')} style={{flex:1}}>
          <View style={{flex:1,backgroundColor:'rgba(29,37,86,0.8)'}}>
            <View style={{height:height(40),alignItems:'center'}}>
              <Image source={require('../img/mainScreenLogo.png')} style={{height:height(35),width:width(75),resizeMode:'contain'}} />
            </View>
            <View style={{height:height(56)}}>
              <View style={{height:height(12),alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2.5),color:'white'}}>YOUR SPORT. YOUR APP.</Text>
                <Text style={{fontSize:totalSize(1.5),color:'white',marginHorizontal:35,marginVertical:3,textAlign:'center'}}>Play4Team is a platform that allows Sports Players, Teams and Social Groups locate and communicate with each other.</Text>
              </View>
              <TouchableOpacity style={this.state.login === true? styles.signUpStyle : styles.signUp}
                onPress={()=>{
                  this.props.navigation.navigate('Login',{type:'1'});
                  // this.setState({ login: !this.state.login, register: false })
                }}
              >
                <Text style={{fontSize:totalSize(2),color:'white'}}>Login</Text>
              </TouchableOpacity>
              {
                this.state.login === true?
                 <View style={{height:height(20),justifyContent:'center',alignItems:'center'}}>
                   <TouchableOpacity style={{height:height(8),width:width(95),margin:10,marginTop:3,borderWidth:0,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(3,3,29,0.5)'}}
                     onPress={()=>{
                         // this.popupDialog.show();
                         this.props.navigation.navigate('Login',{type:'0'});
                     }}
                     >
                     <Text style={{fontSize:totalSize(2),color:'white'}}>Login as a Player</Text>
                   </TouchableOpacity>
                   <TouchableOpacity style={{height:height(8),width:width(95),margin:10,marginTop:3,borderWidth:0,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(3,3,29,0.5)'}}
                     onPress={()=>{
                         // this.popupDialog.show();
                         this.props.navigation.navigate('Login',{type:'1'});
                     }}
                     >
                     <Text style={{fontSize:totalSize(2),color:'white'}}>Login as a Team</Text>
                   </TouchableOpacity>
                 </View>
                 :
                 null
              }
              <TouchableOpacity style={this.state.register === true? styles.signUpStyle : styles.signUp}
                onPress={()=>{
                  this.props.navigation.navigate('SignUpTeam');
                    // this.setState({
                    //   register: !this.state.register,
                    //   login: false
                    // })
                }}
                >
                <Text style={this.state.register === true ? styles.txtStyle : styles.txt }>Register</Text>
              </TouchableOpacity>
              {
                this.state.register === true?
                 <View style={{height:height(20),justifyContent:'center',alignItems:'center'}}>
                   <TouchableOpacity style={{height:height(8),width:width(95),margin:10,marginTop:3,borderWidth:0,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(3,3,29,0.5)'}}
                     onPress={()=>{
                         // this.popupDialog.show();
                         this.props.navigation.navigate('SignUp');
                     }}
                     >
                     <Text style={{fontSize:totalSize(2),color:'white'}}>Player Registration</Text>
                   </TouchableOpacity>
                   <TouchableOpacity style={{height:height(8),width:width(95),margin:10,marginTop:3,borderWidth:0,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(3,3,29,0.5)'}}
                     onPress={()=>{
                         // this.popupDialog.show();
                         this.props.navigation.navigate('SignUpTeam');
                     }}
                     >
                     <Text style={{fontSize:totalSize(2),color:'white'}}>Team Registration</Text>
                   </TouchableOpacity>
                 </View>
                 :
                 null
              }
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  signUp: {
    height:height(8),
    margin:10,
    marginTop:3,
    backgroundColor:'rgba(3,3,29,0.5)',
    borderWidth:0,
    justifyContent:'center',
    alignItems:'center'
  },
  signUpStyle: {
    height:height(8),
    margin:10,
    marginTop:3,
    borderWidth:0,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#03031d',
  },
  txt:{
    fontSize:totalSize(2),
    color:'white'
  },
  txtStyle:{
    fontSize:totalSize(2),
    color:'white'
  },
  preloader: {
    height: height(100),
    width: width(100),
    flex:1
  },
});
