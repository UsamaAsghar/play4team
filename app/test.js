import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,Slider,KeyboardAvoidingView,ToastAndroid,
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
export default class test extends Component<Props>{
  constructor(props){
    super(props);
    this.state = {
      address: ''
    }
  }
  mainCard = () => {
    return(
      <View style={styles.cardCon}>
          <View style={styles.imgCon}>
            <ImageBackground style={styles.imgStyle} source={{ uri: 'https://source.unsplash.com//l1MCA0VyNrk/840x840' }}>
              <View style={styles.imgShadow}>
                <Text style={styles.imgText}>Islamabad is the capital of pakistan!</Text>
              </View>
            </ImageBackground>
          </View> 
          <View style={{ height: height(10), width: '100%' }}>
            <View style={{ height: height(7), width:'92%', alignSelf:'center', flexDirection:'row', justifyContent:'center' }}>
              <View style={{ width: '25%', height: '100%', justifyContent:'center' }}>
                <Text style={{ color: 'black' }}>Lahore </Text>
                <Text style={{ color: 'black' }}>45</Text>
              </View>
              <View style={{ width: '25%', height: '100%', justifyContent:'center' }}>
                <Text style={{ color: 'black' }}>Lahore </Text>
                <Text style={{ color: 'black' }}>45</Text>
              </View>
              <View style={{ width: '25%', height: '100%', justifyContent:'center' }}>
                <Text style={{ color: 'black' }}>Lahore </Text>
                <Text style={{ color: 'black' }}>45</Text>
              </View>
              <View style={{ width: '25%', height: '100%', justifyContent:'center' }}>
                <Text style={{ color: 'black' }}>Lahore </Text>
                <Text style={{ color: 'black' }}>45</Text>
              </View>
            </View>
            <Text style={{ marginHorizontal: 15, color:'gray', fontSize: 10, marginVertical: 3 }}>Like: <Text style={{ color: 'black' }}>Lahore pakistan</Text></Text>
          </View> 
        </View>
    )
  }

  listItemCard = () => {
    return(
      <View></View>
    )
  }

  render() {
    return (
      <View style={{ flex:1 }}>
        {this.mainCard()}
        {this.listItemCard()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
    cardCon: {
      height: height(35), 
      width: width(90), 
      elevation: 3,
      backgroundColor:'white', 
      alignSelf:'center', 
      marginVertical: 20 
    },
    imgCon: {
      height: height(25), 
      width: '100%' 
    },
    imgStyle: {
     height: '100%', 
     width: '100%', 
     resizeMode:'contain' 
    },
    imgShadow: {
      height: '100%', 
      width:'100%', 
      backgroundColor:'rgba(0,0,0,0.5)', 
      justifyContent:'flex-end' 
    },
    imgText: {
      color:'white', fontSize: totalSize(2), marginHorizontal: 15, marginVertical: 15 
    }
});

