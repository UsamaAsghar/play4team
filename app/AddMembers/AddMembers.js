import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import PopupDialog, {slideAnimation,DialogTitle,FadeAnimation} from 'react-native-popup-dialog';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox'
import MemberComp from './MemberComp';
class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Add Members</Text>
          </View>
          <View style={{width:width(15),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Cancel</Text>
          </View>

        </View>
    );
  }
}
export default class AddMembers extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      contact: true,
      friends: false,
      group: false,
      nearby: false,
    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{height:height(9),flexDirection:'row',backgroundColor:'#33327e'}}>
          <TouchableOpacity style={{width:width(25),alignItems:'center'}}
            onPress={()=> this.setState({
              contact: true,
              friends: false,
              group: false,
              nearby: false
            })}
          >
            <Image source={require('../img/contact.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Contact</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:width(25),alignItems:'center'}}
            onPress={()=> this.setState({
              contact: false,
              friends: true,
              group: false,
              nearby: false
            })}
          >
            <Image source={require('../img/friend_icon.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Friends</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:width(25),alignItems:'center'}}
            onPress={()=> this.setState({
              contact: false,
              friends: false,
              group: true,
              nearby: false
            })}
          >
            <Image source={require('../img/friend_icon.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Group</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:width(25),alignItems:'center'}}
            onPress={()=> this.setState({
              contact: false,
              friends: false,
              group: false,
              nearby: true
            })}
          >
            <Image source={require('../img/nearby_user.png')} style={{height:height(5),width:width(7),backgroundColor:'transparent'}}/>
            <Text style={{fontSize: totalSize(1.8),color:'white'}}>Nearby</Text>
          </TouchableOpacity>
        </View>
        <View style={{height:height(7),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(5.5),width:width(85),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/search_friends.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
            <View style={{height:height(7),width:width(70)}}>
              <TextInput
                onChangeText={(value) => this.validate(value,'mobNo')}
                placeholder={"Search"}
                style={{fontSize:totalSize(2),color:'white',backgroundColor:'white',width:width(65)}}
                underlineColorAndroid="transparent"
                />
            </View>
          </View>
          <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}>
            <Image source={require('../img/cross.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
          </TouchableOpacity>
        </View>
          {
            this.state.contact ===true ?
              <View style={{flex: 1,alignItems:'center'}}>
                <ScrollView>
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                  <MemberComp />
                </ScrollView>
              </View>
              :
              <View style={{flex:1}}>
                {
                  this.state.friend === true ?
                    <View style={{flex: 1,alignItems:'center'}}>
                      <ScrollView>
                        <MemberComp />
                      </ScrollView>
                    </View>
                    :
                    <View style={{flex:1}}>
                      {
                        this.state.group === true ?
                          <View style={{flex: 1,alignItems:'center'}}>
                            <ScrollView>
                              <MemberComp />
                            </ScrollView>
                          </View>
                          :
                          <View style={{flex:1, alignItems:'center'}}>
                            
                          </View>
                      }
                    </View>
                }
              </View>
          }
        <PopupDialog
           dialogTitle={<DialogTitle title="Send Your Invites"
           titleStyle = {{backgroundColor:'#33327e'}}
           titleTextStyle = {{ color:'white',fontSize:totalSize(2.2) }}
           />}
           ref={(popupDialog) => { this .popupDialog = popupDialog; }}
           dialogAnimation = {FadeAnimation}
           dialogStyle ={{ height:height(45),width:width(90),marginBottom:0}}

         >
           <View style={{flex:1}}>
             <View style={{height:height(29),alignItems:'center'}}>
               <View style={{height:height(7),width:width(70),justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:totalSize(1.8),color:'#606199',textAlign:'center',margin:25}}>Tap the players you want to invite and we will do the rest.</Text>
               </View>
             </View>
             <TouchableOpacity style={{height:height(8.4),width:width(90),borderBottomRightRadius:5,borderBottomLeftRadius:5,backgroundColor:'#45e061',justifyContent:'center',alignItems:'center'}}
               onPress={()=>{
                 this.popupDialog.dismiss()
               }}
              >
               <Text style={{fontSize:totalSize(1.8),color:'white'}}>Cool, Got It</Text>
             </TouchableOpacity>
           </View>
        </PopupDialog>
        <TouchableOpacity style={{height:height(7),width:width(100),backgroundColor:'#1d1d4f',justifyContent:'center',marginTop:10,alignItems:'center'}}>
          <Text style={{fontSize:totalSize(2),color:'white'}}>Add Group Member(s)</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
