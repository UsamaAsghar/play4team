/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox';
import PopupDialog, {slideAnimation,DialogTitle} from 'react-native-popup-dialog';

export default class InvitationComp extends Component<Props> {
  constructor(props){
    super(props);
    this.state = {
      checked: false,
    }
  }

  static navigationOptions = {
        header:null,

    }
  render() {
    return (

        <TouchableOpacity style={{height:height(8),width:width(93),backgroundColor:'#ffffff',flexDirection:'row'}}
          onPress={()=>{
            this.popupDialog.show();
          }}
        >
          <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <Image source={require('../img/friend_img.png')} style={{height:height(6),width:width(12)}} />
          </View>
          <View style={{width:width(70),flexDirection:'row'}}>
            <View style={{width:width(53),justifyContent:'center'}}>
              <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.8),color:'black'}}>Hamza Butt</Text>
              </View>
              <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.5),color:'gray'}}>Activated</Text>
              </View>
            </View>
            <View style={{width:width(15),justifyContent:'center',alignItems:'flex-end'}}>
              <CircleCheckBox
                checked={this.state.checked}
                onToggle={(checked) => this.setState({checked: checked})}
                outerColor  = {'#5b5b7e'}
                innerColor  = {'#5b5b7e'}
                />
            </View>

          </View>
        </TouchableOpacity>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
