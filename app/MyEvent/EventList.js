
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,KeyboardAvoidingView
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Swipeout from 'react-native-swipeout';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{height:height(9.5),width:width(75),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
        <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
          <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
          <Text style={{fontSize:totalSize(2.5),color:'white'}}>{orderStore.eventStatus} Events</Text>
        </View>
      </View>
    );
  }
}
@observer export default class EventList extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      name: '',
    }
  }
  static navigationOptions = {
      headerTitle: <LogoTitle />,
      headerStyle: {
        backgroundColor:'#1d1d4f'
      },
      headerTintColor:'white',
    }
  componentWillMount(){
    let { orderStore } = Store;
  }
  _member = (item,key) =>{
    let { orderStore } = Store;
    return(
      <TouchableOpacity key={key} style={{height:height(13),flexDirection:'row',marginBottom:0.3,borderBottomWidth:0.5,borderColor:'black'}}
        onPress={()=>{
          if (orderStore.eventStatus === 'Managing') {
            this.props.navigation.push('EventDetail',{eventItem: item})
          }else {
            this.props.navigation.push('detailEvent',{eventItem: item})
          }
        }}
      >
        <View style={{width:width(25),justifyContent:'center',alignItems:'center'}}>
          <Image source={{uri: 'http://play4team.com/AdminApi/public/sports_images/'+item.sport_image}} style={{height:height(7),width:width(12)}} />
          <Text style={{fontSize: totalSize(1.5),color:'gray',marginTop:3}}>{item.sport_name}</Text>
        </View>
        <View style={{width:width(70),justifyContent:'center'}}>
          <Text style={{fontSize: totalSize(2.5),height:height(4),color:'black'}}>{item.Event_Name}</Text>
          <Text style={{fontSize: totalSize(1.7),height:height(3),color:'gray'}}>From {item.Start_Date} to {item.End_Date}</Text>
          <View style={{flexDirection:'row'}}>
            <Image source={require('../img/Addresslocation.png')} style={{height:height(2),width:width(2.5),marginRight:3}} />
            <Text style={{fontSize: totalSize(1.6),width:width(60),textAlign:'left',color:'gray'}}>{item.Address}</Text>
          </View>
        </View>
        <View style={{width:width(5),justifyContent:'center'}}>
          <Image source={require('../img/ic_arrow_rit.png')} />
        </View>
      </TouchableOpacity>
    );
  }
  _header = () => {
      return(
        <View style={{height:height(7),width:width(100),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(5.5),width:width(80),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  if (this.state.name.length === 0) {
                      ToastAndroid.show('Have you enter any member name ???', ToastAndroid.LONG);
                  }else {
                      // this.searchMember()
                  }
                }}
              >
              <Image source={require('../img/search_btn.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
            <View style={{height:height(7),width:width(70)}}>
              <TextInput
                onChangeText={(value) => this.setState({name: value})}
                value={this.state.name}
                placeholder={"Name"}
                placeholderTextColor='gray'
                style={{fontSize:totalSize(2),color:'black',backgroundColor:'white',width:width(65)}}
                underlineColorAndroid="transparent"
                />
            </View>
          </View>
          <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}
            // onPress={()=>{ this.clearSearch() }}
          >
            <Image source={require('../img/cross-icon.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
          </TouchableOpacity>
        </View>
      );
  }
  render() {
    let { orderStore } = Store;
      if(this.state.loading == true){
          return(
              <View style={{flex:1,height:height(100),width:width(100),backgroundColor:'rgba(29,37,86,0.6)'}}>
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
              </View>
          );
      }
    return (
            <View  style={{flex:1}}>
              {
                // this._header()
              }
              <View style={{height:height(90),marginTop:15,alignItems:'center'}}>
                <View style={{height:height(80),width:width(100),alignItems:'center'}}>
                  <ScrollView>
                    {
                     orderStore.eventStatus === 'Managing'?
                       orderStore.events.managingEvents.map((item,key)=>{
                         return(
                            this._member(item,key)
                          );
                        })
                      :
                      orderStore.eventStatus === 'Attending'?
                        orderStore.events.managingEvents.map((item,key)=>{
                          return(
                             this._member(item,key)
                           );
                         })
                       :
                       orderStore.eventStatus === 'Pending'?
                         orderStore.events.pendingEvents.map((item,key)=>{
                           return(
                              this._member(item,key)
                            );
                          })
                        :
                        orderStore.eventStatus === 'Completed'?
                          orderStore.events.completeEventsInvited.map((item,key)=>{
                            return(
                               this._member(item,key)
                             );
                           })
                         :
                          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                           <Text style={{fontSize:totalSize(2.5),color:'black'}}>There no player or team found with this name</Text>
                          </View>
                    }
                  </ScrollView>
                </View>
              </View>
            </View >
     );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },

});
