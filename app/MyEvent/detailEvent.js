import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Event Detail</Text>
          </View>
        </View>
    );
  }
}
@observer export default class detailEvent extends Component<Props> {
  constructor(props){
      super(props);
      this.state = {
        loading: false,
        edit: false,
        chat: false,
        join: false,
        status: {},
      }
    }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle:{
      backgroundColor:'#1d1d4f'
    },
    headerTintColor: 'white',

    }
    componentWillMount(){
      let { orderStore } = Store;
      let {params} = this.props.navigation.state;
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/EventPlayers?eventId='+ params.eventItem.id, {
          method: 'POST',
          }).then((response) => response.json())
         .then((responseJson) => {
          // console.log('responseJson=',responseJson);
          this.setState({loading: false})
          if (responseJson.status === 'True') {
              this.setState({status: responseJson})
              // console.log('status=',this.state.status);
              }else {
                 this.setState({loading: false})
                 // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
              }
           }).catch((error)=>{
              this.setState({loading: false})
              // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
          });
    }
    buttons(btn){
      if (btn === 'edit') {
          this.setState({
            edit: true,
            chat: false,
            join: false,
          })
      }else {
        if (btn === 'chat') {
            this.setState({
              edit: false,
              chat: true,
              join: false,
            })
        }else {
            this.setState({
              edit: false,
              chat: false,
              join: true,
            })
        }
      }
    }
  render() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    console.log('event detail',params.eventItem);
    if(this.state.loading == true){
        return(
          <OrientationLoadingOverlay
              visible={true}
              color="white"
              indicatorSize="small"
              messageFontSize={18}
              message="Loading..."
          />
        );
    }
    return (
      <View style={styles.container}>
        <View style={{height:height(35),width:width(100)}}>
          <ImageBackground source={require('../img/top-bg.jpg')} style={{height:height(35),width:width(100),justifyContent:'flex-end'}}>
            <View style={{height:height(11),backgroundColor:'#33327e',flexDirection:'row',justifyContent:'center',alignItems:'center',marginBottom:10}}>
              <TouchableOpacity style={{height:height(6.5),width:width(27),margin:3,borderRadius:5,backgroundColor:'#5beb33',justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  this.props.navigation.navigate('ViewEvent',{eventDetail: params.eventItem})
                }}
              >
                <Text style={{fontSize:totalSize(2),color:'#33317f',fontWeight:'bold'}}>View</Text>
              </TouchableOpacity>
              <View style={{height:height(7),width:width(27),margin:3}}>
                {
                  this.state.chat === true?
                    <Image source={require('../img/chat-active.jpg')} style={{height:height(7),width:width(27),borderRadius:3}} />
                    :
                    <Image source={require('../img/chat.png')} style={{height:height(6.5),width:width(27),resizeMode:'contain'}} />
                }
              </View>
              <TouchableOpacity style={{height:height(7),width:width(27),margin:3}} onPress={()=>{this.buttons('join')}}>
                {
                  this.state.join === true ?
                    <Image source={require('../img/join-active.jpg')} style={{height:height(6.5),width:width(27),borderRadius:3}} />
                    :
                    <Image source={require('../img/join.png')} style={{height:height(6.5),width:width(27),resizeMode:'contain'}} />
                }
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View style={{height:height(31),width:width(92),margin:15,backgroundColor:'#fdfdfd'}}>
          <View style={{height:height(5),justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:totalSize(1.7),color:'black'}}>Cricket</Text>
          </View>
          <View style={{height:height(16),flexDirection:'row'}}>
            <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#5ae758',borderWidth:2}}>
                <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.attendingPlayers}</Text>
              </View>
              <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>In</Text>
            </View>
            <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#33336e',borderWidth:2}}>
                <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.pendingPlayers}</Text>
              </View>
              <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>Pending</Text>
            </View>
            <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#099792',borderWidth:2}}>
                <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.rejectedPlayers}</Text>
              </View>
              <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>Out</Text>
            </View>
            <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
              <View style={{height:height(11),width:width(17),borderRadius:100}}>
                <Image source={require('../img/requests.jpg')} style={{height:height(10.5),width:width(17),resizeMode:'contain'}} />
              </View>
              <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:0}}>Requests</Text>
            </View>
            <View style={{height:height(15),width:width(8),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/arrow-right.jpg')} style={{height:height(3),width:width(3),marginBottom:25}} />
            </View>
          </View>
          <View style={{height:height(10)}}>
            <View style={{height:height(7),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
              <Image source={require('../img/team2.jpg')} style={{height:height(4),width:width(13),margin:3,resizeMode:'contain'}} />
              <Image source={require('../img/vs.jpg')} style={{height:height(6.7),width:width(11),margin:3,resizeMode:'contain'}} />
              <Image source={require('../img/team1.jpg')} style={{height:height(4),width:width(13),margin:3,resizeMode:'contain'}} />
            </View>
            <View style={{height:height(3),justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:totalSize(1.5),color:'gray'}}>Pick Team</Text>
            </View>
          </View>
        </View>
        <View style={{height:height(30),width:width(92),margin:15,marginTop:0,backgroundColor:'#ffffff',alignItems:'center'}}>
          <Text style={{fontSize:totalSize(1.7),color:'#33327e'}}>Location</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
