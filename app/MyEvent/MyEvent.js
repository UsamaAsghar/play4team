import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, DrawerLayoutAndroid, Image, ImageBackground, ScrollView, TouchableOpacity, ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Icon from 'react-native-vector-icons/Ionicons'
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(70), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>My Event</Text>
        </View>
      </View>
    );
  }
}
export default class MyEvent extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      managingEvents: [],
      attendingEvents: [],
      pendingEvents: [],
      completedEvents: [],
      address: null,
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white'
  }
  componentWillMount() {
    let { orderStore } = Store;
    this.setState({ loading: true })
    var url = 'http://play4team.com/AdminApi/api/playerEvents?teamId=' + orderStore.playerLoginRes.id;
    console.log('url====>>>', url);

    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then(async (responseJson) => {
        console.log('events are===>>>', responseJson);
        orderStore.events = responseJson;
        orderStore.managingEvent = responseJson.managingEvents;
        this.setState({ loading: false })
        if (responseJson.status === 'True') {
          await this.setState({
            managingEvents: responseJson.managingEvents,
            // attendingEvents: responseJson.managingEvents,
            // pendingEvents: responseJson.pendingEvents,
            completedEvents: responseJson.completeEventsInvited,
          })
          // console.log('responseJson data=',responseJson);
          // console.log('managingEvent is =',orderStore.managingEvent);
        } else {
          this.setState({ loading: false })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    console.log('events', orderStore.events.managingEvents);

    if (this.state.loading === true) {
      return (
        <View style={{ flex: 1, height: height(100), width: width(100), backgroundColor: 'rgba(29,37,86,0.6)' }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
         <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                // this.props.navigation.navigate('Setting')
                this.backPressed()
              }}
            >
              <Image source={require('../img/logoutNew.png')} style={{height:height(4),width:width(10),resizeMode:'contain'}} />
            </TouchableOpacity>
            <View style={{width:width(68),justifyContent:'center',alignItems:'center',flexDirection:'row',marginRight:10}}>
              <Image source={require('../img/logo.png')} style={{height:height(8),width:width(14),resizeMode:'contain'}} />
              <Text style={{fontSize: totalSize(2.5),color:'white',paddingRight:5}}>My Events</Text>
            </View>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                if (orderStore.playerLoginRes.Status ===  0) {
                    this.props.navigation.navigate('Profile')
                }else {
                    this.props.navigation.navigate('Profile')
                }
              }}
             >
              <Image source={require('../img/userProfile.png')} style={{height:height(2.5),width:width(5),resizeMode:'contain'}} />
            </TouchableOpacity>
          </View>
        {
          orderStore.playerLoginRes.Status === 1 ?
            <View style={styles.container}>
              <View style={styles.subContainer}>
                <TouchableOpacity style={styles.strip}
                  onPress={() => {
                    if (orderStore.events.managingEvents.length > 0) {
                      this.props.navigation.navigate('EventList')
                      orderStore.eventStatus = 'Managing';
                    } else {
                      ToastAndroid.show('You have not any manainging event', ToastAndroid.LONG);
                    }
                  }}>
                  <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(6), width: width(10.5), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
                      <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.managingEvents.length}</Text>
                    </View>
                  </View>
                  <View style={{ width: width(70), justifyContent: 'center' }}>
                    <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Managing</Text>
                  </View>
                  <View style={{ width: width(5), justifyContent: 'center' }}>
                    <Image source={require('../img/ic_arrow_rit.png')} />
                  </View>
                </TouchableOpacity>
                {/* <TouchableOpacity style={styles.strip}
                  onPress={() => {
                    if (orderStore.events.managingEvents.length > 0) {
                      this.props.navigation.navigate('EventList')
                      orderStore.eventStatus = 'Attending';
                    } else {
                      ToastAndroid.show('You have not any attending event now', ToastAndroid.LONG);
                    }
                  }}
                >
                  <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
                      <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{orderStore.events.managingEvents.length}</Text>
                    </View>
                  </View>
                  <View style={{ width: width(70), justifyContent: 'center' }}>
                    <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Attending</Text>
                  </View>
                  <View style={{ width: width(5), justifyContent: 'center' }}>
                    <Image source={require('../img/ic_arrow_rit.png')} />
                  </View>
                </TouchableOpacity> 
               <TouchableOpacity style={styles.strip}
                  onPress={() => {
                    if (orderStore.events.pendingEvents.length > 0) {
                      this.props.navigation.navigate('EventList')
                      orderStore.eventStatus = 'Pending';
                    } else {
                      ToastAndroid.show('You have not any pending event', ToastAndroid.LONG);
                    }
                  }}
                >
                  <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
                      <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.pendingEvents.length}</Text>
                    </View>
                  </View>
                  <View style={{ width: width(70), justifyContent: 'center' }}>
                    <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Pending</Text>
                  </View>
                  <View style={{ width: width(5), justifyContent: 'center' }}>
                    <Image source={require('../img/ic_arrow_rit.png')} />
                  </View>
                </TouchableOpacity> */}
                <TouchableOpacity style={styles.strip}
                  onPress={() => {
                    if (orderStore.events.completeEventsInvited.length > 0) {
                      this.props.navigation.navigate('EventList')
                      orderStore.eventStatus = 'Completed';
                    } else {
                      ToastAndroid.show('You have not any completed event', ToastAndroid.LONG);
                    }
                  }}
                >
                  <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: height(6), width: width(10.5), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
                      <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.completedEvents.length}</Text>
                    </View>
                  </View>
                  <View style={{ width: width(70), justifyContent: 'center' }}>
                    <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Completed</Text>
                  </View>
                  <View style={{ width: width(5), justifyContent: 'center' }}>
                    <Image source={require('../img/ic_arrow_rit.png')} />
                  </View>
                </TouchableOpacity>
              </View>
              {
                orderStore.events.managingEvents.length > 0 ?
                  <View style={{ flex: 1 }}>
                    <View style={{ height: height(5), backgroundColor: '#1d1d4f', justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ fontSize: totalSize(2), color: 'white' }}>My Events</Text>
                    </View>
                    <ScrollView>
                      {
                        orderStore.events.managingEvents.map((item, key) => {
                          return (
                            <TouchableOpacity key={key} style={{ height: height(13), flexDirection: 'row', marginBottom: 0.3, borderBottomWidth: 0.5, borderColor: 'black' }}
                              onPress={() => {
                                this.props.navigation.navigate('EventList')
                                orderStore.eventStatus = 'Managing';
                              }}
                            >
                              <View style={{ width: width(25), justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: 'http://play4team.com/AdminApi/public/sports_images/' + item.sport_image }} style={{ height: height(7), width: width(12) }} />
                                <Text style={{ fontSize: totalSize(1.5), color: 'gray', marginTop: 3 }}>{item.sport_name}</Text>
                              </View>
                              <View style={{ width: width(70), justifyContent: 'center' }}>
                                <Text style={{ fontSize: totalSize(2.5), height: height(4), color: 'black' }}>{item.Event_Name}</Text>
                                <Text style={{ fontSize: totalSize(1.7), height: height(3), color: 'gray' }}>From {item.Start_Date} to {item.End_Date}</Text>
                                <View style={{ height: height(5), flexDirection: 'row' }}>
                                  <Image source={require('../img/Addresslocation.png')} style={{ height: height(2), width: width(2.5), marginRight: 3 }} />
                                  <Text style={{ fontSize: totalSize(1.6), height: height(5), textAlign: 'center', color: 'gray' }}>{item.Address}</Text>
                                </View>
                              </View>
                              <View style={{ width: width(5), justifyContent: 'center' }}>
                                <Image source={require('../img/ic_arrow_rit.png')} />
                              </View>
                            </TouchableOpacity>
                          );
                        })
                      }
                    </ScrollView>
                  </View>
                  :
                  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {/*<View style={{height:height(10),width:width(95),marginBottom:5,alignItems:'flex-end',justifyContent:'center',flexDirection:'row'}}>
          <Image source={require('../img/refresh.png')} style={{height:height(3),width:width(5),margin:5}}/>
          <Text style={{fontSize: totalSize(1.8),color:'black',margin:5}}>Refresh Events</Text>
        </View>*/}
                    <View style={{ height: height(26), width: width(90), backgroundColor: '#1d1d4f', borderRadius: 5 }}>
                      <View style={{ height: height(5), justifyContent: 'flex-end', alignItems: 'center' }}>
                        <Text style={{ fontSize: totalSize(1.7), color: 'white' }}>No Events?</Text>
                      </View>
                      <View style={{ height: height(10), margin: 30, marginTop: 0, marginBottom: 0, flexWrap: 'wrap', justifyContent: 'center' }}>
                        <Text style={{ fontSize: totalSize(1.5), color: 'white', textAlign: 'center' }}>No problem,simply search your local area for events or why not create your own?</Text>
                      </View>
                      <View style={{ height: height(10), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity style={{ height: height(7), width: width(36), margin: 7, justifyContent: 'center', alignItems: 'center' }}
                          onPress={() => {
                            orderStore.search = 'Events';
                            this.props.navigation.navigate('FindPlayer')
                          }}
                        >
                          <Image source={require('../img/findevent.png')} style={{ height: height(7), width: width(36) }} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ height: height(7), width: width(36), margin: 7, justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, borderColor: 'white' }}
                          onPress={() => {
                            this.props.navigation.navigate('CreateEvent')
                          }}
                        >
                          <Text style={{ fontSize: totalSize(2), color: 'white' }}>Create Event </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
              }
            </View>

            :
            <View style={{ height: height(90), width: width(100), alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="ios-calendar" size={totalSize(10)} />
              <Text style={{ fontSize: totalSize(2), marginHorizontal: 30 }}>You are unable to create an event at this moment in time. To enable this option, kindly create a player profile.</Text>
            </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  subContainer: {
    flex: 1,
    margin: 20,
    marginBottom: 0,
  },
  strip: {
    height: height(8),
    backgroundColor: '#ffffff',
    marginBottom: 5,
    flexDirection: 'row',
    borderRadius: 5
  },
});

// {
//   orderStore.playerLoginRes.Status === 1 ?
//       <View style={styles.container}>
//         <View style={styles.subContainer}>
//           <TouchableOpacity style={styles.strip}
//             onPress={() => {
//               if (orderStore.events.managingEvents.length > 0) {
//                 this.props.navigation.navigate('EventList')
//                 orderStore.eventStatus = 'Managing';
//               } else {
//                 ToastAndroid.show('You have not any manainging event', ToastAndroid.LONG);
//               }
//             }}>
//             <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
//                 <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.managingEvents.length}</Text>
//               </View>
//             </View>
//             <View style={{ width: width(70), justifyContent: 'center' }}>
//               <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Managing</Text>
//             </View>
//             <View style={{ width: width(5), justifyContent: 'center' }}>
//               <Image source={require('../img/ic_arrow_rit.png')} />
//             </View>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.strip}
//             onPress={() => {
//               if (orderStore.events.managingEvents.length > 0) {
//                 this.props.navigation.navigate('EventList')
//                 orderStore.eventStatus = 'Attending';
//               } else {
//                 ToastAndroid.show('You have not any attending event now', ToastAndroid.LONG);
//               }
//             }}
//           >
//             <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
//                 <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{orderStore.events.managingEvents.length}</Text>
//               </View>
//             </View>
//             <View style={{ width: width(70), justifyContent: 'center' }}>
//               <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Attending</Text>
//             </View>
//             <View style={{ width: width(5), justifyContent: 'center' }}>
//               <Image source={require('../img/ic_arrow_rit.png')} />
//             </View>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.strip}
//             onPress={() => {
//               if (orderStore.events.pendingEvents.length > 0) {
//                 this.props.navigation.navigate('EventList')
//                 orderStore.eventStatus = 'Pending';
//               } else {
//                 ToastAndroid.show('You have not any pending event', ToastAndroid.LONG);
//               }
//             }}
//           >
//             <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
//                 <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.pendingEvents.length}</Text>
//               </View>
//             </View>
//             <View style={{ width: width(70), justifyContent: 'center' }}>
//               <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Pending</Text>
//             </View>
//             <View style={{ width: width(5), justifyContent: 'center' }}>
//               <Image source={require('../img/ic_arrow_rit.png')} />
//             </View>
//           </TouchableOpacity>
//           <TouchableOpacity style={styles.strip}
//             onPress={() => {
//               if (orderStore.events.completeEvents.length > 0) {
//                 this.props.navigation.navigate('EventList')
//                 orderStore.eventStatus = 'Completed';
//               } else {
//                 ToastAndroid.show('You have not any completed event', ToastAndroid.LONG);
//               }
//             }}
//           >
//             <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ height: height(6), width: width(10), justifyContent: 'center', alignItems: 'center', backgroundColor: '#2bd89c', borderRadius: 50, borderBottomWidth: 2, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: 'gray' }}>
//                 <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{this.state.completedEvents.length}</Text>
//               </View>
//             </View>
//             <View style={{ width: width(70), justifyContent: 'center' }}>
//               <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Completed</Text>
//             </View>
//             <View style={{ width: width(5), justifyContent: 'center' }}>
//               <Image source={require('../img/ic_arrow_rit.png')} />
//             </View>
//           </TouchableOpacity>
//         </View>
//         {
//           orderStore.events.managingEvents.length > 0 ?
//             <View style={{ flex: 1 }}>
//               <View style={{ height: height(5), backgroundColor: '#1d1d4f', justifyContent: 'center', alignItems: 'center' }}>
//                 <Text style={{ fontSize: totalSize(2), color: 'white' }}>My Events</Text>
//               </View>
//               <ScrollView>
//                 {
//                   orderStore.events.managingEvents.map((item, key) => {
//                     return (
//                       <TouchableOpacity key={key} style={{ height: height(13), flexDirection: 'row', marginBottom: 0.3, borderBottomWidth: 0.5, borderColor: 'black' }}
//                         onPress={() => {
//                           this.props.navigation.navigate('EventList')
//                           orderStore.eventStatus = 'Managing';
//                         }}
//                       >
//                         <View style={{ width: width(25), justifyContent: 'center', alignItems: 'center' }}>
//                           <Image source={{ uri: 'http://play4team.com/AdminApi/public/sports_images/' + item.sport_image }} style={{ height: height(7), width: width(12) }} />
//                           <Text style={{ fontSize: totalSize(1.5), color: 'gray', marginTop: 3 }}>{item.sport_name}</Text>
//                         </View>
//                         <View style={{ width: width(70), justifyContent: 'center' }}>
//                           <Text style={{ fontSize: totalSize(2.5), height: height(4), color: 'black' }}>{item.Event_Name}</Text>
//                           <Text style={{ fontSize: totalSize(1.7), height: height(3), color: 'gray' }}>From {item.Start_Date} to {item.End_Date}</Text>
//                           <View style={{ height: height(5), flexDirection: 'row' }}>
//                             <Image source={require('../img/Addresslocation.png')} style={{ height: height(2), width: width(2.5), marginRight: 3 }} />
//                             <Text style={{ fontSize: totalSize(1.6), height: height(5), textAlign: 'center', color: 'gray' }}>{item.Address}</Text>
//                           </View>
//                         </View>
//                         <View style={{ width: width(5), justifyContent: 'center' }}>
//                           <Image source={require('../img/ic_arrow_rit.png')} />
//                         </View>
//                       </TouchableOpacity>
//                     );
//                   })
//                 }
//               </ScrollView>
//             </View>
//             :
//             <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//               {/*<View style={{height:height(10),width:width(95),marginBottom:5,alignItems:'flex-end',justifyContent:'center',flexDirection:'row'}}>
//           <Image source={require('../img/refresh.png')} style={{height:height(3),width:width(5),margin:5}}/>
//           <Text style={{fontSize: totalSize(1.8),color:'black',margin:5}}>Refresh Events</Text>
//         </View>*/}
//               <View style={{ height: height(26), width: width(90), backgroundColor: '#1d1d4f', borderRadius: 5 }}>
//                 <View style={{ height: height(5), justifyContent: 'flex-end', alignItems: 'center' }}>
//                   <Text style={{ fontSize: totalSize(1.7), color: 'white' }}>No Events?</Text>
//                 </View>
//                 <View style={{ height: height(10), margin: 30, marginTop: 0, marginBottom: 0, flexWrap: 'wrap', justifyContent: 'center' }}>
//                   <Text style={{ fontSize: totalSize(1.5), color: 'white', textAlign: 'center' }}>No problem,simply search your local area for events or why not create your own?</Text>
//                 </View>
//                 <View style={{ height: height(10), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
//                   <TouchableOpacity style={{ height: height(7), width: width(36), margin: 7, justifyContent: 'center', alignItems: 'center' }}
//                     onPress={() => {
//                       orderStore.search = 'Events';
//                       this.props.navigation.navigate('FindPlayer')
//                     }}
//                   >
//                     <Image source={require('../img/findevent.png')} style={{ height: height(7), width: width(36) }} />
//                   </TouchableOpacity>
//                   <TouchableOpacity style={{ height: height(7), width: width(36), margin: 7, justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, borderColor: 'white' }}
//                     onPress={() => {
//                       this.props.navigation.navigate('CreateEvent')
//                     }}
//                   >
//                     <Text style={{ fontSize: totalSize(2), color: 'white' }}>Create Event </Text>
//                   </TouchableOpacity>
//                 </View>
//               </View>
//             </View>
//         }
//     </View>

//     :
//     <View style={{ height: height(90), width: width(100), alignItems: 'center', justifyContent: 'center' }}>
//       <Icon name="ios-calendar" size={totalSize(10)} />
//       <Text style={{ fontSize: totalSize(2) ,marginHorizontal: 30}}>You are unable to create an event at this moment in time. To enable this option, kindly create a team profile.</Text>
//     </View>
// }