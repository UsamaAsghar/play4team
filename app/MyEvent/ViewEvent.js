import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput,KeyboardAvoidingView,Picker
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import Footer from '../Footer/Footer'
import { observer } from 'mobx-react';
import Store from '../Stores';
import DateTimePicker from 'react-native-datepicker'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import PopupDialog, {slideAnimation,DialogTitle,FadeAnimation} from 'react-native-popup-dialog';
class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Review Event</Text>
          </View>
        </View>
    );
  }
}
export default class ViewEvent extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      nameDilog: false,
      sportDilog: false,
      name: '',
      sport: {},
      sport_id: '',
      date: '',
    }
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }
  componentWillMount(){
    let { orderStore } = Store;
    let {params} = this.props.navigation.state;
    orderStore.editLocation.latitude = params.eventDetail.latitude;
    orderStore.editLocation.longitude = params.eventDetail.longitude;
    orderStore.editLocation.address = params.eventDetail.Address;
    this.setState({
      name: params.eventDetail.Event_Name,
      sport_id: params.eventDetail.sport_id,
      date: params.eventDetail.End_Date
    })
  }
  render() {
    var status;
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    // console.warn('event last=',params.eventDetail);
    // console.warn('object=',this.state.sport);
    var name = params.eventDetail.sport_name;
    var img = 'http://play4team.com/AdminApi/public/img/'+params.eventDetail.sport_image;
    if (params.eventDetail.Weakly === '1') {
        status = 'Weakly';
    }else {
      if (params.eventDetail.repeat_status !== null) {
         status = params.eventDetail.repeat_status;
      }else {
         status = 'not selected';
      }
    }
    if(this.state.loading == true){
        return(
          <OrientationLoadingOverlay
              visible={true}
              color="white"
              indicatorSize="small"
              messageFontSize={18}
              message="Loading..."
          />
        );
    }
    return(
      <View style={styles.container}>
          <View style={{height:height(28),width:width(100)}}>
            <Image source={require('../img/review_bck.png')} style={{height:height(28),width:width(100)}}/>
          </View>
          <View style={styles.subContainer}>
              <View style={styles.stripTop} >
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10),justifyContent:'center'}}>
                    <Image source={require('../img/xyz.png')} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(75),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>{this.state.name}</Text>
                </View>
              </View>
              <View style={styles.strip}>
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10)}}>
                    <Image source={{uri: img}} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(66),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>{name}</Text>
                </View>
              </View>
              <View style={styles.strip}>
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10)}}>
                    <Image source={require('../img/calander-btn.png')} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(66),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>{this.state.date}</Text>
                </View>
              </View>
              <View style={styles.strip}>
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10)}}>
                    <Image source={require('../img/cricket-ground.png')} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(66),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.5),color:'black',textAlign:'left'}}>{orderStore.editLocation.address}</Text>
                </View>
              </View>
              <View style={styles.strip}>
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10)}}>
                    <Image source={require('../img/publice-event.png')} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(66),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>{status}</Text>
                </View>
              </View>
              <View style={{height:height(9)}}>
              </View>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    justifyContent:'center',
    alignItems:'center'
  },
  subContainer: {
    flex:1,
    margin:20,
    marginTop:10,
    // position: 'absolute',
    // zIndex:2
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    borderBottomWidth:0.4,
    marginBottom:0.2,
    borderColor:'gray',
    flexDirection:'row',
  },
  stripLast: {
    height:height(8),
    backgroundColor:'#ffffff',
    flexDirection:'row',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  stripTop: {
    height:height(8),
    backgroundColor:'#ffffff',
    flexDirection:'row',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomWidth:0.4,
    marginBottom:0.3,
    borderColor:'gray',
  },

});
