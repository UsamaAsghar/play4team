
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,AsyncStorage, Alert
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import { NavigationActions, StackActions } from 'react-navigation'
import Store from '../Stores';
import firebase from 'react-native-firebase'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer export default class Login extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      email: 'ghayoor@gmail.com',
      password:'123',
     };
  }

  navigationonNotification = () =>{
    const {orderStore} = Store;
    firebase
      .notifications()
      .getInitialNotification()
      .then(notif => {
            if(notif && notif.notification && notif.notification.data){
              if(notif.notification.data.screen === ''){
                this.props.navigation.navigate('Messages');
              }else if(notif.notification.data.screen === 'chat'){
                this.props.navigation.navigate('Message', {
                  isFromNotifcation: true,
                  chatId: notif.notification.data.chat_id,
                  receiverId: notif.notification.data.user_id, 
                });
              }
              orderStore.newMessage = true;
            } else {
              this.props.navigation.navigate('MainTab');
            }
        })
  }

  componentDidMount () {
    let {orderStore} = Store;
    firebase.messaging().getToken().then(token => {
      orderStore.FireBaseNotif = token;
      console.log(token)
    });
    this.notificationListener = firebase.notifications().onNotification(notification => {
      //firebase.notifications().displayNotification(notification);
      console.log('notification payload=====-----------------:', notification);
      this.alert(notification)
      orderStore.newMessage = true;
    })
  }
  alert(notif){
    Alert.alert(
      notif.title,
      notif.body,
      [
        {text: 'OK', onPress: () =>{
          if (notif.data.screen && notif.data.screen === "chat") {
            this.props.navigation.navigate('Message', {
              isFromNotifcation: true,
              chatId: notif.data.chat_id,
              receiverId: notif.data.user_id, 
            });
          } else {
            this.props.navigation.navigate('Messages');
          }
        }},
      ],
      {cancelable: false},
    );
  }
  login = async()=>{
    let { params } = this.props.navigation.state;
    let {orderStore} = Store;
    if (this.state.email.length === 0 ) {
       ToastAndroid.show('Please enter your valid email address', ToastAndroid.SHORT);
    }else {
      if (this.state.password.length === 0) {
        ToastAndroid.show('Please enter your password', ToastAndroid.SHORT);
      }else {
          this.setState({loading: true})
          fetch('http://play4team.com/AdminApi/api/Playerlogin?email='+this.state.email+'&type='+params.type+'&pass='+this.state.password+'&device_token='+orderStore.FireBaseNotif, {
           method: 'POST',
             headers: {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
              }
            }).then((response) => response.json())
                  .then(func=async(responseJson) => {
                      orderStore.playerLoginRes = responseJson.data;
                      console.log('responseLogin',responseJson);
                      this.setState({loading: false})
                      if (responseJson.status === 'True') {
                          await this.userChat(responseJson.data.id)
                          this.navigationonNotification();
                      }else {
                        this.setState({loading: false})
                        ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                      }
                  }).catch((error)=>{
                      this.setState({loading: false})
                      console.log('erro=============>>>>>>>>>>>>>',error)
                      ToastAndroid.show('Please try again', ToastAndroid.LONG);
                  });
        }
      }
    }
    userChat=(id)=>{
        let { params } = this.props.navigation.state;
        let { orderStore } = Store;
       try {
        if (orderStore.playerLoginRes.Status===0) {
          var type='player';
       }else {
          var type = 'team';
       }
       var url = 'http://play4team.com/AdminApi/api/usersChat?userId='+orderStore.playerLoginRes.id+'&type='+type;
       // this.setState({loading: true})
       fetch(url , {
            method: 'POST',
            }).then((response) => response.json())
              .then((responseJson) => {
                orderStore.chatList = responseJson.data;
               this.setState({loading: false})
              //  console.log('get chatlist Data=', responseJson.data );
               if (responseJson.status === 'True') {
                 this.setState({
                   messages: responseJson.data
                 })
                   // ToastAndroid.show('Done', ToastAndroid.LONG);
                 }
            }).catch((error)=>{
                 this.setState({loading: false})
                 ToastAndroid.show('userChat api issue', ToastAndroid.LONG);
           });
       }catch(err){
          console.log('err:',err);

       }
    }
  componentWillMount=async()=>{
      let { orderStore } = Store;
       if (orderStore.RES.register) {
          await this.setState({ email: orderStore.RES.email , password: orderStore.RES.password })
          await this.login()
       } else {
          await this.getData();
       }
       await this.sport();
    }
  sport () {
    let { orderStore } = Store;
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/Sport', {
         method: 'GET',
          }).then((response) => response.json())
                .then((responseJson) => {
                  // console.log('ResponseSport=',responseJson);
                    orderStore.sportEvent = responseJson.data;
                    // console.log('sportAPi=',responseJson.data);
                    this.setState({loading: false})
                  if (responseJson.status === 'True') {
                        for (var i = 0; i < orderStore.sportEvent.length; i++) {
                            orderStore.sportEvent[i].added = false;
                            orderStore.sportEvent[i].checkStatus = false;
                        }
                        // console.log('Response',orderStore.sportEvent);
                  }else {
                    this.setState({loading: false})
                  }
                }).catch((error)=>{
                    this.setState({loading: false})
                    ToastAndroid.show('error', ToastAndroid.SHORT);
                });
    }
  static navigationOptions = {
     header: null
    }
    getData = async() =>{
      let mail =  await  AsyncStorage.getItem('async_email');
      let pass = await AsyncStorage.getItem('async_password');

          if (mail !== null && pass !== null) {
              // console.warn('in if ');
              this.setState({
                email: mail,
                password : pass,
              })
                this.login();
          }
          else {
            this.setState({
              email: '',
              password: ''
            })
          }
    }
    setEmail (value) {
      AsyncStorage.setItem('async_email', value);
      this.setState({email: value})
   }
   setPassword (value) {
      AsyncStorage.setItem('async_password', value);
       this.setState({password: value})
    }
  render() {
      if(this.state.loading === true){
          return(
              <ImageBackground style={styles.preloader} source={require('../img/splash.jpg')} >
                  <OrientationLoadingOverlay
                      visible={true}
                      color="white"
                      indicatorSize="small"
                      messageFontSize={18}
                      message="Loading..."
                  />
              </ImageBackground>
          );
    }
    return (
      <View style={styles.container}>
        <ImageBackground source={require('../img/bg_img.jpg')} style={styles.backgroungImage}>
         <ScrollView>
            <View style={styles.subCon}>
              <View style={styles.title}>
                <Text style={styles.titleText}>SIGN IN</Text>
              </View>
              <View style={[styles.searchCon,{ backgroundColor:'transparent' }]}>
                {/* <TextInput
                    // onChangeText={(value) => {}}
                    underlineColorAndroid='transparent'
                    placeholder='SEARCH FOR YOUR TEAM'
                    placeholderTextColor='gray'
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    keyboardAppearance='dark'
                    style={styles.search}
                    /> */}
              </View>
              <View style={styles.logo}>
                <Image source={require('../img/logo.png')} style={styles.logoImg} />
              </View>
              <View style={styles.email}>
                <View style={styles.emailSub}>
                  <Image source={require('../img/mail_icon.png')} style={styles.emailIcon}/>
                  <Text style={styles.text}>Email</Text>
                </View>
                <View style={styles.textInputCon}>
                  <TextInput
                      onChangeText={(value) => this.setEmail(value)}
                      underlineColorAndroid='transparent'
                      placeholder='Enter Email'
                      value={this.state.email.length>0? this.state.email:""}
                      autoFocus={false}
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={styles.textInput}
                      />
                </View>
              </View>
              <View style={styles.password}>
                <View style={styles.passLogo}>
                  <Image source={require('../img/lock_icon.png')} style={styles.passIcon}/>
                  <Text style={styles.text}>Password</Text>
                </View>
                <View style={styles.textInputCon}>
                  <TextInput
                      onChangeText={(value) => this.setPassword(value)}
                      underlineColorAndroid='transparent'
                      placeholder='Enter Password'
                      value={this.state.password.length>0? this.state.password:""}
                      secureTextEntry={true}
                      placeholderTextColor='gray'
                      underlineColorAndroid='transparent'
                      autoCorrect={false}
                      style={styles.textInput}
                      />
                </View>
              </View>
              <View style={styles.empty}>
              </View>
              <TouchableOpacity style={styles.loginCon}
                onPress={()=>{
                  this.login();
                }}
                >
                <Image source={require('../img/login.png')} style={styles.loginBtn} />
              </TouchableOpacity>
              {/*  <View style={styles.otherCon}>
                <TouchableOpacity style={styles.other}>
                  <Text style={styles.otherText}>Sign in others</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.iconCon}>
                <TouchableOpacity style={styles.icon}>
                  <Image source={require('../img/fb_icon.png')} style={styles.fbIcon} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.icon}>
                  <Image source={require('../img/g_icon.png')} style={styles.gIcon} />
                </TouchableOpacity>
              </View>*/}
              <View style={styles.forgetCon}>
                <TouchableOpacity style={styles.forgetSubcon} onPress={()=>{this.props.navigation.navigate('ForgetPassword')}}>
                  <Text style={styles.forgetText}>Forget Password?</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroungImage: {
    height:height(100)
  },
  icon: {
    width:width(13),
    borderWidth:0.7,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center'
  },
  iconCon: {
    height:height(8),
    width:width(90),
    justifyContent:'center',
    flexDirection:'row',
    marginTop:5
  },
  searchCon: {
    height:height(6.5),
    width:width(90),
    backgroundColor:'white',
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  search: {
    height:height(6.5),
    width:width(90),
    fontSize:totalSize(1.5),
    textAlign :'center',
    marginTop:0
  },
  logo: {
    height:height(20),
    width:width(90),
    alignItems:'center'
  },
  logoImg: {
    height:height(18),
    width:width(30)
  },
  textInputCon: {
    height:height(6),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  textInput: {
    height:height(6),
    width:width(90),
    textAlign :'center',
    fontSize:totalSize(1.5),
    marginTop:0,
    backgroundColor:'white'
  },
  subCon: {
    height:height(100),
    alignItems:'center',
    backgroundColor:'rgba(29,37,86,0.8)'
  },
  preloader: {
    height: height(100),
    width: width(100),
    flex:1
  },
  email: {
    height:height(9),
    width:width(90),
    marginBottom:5
  },
  emailSub: {
    height:height(4),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  emailIcon: {
    height:height(2.5),
    width:width(4.5),
    margin:3,
    resizeMode:'contain'
  },
  text: {
    fontSize:totalSize(1.7),
    color:'white',
    margin:3
  },
  otherText: {
    fontSize:totalSize(1.5),
    color:'white',
    margin:3
  },
  password: {
    height:height(10),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  passLogo: {
    height:height(4),
    width:width(90),
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
  },
  passIcon: {
    height:height(2.5),
    width:width(5),
    margin:3,
    resizeMode:'contain'
  },
  empty: {
    height:height(5),
    width:width(90),
  },
  loginCon: {
    justifyContent:'center',
    alignItems:'center',
  },
  fbIcon: {
    height:height(3),
    width:width(2)
  },
  gIcon: {
    height:height(3),
    width:width(5)
  },
  loginBtn: {
    height:height(8),
    width:width(90),
    borderRadius: 5,
    resizeMode: 'contain'
  },
  otherCon: {
    height:height(6),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  other: {
    height:height(3),
    width:width(22),
    justifyContent:'center',
    alignItems:'center',
    borderBottomWidth:0.5,
    borderBottomColor:'white'
  },
  forgetCon: {
    height:height(10),
    width:width(90),
    justifyContent:'flex-end',
    alignItems:'center',
  },
  forgetSubcon: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    // borderBottomWidth:0.5,
    // borderBottomColor:'white'
  },
  forgetText: {
    marginVertical: 15,
    fontSize:totalSize(1.7),
    color:'white',
    textDecorationLine:'underline'
  },
  title: {
    height:height(7),
    width:width(90),
    justifyContent:'center',
    alignItems:'center'
  },
  titleText: {
    fontSize:totalSize(2),
    color:'white'
  },
});
