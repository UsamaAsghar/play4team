import React, { Component } from 'react';
import {
    // AppRegistry,
    StyleSheet,
    Image,
     View,
     Text,
     ScrollView,
    TouchableOpacity,
    Alert
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
export default class Footer extends Component {
  constructor(props){
    super(props);
    this.state={

    }
  }
  componentWillMount(){

  }
  alert(){
    Alert.alert(
      'Alert',
      'Dear user you have not any event history, for creating events please login as a team',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      { cancelable: false }
    )
  }
  static navigationOptions = {
        header: null
    }
    render() {
      let { orderStore } = Store;
        return (
          <View style={{height:height(10),flexDirection:'row'}}>
              <View style={{flex:1,flexDirection:'row',margin:2,backgroundColor:'white',alignItems:'center'}}>
                <TouchableOpacity style={{height:height(10),width:width(25),justifyContent:'center',alignItems:'center'}}
                  onPress={()=>{
                    this.props.nav.navigate('Home');
                  }}>
                    <Image source={require('../img/home-btn.png')}  style={{height:height(3.5),width:width(6.5),resizeMode:'contain'}}/>
                    <Text style={{fontSize:totalSize(1.5),color:'black'}}> Home </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(25),margin:0,justifyContent:'center',alignItems:'center'}}
                  onPress={()=>{
                    this.props.nav.navigate('FriendsList');
                  }}>
                    <Image source={require('../img/friends.png')}  style={{height:height(3.8),width:width(6.5),resizeMode:'contain'}} />
                    <Text style={{fontSize:totalSize(1.5),color:'black'}}> {orderStore.playerLoginRes.Status===0? "Friends":"Favourite"} </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(25),margin:0,justifyContent:'center',alignItems:'center'}}
                    onPress={()=>{
                      this.props.nav.navigate('Messaging');
                    }}>
                    <Image source={require('../img/messag.png')}  style={{height:height(3.5),width:width(6.5),resizeMode:'contain'}} />
                    <Text style={{fontSize:totalSize(1.5),color:'black'}}> Messages </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:width(25),margin:0,justifyContent:'center',alignItems:'center'}}
                    onPress={()=>{
                      if (orderStore.playerLoginRes.Status === 1) {
                          this.props.nav.navigate('MyEvent');
                      }else {
                          this.alert()
                      }
                    }}>
                    <Image source={require('../img/events.png')}  style={{height:height(3.5),width:width(6.5),resizeMode:'contain'}} />
                    <Text style={{fontSize:totalSize(1.5),color:'black'}}>My Events</Text>
                </TouchableOpacity>
              </View>
          </View>
        );
    }
}

const styles = StyleSheet.create({
  Container:{
    flex:1,
    margin:20,

  },
  Background:{
    flex:1,
    backgroundColor:'#402217',
  },
  logo:{
    flex:1,
    margin:2,
    justifyContent:'center',
    alignItems:'center'
  }

});
