import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { createBottomTabNavigator, createAppContainer, } from 'react-navigation';
import Store from '../Stores';
import Home from '../Home/Home';
import FriendsList from '../FriendsList/FriendsList';
import Messaging from '../Massenger/Messaging';
import MyEvent from '../MyEvent/MyEvent';
import Icon from 'react-native-vector-icons/Ionicons'


const {orderStore} = Store;
const tabNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarLabel: '',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="ios-home" color={tintColor} size={totalSize(3)} />
            )
        }
    },
    Friends: {
        screen: FriendsList,
        navigationOptions: {
            tabBarLabel: "Friends" ,
            tabBarIcon: ({ tintColor }) => (
                <Icon name="ios-people" color={tintColor} size={totalSize(3)} />
            )
        }
    },
    Messages: {
        screen: Messaging,
        navigationOptions: {
            tabBarLabel: '',
            tabBarIcon: ({ tintColor }) => (
                <View>
                    {
                        orderStore.newMessage && 
                        <View style={{height: 10, width: 10, backgroundColor: 'red', position: 'absolute', top: 0, right: -5, borderRadius:50, zIndex:10}} />
                    }
                    
                    <Icon name="ios-chatboxes" color={tintColor} size={totalSize(3)} />
                </View>
            )
        }
    },
    MyEvents: {
        screen: MyEvent,
        navigationOptions: {
            tabBarLabel: 'My Events',
            tabBarIcon: ({ tintColor }) => (
                <Icon name="md-calendar" color={tintColor} size={totalSize(3)} />
            )
        }
    },
}, {
        tabBarOptions: {
            activeTintColor: '#52e63a',
            //inactiveTintColor: 'white',
            activeBackgroundColor: '#1d1d4f',
            inactiveBackgroundColor: '#1d1d4f'
        },

    }
);

export default createAppContainer(tabNavigator);