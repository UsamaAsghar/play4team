
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,KeyboardAvoidingView
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Swipeout from 'react-native-swipeout';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import TeamMember from './TeamMember';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{height:height(9.5),width:width(75),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
        <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
          <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
          <Text style={{fontSize:totalSize(2.5),color:'white'}}> Player Members </Text>
        </View>
      </View>
    );
  }
}
export default class TeamMembers extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      members: [],
      name:'',
      searchMem: [],
    }
  }
  static navigationOptions = {
      headerTitle: <LogoTitle />,
      headerStyle: {
        backgroundColor:'#1d1d4f'
      },
      headerTintColor:'white',
    }
  removeMember(state,player){
      var {params} = this.props.navigation.state;
      let{orderStore} = Store;
      if (this.state.searchMem.length === 0) {
          if (state === false) {
            var index = 0;
            for(var i=0; i< orderStore.teamMembers.length; i++){
                if(orderStore.teamMembers[i].player_id === player.player_id ){
                    index = i;
                    break;
                }
            }
            orderStore.teamMembers.splice(index, 1);
            player.added = false;
            this.removePlayerApi(player.player_id);
            this.setState({
              id: player.player_id
            })
            // console.log('members remove=',orderStore.teamMembers);
          }
      }
      else {
          if (state === false) {
          var index = 0;
          for(var i=0; i< this.state.searchMem.length; i++){
              if(this.state.searchMem[i].player_id === player.player_id ){
                  index = i;
                  break;
              }
          }
          this.state.searchMem.splice(index, 1);
          orderStore.teamMembers.splice(index, 1);
          player.added = false;
          // player.added = false;
          this.removePlayerApi(player.player_id);
          this.setState({
            id: player.player_id
          })
          // console.log('members remove=',this.state.searchMem);
        }
        }
    }
  removePlayerApi(playerId){
    let { orderStore } = Store;
    // this.setState({loading: true})
    fetch('http://play4team.com/AdminApi/api/RemoveMember?teamId='+orderStore.playerLoginRes.id+'&playerId='+playerId, {
       method: 'POST',
       headers: {
         'Content-Type':  'application/json',
         'Accept': 'application/json',
        }
      }).then((response) => response.json())
            .then((responseJson) => {
              // console.log('members data==',responseJson);
                this.setState({loading: false})
              if (responseJson.status === 'True') {
                   ToastAndroid.show('This member has been removed from your team', ToastAndroid.LONG);
              }else {
                   this.setState({loading: false})
                   ToastAndroid.show('error', ToastAndroid.LONG);
              }
            }).catch((error)=>{
                   this.setState({loading: false})
                   ToastAndroid.show('Try again', ToastAndroid.LONG);
            });
  }
  searchMember(){
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.teamMembers.length; i++) {
      if (orderStore.teamMembers[i].Name.includes(this.state.name)) {
          this.state.searchMem.push(orderStore.teamMembers[i])
      }
    }
    this.setState({members: orderStore.teamMembers})
    // console.log('searchMembers=',this.state.searchMem);
  }
  _member = (item,key) =>{
    return(
      <Swipeout
          key={key}
          right={[
            {
              text: 'Remove',
              color: 'white',
              backgroundColor: 'black',
              onPress:()=> {this.removeMember(false,item)},
              type: 'delete',
            },
          ]}
          // scroll={() => console.warn('scroll event') }
          autoClose= {true}
          buttonWidth = { totalSize(10) }
          disabled = { false }
          >
          <View style={{height:height(8),width:width(90),borderBottomWidth:0.5,borderColor:'gray',marginBottom:0.5,backgroundColor:'#ffffff',flexDirection:'row'}}>
            <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <Image source={item.Path.length>0? {uri: item.Path}:require('../img/userprofileImag.png')} style={{height:height(6),width:width(12),resizeMode:'contain',borderRadius: 90}} />
            </View>
            <View style={{width:width(70),flexDirection:'row'}}>
              <View style={{width:width(53),justifyContent:'center'}}>
                <View style={{height:height(6),justifyContent:'center',alignItems:'flex-start'}}>
                  <Text style={{fontSize:totalSize(2),color:'black'}}>{item.Name}</Text>
                  <Text style={{fontSize:totalSize(1.4),color:'black'}}>Fitness:  {item.Fitness}</Text>
                </View>
              </View>
              <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'flex-end'}}
                onPress={()=>{
                  this.props.navigation.navigate('PlayerDetailView',{item: item});
                  // this.removeMember(false,item);
                }}
              >
                <Text style={{ height:height(5),width:width(8),fontSize: 12 , color:'black' }}>View</Text>
                {/* <Image source={require('../img/user_profile.png')} style={{height:height(5),width:width(8),borderWidth:1,borderColor:'black'}} /> */}
              </TouchableOpacity>
            </View>
          </View>
      </Swipeout>
    );
  }
  _header = () => {
      return(
        <View style={{height:height(7),width:width(100),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(5.5),width:width(80),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  if (this.state.name.length === 0) {
                      ToastAndroid.show('Have you enter any member name ???', ToastAndroid.LONG);
                  }else {
                      this.searchMember()
                  }
                }}
              >
              <Image source={require('../img/search_btn.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
            <View style={{height:height(7),width:width(70)}}>
              <TextInput
                onChangeText={(value) => this.setState({name: value})}
                value={this.state.name}
                placeholder={"Name"}
                placeholderTextColor='gray'
                style={{fontSize:totalSize(2),color:'black',backgroundColor:'white',width:width(65)}}
                underlineColorAndroid="transparent"
                />
            </View>
          </View>
          <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}
            onPress={()=>{ this.clearSearch() }}
          >
            <Image source={require('../img/cross-icon.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
          </TouchableOpacity>
        </View>
      );
  }
  clearSearch(){
    let { orderStore } = Store;
    this.setState({
      searchMem: [],
      name: '',
    })
  }
  render() {
    let { orderStore } = Store;
      if(this.state.loading == true){
          return(
              <View style={{flex:1,height:height(100),width:width(100),backgroundColor:'rgba(29,37,86,0.6)'}}>
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
              </View>
          );
      }
    return (
            <View  style={{flex:1}}>
              {
                this._header()
              }
              <View style={{height:height(90),marginTop:15,alignItems:'center'}}>
                <View style={{height:height(70),width:width(95),alignItems:'center'}}>
                  <ScrollView>
                    {
                      this.state.searchMem.length === 0?
                        orderStore.teamMembers.length > 0 ?
                          orderStore.teamMembers.map((item,key)=>{
                            return(
                                this._member(item,key)
                            );
                          })
                          :
                          <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:totalSize(2.5),color:'black'}}>There no member in this team</Text>
                          </View>
                        :
                        this.state.searchMem.map((item,key)=>{
                          return(
                            this._member(item,key)
                          );
                        })
                    }
                  </ScrollView>
                </View>
              </View>
            </View >
     );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },

});
