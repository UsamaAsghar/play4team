
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Footer from '../Footer/Footer';
@observer class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
            <Text style={{fontSize:totalSize(2.5),color:'white'}}> Profile </Text>
          </View>
        </View>
    );
  }
}
@observer export default class PlayerDetailView extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      avatarSource: '',
      skill: 0,
      addFriend: false,
    }
  }
  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerStyle: {
         backgroundColor:'#1d1d4f',
       },
       headerTintColor:'white',
    }
    componentWillMount=async()=>{
      let { orderStore } = Store;
      let { params } = this.props.navigation.state;
      console.warn('item=',params.item);
      this.setState({loading: true})
      // var url = 'http://play4team.com/AdminApi/api/gePlayerSports?playerId='+ params.item.id;
      // console.log('url=',url);
      await fetch('http://play4team.com/AdminApi/api/gePlayerSports?playerId='+ params.item.id || params.item.player_id, {
       method: 'POST',
        }).then((response) => response.json())
              .then(func=async(responseJson)=> {
                  // console.log('player detail data= ',responseJson);
                  orderStore.playerProfile = responseJson.data;
                if (responseJson.status === 'True') {
                  for (var i = 0; i < orderStore.playerProfile.length; i++) {
                      orderStore.playerProfile[i].checkStatus = false;
                      orderStore.playerProfile[i].added = true;
                      orderStore.playerProfile[0].checkStatus = true;
                      await this.setState({
                        loading: false,
                        skill: orderStore.playerProfile[0].rate * 10,
                      })
                    }
                    // console.log('array=======================>>>>',orderStore.playerProfile);
                     this.setState({loading: false})
                }else {
                  this.setState({loading: false})
                  // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                }
              }).catch((error)=>{
                  this.setState({loading: false})
                  // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
              });
    }
    selectSport(sport){
      let { orderStore } = Store;
      for (var i = 0; i < orderStore.playerProfile.length; i++) {
          if (sport.sport_id === orderStore.playerProfile[i].sport_id) {
              // orderStore.playerProfile[i].added = true;
              orderStore.playerProfile[i].checkStatus = true;
              orderStore.sport_id = orderStore.playerProfile[i].sport_id;
              this.setState({
                // fitness: orderStore.playerLoginRes.Fitness,
                skill: orderStore.playerProfile[i].rate * 10,
              })
          }else {
            // orderStore.sportEvent[i].added = false;
            orderStore.playerProfile[i].checkStatus = false;
          }
      }
    }
    sendFriendRequest(){
      let { params } = this.props.navigation.state;
      let { orderStore } = Store;
      this.setState({loading: true})
      var url = 'http://play4team.com/AdminApi/api/sendFriendRequest?senderId='+orderStore.playerLoginRes.id+'&playerId='+ params.item.id;
      // console.log('url=',url);
      fetch(url, {
         method: 'POST',
        }).then((response) => response.json())
              .then((responseJson) => {
                // console.log('friend req==',responseJson);
                  this.setState({loading: false,addFriend: true})
                if (responseJson.status === 'True') {
                      ToastAndroid.show('Friend request is sended ', ToastAndroid.LONG);
                }else {
                  this.setState({loading: false})
                  ToastAndroid.show('Friend request is already sended', ToastAndroid.LONG);
                }
              }).catch((error)=>{
                  this.setState({loading: false})
                  ToastAndroid.show('Try again', ToastAndroid.LONG);
              });
    }
    removeFriendRequest(){
      let { params } = this.props.navigation.state;
      let { orderStore } = Store;
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/removeFriend?playerId='+orderStore.playerLoginRes.id+'&friendId='+ params.item.id, {
         method: 'POST',
        }).then((response) => response.json())
              .then((responseJson) => {
                // console.log('remove req==',responseJson);
                  this.setState({loading: false})
                if (responseJson.status === 'True') {
                      this.setState({addFriend: false})
                      ToastAndroid.show('This player is removed from from your player list', ToastAndroid.LONG);
                }else {
                  this.setState({loading: false})
                  ToastAndroid.show('There must be system issue', ToastAndroid.LONG);
                }
              }).catch((error)=>{
                  this.setState({loading: false})
                  ToastAndroid.show('Try again', ToastAndroid.LONG);
              });
    }
  componentWillUnmount(){
      let { orderStore } = Store;
      orderStore.playerProfile = [];
      orderStore.sport_id = 0;
    }
  render() {
    let { orderStore } =Store;
    let { params } = this.props.navigation.state;
    var fitness = orderStore.playerLoginRes.Fitness * 10;
    // console.log('data',orderStore.playerProfile);
    return (
      <View style={styles.container}>
        <View style={{height:height(12),width:width(100),backgroundColor:'#1d1d4f'}}>
        </View>
        <View style={{height:height(30),width:width(100),flexDirection:'row',justifyContent:'center',marginTop:10,position:'absolute',zIndex: 1,alignItems:'center'}}>
          <View style={{height:height(30),width:width(30),alignItems:'center',justifyContent:'center'}}
              onPress={()=>{
                orderStore.chatStatus=true,
                this.props.navigation.navigate('Message',{item: params.item})
              }}
            >
            {/* <Image source={require('../img/messagePlayer.png')} style={{height:height(3.5),width:width(7),resizeMode:'contain'}} />
            <Text style={{fontSize:totalSize(1.6),color:'black',marginTop:3}}>Message</Text> */}
          </View>
          <View style={{height:height(32),width:width(40),alignItems:'center'}}>
            <Image source={params.item.Path.length >0?{uri: params.item.Path}:require('../img/userprofileImag.png')} style={{height:height(20),width:width(33),resizeMode:'contain'}} />
            <Text style={{fontSize:totalSize(2),color:'#1d1d4f'}}> { params.item.Name } </Text>
            <Text style={{fontSize:totalSize(1.3),color:'#949494',textAlign:'center'}}> { params.item.Address } </Text>
          </View>
          <View style={{height:height(28),width:width(30),justifyContent:'center',alignItems:'center'}}>
          {/*
              params.item.status === 'friend'?
               <TouchableOpacity style={{height:height(10),width:width(22),justifyContent:'center',alignItems:'center'}} onPress={()=>{this.removeFriendRequest()}}>
                 <Image source={require('../img/remove.png')} style={{height:height(3.5),width:width(5.5),resizeMode:'contain'}} />
                 <Text style={{fontSize:totalSize(1.2),color:'black',marginTop:2}}>remove friend</Text>
               </TouchableOpacity>
               :
               params.item.friend === 1 || this.state.addFriend === true?
                 <TouchableOpacity style={{height:height(10),width:width(20),justifyContent:'center',alignItems:'center'}} onPress={()=>{this.removeFriendRequest()}}>
                   <Image source={require('../img/remove.png')} style={{height:height(3.5),width:width(5.5)}} />
                   <Text style={{fontSize:totalSize(1.6),color:'black',marginTop:2}}>remove friend</Text>
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={{height:height(10),width:width(20),justifyContent:'center',alignItems:'center'}} onPress={()=>{this.sendFriendRequest()}}>
                   <Image source={require('../img/friendReq.png')} style={{height:height(3.5),width:width(6)}} />
                   <Text style={{fontSize:totalSize(1.6),color:'black',marginTop:2}}>Send request</Text>
                 </TouchableOpacity>
          */}
          </View>
        </View>
        <View style={{flex:1}}>
          <View style={{height:height(16),width:width(100),backgroundColor:'#ffffff'}}>
          </View>
          <View style={{height:height(47),width:width(100),marginTop:20,backgroundColor:'#ffffff'}}>
            <View style={{height:height(22),alignItems:'center'}}>
              <View style={{height:height(5),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(2.2),color:'#949494'}}>Average Ratings</Text>
              </View>
              <View style={{height:height(17),width:width(90),alignItems:'center',backgroundColor:'#ffffff'}}>
                  <View style={{height:height(17),width:width(80),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator = {false}
                      scrollToEnd = {true}
                      >
                          {
                            orderStore.playerProfile.length > 0 ?
                              orderStore.playerProfile.map((item,key)=>{
                              return(
                                  <TouchableOpacity key={key} style={item.checkStatus === true? styles.selected : styles.unSelect}
                                    onPress={()=>{
                                      this.selectSport(item);
                                    }}
                                  >
                                    {
                                      item.checkStatus === true?
                                        <View style={{flex:1,alignItems:'center'}}>
                                          <Image source={{uri: item.Path}} style={{height:height(10),width:width(17),resizeMode:'contain'}}/>
                                          <Text style={{fontSize:totalSize(1.6),color:'black'}}>{item.sport_name}</Text>
                                        </View>
                                        :
                                        <View style={{flex:1,alignItems:'center'}}>
                                          <Image source={{uri: item.Path}} style={{height:height(8),width:width(12),resizeMode:'contain'}}/>
                                          <Text style={{fontSize:totalSize(1.2),color:'black'}}>{item.sport_name}</Text>
                                        </View>

                                    }
                                  </TouchableOpacity>
                              );
                            })
                            :
                            <View style={{ flex:1,justifyContent:'center',alignItems:'center' }}>
                                <Text style={{ fontSize: 13 , color:'#1d1d4f' }}>This player has no selected skills</Text>
                            </View>
                        }
                    </ScrollView>
                  </View>
              </View>
            </View>
            <View style={{height:height(25),alignItems:'center'}}>
              <View style={{height:height(25),width:width(90),flexDirection:'row',backgroundColor:'#ffffff',justifyContent:'center',alignItems:'center'}}>
                <View style={{width:width(45),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
                  <Text style={{fontSize:totalSize(2),color:'#5c5c7c',marginBottom:10}}>Skill Level</Text>
                  <AnimatedCircularProgress
                      size={80}
                      width={5}
                      fill={this.state.skill }
                      tintColor="#00e0ff"
                      backgroundWidth = {5}
                      backgroundColor="#cfd8dc">
                      {
                        (fill) => (
                          <Text style={{fontSize:totalSize(2),color:'black'}}>
                            { this.state.skill  }%
                          </Text>
                        )
                      }
                  </AnimatedCircularProgress>
                </View>
                {/* <View style={{width:width(45),justifyContent:'center',alignItems:'center'}}>
                   <Text style={{fontSize:totalSize(2),color:'#5c5c7c',marginBottom:10}}>Fitness</Text>
                  <AnimatedCircularProgress
                      size={80}
                      width={5}
                      fill={ fitness }
                      tintColor="#00e0ff"
                      backgroundWidth = {5}
                      backgroundColor="#cfd8dc">
                      {
                        (fill) => (
                          <Text style={{fontSize:totalSize(2),color:'black'}}>
                            { fitness }%
                          </Text>
                        )
                      }
                  </AnimatedCircularProgress>
                </View> */}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  strip: {
    height:height(8),
    width:width(90),
    backgroundColor:'#ffffff',
    borderBottomWidth:0.3,
    marginBottom:0.3,
    borderColor:'gray',
    flexDirection:'row',
    borderRadius:5,
    marginTop:5,
  },
  unSelect: {
    height:height(10),
    width:width(15),
    borderRadius:100,
    margin:0,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
  },
  selected: {
    height:height(14),
    width:width(18),
    borderRadius:100,
    margin:0,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
  },
});
