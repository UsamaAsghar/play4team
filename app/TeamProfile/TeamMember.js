
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import Swipeout from 'react-native-swipeout';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
export default class TeamMember extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      contactList: [],
      id:0,
    }
  }
  static navigationOptions = {
       header: null
    }

    removeMember(state,player){
      var {params} = this.props.navigation.state;
      let{orderStore} = Store;
      if (state === false) {
      var index = 0;

      for(var i=0; i< orderStore.teamMembers.length; i++){
          if(orderStore.teamMembers[i].player_id === player.player_id ){
              index = i;
              break;
          }
      }
      orderStore.teamMembers.splice(index, 1);
      player.added = false;
      this.setState({
        id: player.player_id
      })
      // console.log('****unselected******');
      // console.log('player unselected',player);
      // console.log('player_id ==',this.state.id);

    }
  }
  render() {
    var {params} = this.props.navigation.state;
    var swipeoutBtns = [
      // {
      //   text: 'Primary',
      //   backgroundColor: 'green',
      //   onPress: function(){ ToastAndroid.show('primay button pressed', ToastAndroid.SHORT) },
      //   autoClose: true,
      //   type: 'primary',
      // },
      // {
      //   text: 'Secondary',
      //   backgroundColor: 'blue',
      //   onPress: function(){ ToastAndroid.show('secondary button pressed', ToastAndroid.SHORT) },
      //   autoClose: true,
      //   type: 'secondary',
      // },
      {
        text: 'Remove',
        color: 'white',
        backgroundColor: 'black',
        onPress:()=> {this.removeMember(false,this.props.item)},
        type: 'delete',
      },
    ]
    return (
          <Swipeout
              right={swipeoutBtns}
              // scroll={() => console.warn('scroll event') }
              autoClose= {true}
              buttonWidth = { totalSize(10) }
              disabled = { false }
              >
              <View style={{height:height(8),width:width(90),borderBottomWidth:0.5,borderColor:'gray',marginBottom:0.5,backgroundColor:'#ffffff',flexDirection:'row'}}>
                <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                  <Image source={require('../img/friend_img.png')} style={{height:height(6),width:width(12)}} />
                </View>
                <View style={{width:width(70),flexDirection:'row'}}>
                  <View style={{width:width(53),justifyContent:'center'}}>
                    <View style={{height:height(6),justifyContent:'center',alignItems:'flex-start'}}>
                      <Text style={{fontSize:totalSize(2),color:'black'}}>{this.props.item.Name}</Text>
                      <Text style={{fontSize:totalSize(1.4),color:'black'}}>Fitness:  {this.props.item.Fitness}</Text>
                    </View>
                  </View>
                  <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'flex-end'}}
                    onPress={()=>{
                      this.props.navigation.navigate('PlayerDetailView',{ item: item });
                      // this.removeMember(false,this.props.item);
                    }}
                  >
                    <Text style={{ height:height(5),width:width(8),fontSize: 12 , color:'black' }}>View</Text>
                    {/* <Image source={require('../img/user_profile.png')} style={{height:height(5),width:width(8),borderWidth:1,borderColor:'black'}} /> */}
                  </TouchableOpacity>
                </View>
              </View>
          </Swipeout>
    )
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },

});
