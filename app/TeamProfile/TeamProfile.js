
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import PopupDialog, { slideAnimation } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-crop-picker';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Footer from '../Footer/Footer';
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(76), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>{orderStore.playerLoginRes.Team_Name}</Text>
        </View>
      </View>
    );
  }
}
@observer export default class TeamProfile extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      avatarSource: null
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  }
  imagePicker() {
    this.popupDialog.dismiss();
    ImagePicker.openCamera({
      cropping: true,
      width: 500,
      height: 500,
      includeExif: true,
      avoidEmptySpaceAroundImage: true,
    }).then(image => {
      this.image_update(image)
      //  console.log('received image', image);
      //  this.setState({
      //    avatarSource: {uri: image.path, width: image.width, height: image.height},
      //    images: null
      //  });
    }).catch(e => alert(e));
  }
  gallery() {
    this.popupDialog.dismiss();
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      includeBase64: true,
      cropping: true,
      includeExif: true,
      avoidEmptySpaceAroundImage: true,
    }).then(image => {
      this.image_update(image)
    });
  }
  popupDialog = () => {
    return (
      <PopupDialog
        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
        dialogAnimation={slideAnimation}
        height={height(30)}
        width={width(80)}
      >
        <View style={{ flex: 1 }}>
          <View style={{ height: height(10), justifyContent: 'center' }}>
            <Text style={{ elevation: 4, fontSize: totalSize(1.8), fontWeight: 'bold', color: 'black', marginHorizontal: 20 }}>Choose Photo...</Text>
          </View>
          <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.imagePicker()}>
            <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Take From Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.gallery()}>
            <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Select From Gallery</Text>
          </TouchableOpacity>
        </View>
      </PopupDialog>
    )
  }
  componentWillMount() {
    let { orderStore } = Store;
    this.setState({ loading: true })
    fetch('http://play4team.com/AdminApi/api/Viewmember?teamId=' + orderStore.playerLoginRes.id, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log('members data==',responseJson);
        orderStore.teamMembers = responseJson.Member;
        this.setState({ loading: false })
        if (responseJson.status === 'True') {
          for (var i = 0; i < orderStore.teamMembers.length; i++) {
            // orderStore.teamMembers[i].id = orderStore.teamMembers[i].player_id;
            orderStore.teamMembers[i].added = true;
            orderStore.teamMembers[i].id = orderStore.teamMembers[i].player_id;
          }
          // ToastAndroid.show('true', ToastAndroid.LONG);
        } else {
          this.setState({ loading: false })
          ToastAndroid.show('error', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('Try again', ToastAndroid.LONG);
      });
  }
  image_update(image) {
    let { orderStore } = Store;
    if (orderStore.playerLoginRes.Status === 0)
      var type = 0;
    else
      var type = 1;

    const formData = new FormData();
    const photo = {
      uri: image.path,
      type: image.mime,
      name: 'testPhoto',
    };
    formData.append('profile_photo', photo);
    formData.append('id', orderStore.playerLoginRes.id);
    formData.append('type', type);
    this.setState({ loading: true })

    fetch('http://play4team.com/AdminApi/api/updateImage', {
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        // console.log('response==', responseJson);
        this.setState({ loading: false })
        if (responseJson.status === 'true') {
          orderStore.playerLoginRes = responseJson.data;
          ToastAndroid.show('Your profile Photo has been successfuly updated', ToastAndroid.LONG);
          // this.props.navigation.navigate('Login');
        } else {
          this.setState({ loading: false })
          ToastAndroid.show('Image not successfuly updated', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('Try again', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    if (this.state.loading == true) {
      return (
        <View style={{ flex: 1, height: height(100), width: width(100), backgroundColor: 'rgba(29,37,86,0.6)' }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(22), width: width(100) }}>
          <Image source={require('../img/blue-bg.png')} style={{ height: height(21), width: width(100) }} />
        </View>
        <View style={{ height: height(30), width: width(100), flexDirection: 'row', justifyContent: 'center', marginTop: 10, position: 'absolute', zIndex: 1, alignItems: 'flex-start' }}>
          <TouchableOpacity style={{ height: height(7), width: width(30), justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              this.props.navigation.navigate('TeamEdit', { picture: this.state.avatarSource });
            }}
          >
            <Image source={require('../img/edit-icon.png')} style={{ height: height(3), width: width(5) }} />
            <Text style={{ fontSize: totalSize(1.5), color: 'white' }}> Edit Profile </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ height: height(30), width: width(40), alignItems: 'center' }}
            onPress={() => { this.popupDialog.show() }}
          >
            {
               orderStore.playerLoginRes.Path.length === 0?
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <Image source={require('../img/p_pic.png')} style={{ height: height(23), width: width(40), borderRadius: 100, resizeMode: 'contain' }} />
                  <Text style={{ fontSize: totalSize(2), color: '#1d1d4f' }}> {orderStore.playerLoginRes.Team_Name} </Text>
                </View>
                :
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <Image source={{ uri: orderStore.playerLoginRes.Path }}  style={{ height: height(20), width: width(33), borderRadius: 100, resizeMode: 'contain' }} />
                  <Text style={{ fontSize: totalSize(2), color: '#1d1d4f' }}> {orderStore.playerLoginRes.Team_Name} </Text>
                </View>
            }
            <View style={{ height: height(4), width: width(40), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../img/loca.png')} style={{ height: height(1.5), width: width(5), marginLeft: 7, resizeMode: 'contain' }} />
              {
                orderStore.playerLoginRes.Status === 0 ?
                  <Text style={{ fontSize: totalSize(1.4), color: '#1d1d4f' }}> {orderStore.playerLoginRes.Address} </Text>
                  :
                  <Text style={{ fontSize: totalSize(1.4), color: '#1d1d4f' }}> {orderStore.playerLoginRes.address} </Text>
              }
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={{ height: height(7), width: width(30), justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              // this.props.navigation.navigate('ViewProfile');
            }}
          >
            {  /*<Image source={require('../img/view-icon.png')} style={{height:height(3),width:width(5)}} />
            <Text style={{fontSize:totalSize(1.5),color:'white'}}> View Profile </Text>*/}
          </TouchableOpacity>

        </View>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(16), width: width(100) }}>
          </View>
          <View style={{ height: height(62), width: width(100), alignItems: 'center' }}>

            <TouchableOpacity style={styles.strip}
              onPress={() => {
                this.props.navigation.navigate('PlayerAvailability');
              }}
            >
              <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../img/availability.png')} style={{ height: height(7), width: width(11) }} />
              </View>
              <View style={{ width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Availability</Text>
                </View>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  {
                    orderStore.playerLoginRes.visiblity === 1 ?
                      <Text style={{ fontSize: totalSize(1.2), color: '#1d1d4f' }}>Available</Text>
                      :
                      orderStore.playerLoginRes.visiblity === 0 ?
                        <Text style={{ fontSize: totalSize(1.2), color: '#1d1d4f' }}>Busy</Text>
                        :
                        <Text style={{ fontSize: totalSize(1.2), color: '#1d1d4f' }}>Possibly</Text>
                  }
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.strip}
              onPress={() => {
                this.props.navigation.navigate('TeamMembers');
              }}
            >
              <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../img/contact.png')} style={{ height: height(7), width: width(11) }} />
              </View>
              <View style={{ width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(6), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Player Members ( {orderStore.teamMembers.length} )</Text>
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity>

          </View>
        </View>
        <PopupDialog
          ref={(popupDialog) => { this.popupDialog = popupDialog; }}
          dialogAnimation={slideAnimation}
          height={height(30)}
          width={width(80)}
        >
          <View style={{ flex: 1 }}>
            <View style={{ height: height(10), justifyContent: 'center' }}>
              <Text style={{ elevation: 4, fontSize: totalSize(1.8), fontWeight: 'bold', color: 'black', marginHorizontal: 20 }}>Choose Photo...</Text>
            </View>
            <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.imagePicker()}>
              <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Take From Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.gallery()}>
              <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Select From Gallery</Text>
            </TouchableOpacity>
          </View>
        </PopupDialog>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  strip: {
    height: height(8),
    width: width(90),
    backgroundColor: '#ffffff',
    borderBottomWidth: 0.3,
    marginBottom: 0.3,
    borderColor: 'gray',
    flexDirection: 'row',
    borderRadius: 5,
    marginTop: 5,
  },
});
