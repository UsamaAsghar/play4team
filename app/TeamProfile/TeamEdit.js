
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,Picker
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import { observer } from 'mobx-react';
import Store from '../Stores';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),width:width(80),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(80),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
            <Text style={{fontSize:totalSize(2.5),color:'white'}}> Edit Profile </Text>
          </View>
        </View>
    );
  }
}
@observer export default class TeamEdit extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      name: '',
      email: '',
      sport:'',
      phone_no: "",
      address: '',
      sport_id: '',
      password: "",
      pic:'',
      rewrite: false,
    }
  }
    componentWillMount(){
      let {orderStore}=Store;
      let {params} = this.props.navigation.state;
      // console.log('data==',orderStore.playerLoginRes);
      this.setState({
        name: orderStore.playerLoginRes.Team_Name,
        address: orderStore.playerLoginRes.address,
        email:orderStore.playerLoginRes.Email,
        phone_no:orderStore.playerLoginRes.phone,
        password:orderStore.playerLoginRes.password.toString(),
        sport_id: orderStore.playerLoginRes.Sport_Id,
        // pic: orderStore.playerLoginRes.Image,
      })
      if (params.picture !== null) {
          this.setState({
            pic: params.picture
          })
      }
      // for (var i = 0; i < orderStore.sportEvent.length; i++) {
      //     if (orderStore.playerLoginRes.Sport_Id === orderStore.sportEvent[i].id && orderStore.sportEvent[i].added === false) {
      //       console.log('sport name ',orderStore.sportEvent[i].Name);
      //     }
      // }
    }
  get(){
      let { orderStore } = Store;
      console.log('name',this.state.name);
      console.log('email',this.state.email);
      console.log('phone',orderStore.playerLoginRes.phone);
      console.log('address',this.state.address);
      // console.log('dob',this.state.dob);
      console.log('password',orderStore.playerLoginRes.password);
      console.log('id',orderStore.playerLoginRes.Sport_Id);
      }

  update(){
    let { orderStore } = Store;
    // var url = 'http://play4team.com/AdminApi/public/api/updateTeamInfo?teamId='+orderStore.playerLoginRes.id+'&Team_Name='+this.state.name+'&Email='+this.state.email+'&Sport_Id='+this.state.sport_id+'&phone='+this.state.phone_no+'&address='+this.state.address+'&password='+this.state.password+'&Status=1'+'&visiblity='+orderStore.playerLoginRes.visiblity+'&profile_photo='+this.state.pic;
    // console.log('url is==',url);
        this.setState({loading: true})
        fetch('http://play4team.com/AdminApi/api/updateTeamInfo?teamId='+orderStore.playerLoginRes.id+'&Team_Name='+this.state.name+'&Email='+this.state.email+'&Sport_Id='+this.state.sport_id+'&phone='+this.state.phone_no+'&address='+this.state.address+'&password='+this.state.password+'&Status=1'+'&visiblity='+orderStore.playerLoginRes.visiblity, {
           method: 'POST'
          }).then((response) => response.json())
                .then((responseJson) => {
                  // console.log('response==',responseJson);
                    this.setState({loading: false})
                  if (responseJson.status === 'True') {
                        orderStore.playerLoginRes = responseJson.data;
                        ToastAndroid.show('Your profile data has been successfuly updated', ToastAndroid.LONG);
                       // this.props.navigation.navigate('Login');
                  }else {
                    this.setState({loading: false})
                    ToastAndroid.show('Please enter all required fields', ToastAndroid.LONG);
                  }
                }).catch((error)=>{
                    this.setState({loading: false})
                    ToastAndroid.show('Try again', ToastAndroid.LONG);
                });
    }
  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerStyle: {
         backgroundColor:'#1d1d4f'
       },
       headerTintColor:'white',
    }
  render() {
    let {orderStore} = Store;
    // console.log('name==',orderStore.playerLoginRes.Name);
    if(this.state.loading == true){
        return(
            <ImageBackground style={{height: height(100), width: width(100), flex:1}} source={require('../img/splash.jpg')} >
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </ImageBackground>
        );
    }

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Name</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({name: value , rewrite: true})}
                underlineColorAndroid='transparent'
                placeholder= {orderStore.playerLoginRes.Team_Name}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left',}}
                />
          </View>

          <View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Email</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({email: value , rewrite: true})}
                underlineColorAndroid='transparent'
                placeholder= {orderStore.playerLoginRes.Email}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left'}}
                />
          </View>

          {/*<View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Phone no</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({phone_no: value , rewrite: true})}
                underlineColorAndroid='transparent'
                placeholder={orderStore.playerLoginRes.phone}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                keyboardType='numeric'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left'}}
                />
          </View>*/}

          <View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Home Address</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({address: value ,  rewrite: true})}
                underlineColorAndroid='transparent'
                placeholder= {orderStore.playerLoginRes.address}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left'}}
                />
          </View>

          <View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Sport</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <Picker
              selectedValue={this.state.sport}
              itemStyle = {{color:'black'}}
              style={{ height: height(6), width: width(90),color:'gray'}}
              onValueChange={(itemValue, itemIndex) => this.setState({sport: itemValue}) }>
              <Picker.Item label= "select sport"  />
              {
                orderStore.sportEvent.map((item,key)=>{
                  return(
                    <Picker.Item  key={key} label= {item.Name} value= {item.id} />
                  );
                })
              }
            </Picker>
          </View>

          <View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Password</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({password: value ,  rewrite: true})}
                underlineColorAndroid='transparent'
                value= {this.state.password}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                keyboardType = 'default'
                keyboardAppearance='dark'
                secureTextEntry={true}
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left'}}
                />
          </View>
        </ScrollView>
        <TouchableOpacity style={{height:height(8),width:width(100),justifyContent:'center',backgroundColor:'#ffffff'}}
          onPress={()=>{
              if (this.state.rewrite === true) {
                    this.update();
              }else {
                    ToastAndroid.show('You should first rewrite any field', ToastAndroid.LONG);
              }

          }}
        >
          <ImageBackground source={require('../img/bar.png')} style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:totalSize(2.2),color:'white'}}>SAVE</Text>
          </ImageBackground>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },
  check: {
    height:height(7),
    width:width(90),
    marginTop:15,
    flexDirection:'row',
    borderRadius:10,
    backgroundColor:'#fefefe'
  },
  checked: {
    height:height(7),
    width:width(90),
    marginTop:15,
    flexDirection:'row',
    borderRadius:10,
    backgroundColor:'#1d1d4f'
  },
  txt: {
    fontSize:totalSize(1.8),
    color:'#1d1d4f'
  },
  text: {
    fontSize:totalSize(1.8),
    color:'white'
  },

});
