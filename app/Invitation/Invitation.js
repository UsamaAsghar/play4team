import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View, Image, ScrollView, TouchableOpacity, TextInput, ToastAndroid, Alert, Platform, PermissionsAndroid
} from 'react-native';
import {width, height, totalSize} from 'react-native-dimension';
import PopupDialog, {slideAnimation, DialogTitle, FadeAnimation} from 'react-native-popup-dialog';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Contacts from 'react-native-contacts';
import {Avatar} from 'react-native-elements';
import {CheckBox} from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons';
import Store from '../Stores';

const teamId = 0;

class LogoTitle extends React.Component {
    render() {
        return (
            <View style={{height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f'}}>
                <View style={{width: width(67), justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                    <Image source={require('../img/logo.png')} style={{height: height(8), width: width(15)}}/>
                    <Text style={{fontSize: totalSize(2.5), color: 'white'}}>Invite friends?</Text>
                </View>
                <View style={{width: width(18), justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                    <Text style={{fontSize: totalSize(1.5), color: 'white'}}>Step 2 of 2</Text>
                </View>
            </View>
        );
    }
}

export default class Invitation extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isActive: false,
            checked: false,
            friend: true,
            contact: false,
            contactList: [],
            selectedPlayers: [],
            added: false,
            selfAttending: 0,
            teamId: null,
            team: '',

        };
    }

    static navigationOptions = {
        headerTitle: <LogoTitle/>,
        headerStyle: {
            backgroundColor: '#1d1d4f'
        },
        headerTintColor: 'white'
    }

    async componentWillMount() {
        let {orderStore} = Store;
        await this.fetchteamsMembers()
    }

    getContacts() {
        let {orderStore} = Store;
        if (Platform.OS == 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'Describing why I need to access contact information.'
                }
            )
                .then(granted => {
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        this.setState({loading: true})
                        Contacts.getAll((err, contacts) => {
                            console.log('contactList===>>>', contacts)
                            orderStore.CONTACT_LIST = contacts;
                            console.log('contactList===>>>', orderStore.CONTACT_LIST)
                            this.setState({
                                contacts: contacts
                            });
                            this.setState({loading: false})
                            // console.log('contact func=', this.state.contacts);
                        });
                    } else {
                        // Handle
                    }
                })
                .catch(err => {
                    console.log('PermissionsAndroid', err)
                })
        }
    }

    // selectTeam(team_id){
    //   let { orderStore } = Store;
    //   for (var i = 0; i < orderStore.players.length; i++) {
    //     if (team_id === orderStore.players[i].id) {
    //         teamId = team_id;
    //         orderStore.players[i].checkStatus = true;
    //         orderStore.players[i].added = true;
    //         this.setState({
    //           added: orderStore.players[i].added
    //         })
    //         // console.log('****selected******');
    //         // console.warn('team is ==',teamId);
    //         // console.log('new obj ===',orderStore.players);
    //     }else {
    //       orderStore.players[i].checkStatus = false;
    //       orderStore.players[i].added = false;
    //     }
    //   }
    // }
    selectPlayer(player_id) {
        let {orderStore} = Store;
        for (var i = 0; i < orderStore.teamMembers.length; i++) {
            if (player_id === orderStore.teamMembers[i].id) {
                this.state.selectedPlayers.push(orderStore.teamMembers[i].id);
                orderStore.teamMembers[i].checkStatus = true;
                orderStore.teamMembers[i].added = true;
                this.setState({
                    added: orderStore.teamMembers[i].added
                })
                // console.log('****selected******');
                // console.log('plaers are ==',this.state.selectedPlayers);
                // console.log('new obj ===',orderStore.players);
            }
        }
    }

    unselectPlayer(player, state) {
        let {orderStore} = Store;
        if (state === false) {
            var index = 0;

            for (var i = 0; i < this.state.selectedPlayers.length; i++) {
                if (this.state.selectedPlayers[i] === player.id) {
                    index = i;
                    break;
                }
            }
            this.state.selectedPlayers.splice(index, 1);
            player.checkStatus = false;
            player.added = false;
            this.setState({
                added: player.added
            })
            // console.log('****unselected******');
            // console.log('player unselected',this.state.selectedPlayers);
            // console.log('sport after',orderStore.players);

        }
    }

    invitePlayers() {
        let {orderStore} = Store;
        if (this.state.selectedPlayers.length > 0) {
            this.setState({loading: true})
            var url = 'http://play4team.com/AdminApi/api/inviteFriends?teamId=' + orderStore.playerLoginRes.id + '&playerId=' + this.state.selectedPlayers + '&eventId=' + orderStore.eventResponse.id;
            console.log('players invitation url=', url);
            fetch('http://play4team.com/AdminApi/api/inviteFriends?teamId=' + orderStore.playerLoginRes.id + '&playerId=' + this.state.selectedPlayers + '&eventId=' + orderStore.eventResponse.id, {
                method: 'POST',
            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log('player invitation=', responseJson);
                    this.setState({loading: false})
                    if (responseJson.status === 'True') {
                        ToastAndroid.show('You have successfully create an event and your invitation is sent to this team', ToastAndroid.LONG);
                        this.props.navigation.push('MainTab');
                    } else {
                        this.setState({loading: false})
                    }
                }).catch((error) => {
                this.setState({loading: false})
            });
        } else {
            Alert.alert(
                'Suggestion',
                'You did not send invitation for this event.You can skip this by clicking the skip.',
                [
                    {
                        text: 'Skip',
                        onPress: () => this.props.navigation.push('MainTab'),
                        style: 'cancel',
                    }
                ],
            )
        }

    }

    inviteTeam() {
        //***func is for team and players invitations
        // this.setState({team: teamId})
        let {orderStore} = Store;
        let {params} = this.props.navigation.state;
        // var url = 'http://play4team.com/AdminApi/api/inviteFriends?playerId='+orderStore.playerLoginRes.id+'&friendId='+this.state.selectedPlayers +'&eventId=' + orderStore.eventResponse.id +'&attendingEvent='+ selfAttending;
        if (this.state.selectedPlayers.length !== 0) {
            // var url= 'http://play4team.com/AdminApi/api/inviteTeamForEvents?teamId='+orderStore.playerLoginRes.id+'&requestedTeamId='+this.state.team +'&eventId=' + orderStore.eventResponse.id;
            // console.log('url=',url);
            this.setState({loading: true})
            var url = 'http://play4team.com/AdminApi/api/inviteTeamForEvents?requestedTeamId=' + orderStore.playerLoginRes.id + '&teamName=' + this.state.team + '&eventId=' + orderStore.eventResponse.id;
            console.log('url------>>>>>', url);
            fetch(url, {
                method: 'POST',
            }).then((response) => response.json())
                .then(async (responseJson) => {
                    console.log('team invitation=', responseJson);
                    if (responseJson.status === 'True') {
                        this.setState({loading: false})
                        await this.invitePlayers();
                        // this.props.navigation.push('Home');
                    } else {
                        this.setState({loading: false})
                        ToastAndroid.show('Please try again there may be a network issue', ToastAndroid.SHORT);
                    }
                }).catch((error) => {
                this.setState({loading: false})
                ToastAndroid.show('error', ToastAndroid.SHORT);
            });
        } else {
            ToastAndroid.show('Please select your members to invite them', ToastAndroid.LONG);
        }
    }

    searchTeam = async (name) => {
        let {orderStore} = Store;
        console.log('name===>>>>', name);
        if (name) {
            orderStore.searchTeams = [];
            orderStore.teamMembers.forEach((item) => {
                if (item.Team_Name.includes(name)) {
                    orderStore.searchTeams.push(item);
                    console.log('searchTeam===>>>>', orderStore.searchTeams);
                    this.setState({loading: false})
                }
            });
        } else {
            orderStore.searchTeams = [];
            this.setState({loading: false})
        }
    }

    fetchteamsMembers(id) {
        let {orderStore} = Store;
        this.setState({loading: true})
        var url = 'http://play4team.com/AdminApi/api/getFriendsList?teamId=' + orderStore.playerLoginRes.id;
        console.log('url=', url);
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then(async (responseJson) => {
                orderStore.teamMembers = responseJson.friends;
                await this.getContacts()
                console.log('member====>', orderStore.teamMembers);
                this.setState({loading: false})
                if (responseJson.status === 'True') {
                    for (var i = 0; i < orderStore.teamMembers.length; i++) {
                        orderStore.teamMembers[i].added = false;
                        orderStore.teamMembers[i].checkStatus = false;
                    }
                } else {
                    this.setState({loading: false})
                    ToastAndroid.show('Please try again there may be a network issue', ToastAndroid.SHORT);
                }
            }).catch((error) => {
            this.setState({loading: false})
            ToastAndroid.show('error', ToastAndroid.SHORT);
        });
    }

    render() {
        let {orderStore} = Store;
        if (this.state.loading == true) {
            return (
                <View style={{height: height(100), width: width(100), flex: 1, backgroundColor: 'rgba(29,37,86,0.6)'}}>
                    <OrientationLoadingOverlay
                        visible={true}
                        color="white"
                        indicatorSize="small"
                        messageFontSize={18}
                        message="Loading..."
                    />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                {/* <View style={{height:height(10),flexDirection:'row',justifyContent:'center',backgroundColor:'#33327e'}}>
              <TouchableOpacity style={{width:width(50),alignItems:'center'}}
                  onPress={()=>{
                    this.setState({
                      friend: true,
                      contact: false
                    })
                  }}
                >
                <View style={{height:height(7),justifyContent:'center'}}>
                  {
                    this.state.friend === true?
                      <Image source={require('../img/f_select.png')} style={{height:height(5),width:width(6),backgroundColor:'transparent'}}/>
                      :
                      <Image source={require('../img/f_unselect.png')} style={{height:height(4.5),width:width(5),backgroundColor:'transparent'}}/>
                  }
                </View>
                <View style={{height:height(3),justifyContent:'center'}}>
                  {
                    this.state.friend === true?
                      <Text style={{fontSize:totalSize(2),color:'white'}}>friends</Text>
                      :
                      <Text style={{fontSize:totalSize(1.8),color:'gray'}}>friends</Text>
                  }
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{width:width(50),alignItems:'center'}}
                  onPress={()=>{
                    this.setState({
                      friend: false,
                      contact: true
                    })
                  }}
                >
                <View style={{height:height(7),justifyContent:'center'}}>
                  {
                    this.state.contact === true?
                      <Image source={require('../img/p_select.png')} style={{height:height(4),width:width(6),backgroundColor:'transparent'}}/>
                      :
                      <Image source={require('../img/p_unselect.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
                  }
                </View>
                <View style={{height:height(3),justifyContent:'center'}}>
                  {
                    this.state.contact === true?
                      <Text style={{fontSize:totalSize(2),color:'white'}}>Contacts</Text>
                      :
                      <Text style={{fontSize:totalSize(1.8),color:'gray'}}>Contacts</Text>
                  }
                </View>
              </TouchableOpacity>
          </View> */}
                <View style={{
                    height: height(7),
                    flexDirection: 'row',
                    backgroundColor: '#33327e',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View style={{
                        height: height(5.5),
                        width: width(85),
                        backgroundColor: 'white',
                        borderRadius: 5,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <View style={{height: height(7), width: width(70)}}>
                            <TextInput
                                onChangeText={(value) => this.searchTeam(value)}
                                // onChangeText={(value) => this.setState({ team: value })}
                                placeholder={"Search ..."}
                                style={{
                                    fontSize: totalSize(2),
                                    color: '#33327e',
                                    backgroundColor: 'white',
                                    width: width(65)
                                }}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        {/* <TouchableOpacity style={{alignSelf:'center',justifyContent:'center',alignItems:'center'}}>
                <Icon size={26} name='search' color='#33327e' />
              </TouchableOpacity> */}
                    </View>
                </View>

                {/* <View style={{height:height(7),backgroundColor:'#eaedf2',flexDirection:'row',justifyContent:'center'}}>
            <Picker
              selectedValue={this.state.team}
              itemStyle = {{color:'black'}}
              style={{ height: height(6), width: width(90),marginHorizontal:25,color:'black',alignSelf:'center'}}
              onValueChange={(itemValue, itemIndex) => {
                this.fetchteamsMembers (itemValue),
                this.setState({team: itemValue})
              }}>
              <Picker.Item label= "Choose a Team"  />
              {
                orderStore.players.map((item,key)=>{
                  return(
                    <Picker.Item  key={key} label= {item.Team_Name} value= {item.id} />
                  );
                })
              }
            </Picker>
          </View> */}
                <View style={styles.subContainer}>
                    {
                        orderStore.searchTeams.length > 0 ?
                            orderStore.searchTeams.map((item, key) => {
                                return (
                                    <View style={{
                                        height: height(8),
                                        width: width(90),
                                        alignSelf: 'center',
                                        flexDirection: 'row',
                                        backgroundColor: 'white',
                                        elevation: 3,
                                        alignItems: 'center',
                                        marginHorizontal: 5,
                                        marginVertical: 3
                                    }}>
                                        <Image source={{uri: item.Path}} style={{
                                            height: height(6),
                                            width: width(10),
                                            borderRadius: 100,
                                            resizeMode: 'contain',
                                            marginHorizontal: 10
                                        }}/>
                                        <Text style={{
                                            fontSize: totalSize(2),
                                            color: '#33327e',
                                            width: width(60),
                                            backgroundColor: 'red'
                                        }}>{item.Team_Name}</Text>
                                        <CheckBox
                                            center
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={() => {
                                            }}
                                            checked={this.state.checked}
                                            containerStyle={{
                                                backgroundColor: 'transparent',
                                                borderColor: 'transparent',
                                                marginHorizontal: 0,
                                                marginVertical: 0
                                            }}
                                        />
                                    </View>
                                );
                            })
                            :
                            null
                    }
                    {
                        orderStore.teamMembers.length > 0 ?
                            <ScrollView>
                                {
                                    orderStore.teamMembers.map((item, key) => {
                                        return (
                                            <TouchableOpacity key={key} style={{
                                                height: height(8),
                                                width: width(93),
                                                backgroundColor: '#ffffff',
                                                flexDirection: 'row'
                                            }}
                                                              onPress={() => {
                                                                  if (item.added === false) {
                                                                      this.selectPlayer(item.id);
                                                                  } else {
                                                                      this.unselectPlayer(item, false);
                                                                  }
                                                                  // this.selectTeam(item.id);
                                                              }}
                                            >
                                                <View style={{
                                                    width: width(20),
                                                    flexDirection: 'row',
                                                    justifyContent: 'center',
                                                    alignItems: 'center'
                                                }}>
                                                    {
                                                        item.Path.length > 0 ?
                                                            <Avatar
                                                                medium
                                                                rounded
                                                                source={{uri: item.Path}}
                                                            />
                                                            :
                                                            <Image source={require('../img/friend_img.png')} style={{
                                                                height: height(6),
                                                                width: width(12),
                                                                resizeMode: 'contain'
                                                            }}/>
                                                    }
                                                </View>
                                                <View style={{width: width(70), flexDirection: 'row'}}>
                                                    <View style={{width: width(53), justifyContent: 'center'}}>
                                                        <View style={{
                                                            height: height(3),
                                                            justifyContent: 'center',
                                                            alignItems: 'flex-start'
                                                        }}>
                                                            <Text style={{
                                                                fontSize: totalSize(1.8),
                                                                color: 'black'
                                                            }}>{item.Team_Name}</Text>
                                                        </View>
                                                        <View style={{
                                                            height: height(3),
                                                            justifyContent: 'center',
                                                            alignItems: 'flex-start'
                                                        }}>
                                                            {
                                                                item.visibility === '1' ?
                                                                    <Text style={{
                                                                        fontSize: totalSize(1.5),
                                                                        color: 'gray'
                                                                    }}>Online</Text>
                                                                    :
                                                                    <Text style={{
                                                                        fontSize: totalSize(1.5),
                                                                        color: 'gray'
                                                                    }}>Offline</Text>
                                                            }
                                                        </View>
                                                    </View>
                                                    <View style={{
                                                        width: width(15),
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }}>
                                                        {
                                                            item.added === true ?
                                                                <Image source={require('../img/tick.png')}/>
                                                                :
                                                                <Image source={require('../img/unCheck.png')}/>
                                                        }
                                                    </View>

                                                </View>
                                            </TouchableOpacity>
                                        );
                                    })
                                }
                            </ScrollView>
                            :
                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                <Text>No friends!</Text>
                            </View>
                    }
                </View>
                <PopupDialog
                    dialogTitle={<DialogTitle title="Send Your Invites"
                    titleStyle={{height: height(10), backgroundColor: '#33327e'}}
                    titleTextStyle={{color: 'white', fontSize: totalSize(2.2)}}
                    />}
                    ref={(popupDialog) => {
                        this.popupDialog = popupDialog;
                    }}
                    dialogStyle={{height: height(50), width: width(90), marginBottom: 0}}
                >
                    <View style={{flex: 1, marginTop: 40, position: 'absolute', zIndex: 1}}>
                        <View style={{
                            height: height(9),
                            width: width(90),
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Image source={require('../img/msg.png')} style={{height: height(9), width: width(15)}}/>
                        </View>
                        <View style={{height: height(35), alignItems: 'center'}}>
                            <View style={{
                                height: height(7),
                                width: width(70),
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{
                                    fontSize: totalSize(1.8),
                                    color: '#606199',
                                    textAlign: 'center',
                                    margin: 25
                                }}>Tap the players you want to invite and we will do the rest.</Text>
                            </View>
                            <View style={{
                                height: height(18),
                                width: width(70),
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Image source={require('../img/invites-bg.png')}
                                       style={{height: height(15), width: width(55)}}/>
                            </View>
                            <TouchableOpacity style={{height: height(10), width: width(90)}}
                                              onPress={() => {
                                                  this.popupDialog.dismiss()
                                                  this.props.navigation.navigate('FindPlayers');
                                              }}
                            >
                                <Image source={require('../img/got-it.png')} style={{
                                    height: height(10.2),
                                    width: width(90),
                                    borderBottomLeftRadius: 10,
                                    borderBottomRightRadius: 10
                                }}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </PopupDialog>
                <TouchableOpacity style={{
                    height: height(7),
                    width: width(100),
                    backgroundColor: '#1d1d4f',
                    justifyContent: 'center',
                    marginTop: 10,
                    alignItems: 'center'
                }}
                                  onPress={() => {
                                      this.invitePlayers()
                                      // if (this.state.team.length !== 0) {
                                      //     this.inviteTeam()
                                      // }else {
                                      //     ToastAndroid.show('Please select any frinend which you want to invit', ToastAndroid.SHORT);
                                      //  }
                                  }}
                >
                    <Text style={{fontSize: totalSize(2), color: 'white'}}>Next</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    subContainer: {
        flex: 1,
        margin: 20,
        marginBottom: 5
    },
    strip: {
        height: height(8),
        backgroundColor: '#ffffff',
        marginBottom: 5,
        flexDirection: 'row',
        borderRadius: 5
    },
});
//
// <ScrollView>
//     {
//         this.state.contactList && this.state.contactList.map((item,key)=>{
//             return(
//                 <TouchableOpacity key={key} style={{height:height(8),width:width(92),marginTop:5,backgroundColor:'#ffffff',flexDirection:'row'}}>
//                     <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
//                         {
//                             item.hasThumbnail === false?
//                                 <Image source={require('../img/friend_img.png')} style={{height:height(6),width:width(12)}} />
//                                 :
//                                 <Image source={{uri: item.thumbnailPath}} style={{height:height(7),width:width(12),borderRadius:100}} />
//                         }
//                     </View>
//                     <View style={{width:width(70),flexDirection:'row'}}>
//                         <View style={{width:width(53),justifyContent:'center'}}>
//                             <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
//                                 <Text style={{fontSize:totalSize(1.8),color:'black'}}>{item.givenName}</Text>
//                             </View>
//                             <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
//                                 <Text style={{fontSize:totalSize(1.5),color:'gray'}}>{item.phoneNumbers[0].number} </Text>
//                             </View>
//                         </View>
//                         <View style={{width:width(15),justifyContent:'center',alignItems:'flex-end'}}>
//                             <Image source={require('../img/add_btn.png')} style={{height:height(5),width:width(8)}} />
//                         </View>
//
//                     </View>
//                 </TouchableOpacity>
//             );
//         })
//     }
// </ScrollView>
