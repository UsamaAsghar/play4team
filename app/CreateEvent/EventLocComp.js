
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';

export default class EventLocComp extends Component<Props> {


  static navigationOptions = {
        header:null,

    }
  render() {
    return (
        <TouchableOpacity style={{height:height(10),width:width(93),backgroundColor:'#ffffff',flexDirection:'row'}}>
            <View style={{width:width(20),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/event_loc.png')} style={{height:height(7),width:width(12)}} />
              <Text style={{fontSize:totalSize(1.2),color:'black'}}>0.5 miles</Text>
            </View>
            <View style={{width:width(53),justifyContent:'center'}}>
              <View style={{height:height(4),justifyContent:'flex-end',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.9),color:'black'}}>LasMenStand.location</Text>
              </View>
              <View style={{height:height(4),justifyContent:'flex-start',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.4),color:'gray'}}>Block B, sector 2 Lahore, Pakistan</Text>
              </View>
          </View>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
