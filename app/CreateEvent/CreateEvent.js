import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  View, TextInput, Image, ScrollView, TouchableOpacity, ToastAndroid
} from 'react-native';
import DateTimePicker from 'react-native-datepicker'
import { Avatar } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import { width, height, totalSize } from 'react-native-dimension';
import api from '../ApiController/api';
import { observer } from 'mobx-react';
import Store from '../Stores';
import ClockIcon from 'react-native-vector-icons/AntDesign';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(67), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Create Event</Text>
        </View>
        <View style={{ width: width(18), justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: totalSize(1.5), color: 'white' }}>Step 1 of 2</Text>
        </View>
      </View>
    );
  }
}
@observer export default class CreateEvent extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      sports: [],
      loc_name: '',
      sport_id: 0,
      sportName: '',
      startDate: "",
      endDate: "",
      start_time: "",
      time: "",
      e_name: '',
      weekelyEvents: null,
      address: '',
      latitude: null,
      longitude: null
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  }
  componentWillMount = async () => {
    let { orderStore } = Store;
    if (orderStore.teamMembers === 0) {
      this.alert()
    }
    this.setState({ loading: true })
    let response = await api.get('Sport');
    orderStore.sportEvent = response.data;
    console.log('sportAPi=',response.data);
    this.setState({ loading: false })
    if (response.status === 'True') {
      for (var i = 0; i < orderStore.sportEvent.length; i++) {
        orderStore.sportEvent[i].added = false;
        orderStore.sportEvent[i].checkStatus = false;
      }
      // console.log('Response',orderStore.sportEvent);
    } else {
      this.setState({ loading: false })
    }
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      orderStore.sportEvent[0].added = true;
      orderStore.sportEvent[0].checkStatus = true;
      orderStore.sport_id = orderStore.sportEvent[0].id;
      this.setState({ sportName: orderStore.sportEvent[0].Name })
    }
  }
  componentWillUnmount() {
    let { orderStore } = Store;
    // **** reset the store
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      orderStore.sportEvent[i].added = false;
      orderStore.sportEvent[i].checkStatus = false;
    }
  }
  alert() {
    Alert.alert(
      'Warning',
      'You have not any friend in your friend list, please make friends to create an event.' ,
      [
        {text: 'OK', onPress: () => this.props.navigation.goBack()},
      ],
      {cancelable: false},
    );
  }
  selectSport(sport) {
    // console.log('sport id=',sport.id);
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      if (sport.id === orderStore.sportEvent[i].id) {
        orderStore.sportEvent[i].added = true;
        orderStore.sportEvent[i].checkStatus = true;
        orderStore.sport_id = orderStore.sportEvent[i].id;
        this.setState({
          sport_id: sport.id,
          sportName: sport.Name
        })
      } else {
        orderStore.sportEvent[i].added = false;
        orderStore.sportEvent[i].checkStatus = false;
      }
    }
    // console.log('selected sport=',orderStore.sport_id);
    // console.log('object is =',orderStore.sportEvent);
  }
  getPredictions(postalCode){
    console.log('postal code====>>>',postalCode);
    let { orderStore } = Store;
    // let api_key = "AIzaSyDACwsZaMUcTRxxkcrgNXRKy4hxewYkeew";//"AIzaSyBvKYQ2g6cbcD1mFJwf8r3E_g7l2-JMiec";//'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    if(postalCode.length!==0){
      fetch('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + postalCode + '&key=' + api_key )
          .then((response) => response.json())
          .then((responseJson) => {
          console.log('Predictions', responseJson);
          if (responseJson.status === 'OK') {
              orderStore.predictions = responseJson.predictions;
              for(var i=0;i<orderStore.predictions.length;i++){
                orderStore.predictions[i].added = false;
                orderStore.predictions[i].checkStatus = false;
              }
              this.setState({loading: false})
           }
     })
    }else{
        orderStore.predictions=[];
        this.setState({loading: false})
    }
  }
  async getLatLong(item){
    let { orderStore } = Store;
    await this.setState({address: item.description})
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + item.description + '&key=' + api_key )
          .then((response) => response.json())
          .then(async(responseJson) => {
          console.warn('latLong response', responseJson);
          if (responseJson.status === 'OK') {
              await this.setState({
                 latitude: responseJson.results[0].geometry.location.lat,
                 longitude: responseJson.results[0].geometry.location.lng,
                //  address: responseJson.results[0].formatted_address,
              })
              orderStore.predictions = [];
              this.setState({loading: false})
           }
     })
  }
  CreateEvent() {
    let { orderStore } = Store;

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    if ( this.state.sportName == '') {
      ToastAndroid.show('Please select any sport from list', ToastAndroid.LONG);
    } else {
      if ( this.state.latitude == null || this.state.longitude == null ) {
        ToastAndroid.show('Please enter your postal/zip code', ToastAndroid.LONG);
      } else {
          if (this.state.weekelyEvents == null) {
            ToastAndroid.show('Is this going to a weekly event?', ToastAndroid.LONG);
          } else {
            if ( this.state.startDate == '' ) {
              ToastAndroid.show('Please select event expiry date.', ToastAndroid.LONG);
            } else {

              this.setState({ loading: true })
              var url = 'http://play4team.com/AdminApi/api/createEvents?teamId=' + orderStore.playerLoginRes.id +'&time='+ this.state.start_time + '&eventName=' + this.state.e_name + '&sportId=' + orderStore.sport_id +'&locationName='+ this.state.loc_name + '&startDate=' + date + '&endDate=' + this.state.endDate + '&weekelyEvents=' + this.state.weekelyEvents + '&latitude=' + this.state.latitude + '&longitude=' + this.state.longitude + '&location=' + this.state.address
              console.log('create event url===>>',url);

              fetch('http://play4team.com/AdminApi/api/createEvents?teamId=' + orderStore.playerLoginRes.id + '&eventName=' + this.state.e_name+'&time='+ this.state.start_time + '&sportId=' + orderStore.sport_id +'&locationName='+ this.state.loc_name + '&startDate=' + this.state.startDate + '&weekelyEvents=' + this.state.weekelyEvents + '&latitude=' + this.state.latitude + '&longitude=' + this.state.longitude + '&location=' + this.state.address, {
                method: 'POST',
              }).then((response) => response.json())
                  .then((responseJson) => {
                    console.log('create event===>>>',responseJson);
                    orderStore.eventResponse = responseJson.data;
                    if (responseJson.status === 'True') {
                      // ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                      orderStore.editLocation.status = false;
                      this.props.navigation.navigate('Invitation', { data: responseJson.data, sportName: this.state.sportName })
                      this.setState({ loading: false });
                    } else {
                      this.setState({ loading: false })
                      ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
                    }
                  }).catch((error) => {
                console.log('errpr====>>>',error);
                this.setState({ loading: false })
                ToastAndroid.show('error', ToastAndroid.SHORT);
              });

            }
          }
      }
    }
  }
  _renderItem({ item, index }){
    return (
      <View style={{ alignItems:'center' }}>
        <Avatar
          large
          rounded
          source={{
            uri: item.Path,
          }}
        />
        <Text style={{ marginHorizontal: 10, color: 'black', fontSize: 12, textAlign:'center' }}>{item.Name}</Text>
      </View>
    );
  }
  render() {
    let { orderStore } = Store;
    if (this.state.loading == true) {
      return (
        <View style={{ flex: 1, height: height(100), width: width(100), backgroundColor: 'rgba(29,37,86,0.6)' }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    var today = new Date();
    var todayDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(8), width: width(100), backgroundColor: '#eaedf2', justifyContent: 'center', alignItems: 'center' }}>
            <TextInput
              onChangeText={(value) => this.setState({ e_name: value })}
              underlineColorAndroid='transparent'
              placeholder='Enter Event Name'
              placeholderTextColor='black'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              style={{ height: height(6), width: width(93), textAlign: 'center', borderRadius: 0, marginTop: 0, color: 'black', backgroundColor: '#eaedf2', fontWeight: '600' }}
            />
          </View>
          <ScrollView
             bounces={true}
             bouncesZoom={true}
             indicatorStyle='white'
             pinchGestureEnabled={true}
             ref={ref => this.scrollView = ref}
             onContentSizeChange={()=>{
                 this.scrollView.scrollToEnd({animated: true});
             }}>
            <View style={{ height: height(5), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
            </View>
            <View style={{ height: height(20), width: width(93), borderRadius: 5, backgroundColor: '#ffffff', margin: 15, marginTop: 0, marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(5), width: width(93), flexDirection: 'row' }}>
                <View style={{ width: width(46), justifyContent: 'center', alignItems: 'flex-end' }}>
                  <Text style={{ fontSize: totalSize(1.5), color: 'black' }}>EventType: </Text>
                </View>
                <View style={{ width: width(46), justifyContent: 'center', alignItems: 'flex-start' }}>
                  <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>{this.state.sportName}</Text>
                </View>
              </View>
              <View style={{ height: height(15), width: width(93), alignItems: 'center' }}>
                <View style={{ height: height(15), width: width(80), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <Carousel
                  ref={(c) => { this._carousel = c; }}
                  data={orderStore.sportEvent}
                  renderItem={this._renderItem}
                  // activeAnimationType='timing' //spring
                  inactiveSlideOpacity={0.5}
                  inactiveSlideScale={0.6}
                  sliderWidth={width(80)}
                  itemWidth={width(20)}
                  onSnapToItem={(index) =>{
                    orderStore.sport_id = index + 1
                  }}
                />
                </View>
              </View>
            </View>
            <View style={{ height: height(9), width: width(93), flexDirection: 'row', backgroundColor: '#ffffff', borderRadius: 5, margin: 15, marginTop: 10, marginBottom: 5 }}>
              <TouchableOpacity style={{ width: width(46.5), justifyContent: 'center', alignItems: 'center' }}>
                <DateTimePicker
                  style={{ width: width(43) }}
                  date={this.state.start_time}
                  time={this.state.start_time}
                  mode="time"
                  is24Hour={true}
                  placeholder="Event Time"
                  // format="YYYY-MM-DD"
                  // minDate="2018-05-01"
                  // maxDate="2020-06-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  iconSource={require('../img/clock.png')}
                  // iconComponent={<Image source={require('../img/clock.png')} style={{ height: height(5), width: width(5), resizeMode: 'contain' }} />}
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      right: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 36
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(time) => { 
                    // console.log('event time===:', time)
                    this.setState({ start_time: time }) 
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={{ width: width(43), justifyContent: 'center', alignItems: 'center' }}>
                {/*<Image source={require('../img/uncolor_calender.png')} style={{height:height(9),width:width(46),borderRadius:5}} />*/}
                <DateTimePicker
                  style={{ width: width(43) }}
                  date={this.state.startDate}
                  // time={this.state.time}
                  mode="date"
                  placeholder="Event Date"
                  format="YYYY-MM-DD"
                  minDate={todayDate}
                  // maxDate="2020-06-01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                      marginLeft: 36
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => { this.setState({ startDate: date }) }}
                />
              </TouchableOpacity>
            </View>
            <View style={{ borderRadius: 5, margin: 15, marginTop: 10, marginBottom: 5, backgroundColor: '#ffffff' }}>
              <TextInput
                onChangeText={(value) =>  this.setState({ loc_name: value }) }
                underlineColorAndroid='transparent'
                placeholder='Location Name (Optional)'
                value={this.state.loc_name}
                // onChangeText={() => { this.setState({ loc_name: '' }) }}
                placeholderTextColor='gray'
                keyboardType='email-address'
                underlineColorAndroid='transparent'
                autoCorrect={true}
                style={{ height: height(6), width: width(90), textAlign: 'center', fontSize: totalSize(1.5), marginTop: 0, backgroundColor: 'white', paddingLeft: 10 }}
              />
            </View>
            <View style={{ borderRadius: 5, margin: 15, marginTop: 10, marginBottom: 5, backgroundColor: '#ffffff' }}>
              <TextInput
                onChangeText={(value) => this.getPredictions(value)}
                underlineColorAndroid='transparent'
                placeholder='Postal/Zip Code'
                value={this.state.address.length > 0 ? this.state.address : null}
                onFocus={() => { this.setState({ address: '' }) }}
                placeholderTextColor='gray'
                keyboardType='email-address'
                underlineColorAndroid='transparent'
                autoCorrect={true}
                style={{ height: height(6), width: width(90), textAlign: 'center', fontSize: totalSize(1.5), marginTop: 0, backgroundColor: 'white', paddingLeft: 10 }}
              />
              {
                orderStore.predictions.length > 0?
                  <View style={{width:width(90),backgroundColor:'white',alignSelf:'center'}}>
                    {
                      orderStore.predictions.map((item,key)=>{
                        return(
                          <TouchableOpacity key={key} style={{marginHorizontal:5,borderBottomWidth:0.4,borderColor:'gray',elevation:0}}
                            onPress={()=>{
                              this.getLatLong(item)
                            }}
                          >
                            <Text style={{fontSize:totalSize(1.6),color:'black',marginVertical:10}}>{ item.description }</Text>
                          </TouchableOpacity>
                        )
                      })
                    }
                  </View>
                :
                null
              }
            </View>
            <View style={{ height: height(10), borderRadius: 5, margin: 15, marginTop: 10, marginBottom: 5, backgroundColor: '#ffffff' }}>
              <View style={{ height: height(4), justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: totalSize(1.5), color: 'black' }}>Is this going to a weekly event?</Text>
              </View>
              <View style={{ height: height(6), flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start' }}>
                <TouchableOpacity style={{ height: height(5), width: width(42), alignItems: 'center', marginRight: 3, backgroundColor: this.state.weekelyEvents === 1? '#00FF7F':'#1d1d4f', borderRadius: 7, justifyContent: 'center' }}
                  onPress={() => {
                    this.setState({
                      weekelyEvents: 1
                    })
                  }}
                >
                  {/* <Image source={require('../img/yes-btn.png')} style={{height:height(4.5),width:width(43),borderRadius:5}} /> */}
                  <Text style={{ fontSize: 12, color:this.state.weekelyEvents === 1?  '#1d1d4f':'white' }}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: height(5), width: width(42), marginLeft: 3, alignItems: 'center', backgroundColor: this.state.weekelyEvents===0?'#00FF7F':'#1d1d4f', borderRadius: 7, justifyContent: 'center' }}
                  onPress={() => {
                    this.setState({
                      weekelyEvents: 0
                    })
                    // this.props.navigation.navigate('Recurring')
                  }}
                >
                  <Text style={{ fontSize: 12, color: this.state.weekelyEvents === 0? '#1d1d4f':'white' }}>No</Text>
                  {/* <Image source={require('../img/no.png')} style={{height:height(4.5),width:width(43),borderRadius:5}} /> */}
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
        <TouchableOpacity style={{ height: height(7) }}
          onPress={() => {
            this.CreateEvent()
          }}
        >
          <Image source={require('../img/next.png')} style={{ height: height(7), width: width(100) }} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },
  unSelect: {
    height: height(10),
    width: width(15),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selected: {
    height: height(13),
    width: width(20),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },

});