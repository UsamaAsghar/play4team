import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput,ToastAndroid,Picker
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import PopupDialog, {slideAnimation,DialogTitle,FadeAnimation} from 'react-native-popup-dialog';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Store from '../Stores';
const teamId = 0;
class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(67),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Invite Friends</Text>
          </View>
          <View style={{width:width(18),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(1.5),color:'white'}}></Text>
          </View>
        </View>
    );
  }
}
export default class RequestTeamMembers extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isActive: false,
      checked : false,
      friend: true,
      contact: false,
      contactList: [],
      selectedPlayers: [],
      added: false,
      selfAttending: 0,
      teamId: null,
      team: '',

    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }

  selectPlayer(player_id){
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.teamMembers.length; i++) {
      if (player_id === orderStore.teamMembers[i].id) {
          this.state.selectedPlayers.push(orderStore.teamMembers[i].id);
          orderStore.teamMembers[i].checkStatus = true;
          orderStore.teamMembers[i].added = true;
          this.setState({
            added: orderStore.teamMembers[i].added
          })
          // console.log('****selected******');
          // console.log('plaers are ==',this.state.selectedPlayers);
          // console.log('new obj ===',orderStore.players);
      }
    }
  }
  unselectPlayer(player , state ){
    let{orderStore} = Store;
    if (state === false) {
    var index = 0;

    for(var i=0; i< this.state.selectedPlayers.length; i++){
        if(this.state.selectedPlayers[i] === player.id ){
            index = i;
            break;
        }
    }
    this.state.selectedPlayers.splice(index, 1);
    player.checkStatus = false;
    player.added = false;
    this.setState({
      added: player.added
    })
    // console.log('****unselected******');
    // console.log('player unselected',this.state.selectedPlayers);
    // console.log('sport after',orderStore.players);

    }
  }
  invitePlayers(){
    let {params} = this.props.navigation.state;
    let { orderStore } = Store;
    console.warn('event',params.event);
    this.setState({loading: true})
    fetch('http://play4team.com/AdminApi/api/inviteFriends?teamId='+orderStore.playerLoginRes.id+'&playerId='+this.state.selectedPlayers +'&eventId=' + params.event.id , {
     method: 'POST',
      }).then((response) => response.json())
            .then((responseJson) => {
                console.log('team invitation=',responseJson);
                this.setState({loading: false})
                if (responseJson.status === 'True') {
                    ToastAndroid.show('You event joining invitation has successfuly sended', ToastAndroid.LONG);
                    this.props.navigation.goBack();
                }else {
                  this.setState({loading: false})
                  ToastAndroid.show('Please try again there may be a network issue', ToastAndroid.SHORT);
                }
            }).catch((error)=>{
                this.setState({loading: false})
                ToastAndroid.show('error', ToastAndroid.SHORT);
            });
  }

    componentWillMount(){
      let { orderStore } = Store;
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/teamPlayersList?teamId='+orderStore.playerLoginRes.id, {
       method: 'POST',
        }).then((response) => response.json())
              .then((responseJson) => {
                  orderStore.teamMembers = responseJson.data;
                  this.setState({loading: false})
                  if (responseJson.status === 'True') {
                        for (var i = 0; i < orderStore.teamMembers.length; i++) {
                            orderStore.teamMembers[i].added = false;
                            orderStore.teamMembers[i].checkStatus = false;
                        }
                  }else {
                    this.setState({loading: false})
                    ToastAndroid.show('Please try again there may be a network issue', ToastAndroid.SHORT);
                  }
              }).catch((error)=>{
                  this.setState({loading: false})
                  ToastAndroid.show('error', ToastAndroid.SHORT);
              });
    }
  render() {
    let { orderStore } = Store;
      if(this.state.loading == true){
          return(
              <View style={{height: height(100), width: width(100), flex:1,backgroundColor:'rgba(29,37,86,0.6)'}} >
                  <OrientationLoadingOverlay
                      visible={true}
                      color="white"
                      indicatorSize="small"
                      messageFontSize={18}
                      message="Loading..."
                  />
              </View>
          );
      }
    return(
      <View style={styles.container}>
          {/* <View style={{height:height(7),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5.5),width:width(85),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/search_friends.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
              </TouchableOpacity>
              <View style={{height:height(7),width:width(70)}}>
                <TextInput
                  onChangeText={(value) => this.setState({search: value})}
                  placeholder={"Search"}
                  style={{fontSize:totalSize(2),color:'white',backgroundColor:'white',width:width(65)}}
                  underlineColorAndroid="transparent"
                  />
              </View>
            </View>
            <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}>
              <Image source={require('../img/cross.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
          </View> */}
          <View style={styles.subContainer}>
                  <ScrollView>
                    {
                      orderStore.teamMembers.map((item,key)=>{
                        return(
                          <TouchableOpacity key={key} style={{height:height(8),width:width(93),backgroundColor:'#ffffff',flexDirection:'row'}}
                            onPress={()=>{
                              if (item.added === false) {
                                  this.selectPlayer(item.id);
                              }else {
                                  this.unselectPlayer(item,false);
                              }
                              // this.selectTeam(item.id);
                            }}
                          >
                            <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                              {
                                item.Path.length > 0 ?
                                  <Image source={{uri:item.Path}} style={{height:height(6),width:width(12),resizeMode:'contain'}} />
                                  :
                                  <Image source={require('../img/friend_img.png')} style={{height:height(6),width:width(12),resizeMode:'contain'}} />
                              }
                            </View>
                            <View style={{width:width(70),flexDirection:'row'}}>
                              <View style={{width:width(53),justifyContent:'center'}}>
                                <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
                                  <Text style={{fontSize:totalSize(1.8),color:'black'}}>{item.Name}</Text>
                                </View>
                                <View style={{height:height(3),justifyContent:'center',alignItems:'flex-start'}}>
                                  {
                                    item.visibility === '1' ?
                                      <Text style={{fontSize:totalSize(1.5),color:'gray'}}>Online</Text>
                                      :
                                      <Text style={{fontSize:totalSize(1.5),color:'gray'}}>Offline</Text>
                                  }
                                </View>
                              </View>
                              <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                                {
                                  item.added === true?
                                    <Image source={require('../img/tick.png')} />
                                    :
                                    <Image source={require('../img/unCheck.png')} />
                                }
                              </View>

                            </View>
                          </TouchableOpacity>
                        );
                      })
                    }
                  </ScrollView>
          </View>
          <PopupDialog
             dialogTitle={<DialogTitle title="Send Your Invites"
             titleStyle = {{height:height(10),backgroundColor:'#33327e'}}
             titleTextStyle = {{ color:'white',fontSize:totalSize(2.2) }}
             />}
             ref={(popupDialog) => { this.popupDialog = popupDialog; }}
             dialogStyle ={{ height:height(50),width:width(90),marginBottom:0}}
             >
             <View style={{flex:1,marginTop:40,position: 'absolute',zIndex:1}}>
               <View style={{height:height(9),width:width(90),justifyContent:'center',alignItems:'center'}}>
                  <Image source={require('../img/msg.png')} style={{height:height(9),width:width(15)}} />
               </View>
               <View style={{height:height(35),alignItems:'center'}}>
                 <View style={{height:height(7),width:width(70),justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:totalSize(1.8),color:'#606199',textAlign:'center',margin:25}}>Tap the players you want to invite and we will do the rest.</Text>
                 </View>
                 <View style={{height:height(18),width:width(70),justifyContent:'center',alignItems:'center'}}>
                    <Image source={require('../img/invites-bg.png')} style={{height:height(15),width:width(55)}} />
                 </View>
                 <TouchableOpacity style={{height:height(10),width:width(90)}}
                     onPress={()=>{
                       this.popupDialog.dismiss()
                       this.props.navigation.navigate('FindPlayers');
                     }}
                   >
                    <Image source={require('../img/got-it.png')} style={{height:height(10.2),width:width(90),borderBottomLeftRadius:10,borderBottomRightRadius:10}} />
                 </TouchableOpacity>
               </View>
             </View>
          </PopupDialog>
          <TouchableOpacity style={{height:height(7),width:width(100),backgroundColor:'#1d1d4f',justifyContent:'center',marginTop:10,alignItems:'center'}}
            onPress={()=>{
              if (this.state.selectedPlayers.length !== 0) {
                  this.invitePlayers()
              }else {
                  ToastAndroid.show('Please select any frinend which you want to invit', ToastAndroid.SHORT);
               }
              }}
             >
            <Text style={{fontSize:totalSize(2),color:'white'}}>Next</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
