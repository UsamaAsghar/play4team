/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import Store from '../Stores';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox'
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(75), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Recurring</Text>
        </View>
      </View>
    );
  }
}
export default class Recurring extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      recurring: [
        {
          name: 'One Off',
          isSelect: false
        },
        {
          name: 'Every Day',
          isSelect: false
        },
        {
          name: 'Every Week',
          isSelect: false
        },
        {
          name: 'Every 2 Weeks',
          isSelect: false
        },
        {
          name: 'Every Month',
          isSelect: false
        },
        {
          name: 'Every Year',
          isSelect: false
        },
      ],
      checked: false,
    };
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white'
  }
  handleClick = (item) => {
    const { orderStore } = Store;
    const { recurring } = this.state;
    recurring.forEach(value => {
      if (item.name == value.name) {
        orderStore.eventLife = item.name;
        value.isSelect = true;
        this.setState({ recurring })
      } else {
        value.isSelect = false;
      }
    })
  }
  card = () => {
    return (
      <View style={{ height: height(8), width: width(93), backgroundColor: '#ffffff', flexDirection: 'row', borderBottomWidth: 0.5, borderColor: 'gray' }}>
        <View style={{ width: width(83) }}>
          <Text style={{ fontSize: totalSize(2), margin: 15, color: 'black' }}>One Off</Text>
        </View>
        <View style={{ width: width(10), justifyContent: 'center', alignItems: 'center' }}>
          <CircleCheckBox
            checked={this.state.checked}
            onToggle={(checked) => this.setState({ checked: checked })}
            outerColor={'#5b5b7e'}
            innerColor={'#5b5b7e'}
          />
        </View>
      </View>
    )
  }
  render() {
    const { recurring } = this.state;
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(75), width: width(93), alignItems: 'center', margin: 15, marginBottom: 5, marginTop: 15 }}>
            {
              recurring.map((item, key) => {
                return (
                  <View style={{ height: height(8), width: width(93), backgroundColor: '#ffffff', flexDirection: 'row', borderBottomWidth: 0.5, borderColor: 'gray' }}>
                    <View style={{ width: width(83) }}>
                <Text style={{ fontSize: totalSize(2), margin: 15, color: 'black' }}>{item.name}</Text>
                    </View>
                    <View style={{ width: width(10), justifyContent: 'center', alignItems: 'center' }}>
                      <CircleCheckBox
                        checked={item.isSelect}
                        onToggle={() => this.handleClick(item)}
                        outerColor={'#5b5b7e'}
                        innerColor={'#5b5b7e'}
                      />
                    </View>
                  </View>
                )
              })
            }
            <View style={{ height: height(10), width: width(70), justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' }}>
              <Text style={{ fontSize: totalSize(1.4), color: 'gray', textAlign: 'center' }}>Recurring events will automatically created as soon as the
                current one has been completed</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity 
          onPress={()=> this.props.navigation.goBack()}
          style={{ height: height(9), width: width(100), backgroundColor: '#1d1d4f', justifyContent: 'center', marginTop: 10, alignItems: 'center' }}>
          <Image source={require('../img/done.png')} style={{ height: height(9), width: width(100) }} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },

});
