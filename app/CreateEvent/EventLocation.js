/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, CheckBox, ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import EventLocComp from './EventLocComp';
import LatLon from 'mt-latlon';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(8), justifyContent: 'center', backgroundColor: '#1d1d4f' }}>
        <View style={{ height: height(7), width: width(85), flexDirection: 'row' }}>
          <View style={{ width: width(67), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
            <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
            <Text style={{ fontSize: totalSize(2), color: 'white' }}>Event Location</Text>
          </View>
          <View style={{ width: width(18), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: totalSize(1.5), color: 'white' }}></Text>
          </View>
        </View>
        { /* <View style={{height:height(7),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5.5),width:width(80),backgroundColor:'#33327e',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/event_search.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
              </TouchableOpacity>
              <View style={{height:height(7),width:width(70)}}>
                <TextInput
                  onChangeText={(value) => this.validate(value,'mobNo')}
                  placeholder={"Post/ZipCode"}
                  placeholderTextColor = 'white'
                  style={{fontSize:totalSize(2),color:'white',backgroundColor:'#33327e',width:width(65)}}
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                  />
              </View>
            </View>
            <TouchableOpacity style={{height:height(5.5),width:width(10),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/cross-icon.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
          </View>*/}

      </View>

    );
  }
}
@observer export default class EventLocation extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      places: [],
      id: '',
      lat: null,
      long: null,
      address: '',
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f',
    },
    headerTintColor: 'white',
  }
  componentWillMount() {
    let { orderStore } = Store;
    // 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI '
    // https://developers.google.com/places/web-service/search   (#reference link for google api)
    // let { params } = this.props.navigation.state;
    // this.setState({ loading: true })
    // let api_key = 'AIzaSyDrhwcy9mCZMdTvcFME4epizyGfbWJ-BOw';
    // fetch('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + orderStore.currentLocation.latitude + ',' + orderStore.currentLocation.longitude + '&fields=photos,formatted_address,name,rating,opening_hours,geometry,distance&radius=50000&type=sportsComplex&keyword=' + params.sportName + '&key=' + api_key)
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     console.log('data location:', responseJson);
    //     orderStore.sportEventLoc = responseJson.results;
    //     if (responseJson.status === 'OK') {
    //       for (var i = 0; i < orderStore.sportEventLoc.length; i++) {
    //         this.getAdd(orderStore.sportEventLoc[i])
    //       }
    //       this.setState({ loading: false })
    //       // console.log('location=',orderStore.sportEventLoc);
    //     }
    //   }).catch((error) => {
    //     this.setState({ loading: false })
    //     ToastAndroid.show('error', ToastAndroid.SHORT);
    //   });
  }
  //https://developers.google.com/maps/documentation/geocoding/start (reference link for geocoding api reverse get address from lat long)
  getAdd(place) {
    let { orderStore } = Store;
    let api_key = 'AIzaSyDrhwcy9mCZMdTvcFME4epizyGfbWJ-BOw';
    fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + place.geometry.location.lat + ',' + place.geometry.location.lng + '&key=' + api_key)
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === 'OK') {
          // console.warn('address==', responseJson);
          place.address = responseJson.results[0].formatted_address,
            place.selected = false;
          this.setState({
            places: orderStore.sportEventLoc
          })
          // console.warn('new obj=', orderStore.sportEventLoc);
          var lat1 = place.geometry.location.lat;
          var long1 = place.geometry.location.lng;
          var p1 = new LatLon(lat1, long1);
          // console.warn('point',p1);
          var p2 = new LatLon(orderStore.currentLocation.latitude, orderStore.currentLocation.longitude);
          // console.warn('point',p2);
          var dist = p1.distanceTo(p2);
          place.distance = Math.round(dist, 1);
          // console.log('new obj=', orderStore.sportEventLoc);
        }
      })

  }
  async selectPlace(item, status) {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    for (var i = 0; i < orderStore.sportEventLoc.length; i++) {
      if (item.id === orderStore.sportEventLoc[i].id && status === true) {
        orderStore.sportEventLoc[i].selected = true;
        orderStore.placeId = orderStore.sportEventLoc[i].id;
        //** for event edit screen
        if (orderStore.editLocation.status) {
          console.warn('location');
          
          // console.log('status=',orderStore.editLocation.status);
          await params.selectPlace(item.geometry.location.lat, item.geometry.location.lng, orderStore.sportEventLoc[i].address);
          // orderStore.editLocation.latitude = item.geometry.location.lat;
          // orderStore.editLocation.longitude = item.geometry.location.lng;
          // orderStore.editLocation.address = orderStore.sportEventLoc[i].address;
        }
        this.setState({
          id: orderStore.sportEventLoc[i].id,
          lat: item.geometry.location.lat,
          long: item.geometry.location.lng,
          address: orderStore.sportEventLoc[i].address,
        })
      } else {
        orderStore.sportEventLoc[i].selected = false;
      }
    }
    // console.log('id=',orderStore.placeId);
  }
  getPredictions(postalCode){
    let { orderStore } = Store;
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    if(postalCode.length!==0){
      fetch('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + postalCode + '&key=' + api_key )
          .then((response) => response.json())
          .then((responseJson) => {
          console.log('Predictions', responseJson);
          if (responseJson.status === 'OK') {
              orderStore.predictions = responseJson.predictions;
              for(var i=0;i<orderStore.predictions.length;i++){
                orderStore.predictions[i].added = false;
                orderStore.predictions[i].checkStatus = false;
              }
              this.setState({loading: false})
           }
     })
    }else{
        orderStore.predictions=[];
        this.setState({loading: false})
    }
  }
  getLatLong(item){
    let { orderStore } = Store;
    this.setState({address: item.description})
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + item.description + '&key=' + api_key )
          .then((response) => response.json())
          .then(async(responseJson) => {
          console.warn('latLong response', responseJson);
          if (responseJson.status === 'OK') {
              await this.setState({
                 latitude: responseJson.results[0].geometry.location.lat,
                 longitude: responseJson.results[0].geometry.location.lng,
                //  address: responseJson.results[0].formatted_address,
              })
              orderStore.predictions = [];
              this.setState({loading: false})
           }
     })
  }
  selectLocation() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    this.setState({ loading: true })
    // var url = 'http://play4team.com/AdminApi/api/addEventLocation?eventId='+ params.data.id +'&latitude='+ this.state.lat +'&longitude='+ this.state.long+'&location='+this.state.address;
    // console.log('url=',url);
    fetch('http://play4team.com/AdminApi/api/addEventLocation?eventId=' + params.data.id + '&latitude=' + this.state.lat + '&longitude=' + this.state.long + '&location=' + this.state.address, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === 'True') {
          ToastAndroid.show('location is selected', ToastAndroid.SHORT);
          this.setState({ loading: false })
          this.props.navigation.navigate('Invitation');
        } else {
          this.setState({ loading: false })
          ToastAndroid.show(responseJson.msg, ToastAndroid.SHORT);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('error', ToastAndroid.SHORT);
      });

  }
  render() {
    let { orderStore } = Store;
    // console.log('render obj=',orderStore.sportEventLoc);
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1, backgroundColor: 'rgba(29,37,86,0.6)' }} >
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={{ height: height(5), backgroundColor: '#eaedf2', justifyContent: 'center' }}>
          <Text style={{ fontSize: totalSize(1.8), color: '#7a7c98', marginLeft: 15 }}>Location</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <View style={{ borderRadius: 5, margin: 15, marginTop: 10, marginBottom: 5, backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.getPredictions(value)}
              underlineColorAndroid='transparent'
              placeholder='Postal/Zip Code'
              value={this.state.address.length > 0 ? this.state.address : null}
              onFocus={() => { this.setState({ address: '' }) }}
              placeholderTextColor='gray'
              keyboardType='email-address'
              underlineColorAndroid='transparent'
              autoCorrect={true}
              style={{ height: height(6), width: width(90), textAlign: 'center', fontSize: totalSize(1.5), marginTop: 0, backgroundColor: 'white', paddingLeft: 10 }}
            />
          </View>
          <View style={{ height: height(80), width: width(93), alignItems: 'center', margin: 15, marginBottom: 5 }}>
            <ScrollView>
              {
                orderStore.predictions.map((item, key) => {
                  return (
                    <TouchableOpacity key={key} style={item.selected === false ? styles.unselected : styles.selected}
                      onPress={async() => {
                        await this.selectPlace(item, true);
                        await this.getLatLong(item);
                      }}
                    >
                      <View style={{ width: width(20), justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../img/event_loc.png')} style={{ height: height(7), width: width(12), resizeMode: 'contain' }} />
                        {/* <Text style={{ fontSize: totalSize(1.2), color: 'black' }}>{item.distance} miles</Text> */}
                      </View>
                      <View style={{ width: width(70), justifyContent: 'center' }}>
                        <View style={{ height: height(4), justifyContent: 'center', alignItems: 'flex-start' }}>
                          <Text style={{ fontSize: totalSize(1.9), color: 'black' }}>{item.structured_formatting.main_text}</Text>
                        </View>
                        <View style={{ height: height(5), justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                          <Text style={{ fontSize: totalSize(1.4), color: 'gray' }}>{item.description}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                })
              }
            </ScrollView>
          </View>
        </View>
        <TouchableOpacity style={{ height: height(7) }}
          onPress={() => {
            if (orderStore.editLocation.status === true) {
              this.props.navigation.goBack()
            } else {
              this.selectLocation()
            }
          }}
        >
          <Image source={require('../img/next.png')} style={{ height: height(7), width: width(100) }} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },
  selected: {
    height: height(12),
    width: width(93),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    marginBottom: 0.4,
    borderBottomWidth: 0.3,
    borderColor: 'gray',
    backgroundColor: '#aedeff',
  },
  unselected: {
    height: height(11),
    width: width(93),
    flexDirection: 'row',
    marginBottom: 0.4,
    borderBottomWidth: 0.3,
    borderColor: 'gray'
  },
});
