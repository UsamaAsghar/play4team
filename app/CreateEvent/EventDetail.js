import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, RefreshControl, Image, ScrollView, TouchableOpacity, ToastAndroid, Alert
} from 'react-native';
import EditIcon from 'react-native-vector-icons/FontAwesome'
import BackArrow from 'react-native-vector-icons/AntDesign'
import { width, height, totalSize } from 'react-native-dimension';
import { WP, HP } from '../Styling'
import { observer } from 'mobx-react';
import Store from '../Stores';
import MapView, { Polyline, Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
let _this = null;
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(70), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Event Detail</Text>
        </View>
        <TouchableOpacity style={{ alignSelf: 'center' }}>
            <EditIcon name='edit' size={30} color="#5ae758" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
      </View>
    );
  }
}
@observer export default class EventDetail extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      edit: true,
      chat: false,
      join: false,
      status: {},
    }
    _this = this;
  }
  static navigationOptions =({ navigation })=> ({
    header: (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <TouchableOpacity style={{ width: width('15'), alignSelf: 'center' }}
           onPress={()=> navigation.goBack()}
        >
            <BackArrow name='arrowleft' size={25} color="#fff" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
        <View style={{ width: width(70), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Event Detail</Text>
        </View>
        <TouchableOpacity style={{ width: width('15'), alignSelf: 'center' }}
            onPress={() => {
              _this.editHandler()
            }}
        >
            <EditIcon name='edit' size={30} color="#5ae758" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
      </View>
    ),
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',

  })
  editHandler = () => {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    if (orderStore.checkToken === 'teamInvitation') {
      this.alert()
    } else {
      this.buttons('edit')
      this.props.navigation.navigate('ReviewEvent', { eventDetail: params.eventItem })
    }
  }
  componentWillMount = async() => {
    await this.eventDetails()
  }
  eventDetails() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    this.setState({ loading: true })
    // var url = 'http://play4team.com/AdminApi/api/EventPlayers?eventId=' + params.eventItem.id;
    // console.log('url eventDetails==========:',url);
    fetch('http://play4team.com/AdminApi/api/EventPlayers?eventId=' + params.eventItem.id, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log('[EventDetail.js] componentWillMount===:', responseJson);
        this.setState({ loading: false })
        if (responseJson.status === 'True') {
          this.setState({ status: responseJson })
          // console.log('status=',this.state.status);
        } else {
          this.setState({ loading: false })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  buttons(btn) {
    if (btn === 'edit') {
      this.setState({
        edit: true,
        chat: false,
        join: false,
      })
    } else {
      if (btn === 'chat') {
        this.setState({
          edit: false,
          chat: true,
          join: false,
        })
      } else {
        this.setState({
          edit: false,
          chat: false,
          join: true,
        })
      }
    }
  }
  alert() {
    Alert.alert(
      'Waring',
      'You can not edit this event',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false }
    )
  }
  componentWillUnmount() {
    let { orderStore } = Store;
    orderStore.checkToken = '';
  }
  render() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    // console.warn('event=',params.eventItem);
    if (this.state.loading == true) {
      return (
        <OrientationLoadingOverlay
          visible={true}
          color="white"
          indicatorSize="small"
          messageFontSize={18}
          message="Loading..."
        />
      );
    }
    return (
      <ScrollView 
        refreshControl={
          <RefreshControl
            enabled
            onRefresh={()=> this.eventDetails()}
            refreshing={this.state.loading}
            size={24}
            progressBackgroundColor={'#fff'}
            colors={['#33327e','#5ae758']}
          />
        }
        style={styles.container}>
        <View style={{ width: width(100) }}>
          <ImageBackground source={require('../img/eventbanner.jpeg')} style={{ height: height(30), width: width(100), resizeMode: 'stretch', justifyContent: 'flex-end' }}>
            {/* <View style={{ height: height(11), backgroundColor: '#33327e', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity style={{ height: height(7), width: width(27), margin: 3 }}
                onPress={() => {
                  if (orderStore.checkToken === 'teamInvitation') {
                    this.alert()
                  } else {
                    this.buttons('edit')
                    this.props.navigation.navigate('ReviewEvent', { eventDetail: params.eventItem })
                  }
                }}
              >
                {
                  this.state.edit === true ?
                    <Image source={require('../img/edit-active.jpg')} style={{ height: height(6.5), width: width(27), borderRadius: 3 }} />
                    :
                    <Image source={require('../img/edit.png')} style={{ height: height(6.5), width: width(27), resizeMode: 'contain' }} />
                }
              </TouchableOpacity>
              <TouchableOpacity style={{ height: height(7), width: width(27), margin: 3 }} onPress={() => { this.buttons('chat') }}>
                {
                  this.state.chat === true ?
                    <Image source={require('../img/chat-active.jpg')} style={{ height: height(6.5), width: width(27), borderRadius: 3 }} />
                    :
                    <Image source={require('../img/chat.png')} style={{ height: height(6.5), width: width(27), resizeMode: 'contain' }} />
                }
              </TouchableOpacity>
              <TouchableOpacity style={{ height: height(7), width: width(27), margin: 3 }} onPress={() => { this.buttons('join') }}>
                {
                  this.state.join === true ?
                    <Image source={require('../img/join-active.jpg')} style={{ height: height(6.5), width: width(27), borderRadius: 3 }} />
                    :
                    <Image source={require('../img/join.png')} style={{ height: height(6.5), width: width(27), resizeMode: 'contain' }} />
                }
              </TouchableOpacity>
            </View> */}
          </ImageBackground>
        </View>
        <View style={{ height: height(23), width: width(92), marginHorizontal: 10, marginTop: 10, alignSelf: 'center', borderRadius: 3, marginBottom: 1, backgroundColor: '#fdfdfd', elevation: 2 }}>
          <View style={{ height: height(5), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: totalSize(1.7), color: 'black' }}>{params.eventItem.Event_Name}</Text>
          </View>
          <View style={{ height: height(16), flexDirection: 'row' }}>
            <View style={{ height: height(15), width: width(21), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: WP('15'), width: WP('15'), justifyContent: 'center', alignItems: 'center', borderRadius: WP('30'), borderColor: '#5ae758', borderWidth: 2 }}>
                <Text style={{ fontSize: totalSize(2.5), color: 'black', marginTop: 3 }}>{this.state.status.attendingPlayers}</Text>
              </View>
              <Text style={{ fontSize: totalSize(1.5), color: 'black', marginTop: 3 }}>In</Text>
            </View>
            <View style={{ height: height(15), width: width(21), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: WP('15'), width: WP('15'), justifyContent: 'center', alignItems: 'center', borderRadius: WP('30'), borderColor: '#33336e', borderWidth: 2 }}>
                <Text style={{ fontSize: totalSize(2.5), color: 'black', marginTop: 3 }}>{this.state.status.pendingPlayers}</Text>
              </View>
              <Text style={{ fontSize: totalSize(1.5), color: 'black', marginTop: 3 }}>Pending</Text>
            </View>
            <View style={{ height: height(15), width: width(21), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: WP('15'), width: WP('15'), justifyContent: 'center', alignItems: 'center', borderRadius: WP('30'), borderColor: '#099792', borderWidth: 2 }}>
                <Text style={{ fontSize: totalSize(2.5), color: 'black', marginTop: 3 }}>{this.state.status.rejectedPlayers}</Text>
              </View>
              <Text style={{ fontSize: totalSize(1.5), color: 'black', marginTop: 3 }}>Out</Text>
            </View>
            <View style={{ height: height(15), width: width(21), justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity style={{ height: height(11), width: width(17), borderRadius: 100 }} 
                // onPress={() => { this.props.navigation.navigate('RequestTeamMembers', { event: params.eventItem }) }} 
              >
                {/* <Image source={require('../img/requests.jpg')} style={{ height: height(10.5), width: width(17), resizeMode: 'contain' }} /> */}
                <Image source={{uri: 'http://play4team.com/AdminApi/public/sports_images/'+ params.eventItem.sport_image}} style={{ height: height(10.5), width: width(17), resizeMode: 'contain' }} />
              </TouchableOpacity>
              <Text style={{ fontSize: totalSize(1.5), color: 'black', marginTop: 0 }}>Requests</Text>
            </View>
            <View style={{ height: height(15), width: width(8), justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../img/arrow-right.jpg')} style={{ height: height(3), width: width(3), marginBottom: 25 }} />
            </View>
          </View>
          {/* <View style={{height:height(10)}}>
            <View style={{height:height(7),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
              <Image source={require('../img/team2.jpg')} style={{height:height(4),width:width(13),margin:3,resizeMode:'contain'}} />
              <Image source={require('../img/vs.jpg')} style={{height:height(6.7),width:width(11),margin:3,resizeMode:'contain'}} />
              <Image source={require('../img/team1.jpg')} style={{height:height(4),width:width(13),margin:3,resizeMode:'contain'}} />
            </View>
            <View style={{height:height(3),justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:totalSize(1.5),color:'gray'}}>Pick Team</Text>
            </View>
          </View> */}
        </View>
        <Text style={{ fontSize: totalSize(1.7), color: '#33327e', alignSelf: 'center', marginVertical: 5 }}>Location</Text>
        <View style={{ height: height(24), width: width(92), margin: 15, marginTop: 0, backgroundColor: '#c4c4c4', borderRadius: 3, overflow:'hidden',alignItems: 'center' }}>
          <MapView
            zoomEnabled={true}
            zoomControlEnabled={true}
            toolbarEnabled={true}
            showsCompass={true}
            showsBuildings={true}
            showsIndoors={true}
            provider={PROVIDER_GOOGLE}
            showsMyLocationButton={true}
            showsUserLocation={true}
            followsUserLocation={true}
            minZoomLevel={5}
            maxZoomLevel={20}
            mapType={"standard"}
            loadingEnabled={true}
            loadingIndicatorColor={'white'}
            loadingBackgroundColor={'gray'}
            moveOnMarkerPress={true}
            style={{
              height: height(24),
              width: width(92)
            }}
            region={{
              latitude: parseFloat(params.eventItem.latitude) || 31.179,
              longitude: parseFloat(params.eventItem.longitude) || 70.435,
              latitudeDelta: 2*0.0922,
              longitudeDelta: 2*0.0421
            }}
          // onRegionChange={this.onRegionChange.bind(this)}
          >
            {
              params.eventItem.latitude && params.eventItem.longitude ?
                <MapView.Marker
                  coordinate={
                    { latitude: parseFloat(params.eventItem.latitude), longitude: parseFloat(params.eventItem.longitude) }
                  }
                  title={params.eventItem.Event_Name}
                  description={""}
                  // onPress={()=>alert('player')}
                  pinColor={'red'}
                // image={require('../img/friend_img.png')}
                >

                </MapView.Marker>
                : null
            }
          </MapView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
