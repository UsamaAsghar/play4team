import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';


class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>My Group</Text>
          </View>
        </View>
    );
  }
}
export default class Group extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,alignItems:'center'}}>
          <TouchableOpacity style={{height:height(7),width:width(90),flexDirection:'row',margin:5,borderBottomWidth:0.5,borderColor:'gray'}}>
            <View style={{width:width(80),justifyContent:'center'}}>
              <Text style={{fontSize:totalSize(1.7),color:'black',marginLeft:5}}>ABC</Text>
              <Text style={{fontSize:totalSize(1.7),color:'gray',marginLeft:5}}>1 Member</Text>
            </View>
            <View style={{width:width(10),justifyContent:'center',alignItems:'flex-end'}}>
              <Image source={require('../img/ic_arrow_rit.png')} />
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{height:height(8),width:width(100)}}>
          <Image source={require('../img/new_group.png')} style={{height:height(8),width:width(100)}} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
