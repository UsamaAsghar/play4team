import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';

class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(8),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>My Group</Text>
          </View>
        </View>
    );
  }
}
export default class MyGroup extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static navigationOptions = {
    headerTitle :<LogoTitle />,
    headerStyle: {
      backgroundColor:'#1d1d4f'
    },
    headerTintColor :'white'
  }
  render() {
    return(
      <View style={styles.container}>
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(30),width:width(80)}}>
            <View style={{height:height(15),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/gruop_icon.png')} />
            </View>
            <View style={{height:height(15),flexWrap:'wrap'}}>
              <Text style={{fontSize:totalSize(1.7),color:'gray',textAlign:'center'}}>Player groups alow you to create a group of players so you can quickly and easily step games and contact them</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity style={{height:height(8),width:width(100)}}>
          <Image source={require('../img/new_group.png')} style={{height:height(8),width:width(100)}} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex:1,
    margin:20,

    marginBottom:5
  },
  strip: {
    height:height(8),
    backgroundColor:'#ffffff',
    marginBottom:5,
    flexDirection:'row',
    borderRadius:5
  },
});
