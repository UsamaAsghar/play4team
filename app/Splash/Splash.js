import React, { Component } from 'react';
import {
    // AppRegistry,
    StyleSheet,
    Image,
     View,
     Text,
     ScrollView,
    // TouchableOpacity
} from 'react-native';

export default class Splash extends Component {
  componentDidMount(){
        setTimeout(() => {
            this.props.navigation.navigate('MainScreen');
        }, 3000);
    }
    static navigationOptions = {
          header: null
      }
    render() {
        return (
              <View style={styles.Container}>
                <Image source={require('../img/splash.jpg')} style={{flex:1,width:null,height:null}} />
              </View>
        );
    }
}

const styles = StyleSheet.create({
  Container:{
    flex:1,
  },
  Background:{
    width:null
  },
  logo:{
    flex:1,
    margin:2,
    justifyContent:'center',
    alignItems:'center'
  }

});
