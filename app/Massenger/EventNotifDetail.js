import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,Alert
} from 'react-native';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import DateTimePicker from 'react-native-datepicker'
import  { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

@observer export default class EventNotifDetail extends Component<Props> {
  constructor(props){
      super(props);
      this.state = {
        loading: false,
        status: {},
        mapRegion: null,
        checked: 0,
      }
    }
  static navigationOptions = {
    header: null
    }
  componentWillMount(){
      let { orderStore } = Store;
      let {params} = this.props.navigation.state;
      let region = {
          latitude:       orderStore.currentLocation.latitude,
          longitude:      orderStore.currentLocation.longitude,
          latitudeDelta:  0.0922,
          longitudeDelta: 0.0421
       }
      this.setState({
        mapRegion: region,
      })
      // console.log('region=',region);
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/EventPlayers?eventId='+ params.data.id, {
          method: 'POST',
          }).then((response) => response.json())
         .then((responseJson) => {
          // console.log('responseJson=',responseJson);
          this.setState({loading: false})
          if (responseJson.status === 'True') {
              this.setState({status: responseJson})
              // console.log('status=',this.state.status);
              }else {
                 this.setState({loading: false})
                 // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
              }
           }).catch((error)=>{
              this.setState({loading: false})
              // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
          });

          if (params.data.checked === 0) {
              Alert.alert(
                'Invitation of Event',
                'Please click on Accept button to attend this event or reject button to reject this invitation ',
                [
                {text: 'Ask me later', onPress: () => this.props.navigation.push('Messaging')},
                {text: 'Reject', onPress: () => this.acceptRejectInvitation(0)},
                {text: 'Accept', onPress: () => this.acceptRejectInvitation(1)},
                ],
                { cancelable: false }
                )
           }else {
              this.setState({checked: 1})
           }
    }
    acceptRejectInvitation(status){
      let { orderStore } = Store;
      let {params} = this.props.navigation.state;
      this.setState({
        loading: true,
        checked: status
      })
      fetch('http://play4team.com/AdminApi/api/changePlayerEventStatus?playerId='+orderStore.playerLoginRes.id+'&eventId='+ params.data.id+'&status='+status, {
           method: 'POST',
           }).then((response) => response.json())
             .then((responseJson) => {
              console.log('responseJson=',responseJson);
              this.setState({loading: false})
              if (responseJson.status === 'True' && status === 1) {
                  // this.setState({checked: true})
                  ToastAndroid.show('You accepted this invitation', ToastAndroid.LONG);
                }else {
                this.setState({loading: false})
                this.props.navigation.push('Messaging');
                ToastAndroid.show('You rejected this invitation', ToastAndroid.LONG);
                }
           }).catch((error)=>{
                this.setState({loading: false})
                // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        });
    }
  render() {
    // console.log('mapRegion=',this.state.mapRegion);
    let { orderStore } = Store;
    let {params} = this.props.navigation.state;
    let lat = parseFloat(params.data.latitude);
    let long = parseFloat(params.data.longitude);
    if(this.state.loading == true){
        return(
          <OrientationLoadingOverlay
              visible={true}
              color="white"
              indicatorSize="small"
              messageFontSize={18}
              message="Loading..."
          />
        );
    }
    return (
      <View style={styles.container}>
        <View style={{height:height(35),width:width(100)}}>
          <ImageBackground source={require('../img/top-bg.jpg')} style={{height:height(35),width:width(100),justifyContent:'flex-end'}}>
            {
              this.state.checked === 1?
                <View style={{height:height(11),justifyContent:'center',marginBottom:10}}>
                  <Text style={{height:height(3),fontSize:totalSize(2.2),color:'white',marginLeft:10}}>{params.data.Event_Name}</Text>
                  <Text style={{height:height(2.5),fontSize:totalSize(1.7),color:'white',marginLeft:10}}>Start from {params.data.Start_Date} to {params.data.End_Date}</Text>
                </View>
                :
                null
            }
          </ImageBackground>
        </View>
        {
          this.state.checked === 1 ?
            <View style={{flex:1}}>
              <View style={{height:height(21),width:width(92),margin:15,backgroundColor:'#fdfdfd'}}>
                <View style={{height:height(5),justifyContent:'center',alignItems:'center'}}>
                  <Text style={{fontSize:totalSize(1.7),color:'black'}}>{params.data.Event_Name}</Text>
                </View>
                <View style={{height:height(16),flexDirection:'row'}}>
                  <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#5ae758',borderWidth:2}}>
                      <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.attendingPlayers}</Text>
                    </View>
                    <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>In</Text>
                  </View>
                  <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#33336e',borderWidth:2}}>
                      <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.pendingPlayers}</Text>
                    </View>
                    <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>Pendding</Text>
                  </View>
                  <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:height(10),width:width(17),justifyContent:'center',alignItems:'center',borderRadius:100,borderColor:'#099792',borderWidth:2}}>
                      <Text style={{fontSize:totalSize(2.5),color:'black',marginTop:3}}>{this.state.status.rejectedPlayers}</Text>
                    </View>
                    <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:3}}>rejected</Text>
                  </View>
                  <View style={{height:height(15),width:width(21),justifyContent:'center',alignItems:'center'}}>
                    <View style={{height:height(11),width:width(17),borderRadius:100}}>
                      <Image source={require('../img/requests.jpg')} style={{height:height(10.5),width:width(17)}} />
                    </View>
                    <Text style={{fontSize:totalSize(1.5),color:'black',marginTop:0}}>Requests</Text>
                  </View>
                  <View style={{height:height(15),width:width(8),justifyContent:'center',alignItems:'center'}}>
                    <Image source={require('../img/arrow-right.jpg')} style={{height:height(3),width:width(3),marginBottom:25}} />
                  </View>
                </View>
              </View>
              <View style={{height:height(35),width:width(100),margin:0,marginTop:0,backgroundColor:'#ffffff',alignItems:'center'}}>
                <Text style={{fontSize:totalSize(1.7),color:'#33327e'}}>Location</Text>
                <MapView
                    zoomEnabled={true}
                    zoomControlEnabled={true}
                    toolbarEnabled={true}
                    showsCompass={true}
                    showsIndoors={true}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    minZoomLevel={5}
                    maxZoomLevel = {20}
                    mapType = {"standard"}
                    style={styles.map}
                    region={this.state.mapRegion}
                    // onRegionChange={this.onRegionChange.bind(this)}
                    >
                    <MapView.Marker
                      coordinate={
                      { latitude: orderStore.currentLocation.latitude, longitude: orderStore.currentLocation.longitude }
                      }
                      title={'My Current location'}
                      description={''}
                      pinColor={'red'}
                      />
                      <MapView.Marker
                        coordinate={
                        { latitude: lat, longitude: long }
                        }
                        title={'Event location'}
                        description={''}
                        pinColor={'#3edc6d'}
                        />
                      <Polyline
                            coordinates={[
                              { latitude: orderStore.currentLocation.latitude , longitude: orderStore.currentLocation.longitude  },
                              { latitude: lat, longitude: long }
                            ]}
                            strokeColor="red" // fallback for when `strokeColors` is not supported by the map-provider
                            strokeWidth={2}
                       />
                  </MapView>
              </View>
            </View>
            :
            <View style={{flex:1,justifyContent:'center',alignItems:'center',flexWrap:'wrap'}}>
              <Text style={{fontSize:totalSize(2.5),color:'black',textAlign:'center'}}>This invitation is rejected by you,So you can not see its detail</Text>
            </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
  height:height(35),
  width:width(100),
  zIndex : -10,
  position: 'absolute',
},
});
