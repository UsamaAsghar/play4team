
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid,Image,ImageBackground,ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import  { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
export default class NotificationList extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static navigationOptions = {
    header: null
  }
  render() {
    let { orderStore } = Store;
    return(
      <TouchableOpacity style={{height:height(10),backgroundColor:'#e5eff9',flexDirection:'row',marginLeft:0,borderBottomWidth:0.5,borderColor:'gray'}}
        onPress={()=>{
          this.props.navigation.navigate('EventNotifDetail',{data: this.props.item})
        }}
      >
        <View style={{width:width(18),justifyContent:'center',alignItems:'center'}}>
          <Image source={require('../img/notification_img.png')} style={{height:height(8.5),width:width(14)}} />
        </View>
        <View style={{width:width(82),justifyContent:'center'}}>
          <Text style={{fontSize:totalSize(1.7),color:'gray'}}>{this.props.item.msg}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },

});
