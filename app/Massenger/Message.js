import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ToastAndroid, Image, ImageBackground, ScrollView, TouchableOpacity, TextInput, alert
} from 'react-native';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
const IntervalId = 0;
export default class Message extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      message: '',
      messages: [],
      receiverId: '',
      chatId: ''
    };
  }
  static navigationOptions = ({ navigation, orderStore }) => ({
    headerTitle: 'Chat',
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTitleStyle: {
      fontWeight: 'normal',
      fontSize: totalSize(2)
    }
  })

  
  componentWillMount = async () => {
    let { params } = this.props.navigation.state;
    let { orderStore } = Store;

    console.log("testing params --> ",orderStore.playerLoginRes );
    if(params.isFromNotifcation){
      console.log("got params from notifications", params);
      await this.setState({ 
        chatId: params.chatId,
        receiverId: params.receiverId
     })
    }else{
      if (orderStore.chatStatus === true) {
        for (var i = 0; i < orderStore.chatList.length; i++) {
          if (params.item.id === orderStore.chatList[i].receiver_id) {
            await this.setState({ chatId: orderStore.chatList[i].chatId })
          }
        }
      }
    }

    // console.log('chatId=',this.state.chatId);
    this.setState({ loading: true  })
    IntervalId = setInterval(async () => {
      await this.getChat()
    }, 2000);

  }
  componentWillUnmount() {
    clearInterval(IntervalId);
  }
  getChat() {
    let { params } = this.props.navigation.state;
    let { orderStore } = Store;
    if(params.isFromNotifcation){
      var url = 'http://play4team.com/AdminApi/api/getChat?chatId=' + params.chatId;
    }else{
      if (orderStore.chatStatus === true) {
        var url = 'http://play4team.com/AdminApi/api/getChat?chatId=' + this.state.chatId;
      } else {
        var url = 'http://play4team.com/AdminApi/api/getChat?chatId=' + params.item.chatId;
      }
    }
    console.log('Send message url=', url);
    // this.setState({loading: true})
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        orderStore.chat = responseJson.chat;
        this.setState({ loading: false })
        // console.log('get Chat Data=', orderStore.chat);
        if (responseJson.status === 'True') {
          // ToastAndroid.show('Done', ToastAndroid.LONG);
        }
      }).catch((error) => {
        console.log('error=======>>>>>>',error)
        this.setState({ loading: false })
        // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
      });
  }
  onSendMessage() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    // console.log('props are=',params.item);
    // if (orderStore.playerLoginRes.Status === 0) {
    //   var senderType = 'player';
    // } else {
    //   var senderType = 'team';
    // }
    // if (orderStore.chatStatus === true) {
    //   if (params.item.Status === 0) {
    //     var receiverType = 'player';
    //   } else {
    //     var receiverType = 'team';
    //   }
    // } else {
    //   if (params.item.Status === 0) {
    //     var receiverType = 'player';
    //   } else {
    //     var receiverType = 'team';
    //   }
    // }
    if (params.isFromNotifcation) {
      // `http://play4team.com/AdminApi/api/sendMessage?senderId=20&receiverId=21&senderType=team&receiverType=team&message=trdt`
      var url = 'http://play4team.com/AdminApi/api/sendMessage?senderId=' + orderStore.playerLoginRes.id + '&receiverId=' + params.receiverId + '&senderType=team' + '&receiverType=team' + '&message=' + this.state.message;
    } else {
      if (orderStore.playerLoginRes.id !== params.item.receiver_id) {
        var rec_id = params.item.receiver_id;
      } else {
        var rec_id = params.item.sender_id;
      }
      if (orderStore.chatStatus === true) {
        var url = 'http://play4team.com/AdminApi/api/sendMessage?senderId=' + orderStore.playerLoginRes.id + '&receiverId=' + params.item.id + '&senderType=' + 'team' + '&receiverType=' + 'team' + '&message=' + this.state.message;
      } else {
        var url = 'http://play4team.com/AdminApi/api/sendMessage?senderId=' + orderStore.playerLoginRes.id + '&receiverId=' + rec_id + '&senderType=' + 'team' + '&receiverType=' + 'team' + '&message=' + this.state.message;
      }
    }
    console.log('Send message url=2222',url);
    this.setState({ loading: true })
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then(fun = async (responseJson) => {
        await this.setState({ chatId: responseJson.chatId, loading: false })
        await this.chatList();
        // console.log('Send message Data=', responseJson);
        // console.log('Send message Data=', this.state.chatId);
        if (responseJson.status === 'true') {
          orderStore.chat.push({
            sender_id: orderStore.playerLoginRes.id,
            message: this.state.message
          });

          //   if (orderStore.chatStatus===true) {
          //     for (var i = 0; i < orderSt ore.chatList.length; i++) {
          //       if ( params.item.id === orderStore.chatList[i].receiver_id ) {
          //          await this.setState({chatId: orderStore.chatList[i].chatId})
          //       }
          //     }

          //  }
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
      });
  }
  chatList = () => {
    let { params } = this.props.navigation.state;
    let { orderStore } = Store;
    if (orderStore.playerLoginRes.Status === 0) {
      var type = 'player';
    } else {
      var type = 'team';
    }
    var url = 'http://play4team.com/AdminApi/api/usersChat?userId=' + orderStore.playerLoginRes.id + '&type=' + type;
    // console.log('Chat list url=',url);
    // this.setState({loading: true})
    fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        orderStore.chatList = responseJson.data;
        console.log('get chatlist Data=', orderStore.chatList );
        if (responseJson.status === 'True') {
          this.setState({ loading: false })
          // ToastAndroid.show('Done', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    if (this.state.loading == true) {
      return (
        <View style={{ flex: 1, backgroundColor: 'transparent', position: 'absolute' }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#f6f6f6' }}>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          {
            orderStore.chat ?
              <ScrollView
                ref={ref => this.scrollView = ref}
                onContentSizeChange={(contentWidth, contentHeight) => {
                  this.scrollView.scrollToEnd({ animated: true });
                }}>
                {
                  orderStore.chat.map((item, key) => {
                    return (
                      <View key={key}>
                        {
                          orderStore.playerLoginRes.id === item.sender_id ?
                            <View style={{ width: width(70), marginVertical: 8, alignSelf: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row' }}>
                              <View style={{ justifyContent: 'center', borderRadius: 10, backgroundColor: 'white', elevation: 10, alignItems: 'flex-end' }}>
                                <Text style={{ fontSize: totalSize(1.8), color: 'black', marginHorizontal: 10 }}>{item.message}</Text>
                              </View>
                              {
                                orderStore.playerLoginRes.Path.length > 0 ?
                                  <Image source={{ uri: orderStore.playerLoginRes.Path }} style={{ height: 45, width: 45, borderRadius: 100, marginHorizontal: 10, alignSelf: 'center' }} />
                                  :
                                  <Image source={require('../img/userprofileImag.png')} style={{ height: 45, width: 45, borderRadius: 100, marginHorizontal: 10, alignSelf: 'center' }} />
                              }
                            </View>
                            :
                            <View style={{ width: width(70), marginVertical: 8, alignSelf: 'flex-start', flexDirection: 'row' }}>
                              
                              {
                                params.isFromNotifcation ?                                 
                                <Image source={require('../img/forum-user.png')} style={{ height: 45, width: 45, borderRadius: 100, marginHorizontal: 10, alignSelf: 'center' }} />
                                : 
                                <Image source={params.item.Path.length > 0 ? { uri: params.item.Path } : require('../img/forum-user.png')} style={{ height: 45, width: 45, borderRadius: 100, marginHorizontal: 10, alignSelf: 'center' }} />

                              }
                              <View style={{ justifyContent: 'center', alignItems: 'flex-start', borderRadius: 10, backgroundColor: 'white', elevation: 10, }}>
                                <Text style={{ fontSize: totalSize(1.8), color: 'black', marginHorizontal: 10 }}>{item.message}</Text>
                              </View>
                            </View>
                        }
                      </View>
                    );
                  })
                }
              </ScrollView>
              :
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>No Record!</Text>
              </View>
          }
        </View>
        <View style={{ height: height(10), width: width(100), justifyContent: 'center', flexDirection: 'row', backgroundColor: 'white', elevation: 10 }}>
          <TextInput
            onChangeText={(value) => this.setState({ message: value })}
            underlineColorAndroid='transparent'
            placeholder='Write message'
            placeholderTextColor='gray'
            underlineColorAndroid='transparent'
            autoCorrect={true}
            multiLines={true}
            keyboardAppearance='dark'
            style={{ height: height(6), width: width(65), alignSelf: 'center', fontSize: totalSize(1.6), color: 'black', borderRadius: 5, marginHorizontal: 10, backgroundColor: 'white', elevation: 5 }}
          />
          <TouchableOpacity style={{ elevation: 5, height: height(6), width: width(22), justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 5, marginHorizontal: 10, backgroundColor: '#1d1d4f' }}
            onPress={() => this.onSendMessage()}
          >
            <Text style={{ fontSize: totalSize(1.8), color: 'white', fontWeight: 'bold' }}>Send</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#f6f6f6',
  },
  subContainer: {
    flex: 1,
    margin: 20,

    marginBottom: 5
  },
  strip: {
    height: height(8),
    backgroundColor: '#ffffff',
    marginBottom: 5,
    flexDirection: 'row',
    borderRadius: 5
  },
});

