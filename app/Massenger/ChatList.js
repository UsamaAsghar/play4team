import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, DrawerLayoutAndroid, Image, ImageBackground, ScrollView, TouchableOpacity, ToastAndroid, alert
} from 'react-native';
import {width, height, totalSize} from 'react-native-dimension';
import {Avatar} from 'react-native-elements';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

const IntervalId = 0;
export default class ChatList extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            message: '',
            messages: [],
            receiverId: '',
        };
    }

    static navigationOptions = {
        header: null
    }

    componentWillMount() {
        let {params} = this.props.navigation.state;
        let {orderStore} = Store;
        this.setState({loading: true})
        setTimeout(() => {
            this.setState({loading: true})
        }, 3000);
        IntervalId = setInterval(() => {
            this.chatList()
        }, 5000);

    }

    componentWillUnmount() {
        clearInterval(IntervalId);
    }

    chatList = () => {
        let {params} = this.props.navigation.state;
        let {orderStore} = Store;
        if (orderStore.playerLoginRes.Status === 0) {
            var type = 'player';
        } else {
            var type = 'team';
        }
        var url = 'http://play4team.com/AdminApi/api/usersChat?userId=' + orderStore.playerLoginRes.id + '&type=' + type;
        // console.log('Chat list url=',url);
        // this.setState({loading: true})
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then((responseJson) => {
                orderStore.chatList = responseJson.data;
                this.setState({loading: false})
                // console.log('get chatlist Data=', orderStore.chatList );
                if (responseJson.status === 'True') {
                    this.setState({
                        messages: responseJson.data
                    })
                    // ToastAndroid.show('Done', ToastAndroid.LONG);
                }
            }).catch((error) => {
            this.setState({loading: false})
            // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
        });
    }

    render() {
        let {orderStore} = Store;
        if (this.state.loading == true) {
            return (
                <View style={{flex: 1, backgroundColor: 'transparent', position: 'absolute'}}>
                    <OrientationLoadingOverlay
                        visible={true}
                        color="white"
                        indicatorSize="small"
                        messageFontSize={18}
                        message="Loading..."
                    />
                </View>
            );
        }
        return (
            <View style={{flex: 1,}}>
                <ScrollView>
                    {
                        orderStore.chatList.length > 0 ?
                            orderStore.chatList.map((item, key) => {
                                return (
                                    <TouchableOpacity key={key} style={{
                                        height: height(8),
                                        width: width(95),
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        borderBottomWidth: 0.4,
                                        borderColor: 'gray',
                                        alignSelf: 'center'
                                    }}
                                                      onPress={() => {
                                                          orderStore.chatStatus = false,
                                                              this.props.navigation.navigate('Message', {
                                                                  item: item,
                                                                  chatList: this.chatList
                                                              })
                                                      }}>
                                        {
                                            item.Path.length > 0 ?
                                                <Avatar
                                                    rounded
                                                    size="medium"
                                                    source={{
                                                        uri: item.Path,
                                                    }}
                                                    containerStyle={{alignSelf: 'center'}}
                                                />
                                                :
                                                <Image
                                                    source={item.Path.length > 0 ? {uri: item.Path} : require('../img/contact.png')}
                                                    style={{
                                                        height: height(5),
                                                        width: width(10),
                                                        alignSelf: 'center',
                                                        resizeMode: 'contain'
                                                    }}/>
                                        }
                                        <View style={{flex: 1, alignItems: 'center', flexDirection: 'row'}}>
                                            <Text style={{
                                                flex: 1,
                                                fontSize: totalSize(1.8),
                                                color: 'black',
                                                marginHorizontal: 10
                                            }}>{item.name}</Text>
                                            <Text style={{
                                                fontSize: totalSize(1.8),
                                                color: 'black',
                                                marginHorizontal: 10
                                            }}>{item.visiblity === 1 ? "Online" : "Offline"}</Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            })
                            :
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                alignSelf: 'center',
                            }}>
                                <Text style={{marginTop: '60%', color: '#c4c4c4'}}>Empty</Text>
                            </View>
                    }
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 10,
        backgroundColor: '#f6f6f6',
    },
    subContainer: {
        flex: 1,
        margin: 20,

        marginBottom: 5
    },
    strip: {
        height: height(8),
        backgroundColor: '#ffffff',
        marginBottom: 5,
        flexDirection: 'row',
        borderRadius: 5
    },
});
