import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Alert, Image, ScrollView, TouchableOpacity, ToastAndroid, BackHandler
} from 'react-native';
import {width, height, totalSize} from 'react-native-dimension';
import Message from './Message';
import ChatList from './ChatList';
import NotificationList from './NotificationList';
import {observer} from 'mobx-react';
import Store from '../Stores';
import PopupDialog, {slideAnimation, DialogTitle, FadeAnimation} from 'react-native-popup-dialog';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

var team_id = 0;
var player_id = 0;
var requestId = 0;
var event_id = 0;

class LogoTitle extends React.Component {
    render() {
        let {orderStore} = Store;
        return (
            <View style={{height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f'}}>
                <View style={{width: width(80), justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: totalSize(2.5), color: 'white'}}>Messaging</Text>
                </View>
            </View>
        );
    }
}

@observer export default class Messaging extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            Notifications: true,
            Messages: false,
            notify: [],
            joinMembers: [],
            player: false,
            team: false,
            status: null,
        }
    }

    componentDidMount() {
        const { orderStore} = Store;
        orderStore.newMessage = false;
        this._onFocusListener = this.props.navigation.addListener('didFocus', async (payload) => {
            // console.warn('Focus');
            await this.notifications()

            // refresh the component here
            // or update based on your requirements
        });
        BackHandler.addEventListener('hardwareBackPress', this.backPressed);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    backPressed = () => {
        Alert.alert(
            'Logout',
            'Do you want to logout and exit?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Yes', onPress: () => this.clearStorage()},
            ],
            {cancelable: false});
        return true;
    }
    //  componentWillMount = async() => {
    //     await this.notifications()
    //  }
    getFriendReguest = async () => {
        let {orderStore} = Store;
        var url = 'http://play4team.com/AdminApi/api/joinRequests?teamId=' + orderStore.playerLoginRes.id + '&type=';
        console.log('getFriendReguest url=', url);
        // this.setState({ loading: true })
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('getFriendReguest Data======>>>>', responseJson);
                if (responseJson.status === 'True') {
                    orderStore.getFriendReguest = responseJson;
                    this.setState({loading: false})
                    // ToastAndroid.show('Done', ToastAndroid.LONG);
                }
            }).catch((error) => {
            this.setState({loading: false})
            // console.log('cateh====>>>>>', responseJson);
        });
    }

    notifications() {
        let {orderStore} = Store;
        var url = 'http://play4team.com/AdminApi/api/getNotifications?id=' + orderStore.playerLoginRes.id + '&type=' + orderStore.playerLoginRes.Status;
        console.log('notifications url=', url);
        this.setState({loading: true})
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then(async (responseJson) => {
                console.log('notification Data=', responseJson);
                if (responseJson.status === 'True') {
                    orderStore.notifications = responseJson;
                    // await this.getFriendReguest()
                    this.setState({ loading: false })
                }
            }).catch((error) => {
            this.setState({loading: false})
            console.log('cateh====>>>>>', error);
        });
    }

    //***Friend request of player to other player and accet or reject
    friendRequest(status) {
        let {orderStore} = Store;
        this.popupDialog.dismiss()
        this.setState({loading: true})
        // var url = 'http://play4team.com/AdminApi/api/changeFriendRequestStatus?requestId='+requestId+'&status='+status ;
        // console.log('url friend req=',url);
        fetch('http://play4team.com/AdminApi/api/changeFriendRequestStatus?requestId=' + requestId + '&status=' + status, {
            method: 'POST',
        }).then((response) => response.json())
            .then(async(responseJson) => {
                this.setState({loading: false})
                // console.log('friendRequest=',responseJson);
                if (responseJson.status === 'True' && status === 1) {
                    await this.notifications()
                    ToastAndroid.show('Congratulations now you both are friends to each other', ToastAndroid.LONG);
                } else {
                    this.setState({loading: false})
                    ToastAndroid.show('Your friend request is rejected', ToastAndroid.LONG);
                }
            }).catch((error) => {
            this.setState({loading: false})
            ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
        });
    }

    addTeamMembers(status) {
        console.warn('done');
        let {orderStore} = Store;
        this.popupDialog.dismiss()
        this.setState({loading: true})
        if (orderStore.playerLoginRes.Status === 1 && status === 'accept') {
            // 'play4team.com/AdminApi/api/addTeamMembers?requestId=3&team_id=1&friend_team_id=2&status=accept'
            var url = 'http://play4team.com/AdminApi/api/addTeamMembers?requestId=' + requestId + '&status=accept' + '&friend_team_id=' + player_id + '&team_id=' + orderStore.playerLoginRes.id;
        } else {
            var url = 'http://play4team.com/AdminApi/api/addTeamMembers?requestId=' + requestId + '&status=reject' + '&player_id=' + player_id + '&team_id=' + orderStore.playerLoginRes.id;
            // console.warn('status1 & status(reject)');
            // console.log('url reject add member=',url);
        }
        console.log('url accept add member=', url);
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then(async(responseJson) => {
                this.setState({loading: false})
                console.log('friendRequest=', responseJson);
                if (responseJson.status === 'True') {
                    if (orderStore.playerLoginRes.Status === 1 && status === 'accept') {
                        await this.notifications()
                        ToastAndroid.show('Congratulations now you both are friends to each other', ToastAndroid.LONG);
                        // ToastAndroid.show('Congratulations now this player is your team member.', ToastAndroid.LONG);
                    } else {
                        if (orderStore.playerLoginRes.Status === 1 && status === 'reject') {
                            ToastAndroid.show('You have rejected friend request', ToastAndroid.LONG);
                        } else {
                            if (orderStore.playerLoginRes.Status === 0 && status === 'accept') {
                                ToastAndroid.show('Congratulations now you are member of this team.', ToastAndroid.LONG);
                            } else {
                                ToastAndroid.show('You have successfuly rejected this team request', ToastAndroid.LONG);
                            }
                        }
                    }
                } else {
                    this.setState({loading: false})
                    ToastAndroid.show('Try again', ToastAndroid.LONG);
                }
            }).catch((error) => {
            this.setState({loading: false})
            console.log('cateh=====>>>>>', error);
            // ToastAndroid.show('Please try again there must be a network issue', ToastAndroid.LONG);
        });
    }

    eventInvitNotication(status) {
        let {orderStore} = Store;
        var url = 'http://play4team.com/AdminApi/api/changePlayerEventStatus?playerId=' + orderStore.playerLoginRes.id + '&status=' + status + '&teamId=' + requestId + '&eventId=' + event_id;
        console.log('url eventPlayer===============:', url);
        fetch(url, {
            method: 'POST',
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({loading: false})
                console.log('friendRequest=', responseJson);
                if (responseJson.status === 'True' && orderStore.checkToken === 'teamInvitation') {
                    ToastAndroid.show('Congratulations you can attend this event now', ToastAndroid.LONG);
                    this.props.navigation.navigate('EventDetail', {eventItem: {id: event_id}});
                } else {
                    ToastAndroid.show('Congratulations now you attend this event,please see detials of event', ToastAndroid.LONG);
                    this.props.navigation.navigate('detailEvent', {eventItem: {id: event_id}});
                }
            }).catch((error) => {
            this.setState({loading: false})
            console.log('error=======>>>>', error);

        });
    }

    _dilog() {
        let {params} = this.props.navigation.state;
        let {orderStore} = Store;
        return (
            <PopupDialog
                ref={(popupDialog) => {
                    this.popupDialog = popupDialog;
                }}
                dialogStyle={{height: height(25), width: width(90), marginBottom: 200, borderRadius: 5}}
            >
                <View style={{flex: 1, backgroundColor: '#e9e9ef', alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{
                        height: height(8),
                        width: width(90),
                        backgroundColor: '#1d1d4f',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{fontSize: totalSize(2.2), color: 'white'}}>Friend Request</Text>
                    </View>
                    <View style={{
                        height: height(9),
                        width: width(90),
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 5
                    }}>
                        <Text style={{fontSize: totalSize(1.7), color: '#1d1d4f', textAlign: 'center'}}>Would you like
                            to Accept or Reject this request?</Text>
                    </View>
                    <View style={{
                        height: height(8),
                        width: width(90),
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <TouchableOpacity style={{
                            width: width(40),
                            height: height(7),
                            margin: 10,
                            borderRadius: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#1d1d4f'
                        }} onPress={() => {
                            if (orderStore.playerLoginRes.Status === 1 && orderStore.checkToken === 'teamInvitation') {
                                // this.eventInvitNotication('accept')
                                this.eventInvitNotication('1')
                                // this.friendRequest(1)
                            } else {
                                if (orderStore.playerLoginRes.Status === 1 && orderStore.checkToken === 'playerJoinNotification') {
                                    this.addTeamMembers('accept')
                                } else {
                                    if (orderStore.playerLoginRes.Status === 0 && orderStore.checkToken === 'friendRequest') {
                                        this.friendRequest(1)
                                    } else {
                                        if (orderStore.playerLoginRes.Status === 0 && orderStore.checkToken === 'playerToJoinTeam') {
                                            this.addTeamMembers('accept')
                                        } else {
                                            this.eventInvitNotication('accept')
                                        }
                                    }
                                }
                            }

                        }}>
                            <Text style={{fontSize: totalSize(1.8), color: 'white'}}>Accept</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            width: width(40),
                            height: height(7),
                            margin: 10,
                            borderRadius: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#1d1d4f'
                        }} onPress={() => {
                            if (orderStore.playerLoginRes.Status === 1 && orderStore.checkToken === 'teamInvitation') {
                                this.eventInvitNotication('0')

                            } else {
                                if (orderStore.playerLoginRes.Status === 1 && orderStore.checkToken === 'playerJoinNotification') {
                                    this.addTeamMembers('reject')
                                } else {
                                    if (orderStore.playerLoginRes.Status === 0 && orderStore.checkToken === 'friendRequest') {
                                        this.friendRequest(0)
                                    } else {
                                        if (orderStore.playerLoginRes.Status === 0 && orderStore.checkToken === 'playerToJoinTeam') {
                                            this.addTeamMembers('reject')
                                        } else {
                                            this.eventInvitNotication('reject')
                                        }
                                    }
                                }
                            }
                        }}>
                            <Text style={{fontSize: totalSize(1.8), color: 'white'}}>Reject</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </PopupDialog>
        )
    }

    _header = () => {
        return (
            <View style={{height: height(7), width: width(100), backgroundColor: '#33327e', flexDirection: 'row'}}>
                <TouchableOpacity style={this.state.Notifications === false ? styles.unSelect : styles.selected}
                                  onPress={() => {
                                      this.setState({
                                          Notifications: true,
                                          Messages: false,
                                      })
                                  }}
                >
                    {
                        this.state.Notifications === true ?
                            <Text style={{fontSize: totalSize(2.2), color: 'white'}}>Notifications</Text>
                            :
                            <Text style={{fontSize: totalSize(1.8), color: 'gray'}}>Notifications</Text>
                    }
                </TouchableOpacity>
                <TouchableOpacity style={this.state.Messages === false ? styles.unSelect : styles.selected}
                                  onPress={() => {
                                      this.setState({
                                          Notifications: false,
                                          Messages: true,
                                      })
                                  }}
                >
                    {
                        this.state.Messages === true ?
                            <Text style={{fontSize: totalSize(2.2), color: 'white'}}>Messages</Text>
                            :
                            <Text style={{fontSize: totalSize(1.8), color: 'gray'}}>Messages</Text>
                    }
                </TouchableOpacity>
            </View>
        );
    }
    _friendRequest = (item, key) => {
        let {orderStore} = Store;
        return (
            <TouchableOpacity key={key} style={{
                height: height(10),
                backgroundColor: '#e5eff9',
                flexDirection: 'row',
                marginLeft: 0,
                borderBottomWidth: 0.5,
                borderColor: 'gray'
            }}
                              onPress={() => {
                                  this.popupDialog.show()
                                  requestId = item.requested_id;
                                  player_id = item.requested_player_id;
                                  orderStore.checkToken = 'friendRequest';
                              }}
            >
                <View style={{width: width(18), justifyContent: 'center', alignItems: 'center'}}>
                    <Image source={require('../img/req_icon.png')}
                           style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                </View>
                <View style={{width: width(82), justifyContent: 'center'}}>
                    <Text style={{fontSize: totalSize(1.7), color: 'gray'}}>{item.requested_player_name} player has sent
                        you a friend request, would you like to accept or reject?</Text>
                </View>
            </TouchableOpacity>
        );
    }
    _playerResquest = (item, key, state) => {
        let {orderStore} = Store;
        return (
            <TouchableOpacity key={key} style={{
                height: height(10),
                backgroundColor: '#e5eff9',
                flexDirection: 'row',
                marginLeft: 0,
                borderBottomWidth: 0.5,
                borderColor: 'gray'
            }}
                              onPress={() => {
                                  if (state === true) {
                                      requestId = item.team_id;
                                      event_id = item.event_id;
                                      orderStore.checkToken = 'teamInvitation';
                                  } else {
                                      requestId = item.requested_id;
                                      event_id = item.event_id;
                                      player_id = item.id;
                                      orderStore.checkToken = 'playerJoinNotification';
                                  }
                                  // console.warn('token',orderStore.checkToken);
                                  this.popupDialog.show()
                              }}
            >
                <View style={{width: width(18), justifyContent: 'center', alignItems: 'center'}}>
                    {
                        item.Path.length > 0 ?
                            <Image source={{uri: item.Path}}
                                   style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                            :
                            <Image source={require('../img/req_icon.png')}
                                   style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                    }
                </View>
                <View style={{width: width(82), justifyContent: 'center'}}>
                    {
                        state === false ?
                            <Text style={{fontSize: totalSize(1.7), color: 'gray'}}>{item.Team_Name} has sent you a
                                friend request, would you like to accept or reject?</Text>
                            :
                            <Text style={{fontSize: totalSize(1.7), color: 'gray'}}>{item.request_team_name} Team has
                                send you request to join {item.event_name} Event, would you like to accept or reject
                                this invitation?</Text>
                    }
                </View>
            </TouchableOpacity>
        );
    }
    _teamRequests = (item, key, state) => {
        let {orderStore} = Store;
        return (
            <TouchableOpacity key={key} style={{
                height: height(10),
                backgroundColor: '#e5eff9',
                flexDirection: 'row',
                marginLeft: 0,
                borderBottomWidth: 0.5,
                borderColor: 'gray'
            }}
                              onPress={() => {
                                  if (state === true) {
                                      requestId = item.requested_id;
                                      team_id = item.team_id;
                                      event_id = item.event_id;
                                      orderStore.checkToken = 'playerEventInvitation';
                                  } else {
                                      event_id = item.event_id;
                                      requestId = item.requested_id;
                                      team_id = item.team_id;
                                      orderStore.checkToken = 'playerToJoinTeam';
                                  }
                                  // console.warn('token',orderStore.checkToken);
                                  this.popupDialog.show()
                              }}
            >
                <View style={{width: width(18), justifyContent: 'center', alignItems: 'center'}}>
                    {
                        state === false ?
                            item.Path.length > 0 ?
                                <Image source={{uri: item.Path}}
                                       style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                                :
                                <Image source={require('../img/req_icon.png')}
                                       style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                            :
                            <Image source={require('../img/req_icon.png')}
                                   style={{height: height(5), width: width(8), resizeMode: 'contain'}}/>
                    }
                </View>
                <View style={{width: width(82), justifyContent: 'center'}}>
                    {
                        state === false ?
                            <Text style={{fontSize: totalSize(1.7), color: 'gray'}}>{item.Team_Name} team has sent you a
                                joining request, would you like to accept or reject?</Text>
                            :
                            <Text style={{fontSize: totalSize(1.7), color: 'gray'}}>{item.request_team_name} Team has
                                send you request to join {item.event_name} Event, would you like to accept or reject
                                this invitation?</Text>
                    }
                </View>
            </TouchableOpacity>
        );
    }
    static navigationOptions = {
        headerTitle: <LogoTitle/>,
        headerStyle: {
            backgroundColor: '#1d1d4f'
        },
        headerTintColor: 'white',
    }

    render() {
        let {orderStore} = Store;
        if (this.state.loading === true) {
            return (
                <View style={{height: height(100), width: width(100), flex: 1}}>
                    <OrientationLoadingOverlay
                        visible={true}
                        color="white"
                        indicatorSize="small"
                        messageFontSize={18}
                        message="Loading..."
                    />
                </View>
            );
        }
        // console.log('render func',orderStore.eventInvitNotify);
        return (
            <View style={styles.container}>
                <View style={{height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f'}}>
                    <TouchableOpacity style={{width: width(15), justifyContent: 'center', alignItems: 'center'}}
                                      onPress={() => {
                                          // this.props.navigation.navigate('Setting')
                                          this.backPressed()
                                      }}
                    >
                        <Image source={require('../img/logoutNew.png')}
                               style={{height: height(4), width: width(10), resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <View style={{
                        width: width(68),
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        marginRight: 10
                    }}>
                        <Image source={require('../img/logo.png')}
                               style={{height: height(8), width: width(14), resizeMode: 'contain'}}/>
                        <Text style={{fontSize: totalSize(2.5), color: 'white', paddingRight: 5}}>Messages</Text>
                    </View>
                    <TouchableOpacity style={{width: width(15), justifyContent: 'center', alignItems: 'center'}}
                                      onPress={() => {
                                          if (orderStore.playerLoginRes.Status === 0) {
                                              this.props.navigation.navigate('Profile')
                                          } else {
                                              this.props.navigation.navigate('Profile')
                                          }
                                      }}
                    >
                        <Image source={require('../img/userProfile.png')}
                               style={{height: height(2.5), width: width(5), resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                </View>
                {
                    this._header()
                }
                <View style={{flex: 1}}>
                    {
                        this.state.Notifications === true ?
                            <View style={{flex: 1}}>
                                <ScrollView>
                                    {
                                            orderStore.notifications.playersTeamJoiningRequests && orderStore.notifications.playersTeamJoiningRequests.length > 0 ?
                                            orderStore.notifications.playersTeamJoiningRequests.map((item, key) => {
                                                return (
                                                    this._playerResquest(item, key, false)
                                                );
                                            })
                                            :
                                                orderStore.notifications.eventInvititionsLists && orderStore.notifications.eventInvititionsLists.length > 0 ?
                                                orderStore.notifications.eventInvititionsLists.map((item, key) => {
                                                    return (
                                                        this._playerResquest(item, key, true)
                                                    )
                                                })
                                                :
                                                <View style={{
                                                    flex: 1,
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    alignSelf: 'center',
                                                }}>
                                                    <Text style={{marginTop: '60%', color: '#c4c4c4'}}>Empty</Text>
                                                </View>
                                    }
                                </ScrollView>

                                {
                                    this._dilog()
                                }
                            </View>
                            :
                            <View style={{flex: 1}}>
                                <ChatList navigation={this.props.navigation}/>
                            </View>
                    }
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f6f6',
    },
    unSelect: {
        height: height(7),
        width: width(50),
        justifyContent: 'center',
        alignItems: 'center'
    },
    selected: {
        height: height(7),
        width: width(50),
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 3,
        borderColor: 'white',
    },

});


// <View style={{flex: 1}}>
//     <ScrollView>
//         {
//             orderStore.notifications.friendRequests.length > 0 ?
//                 orderStore.notifications.friendRequests.map((item, key) => {
//                     return (
//                         this._friendRequest(item, key)
//                     );
//                 })
//                 :
//                 null
//         }
//         {
//             orderStore.notifications.invititionToJoinEvents.length > 0 ?
//                 orderStore.notifications.invititionToJoinEvents.map((item, key) => {
//                     return (
//                         this._teamRequests(item, key, true)
//                     )
//                 })
//                 :
//                 null
//         }
//         {
//             orderStore.notifications.invititionToJoinTeam.length > 0 ?
//                 orderStore.notifications.invititionToJoinTeam.map((item, key) => {
//                     return (
//                         this._teamRequests(item, key, false)
//                     )
//                 })
//                 :
//                 null
//         }
//     </ScrollView>
//     {
//         this._dilog()
//     }
// </View>
