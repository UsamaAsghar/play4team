import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,DrawerLayoutAndroid
} from 'react-native';
import Splash from '../Splash/Splash';
import Login from '../Login/Login'
import SignUp from '../SignUp/SignUp';
import MainScreen from '../MainScreen/MainScreen';
import Home from '../Home/Home';
import Footer from '../Footer/Footer';
import FriendsList from '../FriendsList/FriendsList';
import FriendComponent from '../FriendsList/FriendComponent';
import AllSupport from '../AllSupports/AllSupport';
import SupportComp from '../AllSupports/SupportComp';
import FindPlayers from '../FindPlayers/FindPlayers';
import FindPlayersTab from '../FindPlayers/FindPlayersTab';
import CreateEvent from '../CreateEvent/CreateEvent';
import EventDetail from '../CreateEvent/EventDetail';
import GeneralSearch from '../FindPlayer/GeneralSearch';
import SearchByName from '../FindPlayer/SearchByName';
import FindPlayer from '../FindPlayer/FindPlayer';
import PlayerResult from '../FindPlayer/PlayerResult';
import TabContainer from '../FindPlayer/TabContainer';
import Recurring from '../CreateEvent/Recurring';
import RequestTeamMembers from '../CreateEvent/RequestTeamMembers';
import EventLocation from '../CreateEvent/EventLocation';
import EventLocComp from '../CreateEvent/EventLocComp';
import GeneralEventSearch from '../FindEvent/GeneralEventSearch';
import SearchEventByName from '../FindEvent/SearchEventByName';
import MyEvent from '../MyEvent/MyEvent';
import EventList from '../MyEvent/EventList';
import detailEvent from '../MyEvent/detailEvent';
import ViewEvent from '../MyEvent/ViewEvent';
import Invitation from '../Invitation/Invitation';
import ReviewEvent from '../ReviewEvent/ReviewEvent';
import AddFriends from '../AddFriends/AddFriends';
import MyGroup from '../MyGroup/MyGroup';
import AddMembers from '../AddMembers/AddMembers';
import Group from '../MyGroup/Group';
import Message from '../Massenger/Message';
import NewGroup from '../NewGroup/NewGroup';
import AddPeople from '../AddPeople/AddPeople';
import Setting from '../Setting/Setting';
import SignUpTeam from '../SignUp/SignUpTeam';
import FeedBack from '../FeedBack/FeedBack';
import AttendingEvents from '../AttendingEvents/AttendingEvents';
import Profile from '../Profile/Profile';
import TeamDetail from '../Profile/TeamDetail';
import Availability from '../Profile/Availability';
import MySport from '../Profile/MySport';
import MyRating from '../Profile/MyRating';
import EditProfile from '../Profile/EditProfile';
import playerDetail from '../FindPlayer/playerDetail';
import PlayerDetailView from '../TeamProfile/PlayerDetailView';
import ViewProfile from '../Profile/ViewProfile';
import ContactList from '../FriendsList/ContactList';
import TeamProfile from '../TeamProfile/TeamProfile';
import TeamMember from '../TeamProfile/TeamMember';
import TeamMembers from '../TeamProfile/TeamMembers';
import TeamEdit from '../TeamProfile/TeamEdit';
import TeamAvailability from '../TeamProfile/TeamAvailability';
import Messaging from '../Massenger/Messaging';
import EventNotifDetail from '../Massenger/EventNotifDetail';
import ForgetPassword from '../ForgetPassword/ForgetPassword'
import test from '../test'
import Teams from '../Profile/Teams'
import ChatList from '../Massenger/ChatList'
import Lisence from '../SignUp/Lisence'
import { StackNavigator } from 'react-navigation';
import TabNavigator from '../Footer/TabNavigator';
import { createStackNavigator, createAppContainer } from "react-navigation";


const RootStack = createStackNavigator(
    {
        Splash: {
          screen: Splash
        },
        Login: {
          screen: Login
        },
        SignUp: {
          screen: SignUp
        },

        MainScreen: {
          screen: MainScreen
        },
        Home: {
          screen: Home
        },
        Footer: {
          screen: Footer
        },
        FriendsList: {
          screen: FriendsList
        },
        AllSupport: {
          screen: AllSupport
        },
        SupportComp: {
          screen: SupportComp
        },
        FindPlayers: {
          screen: FindPlayers
        },
        FindPlayersTab: {
          screen: FindPlayersTab
        },
        FriendComponent: {
          screen: FriendComponent
        },
        PlayerDetailView : PlayerDetailView,
        CreateEvent: {
          screen: CreateEvent
        },
        RequestTeamMembers: {
          screen: RequestTeamMembers
        },
        EventDetail: {
          screen: EventDetail
        },
        GeneralSearch: {
          screen: GeneralSearch
        },
        SearchByName: {
          screen: SearchByName
        },
        playerDetail: {
          screen: playerDetail
        },
        TabContainer: {
          screen: TabContainer
        },
        PlayerResult: {
          screen: PlayerResult
        },
        FindPlayer: {
          screen:FindPlayer
        },
        Recurring: {
          screen: Recurring
        },
        EventLocation: {
          screen: EventLocation
        },
        GeneralEventSearch: {
          screen: GeneralEventSearch
        },
        SearchEventByName: {
          screen: SearchEventByName
        },
        MyEvent: {
          screen: MyEvent
        },
        EventList: {
          screen: EventList
        },
        detailEvent: {
          screen: detailEvent
        },
        TeamDetail: TeamDetail,
        Teams: Teams,
        ViewEvent: {
          screen: ViewEvent
        },
        Invitation: {
          screen: Invitation
        },
        ReviewEvent: {
          screen: ReviewEvent
        },
        AddFriends: {
          screen: AddFriends
        },
        MyGroup: {
          screen: MyGroup
        },
        AddMembers: {
          screen: AddMembers
        },
        Group: {
          screen: Group
        },
        Messaging:{
          screen: Messaging
        },
        Message: {
          screen: Message
        },
        NewGroup: {
          screen: NewGroup
        },
        AddPeople: {
          screen: AddPeople
        },
        Setting: {
          screen: Setting
        },
        SignUpTeam: {
          screen: SignUpTeam
        },
        FeedBack: {
          screen: FeedBack
        },
        AttendingEvents: {
          screen: AttendingEvents
        },
        Profile: {
          screen: Profile
        },
        Availability: {
          screen: Availability
        },
        MySport: {
          screen: MySport
        },
        MyRating: {
          screen: MyRating
        },
        EditProfile: {
          screen: EditProfile
        },
        ViewProfile: {
          screen: ViewProfile
        },
        ContactList: {
          screen: ContactList
        },
        TeamProfile: {
          screen: TeamProfile
        },
        TeamMember: {
          screen: TeamMember
        },
        TeamMembers: {
          screen: TeamMembers
        },
        TeamEdit: {
          screen: TeamEdit
        },
        TeamAvailability: {
          screen: TeamAvailability
        },
        EventNotifDetail: {
          screen: EventNotifDetail
        },
        ForgetPassword: {
          screen:ForgetPassword
        },
        MainTab:{
          screen: TabNavigator,
          navigationOptions: {
            header: null
        }
        },
        ChatList: ChatList, 
        test: test,
        Lisence: Lisence
    },
    
    {
       initialRouteName: 'Splash',
    },
  );
export default createAppContainer(RootStack)
  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
