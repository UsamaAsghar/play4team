import React, { Component } from 'react';
import {
    // AppRegistry,
    StyleSheet,
    Image,
     View,
     Text,
     ScrollView,
    TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
export default class SupportComp extends Component {
  constructor(props){
    super(props);
    this.state={

    }
  }
  componentWillMount(){

  }
  static navigationOptions = {
        header: null
    }
    render() {
        return (

          <View style={{height:height(10),width:width(90),flexDirection:'row',backgroundColor:'#ffffff',borderBottomWidth:1,borderColor:'gray'}}>
              <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/select.png')} style={{height:height(7),width:width(12)}} />
              </View>
              <View style={{width:width(57),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize: totalSize(2.2),color:'black',marginLeft:5}}>Badminton</Text>
              </View>
              <View style={{width:width(18),justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize: totalSize(1.5),color:'#c5c6ff'}}>Sports</Text>
              </View>
          </View>

        );
    }
}

const styles = StyleSheet.create({
  Container:{
    flex:1,
    margin:20,

  },
  Background:{
    flex:1,
    backgroundColor:'#402217',
  },
  logo:{
    flex:1,
    margin:2,
    justifyContent:'center',
    alignItems:'center'
  }

});
