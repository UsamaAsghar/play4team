/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import SupportComp from './SupportComp';
import { width, height, totalSize } from 'react-native-dimension';
class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(15),backgroundColor:'#1d1d4f'}}>
          <View style={{height:height(8),flexDirection:'row'}}>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/setting.png')}  />
            </TouchableOpacity>
            <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
              <Image source={require('../img/logo.png')} style={{height:height(9),width:width(15)}} />
              <Text style={{fontSize: totalSize(2.5),color:'white'}}>All Support</Text>
            </View>
            <TouchableOpacity style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/friend.png')}  />
            </TouchableOpacity>
          </View>
          <View style={{height:height(6),backgroundColor:'#1d1d4f',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <View style={{height:height(5.5),width:width(80),backgroundColor:'#33327e',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <View style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/search.jpg')} style={{height:height(5),width:width(10),backgroundColor:'transparent'}}/>
              </View>
              <View style={{height:height(7),width:width(70)}}>
                <TextInput
                  onChangeText={(value) => this.validate(value,'mobNo')}
                  placeholder={"Search for anything"}
                  style={{fontSize:totalSize(2),color:'gray',backgroundColor:'#33327e',width:width(70)}}
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                  />
              </View>
            </View>
            <View style={{height:height(5.5),width:width(10),justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../img/search.jpg')} style={{height:height(5),width:width(10),backgroundColor:'transparent'}}/>
            </View>
          </View>

        </View>
    );
  }
}
export default class AllSupport extends Component<Props> {


  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerLeft: null,
       headerStyle: {
            backgroundColor: '#1d1d4f',
        },
        headerTintColor: '#fff',
        headerStyle: {
          height:height(15)
        },
        headerTitleStyle: {
            fontWeight: 'bold',
        },
        headerTitleStyle: {
            fontSize: totalSize(2),
        },
    }
  render() {
    return (
      <View style={styles.container}>
        <View style={{height:height(6),backgroundColor:'#eaedf2'}}>
          <Text style={{fontSize: totalSize(1.8),color:'black',margin:10,marginLeft:19}}>Most Popular</Text>
        </View>
        <View style={{height:height(66),justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(62),width:width(90)}}>
          <ScrollView>
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
            <SupportComp />
          </ScrollView>
          </View>
        </View>
        <View style={{height:height(10),backgroundColor:'#2f2f77',justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize: totalSize(2.2),color:'white',margin:10,marginLeft:19}}>Done</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height:height(82),

  },

});
