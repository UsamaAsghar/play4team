import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  View, ToastAndroid, Image, ImageBackground, ScrollView, TouchableOpacity, TextInput
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import Store from '../Stores';
import Icon from 'react-native-vector-icons/AntDesign'
import BackArrow from 'react-native-vector-icons/AntDesign'
import DateTimePicker from 'react-native-datepicker'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import PopupDialog, { slideAnimation, DialogTitle, FadeAnimation } from 'react-native-popup-dialog';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(75), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Review Event</Text>
        </View>
        <TouchableOpacity style={{ alignSelf: 'center' }}>
            <Icon name='delete' size={30} color="#5ae758" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
      </View>
    );
  }
}
export default class ReviewEvent extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      nameDilog: false,
      sportDilog: false,
      name: '',
      sport: {},
      sport_id: '',
      date: '',
      edit: false,
      address: '',
      loc_name: '',
      isLocationEdit: false
    }
    _this = this;
  }
  static navigationOptions = ({navigation}) => ({
    header: (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <TouchableOpacity style={{ width: width('15'), alignSelf: 'center' }}
           onPress={()=> navigation.goBack()}
        >
            <BackArrow name='arrowleft' size={25} color="#fff" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
        <View style={{ width: width(70), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(8), width: width(15) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>Event Detail</Text>
        </View>
        <TouchableOpacity style={{ width: width('15'), alignSelf: 'center' }}
            onPress={() => {
              _this.alert()
            }}
        >
            <Icon name='delete' size={30} color="#5ae758" style={{ marginLeft: 10 }} />
        </TouchableOpacity>
      </View>
    ),
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white'
  });
  componentWillMount() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    console.log('event=====------------------:', params.eventDetail);
    orderStore.editLocation.latitude = params.eventDetail.latitude;
    orderStore.editLocation.longitude = params.eventDetail.longitude;
    orderStore.editLocation.address = params.eventDetail.Address;
    this.setState({
      name: params.eventDetail.Event_Name,
      sport_id: params.eventDetail.sport_id,
      date: params.eventDetail.Start_Date,
      loc_name: params.eventDetail.location_name ? params.eventDetail.location_name : 'Enter location name',
      isLocationEdit: params.eventDetail.location_name ? false : true
    })
  }
  alert() {
    Alert.alert(
      'Alert!',
      'Are you sure you want to delete this event',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.eventDelete() },
      ],
      { cancelable: false }
    )
  }
  editEvent() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    this.setState({ loading: true })
    // two_weeks
    url = 'http://play4team.com/AdminApi/api/editEvent?eventId=' + params.eventDetail.id + '&eventName=' + this.state.name + '&sportId=' + this.state.sport_id + '&latitude=' + orderStore.editLocation.latitude + '&longitude=' + orderStore.editLocation.longitude + '&address=' + orderStore.editLocation.address + '&startDate=' + this.state.date + '&endDate=' + this.state.date + '&repeatStatus=' + orderStore.eventLife;
    // console.log('url=',url);
    fetch('http://play4team.com/AdminApi/api/editEvent?eventId=' + params.eventDetail.id + '&eventName=' + this.state.name + '&sportId=' + this.state.sport_id +'&locationName='+ this.state.loc_name +  '&latitude=' + orderStore.editLocation.latitude + '&longitude=' + orderStore.editLocation.longitude + '&address=' + orderStore.editLocation.address + '&startDate=' + this.state.date + '&endDate=' + this.state.date, {
      method: 'POST',
    }).then((response) => response.json())
      .then(async (responseJson) => {
        console.log('responseJson===========:', responseJson);
        // this.setState({loading: false})
        if (responseJson.status === "True") {
          ToastAndroid.show('Event updated successfully', ToastAndroid.LONG);
          // this.props.navigation.push('MainTab')
          orderStore.events.managingEvents.forEach(item => {
            if (params.eventDetail.id === item.id) {
              item = responseJson.data;
              this.props.navigation.push('MainTab')
            }
          });
          // for (var i = 0; i < orderStore.events.managingEvents.length; i++) {
          //   if (params.eventDetail.id === orderStore.managingEvents[i].id) {
          // orderStore.events.managingEvents[i].Event_Name = this.state.name;
          // orderStore.events.managingEvents[i].sport_id = this.state.sport_id;
          // orderStore.events.managingEvents[i].latitude = orderStore.editLocation.latitude;
          // orderStore.events.managingEvents[i].longitude = orderStore.editLocation.longitude;
          // orderStore.events.managingEvents[i].Address = orderStore.editLocation.address;
          // orderStore.events.managingEvents[i].End_Date = this.state.date;
          // if (Object.keys(this.state.sport).length !== 0) {
          //    orderStore.events.managingEvents[i].sport_name = this.state.sport.Name;
          //    orderStore.events.managingEvents[i].sport_image = 'http://play4team.com/AdminApi/public/img/'+this.state.sport.Image;
          // }
          // this.setState({loading: false})
          //   }
          // }
          // console.log('status=',this.state.status);
          await this.reLoadEvents();
        } else {
          this.setState({ loading: false })
          ToastAndroid.show('Please try again', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  eventDelete = async() => {
    this.setState({ loading: true })
    let { params } = this.props.navigation.state;
    var url = 'http://play4team.com/AdminApi/api/deleteEvent?eventId=' + params.eventDetail.id ;
    await fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then(async (responseJson) => {
        // console.log('events details===---------------------:', responseJson);
        if (responseJson.status === 'True') {
          this.setState({ loading: false })
          this.props.navigation.push('MainTab')
        } else {
          this.setState({ loading: false })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  reLoadEvents = async () => {
    var url = 'http://play4team.com/AdminApi/api/playerEvents?teamId=' + orderStore.playerLoginRes.id;
    await fetch(url, {
      method: 'POST',
    }).then((response) => response.json())
      .then(async (responseJson) => {
        console.log('events are===>>>', responseJson);
        orderStore.events = responseJson;
        if (responseJson.status === 'True') {
          orderStore.managingEvent = responseJson.managingEvents;
          orderStore.events.completeEventsInvited = responseJson.completeEventsInvited;
          this.setState({ loading: false })
        } else {
          this.setState({ loading: false })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  selectPlace = (lat, lng, address) => {
    let { orderStore } = Store;
    orderStore.editLocation.latitude = lat;
    orderStore.editLocation.longitude = lng;
    orderStore.editLocation.address = address;
    console.warn('address comming from eventLocation=', address);
    console.log('editLocation object=', orderStore.editLocation);
    this.setState({ loading: false })
  }
  selectSport(sport) {
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.sportEvent.length; i++) {
      if (sport.id === orderStore.sportEvent[i].id) {
        this.setState({
          sport: orderStore.sportEvent[i],
          sport_id: sport.id,
        })
      }
    }
  }
  getPredictions(postalCode) {
    let { orderStore } = Store;
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    if (postalCode.length !== 0) {
      fetch('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + postalCode + '&key=' + api_key)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('Predictions', responseJson);
          if (responseJson.status === 'OK') {
            orderStore.predictions = responseJson.predictions;
            for (var i = 0; i < orderStore.predictions.length; i++) {
              orderStore.predictions[i].added = false;
              orderStore.predictions[i].checkStatus = false;
            }
            this.setState({ loading: false })
          }
        })
    } else {
      orderStore.predictions = [];
      this.setState({ loading: false })
    }
  }
  getLatLong(item) {
    let { orderStore } = Store;
    this.setState({ address: item.description })
    let api_key = 'AIzaSyDYq16-4tDS4S4bcwE2JiOa2FQEF5Hw8ZI';
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + item.description + '&key=' + api_key)
      .then((response) => response.json())
      .then((responseJson) => {
        console.warn('latLong response', responseJson);
        if (responseJson.status === 'OK') {
          orderStore.editLocation.latitude = responseJson.results[0].geometry.location.lat;
          orderStore.editLocation.longitude = responseJson.results[0].geometry.location.lng;
          orderStore.editLocation.address = item.description;
          this.setState({
            latitude: responseJson.results[0].geometry.location.lat,
            longitude: responseJson.results[0].geometry.location.lng,
            //  address: responseJson.results[0].formatted_address,
          })
          orderStore.predictions = [];
          this.setState({ loading: false, edit: false })
        }
      })
  }
  _dilog() {
    let { params } = this.props.navigation.state;
    return (
      <PopupDialog
        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
        dialogStyle={{ height: height(25), width: width(90), marginBottom: 200, borderRadius: 5, borderColor: 'gray', borderWidth: 0.5 }}>
        <View style={{ flex: 1, backgroundColor: '#e9e9ef', alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ height: height(8), width: width(90), backgroundColor: '#1d1d4f', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: totalSize(2.2), color: 'white' }}>Edit Name</Text>
          </View>
          <View style={{ height: height(9), width: width(90), justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
            <TextInput
              onChangeText={(value) => this.setState({ name: value })}
              underlineColorAndroid='transparent'
              value={this.state.name}
              placeholderTextColor='gray'
              underlineColorAndroid='transparent'
              autoCorrect={true}
              style={{ height: height(7), width: width(80), textAlign: 'left', borderRadius: 5, color: 'black', backgroundColor: 'white' }}
            />
          </View>
          <View style={{ height: height(8), width: width(90), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: width(40), height: height(7), margin: 10, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: '#1d1d4f' }} onPress={() => { this.popupDialog.dismiss() }}>
              <Text style={{ fontSize: totalSize(1.8), color: 'white' }}>Cencel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ width: width(40), height: height(7), margin: 10, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: '#1d1d4f' }} onPress={() => { this.popupDialog.dismiss() }}>
              <Text style={{ fontSize: totalSize(1.8), color: 'white' }}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </PopupDialog>
    )
  }
  _sportPicker() {
    let { orderStore } = Store;
    return (
      <PopupDialog
        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
        dialogStyle={{ height: height(70), width: width(90), marginBottom: 100, borderRadius: 0 }}
      >
        <View style={{ flex: 1 }}>
          <ScrollView>
            {
              orderStore.sportEvent.map((item, key) => {
                return (
                  <TouchableOpacity key={key} style={{ height: height(8), width: width(90), flexDirection: 'row', borderBottomWidth: 0.4, marginBottom: 0.2, borderColor: 'gray' }}
                    onPress={() => {
                      this.selectSport(item)
                      this.popupDialog.dismiss()
                    }}
                  >
                    <View style={{ height: height(8), width: width(20), justifyContent: 'center', alignItems: 'center' }}>
                      <Image source={{ uri: item.Path }} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
                    </View>
                    <View style={{ height: height(8), width: width(70), justifyContent: 'center' }}>
                      <Text style={{ fontSize: totalSize(1.6), color: 'black' }}>{item.Name}</Text>
                    </View>
                  </TouchableOpacity>
                );
              })
            }
          </ScrollView>
        </View>
      </PopupDialog>
    )
  }
  render() {
    var status;
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    // console.warn('event last=',params.eventDetail);
    // console.warn('object=',this.state.sport);
    if (Object.keys(this.state.sport).length == 0) {
      var name = params.eventDetail.sport_name;
      var img = 'http://play4team.com/AdminApi/public/img/' + params.eventDetail.sport_image;
    } else {
      name = this.state.sport.Name;
      img = 'http://play4team.com/AdminApi/public/img/' + this.state.sport.Image;
    }
    if (params.eventDetail.Weakly === '1') {
      status = 'Weakly';
    } else {
      if (params.eventDetail.repeat_status !== null) {
        status = params.eventDetail.repeat_status;
      } else {
        status = 'not selected please select';
      }
    }
    if (this.state.loading == true) {
      return (
        <OrientationLoadingOverlay
          visible={true}
          color="white"
          indicatorSize="small"
          messageFontSize={18}
          message="Loading..."
        />
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(28), width: width(100) }}>
          <Image source={require('../img/eventbanner.jpeg')} style={{ height: height(28), width: width(100) }} />
        </View>
        <View style={styles.subContainer}>
          <View style={styles.stripTop} >
            <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(7), width: width(10), justifyContent: 'center' }}>
                <Image source={require('../img/xyz.png')} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
              </View>
            </View>
            <View style={{ width: width(66), justifyContent: 'center' }}>
              <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>{this.state.name}</Text>
            </View>
            <TouchableOpacity style={{ width: width(9), justifyContent: 'center', alignItems: 'center' }}
              onPress={() => {
                this.popupDialog.show()
                this.setState({
                  nameDilog: true,
                  sportDilog: false
                })
              }}>
              <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>Edit</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.strip}>
            <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(7), width: width(10) }}>
                <Image source={{ uri: 'http://play4team.com/AdminApi/public/sports_images/' + params.eventDetail.sport_image }} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
              </View>
            </View>
            <View style={{ width: width(66), justifyContent: 'center' }}>
              <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>{name}</Text>
            </View>
            {/* <TouchableOpacity style={{width:width(9),justifyContent:'center',alignItems:'center'}} onPress={()=>{
                  this.popupDialog.show()
                  this.setState({
                    nameDilog: false,
                    sportDilog: true
                  })
                 }}>
                  <Text style={{fontSize: totalSize(1.5),color:'gray'}}>Edit</Text>
                </TouchableOpacity> */}
          </View>
          <View style={styles.strip}>
            <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(7), width: width(10) }}>
                <Image source={require('../img/calander-btn.png')} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
              </View>
            </View>
            <View style={{ width: width(66), justifyContent: 'center' }}>
              <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>{this.state.date}</Text>
            </View>
            <TouchableOpacity style={{ width: width(9), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>Edit</Text>
              <DateTimePicker
                style={{ width: width(9), height: height(5), backgroundColor: 'transparent' }}
                date={this.state.date}
                // time={this.state.time}
                mode="date"
                placeholder="Event Start"
                format="YYYY-MM-DD"
                minDate="2018-8-01"
                maxDate="2020-8-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={true}
                hideText='false'
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => { this.setState({ date: date }) }}
              />
            </TouchableOpacity>
          </View>
          <View style={[styles.strip,{ marginBottom: 2 }]}>
            <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(7), width: width(10) }}>
                <Image source={require('../img/cricket-ground.png')} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
              </View>
            </View>
            <View style={{ width: width(66), justifyContent: 'center' }}>
              {
                this.state.edit ?
                  <TextInput
                    onChangeText={(value) => this.getPredictions(value)}
                    underlineColorAndroid='transparent'
                    placeholder='Enter postal/Zip Code'
                    value={this.state.address.length > 0 ? this.state.address : null}
                    onFocus={() => { this.setState({ address: '' }) }}
                    placeholderTextColor='gray'
                    keyboardType='email-address'
                    underlineColorAndroid='transparent'
                    autoCorrect={true}
                    style={{ height: height(6), width: width(66), backgroundColor: 'white', textAlign: 'left', fontSize: totalSize(1.5), marginTop: 0 }}
                  />
                  :
                  <Text style={{ fontSize: totalSize(1.5), color: 'black', textAlign: 'left' }}>{orderStore.editLocation.address}</Text>
              }
            </View>
            <TouchableOpacity style={{ width: width(9), justifyContent: 'center', alignItems: 'center' }}
              onPress={() => {
                this.setState({ edit: true })
                //this.props.navigation.navigate('EventLocation',{
                // sportName: name,
                // selectPlace: this.selectPlace

                //});
                // orderStore.editLocation.status = true;
              }}
            >
              <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>Edit</Text>
            </TouchableOpacity>
          </View>
          {
            orderStore.predictions.length > 0 ?
              <View style={{ width: width(90), backgroundColor: 'white', elevation: 5 }}>
                {
                  orderStore.predictions.map((item, key) => {
                    return (
                      <TouchableOpacity key={key} style={{ marginHorizontal: 5, borderBottomWidth: 0.4, borderColor: 'gray', elevation: 0 }}
                        onPress={() => {
                          this.getLatLong(item)
                        }}
                      >
                        <Text style={{ fontSize: totalSize(1.6), color: 'black', marginVertical: 10 }}>{item.description}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
              :
              null
          }
         <View style={styles.strip}>
            <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: height(7), width: width(10) }}>
                <Image source={require('../img/cricket-ground.png')} style={{ height: height(7), width: width(10), resizeMode: 'contain' }} />
              </View>
            </View>
            <View style={{ width: width(66), justifyContent: 'center' }}>
              {
                this.state.isLocationEdit ?
                  <TextInput
                    onChangeText={(value) => this.setState({loc_name: value})}
                    underlineColorAndroid='transparent'
                    placeholder='Enter postal/Zip Code'
                    // value={this.state.loc_name.length > 0 ? this.state.loc_name : null}
                    // onFocus={() => { this.setState({ address: '' }) }}
                    placeholderTextColor='gray'
                    keyboardType='email-address'
                    underlineColorAndroid='transparent'
                    autoCorrect={true}
                    style={{ height: height(6), width: width(66), backgroundColor: 'white', textAlign: 'left', fontSize: totalSize(1.5), marginTop: 0 }}
                  />
                  :
                  <Text style={{ fontSize: totalSize(1.5), color: 'black', textAlign: 'left' }}>{this.state.loc_name}</Text>
              }
            </View>
            <TouchableOpacity style={{ width: width(9), justifyContent: 'center', alignItems: 'center' }}
              onPress={() => {
                this.setState({ isLocationEdit: !this.state.isLocationEdit })
              }}
            >
              <Text style={{ fontSize: totalSize(1.5), color: 'gray' }}>Edit</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={styles.strip}>
                <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:height(7),width:width(10)}}>
                    <Image source={require('../img/publice-event.png')} style={{height:height(7),width:width(10),resizeMode:'contain'}} />
                  </View>
                </View>
                <View style={{width:width(66),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>{status}</Text>
                </View>
                <TouchableOpacity style={{width:width(9),justifyContent:'center',alignItems:'center'}}
                  onPress={()=>{
                    this.props.navigation.navigate('Recurring')
                  }}
                >
                  <Text style={{fontSize: totalSize(1.5),color:'gray'}}>Edit</Text>
                </TouchableOpacity>
              </View> */}
          <View style={{ height: height(9) }}>
          </View>
        </View>
        {
          this.state.nameDilog === true ?
            this._dilog()
            :
            this._sportPicker()
        }
        <TouchableOpacity style={{ height: height(8), width: width(100) }}
          onPress={() => {
            this.editEvent()
          }}
        >
          <ImageBackground source={require('../img/bar.png')} style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: totalSize(2), color: 'white' }}>Update Event</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center'
  },
  subContainer: {
    flex: 1,
    margin: 20,
    marginTop: 10,
    // position: 'absolute',
    // zIndex:2
  },
  strip: {
    height: height(8),
    backgroundColor: '#ffffff',
    borderBottomWidth: 0.4,
    marginBottom: 0.2,
    borderColor: 'gray',
    flexDirection: 'row',
  },
  stripLast: {
    height: height(8),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  stripTop: {
    height: height(8),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomWidth: 0.4,
    marginBottom: 0.3,
    borderColor: 'gray',
  },

});
