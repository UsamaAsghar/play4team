
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, ToastAndroid
} from 'react-native';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import { width, height, totalSize } from 'react-native-dimension';
import PopupDialog, { slideAnimation } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-crop-picker';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Footer from '../Footer/Footer';
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(76), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>{params.item.Team_Name}</Text>
        </View>
      </View>
    );
  }
}
@observer export default class TeamDetail extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      avatarSource: null
    }
  }
  static navigationOptions =(navigation,state)=>({
    headerTitle:  'Team',
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  })
 
  render() {
    let { orderStore } = Store;
    let { params } = this.props.navigation.state;
    if (this.state.loading == true) {
      return (
        <View style={{ flex: 1, height: height(100), width: width(100), backgroundColor: 'rgba(29,37,86,0.6)' }}>
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(22), width: width(100) }}>
          <Image source={require('../img/blue-bg.png')} style={{ height: height(21), width: width(100) }} />
        </View>
        <View style={{ height: height(30), width: width(100), flexDirection: 'row', justifyContent: 'center', marginTop: 10, position: 'absolute', zIndex: 1, alignItems: 'flex-start' }}>
          <View style={{ height: height(7), width: width(30), justifyContent: 'center', alignItems: 'center' }}
            // onPress={() => {
            //   this.props.navigation.navigate('TeamEdit', { picture: this.state.avatarSource });
            // }}
          >
            {/* <Image source={require('../img/edit-icon.png')} style={{ height: height(3), width: width(5) }} />
            <Text style={{ fontSize: totalSize(1.5), color: 'white' }}> Edit Profile </Text> */}
          </View>
          <TouchableOpacity style={{ height: height(30), width: width(40), alignItems: 'center' }}
          >
          
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <Image source={require('../img/p_pic.png')} style={{ height: height(23), width: width(40), borderRadius: 100, resizeMode: 'contain' }} />
                  <Text style={{ fontSize: totalSize(2), color: '#1d1d4f' }}> {params.item.Team_Name} </Text>
                </View>
            
            <View style={{ height: height(4), width: width(40), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <Image source={require('../img/loca.png')} style={{ height: height(1.5), width: width(5), marginLeft: 7, resizeMode: 'contain' }} />
                  <Text style={{ fontSize: totalSize(1.4), color: '#1d1d4f' }}> {params.item.address} </Text>
            </View>
          </TouchableOpacity>

          <View style={{ height: height(7), width: width(30), justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              // this.props.navigation.navigate('ViewProfile');
            }}
          >
            {  /*<Image source={require('../img/view-icon.png')} style={{height:height(3),width:width(5)}} />
            <Text style={{fontSize:totalSize(1.5),color:'white'}}> View Profile </Text>*/}
          </View>

        </View>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(16), width: width(100) }}>
          </View>
          <View style={{ height: height(62), width: width(100), alignItems: 'center' }}>
            <View style={styles.strip}>

              <View style={{ marginLeft: 40,width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(6), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>{params.item.Email}</Text>
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </View>
            <View style={{ flex:1 }}>
            <Text style={{ color:'black',marginVertical: 4 }}>Location:</Text>
            <MapView
                // zoomEnabled={true}
                // zoomControlEnabled={true}
                toolbarEnabled={true}
                showsCompass={true}
                showsBuildings={true}
                showsIndoors={true}
                provider={PROVIDER_GOOGLE}
                showsMyLocationButton={true}
                showsUserLocation={true}
                followsUserLocation={true}
                minZoomLevel={5}
                maxZoomLevel = {20}
                mapType = {"standard"}
                loadingEnabled = {true}
                loadingIndicatorColor = {'white'}
                loadingBackgroundColor = {'gray'}
                moveOnMarkerPress={true}
                style={{
                        height:height(30),
                        width:width(95),
                        alignSelf:'center',
                        zIndex : -10,
                        position: 'absolute',
                }}
                region={{
                  latitude:       params.item.team_latitude || 31.179,
                  longitude:      params.item.team_longitude || 70.435,
                  latitudeDelta:  0.922,
                  longitudeDelta: 0.421
                }}
                // onRegionChange={this.onRegionChange.bind(this)}
                >
                {
                    params.item.team_longitude && params.item.team_longitude ?
                     <MapView.Marker
                       coordinate={
                       { latitude: params.item.team_latitude, longitude: params.item.team_longitude }
                       }
                    //    title={'My Location'}
                    //    description={orderStore.playerLoginRes.address}
                       // onPress={()=>alert('player')}
                       pinColor={'#3edc6d'}
                       // image={require('../img/friend_img.png')}
                       >

                      </MapView.Marker>
                      :
                      null
                  }
                </MapView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  strip: {
    height: height(8),
    width: width(90),
    backgroundColor: '#ffffff',
    borderBottomWidth: 0.3,
    marginBottom: 0.3,
    borderColor: 'gray',
    flexDirection: 'row',
    borderRadius: 5,
    marginTop: 5,
  },
});
