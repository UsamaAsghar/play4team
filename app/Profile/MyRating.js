
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { Avatar } from 'react-native-elements';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { observer } from 'mobx-react';
import Store from '../Stores';

class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),width:width(75),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
            <Text style={{fontSize:totalSize(2.5),color:'white'}}>My Rating </Text>
          </View>
        </View>
    );
  }
}
@observer export default class MyRating extends Component<Props> {

  constructor(props){
    super(props)
    this.state = {
      avail: false,
      busy : false,
      skill: 0,
      fitness: 90,
      sport_id:'',
      sports: [],
    }
  }
  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerStyle: {
         backgroundColor:'#1d1d4f'
       },
       headerTintColor:'white',
    }
    componentWillMount(){
      let { orderStore } = Store;
      console.log('playstore===>>>',orderStore.playerProfile);
      
      for (var i = 0; i < orderStore.playerProfile.length; i++) {
        // orderStore.playerProfile[0].added = true;
        orderStore.playerProfile[0].checkStatus = true;
        this.setState({
          skill: orderStore.playerProfile[0].rate * 10,
        })
      }
    }

    selectSport(sport){

      let { orderStore } = Store;
      for (var i = 0; i < orderStore.playerProfile.length; i++) {
          if (sport.sport_id === orderStore.playerProfile[i].sport_id) {
              // orderStore.playerProfile[i].added = true;
              orderStore.playerProfile[i].checkStatus = true;
              orderStore.sport_id = orderStore.playerProfile[i].sport_id;
              this.setState({
                // fitness: orderStore.playerLoginRes.Fitness,
                skill: orderStore.playerProfile[i].rate * 10,
              })
          }else {
            // orderStore.sportEvent[i].added = false;
            orderStore.playerProfile[i].checkStatus = false;
          }
      }
      // console.log('selected id=',orderStore.sport_id);
      // console.log('selected sport=',this.state.skill);
    }

    componentWillUnmount(){
     
      let { orderStore } = Store;
      for (var i = 0; i < orderStore.playerProfile.length; i++) {
        // orderStore.playerProfile[i].added = false;
        orderStore.playerProfile[i].checkStatus = false;
      }
      orderStore.sport_id = 0;
    }
  render() {
    let { orderStore } =Store;
    var fitness = orderStore.playerLoginRes.Fitness * 10;
    return (
      <View style={styles.container}>
        <View style={{height:height(5),width:width(100),backgroundColor:'#eaedf2',justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:totalSize(1.5),color:'black',marginRight:15}}>My Ratings</Text>
        </View>
        <View style={{height:height(17),marginBottom:20,width:width(90),alignItems:'center',backgroundColor:'#ffffff'}}>
            <View style={{height:height(17),width:width(80),flexDirection:'row',justifyContent:'center',alignItems:'center',alignSelf:'center',}}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator = {false}
                scrollToEnd = {true}
                >
                {
                  orderStore.playerProfile.map((item,key)=>{
                    return(
                        <TouchableOpacity key={key} style={item.checkStatus === true? styles.selected : styles.unSelect}
                          onPress={()=>{
                            // this.setState({
                            //   sport_id: item.id
                            // })
                            // console.warn('id = ', item.id);
                            this.selectSport(item);
                          }}
                        >
                          {
                            item.checkStatus === true?
                              <View style={{flex:1,alignItems:'center'}}>
                                <Avatar
                                    size="large"
                                    // rounded
                                    source={{
                                      uri: item.Path,
                                    }}
                                  />
                                {/* <Image source={{uri: item.Path}} style={{height:height(10),width:width(20),resizeMode:'contain'}}/> */}
                                <Text style={{fontSize:totalSize(1.6),color:'black'}}>{item.sport_name}</Text>
                              </View>
                              :
                              <View style={{flex:1,alignItems:'center'}}>
                                <Avatar
                                    size="medium"
                                    // rounded
                                    source={{
                                      uri: item.Path,
                                    }}
                                  />
                                {/* <Image source={{uri: item.Path}} style={{height:height(8),width:width(12),resizeMode:'contain'}}/> */}
                                <Text style={{fontSize:totalSize(1.2),color:'black'}}>{item.sport_name}</Text>
                              </View>

                          }
                        </TouchableOpacity>
                    );
                  })
                }
              </ScrollView>
            </View>
        </View>

        <View style={{height:height(22),width:width(100),justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(22),width:width(90),flexDirection:'row',backgroundColor:'#ffffff',justifyContent:'center',alignItems:'center'}}>
            <View style={{width:width(45),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
              <Text style={{fontSize:totalSize(2),color:'#5c5c7c',marginBottom:10}}>Skill Level</Text>
              <AnimatedCircularProgress
                  size={80}
                  width={5}
                  fill={this.state.skill }
                  tintColor="#00e0ff"
                  backgroundWidth = {5}
                  backgroundColor="#d4d4d4">
                  {
                    (fill) => (
                      <Text style={{fontSize:totalSize(2),color:'black'}}>
                        { this.state.skill  }%
                      </Text>
                    )
                  }
              </AnimatedCircularProgress>
            </View>
            <View style={{width:width(45),justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:totalSize(2),color:'#5c5c7c',marginBottom:10}}>Fitness</Text>
              <AnimatedCircularProgress
                  size={80}
                  width={5}
                  fill={ fitness }
                  tintColor="#00e0ff"
                  backgroundWidth = {5}
                  backgroundColor="#d4d4d4">
                  {
                    (fill) => (
                      <Text style={{fontSize:totalSize(2),color:'black'}}>
                        { fitness }%
                      </Text>
                    )
                  }
              </AnimatedCircularProgress>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },
  unSelect: {
    height:height(10),
    width:width(15),
    borderRadius:100,
    margin:0,
    justifyContent:'center',
    alignItems:'center',

  },
  selected: {
    height:height(14),
    width:width(18),
    borderRadius:100,
    margin:0,
    justifyContent:'center',
    alignItems:'center',

  },

});
