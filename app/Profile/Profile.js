
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { Icon } from 'react-native-elements'
import { Avatar } from 'react-native-elements';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import ImagePicker from 'react-native-image-crop-picker';
import { observer } from 'mobx-react';
import Store from '../Stores';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Footer from '../Footer/Footer';
const slideAnimation = new SlideAnimation({
  slideFrom: 'bottom',
});
@observer class LogoTitle extends React.Component {
  render() {
    let { orderStore } = Store;
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(74), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}>{orderStore.playerLoginRes.Team_Name}</Text>
        </View>
      </View>
    );
  }
}
@observer export default class Profile extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      avatarSource: '',
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  }
  imagePicker() {
    this.popupDialog.dismiss();
    ImagePicker.openCamera({
      cropping: true,
      width: 500,
      height: 500,
      includeExif: true,
      avoidEmptySpaceAroundImage: true,
    }).then(image => {
      this.image_update(image)
      //  console.log('received image', image);
      //  this.setState({
      //    avatarSource: {uri: image.path, width: image.width, height: image.height},
      //    images: null
      //  });
    }).catch(e => alert(e));
  }
  gallery() {
    this.popupDialog.dismiss();
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      includeBase64: true,
      cropping: true,
      includeExif: true,
      avoidEmptySpaceAroundImage: true,
    }).then(image => {
      this.image_update(image)
    });
  }
  popupDialog = () => {
    return (
      <PopupDialog
        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
        dialogAnimation={slideAnimation}
        height={height(30)}
        width={width(80)}
      >
        <View style={{ flex: 1 }}>
          <View style={{ height: height(10), justifyContent: 'center' }}>
            <Text style={{ elevation: 4, fontSize: totalSize(1.8), fontWeight: 'bold', color: 'black', marginHorizontal: 20 }}>Choose Photo...</Text>
          </View>
          <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.imagePicker()}>
            <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Take From Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ height: height(5), justifyContent: 'center' }} onPress={() => this.gallery()}>
            <Text style={{ fontSize: totalSize(1.6), color: 'black', marginHorizontal: 30 }}>Select From Gallery</Text>
          </TouchableOpacity>
        </View>
      </PopupDialog>
    );
  }
  componentWillMount() {
    let { orderStore } = Store;
    // let url = 'http://play4team.com/AdminApi/api/gePlayerSports?playerId='+orderStore.playerLoginRes.id;
    this.setState({ loading: true })
    fetch('http://play4team.com/AdminApi/api/gePlayerSports?playerId=' + orderStore.playerLoginRes.id, {
      method: 'POST',
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log('getPlayerSport = ', responseJson);
        orderStore.playerProfile = responseJson.data;
        if (responseJson.status === 'True') {
          for (var i = 0; i < orderStore.playerProfile.length; i++) {
            orderStore.playerProfile[i].checkStatus = false;
            orderStore.playerProfile[i].added = true;
          }
          // console.log('playerProfile = ',orderStore.playerProfile);
          this.setState({ loading: false })
        } else {
          this.setState({ loading: false })
          // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
      });
  }
  image_update(image) {
    let { orderStore } = Store;
    if (orderStore.playerLoginRes.Status === 0)
      var type = 0;
    else
      var type = 1;

    const formData = new FormData();
    const photo = {
      uri: image.path,
      type: image.mime,
      name: 'testPhoto',
    };
    formData.append('profile_photo', photo);
    formData.append('id', orderStore.playerLoginRes.id);
    formData.append('type', type);
    this.setState({ loading: true })

    fetch('http://play4team.com/AdminApi/api/updateImage', {
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        // console.log('response==',responseJson);
        this.setState({ loading: false })
        if (responseJson.status === 'true') {
          orderStore.playerLoginRes = responseJson.data;
          ToastAndroid.show('Your profile Photo has been successfuly updated', ToastAndroid.LONG);
          // this.props.navigation.navigate('Login');
        } else {
          this.setState({ loading: false })
          ToastAndroid.show('Image not successfuly updated', ToastAndroid.LONG);
        }
      }).catch((error) => {
        this.setState({ loading: false })
        ToastAndroid.show('Try again', ToastAndroid.LONG);
      });
  }
  render() {
    let { orderStore } = Store;
    let sports = orderStore.playerProfile.length;
    if (this.state.loading == true) {
      return (
        <View style={{ height: height(100), width: width(100), flex: 1 }} >
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{ height: height(22), width: width(100) }}>
          <Image source={require('../img/blue-bg.png')} style={{ height: height(21), width: width(100) }} />
        </View>
        <View style={{ height: height(30), width: width(100), flexDirection: 'row', justifyContent: 'center', marginTop: 10, position: 'absolute', zIndex: 1, alignItems: 'flex-start' }}>
          <TouchableOpacity style={{ height: height(7), width: width(20), justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              this.props.navigation.navigate('EditProfile');
            }}
          >
            <Icon
              size={26}
              name='pencil'
              type='evilicon'
              color='white'
            />
            {/* <Image source={require('../img/edit-icon.png')} style={{height:height(3),width:width(5),resizeMode:'contain'}} /> */}
            <Text style={{ fontSize: totalSize(1.5), color: 'white' }}> Edit Profile </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ height: height(30), width: width(60), alignItems: 'center' }} onPress={() => { this.popupDialog.show() }}>
            {
              orderStore.playerLoginRes.Path.length === 0 ?
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../img/userprofileImag.png')} style={{ height: height(22), width: width(37), resizeMode: 'contain' }} />
                  <Text style={{ fontSize: totalSize(2), color: '#1d1d4f', alignSelf: 'center' }}> {orderStore.playerLoginRes.Team_Name} </Text>
                </View>
                :
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Avatar
                    xlarge
                    rounded
                    source={{
                      uri: orderStore.playerLoginRes.Path
                    }}
                  />
                  <Text style={{ fontSize: totalSize(2.5), color: '#1d1d4f', alignSelf: 'center', fontWeight: '500' }}> {orderStore.playerLoginRes.Team_Name} </Text>
                </View>
            }
            <View style={{ height: height(5), width: width(60), flexDirection: 'row', marginHorizontal: 5, justifyContent: 'center', alignItems: 'center' }}>
              {/* <Icon size={26} name='location-pin' type='entypo' color='gray'/> */}
              <Text style={{ fontSize: totalSize(1.4), color: '#1d1d4f', textAlign: 'center' }}> {orderStore.playerLoginRes.address} </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{ height: height(7), width: width(20), justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              this.props.navigation.navigate('ViewProfile');
            }}
          >
            <Icon
              size={26}
              name='user'
              type='entypo'
              color='white'
            />
            {/* <Image source={require('../img/view-icon.png')} style={{height:height(3),width:width(5),resizeMode:'contain'}} /> */}
            <Text style={{ fontSize: totalSize(1.5), color: 'white' }}> View Profile </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(16), width: width(100) }}></View>
          <View style={{ height: height(62), width: width(100), alignItems: 'center' }}>
            <TouchableOpacity style={styles.strip}
              onPress={() => {
                this.props.navigation.navigate('Availability');
              }}
            >
              <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../img/availability.png')} style={{ height: height(7), width: width(11.5) }} />
              </View>
              <View style={{ width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>Availability</Text>
                </View>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  {
                    orderStore.playerLoginRes.visiblity === 1 ?
                      <Text style={{ fontSize: totalSize(1.2), color: 'gray' }}>Available</Text>
                      :
                      orderStore.playerLoginRes.visiblity === 0 ?
                        <Text style={{ fontSize: totalSize(1.2), color: 'gray' }}>Busy</Text>
                        :
                        <Text style={{ fontSize: totalSize(1.2), color: 'gray' }}>Possibly</Text>
                  }
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.strip}
              onPress={() => {
                this.props.navigation.navigate('MySport');
              }}
            >
              <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../img/sport.png')} style={{ height: height(7), width: width(11.5), resizeMode: 'contain' }} />
              </View>
              <View style={{ width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>My Sports</Text>
                </View>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.2), color: 'gray' }}>selected sports are {sports}</Text>
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.strip}
              onPress={() => {
                this.props.navigation.navigate('MyRating');
              }}
            >
              <View style={{ width: width(15), justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../img/rating.png')} style={{ height: height(7), width: width(11.5), resizeMode: 'contain' }} />
              </View>
              <View style={{ width: width(70), justifyContent: 'center' }}>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.8), color: 'black' }}>My Rating</Text>
                </View>
                <View style={{ height: height(3), justifyContent: 'center' }}>
                  <Text style={{ fontSize: totalSize(1.2), color: 'gray' }}>Skill , Fitness</Text>
                </View>
              </View>
              <View style={{ width: width(5), justifyContent: 'center' }}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.strip}
              onPress={()=>{
                this.props.navigation.navigate('Teams');
              }}
            >
              <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../img/contact.png')} style={{height:height(7),width:width(11.5),resizeMode:'contain'}} />
              </View>
              <View style={{width:width(70),justifyContent:'center'}}>
                <View style={{height:height(3),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.8),color:'black'}}>Teams</Text>
                </View>
                <View style={{height:height(3),justifyContent:'center'}}>
                  <Text style={{fontSize: totalSize(1.2),color:'gray'}}>My joining teams </Text>
                </View>
              </View>
              <View style={{width:width(5),justifyContent:'center'}}>
                <Image source={require('../img/ic_arrow_rit.png')} />
              </View>
            </TouchableOpacity> */}
          </View>
        </View>

        <PopupDialog
          ref={(popupDialog) => { this.popupDialog = popupDialog; }}
          dialogAnimation={slideAnimation}
          height={height(20)}
          width={width(70)}
        >
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ height: height(6), width: width(50), justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ elevation: 4, fontSize: totalSize(1.8), fontWeight: '500', color: 'black' }}>Choose Photo...</Text>
            </View>
            <TouchableOpacity style={{ height: height(5), width: width(50), justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray', marginVertical: height(1), opacity: 0.6 }} onPress={() => this.imagePicker()}>
              <Text style={{ fontSize: totalSize(1.6), color: 'white' }}>Take From Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ height: height(5), width: width(50), justifyContent: 'center', alignItems: 'center', backgroundColor: 'gray', opacity: 0.6 }} onPress={() => this.gallery()}>
              <Text style={{ fontSize: totalSize(1.6), color: 'white' }}>Select From Gallery</Text>
            </TouchableOpacity>
          </View>
        </PopupDialog>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  strip: {
    height: height(8),
    width: width(90),
    backgroundColor: '#ffffff',
    borderBottomWidth: 0.3,
    marginBottom: 0.3,
    borderColor: 'gray',
    flexDirection: 'row',
    borderRadius: 5,
    marginTop: 5,
  },
});
