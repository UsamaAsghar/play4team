
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, ImageBackground, TextInput, Image, ScrollView, TouchableOpacity, ToastAndroid, Slider
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { Polyline, Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import { observer } from 'mobx-react';
import Store from '../Stores';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-circle-checkbox'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), width: width(80), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(80), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}> Edit Profile </Text>
        </View>
      </View>
    );
  }
}
@observer export default class EditProfile extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      name: '',
      email: '',
      phone_no: '',
      address: '',
      dob: '',
      password: '',
      rewrite: false,
      fitness: 0
    }
  }
  // get() {
  //   let { orderStore } = Store;
  //   console.log('name', this.state.name);
  //   console.log('email', this.state.email);
  //   console.log('phone', this.state.phone_no);
  //   console.log('address', this.state.address);
  //   console.log('dob', this.state.dob);
  //   console.log('password', this.state.password);
  //   console.log('id', orderStore.playerLoginRes.id);
  // }
  componentWillMount = async () => {
    let { orderStore } = Store;
    await this.setState({
      name: orderStore.playerLoginRes.Team_Name,
      address: orderStore.playerLoginRes.address,
      email: orderStore.playerLoginRes.Email,
      phone_no: orderStore.playerLoginRes.phone,
      dob: orderStore.playerLoginRes.Dates,
      password: orderStore.playerLoginRes.password,
      fitness: orderStore.playerLoginRes.Fitness

    })
    //console.warn('fitness==>', this.state.fitness)
  }
  update() {
    let { orderStore } = Store;
    if (this.state.fitness === 0 && this.state.email.length === 0 && this.state.name.length === 0 && this.state.phone_no.length === 0 && this.state.address.length === 0 && this.state.dob.length === 0 && this.state.password.length === 0) {
      ToastAndroid.show('Please update anything first', ToastAndroid.LONG);
    }
    else {
      // this.get();
      this.setState({ loading: true })
      var url= 'http://play4team.com/AdminApi/api/updateTeamInfo?teamId='+orderStore.playerLoginRes.id+'&Team_Name='+this.state.name+'&Email='+this.state.email+'&Phone='+this.state.phone_no+'&address='+this.state.address+'&Dates='+this.state.dob+'&password='+this.state.password+'&profile_photo='+orderStore.playerLoginRes.Path + '&Rate=' + this.state.fitness ;
      console.log('Update Screen Url=',url);
      fetch('http://play4team.com/AdminApi/api/updateTeamInfo?teamId=' + orderStore.playerLoginRes.id + '&Team_Name=' + this.state.name + '&Email=' + this.state.email + '&phone=' + this.state.phone_no + '&address=' + this.state.address + '&Dates=' + this.state.dob + '&password=' + this.state.password + '&Rate=' + this.state.fitness, {
        method: 'POST'
      }).then((response) => response.json())
        .then((responseJson) => {
          console.log('Editresponse==',responseJson);
          this.setState({ loading: false })
          if (responseJson.status === 'True') {
            orderStore.playerLoginRes = responseJson.data;
            ToastAndroid.show('Your profile data has been successfuly updated', ToastAndroid.LONG);
            // this.props.navigation.navigate('Login');
          } else {
            this.setState({ loading: false })
            ToastAndroid.show('Please enter all required fields', ToastAndroid.LONG);
          }
        }).catch((error) => {
          this.setState({ loading: false })
          ToastAndroid.show('Try again', ToastAndroid.LONG);
        });

    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f'
    },
    headerTintColor: 'white',
  }
  render() {
    let { orderStore } = Store;
    // console.log('name==',orderStore.playerLoginRes.Name);
    if (this.state.loading == true) {
      return (
        <ImageBackground style={{ height: height(100), width: width(100), flex: 1 }} source={require('../img/splash.jpg')} >
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="small"
            messageFontSize={18}
            message="Loading..."
          />
        </ImageBackground>
      );
    }

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>Name</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.setState({ name: value, rewrite: true })}
              underlineColorAndroid='transparent'
              value={this.state.name}
              placeholderTextColor='black'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              keyboardAppearance='dark'
              style={{ height: height(6.5), width: width(90), fontSize: totalSize(1.7), textAlign: 'left', }}
            />
          </View>

          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>Email</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.setState({ email: value, rewrite: true })}
              underlineColorAndroid='transparent'
              value={this.state.email}
              placeholderTextColor='black'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              keyboardAppearance='dark'
              style={{ height: height(6.5), width: width(90), fontSize: totalSize(1.7), textAlign: 'left' }}
            />
          </View>

          {/*<View style={{height:height(6),width:width(100),justifyContent:'center',backgroundColor:'#f0f0f0'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#6f6f6f',marginLeft:15}}>Phone no</Text>
          </View>
          <View style={{height:height(8),width:width(100),justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff'}}>
            <TextInput
                onChangeText={(value) => this.setState({phone_no: value , rewrite: true})}
                underlineColorAndroid='transparent'
                value= {this.state.phone_no}
                placeholderTextColor='black'
                underlineColorAndroid='transparent'
                keyboardType='numeric'
                autoCorrect={false}
                keyboardAppearance='dark'
                style={{height:height(6.5),width:width(90),fontSize:totalSize(1.7),textAlign :'left'}}
                />
          </View>*/}

          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>Home Address</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.setState({ address: value, rewrite: true })}
              underlineColorAndroid='transparent'
              value={this.state.address}
              placeholderTextColor='black'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              keyboardAppearance='dark'
              style={{ height: height(6.5), width: width(90), fontSize: totalSize(1.7), textAlign: 'left' }}
            />
          </View>

          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>Dob</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.setState({ dob: value, rewrite: true })}
              underlineColorAndroid='transparent'
              value={this.state.dob}
              placeholderTextColor='black'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              keyboardAppearance='dark'
              style={{ height: height(6.5), width: width(90), fontSize: totalSize(1.7), textAlign: 'left' }}
            />
          </View>

          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>Password</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
            <TextInput
              onChangeText={(value) => this.setState({ password: value, rewrite: true })}
              underlineColorAndroid='transparent'
              value={this.state.password}
              // placeholderTextColor='black'
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              // keyboardType = 'numeric'
              keyboardAppearance='dark'
              style={{ height: height(6.5), width: width(90), fontSize: totalSize(1.7), textAlign: 'left', color:'black' }}
            />
          </View>

          <View style={{ height: height(6), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Text style={{ fontSize: totalSize(1.8), color: '#6f6f6f', marginLeft: 15 }}>rate your general fitness</Text>
          </View>
          <View style={{ height: height(8), width: width(100), justifyContent: 'center', backgroundColor: '#f0f0f0' }}>
            <Slider
              value={this.state.fitness}
              onValueChange={value => this.setState({ fitness: parseInt(value) , rewrite: true })}
              maximumValue={10}
              minimumValue={0}
              thumbTintColor='#29d9a4'
              maximumTrackTintColor='#1fefd5'
              minimumTrackTintColor='#1fefd5'
              style={{ backgroundColor: 'white', height: height(6) }}
            />
            <View style={{ height: height(3), justifyContent: 'center', alignItems: 'flex-end', marginRight: 15 }}>
              <Text style={{ fontSize: totalSize(1.9), color: '#32325e' }}>Your rate {parseInt(this.state.fitness)} out of 10</Text>
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity style={{ height: height(8), width: width(100), justifyContent: 'center', backgroundColor: '#ffffff' }}
          onPress={() => {
            if (this.state.rewrite === true) {
              this.update();
            } else {
              ToastAndroid.show('You should first rewrite any field', ToastAndroid.LONG);
            }

          }}
        >
          <ImageBackground source={require('../img/bar.png')} style={{ height: height(8), width: width(100), justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: totalSize(2.2), color: 'white' }}>SAVE</Text>
          </ImageBackground>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  check: {
    height: height(7),
    width: width(90),
    marginTop: 15,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#fefefe'
  },
  checked: {
    height: height(7),
    width: width(90),
    marginTop: 15,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#1d1d4f'
  },
  txt: {
    fontSize: totalSize(1.8),
    color: '#1d1d4f'
  },
  text: {
    fontSize: totalSize(1.8),
    color: 'white'
  },

});
