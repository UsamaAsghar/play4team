
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, Image, ScrollView, TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Avatar } from 'react-native-elements';
import { observer } from 'mobx-react';
import Store from '../Stores';
@observer class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{ height: height(9.5), flexDirection: 'row', backgroundColor: '#1d1d4f' }}>
        <View style={{ width: width(70), justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Image source={require('../img/logo.png')} style={{ height: height(7), width: width(12) }} />
          <Text style={{ fontSize: totalSize(2.5), color: 'white' }}> Profile </Text>
        </View>
      </View>
    );
  }
}
@observer export default class ViewProfile extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      avatarSource: '',
      skill: 0,
    }
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerStyle: {
      backgroundColor: '#1d1d4f',
    },
    headerTintColor: 'white',
  }

  componentWillMount() {
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.playerProfile.length; i++) {
      // orderStore.playerProfile[0].added = true;
      orderStore.playerProfile[0].checkStatus = true;
      this.setState({
        skill: orderStore.playerProfile[0].rate * 10,
      })
    }
  }
  selectSport(sport) {
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.playerProfile.length; i++) {
      if (sport.sport_id === orderStore.playerProfile[i].sport_id) {
        // orderStore.playerProfile[i].added = true;
        orderStore.playerProfile[i].checkStatus = true;
        orderStore.sport_id = orderStore.playerProfile[i].sport_id;
        this.setState({
          // fitness: orderStore.playerLoginRes.Fitness,
          skill: orderStore.playerProfile[i].rate * 10,
        })
      } else {
        // orderStore.sportEvent[i].added = false;
        orderStore.playerProfile[i].checkStatus = false;
      }
    }
    // console.log('selected id=',orderStore.sport_id);
    // console.log('selected sport=',this.state.skill);
  }

  componentWillUnmount() {
    let { orderStore } = Store;
    for (var i = 0; i < orderStore.playerProfile.length; i++) {
      // orderStore.playerProfile[i].added = false;
      orderStore.playerProfile[i].checkStatus = false;
    }
    orderStore.sport_id = 0;  
  }

  render() {
    let { orderStore } = Store;
    var fitness = orderStore.playerLoginRes.Fitness * 10;
    // console.log('data',orderStore.playerProfile);
    return (
      <View style={styles.container}>
        <View style={{ height: height(12), width: width(100), backgroundColor: '#1d1d4f' }}>

        </View>
        <View style={{ height: height(26), width: width(100), justifyContent: 'center', marginTop: 10, position: 'absolute', zIndex: 1, alignItems: 'center' }}>
          <TouchableOpacity style={{height: height(26), width: width(40), alignItems: 'center' }}>
            {
              orderStore.playerLoginRes.Path.length === 0 ?
                <Image source={require('../img/userprofileImag.png')} style={{ height: height(17), width: width(35), borderRadius: 100, resizeMode: 'contain' }} />
                :
                <Image source={{ uri: orderStore.playerLoginRes.Path }} style={{ height: height(17), width: width(35), borderRadius: 100, resizeMode: 'contain' }} />

            }
            <Text style={{ fontSize: totalSize(2), color: '#1d1d4f', textAlign: 'center' }}> {orderStore.playerLoginRes.Team_Name} </Text>
            <Text style={{ fontSize: totalSize(1.5), color: '#949494', textAlign: 'center', marginBottom: 5 }}> {orderStore.playerLoginRes.address} </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ height: height(16), width: width(100), backgroundColor: '#ffffff' }}>
          </View>
          <View style={{ height: height(47), width: width(100), marginTop: 20, backgroundColor: '#ffffff' }}>
            <View style={{ height: height(22), alignItems: 'center' }}>
              <View style={{ height: height(5), justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: totalSize(2.2), color: '#949494' }}>Average Ratings</Text>
              </View>
              <View style={{ height: height(17), width: width(90), alignItems: 'center', backgroundColor: '#ffffff' }}>
                <View style={{ height: height(17), width: width(80), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    scrollToEnd={true}
                  >
                    {
                      orderStore.playerProfile.map((item, key) => {
                        return (
                          <TouchableOpacity key={key} style={item.checkStatus === true ? styles.selected : styles.unSelect}
                            onPress={() => {
                              // this.setState({
                              //   sport_id: item.id
                              // })
                              // console.warn('id = ', item.id);
                              this.selectSport(item);
                            }}
                          >
                            {
                              item.checkStatus === true ?
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                  <Avatar
                                    size="xlarge"
                                    // rounded
                                    source={{
                                      uri: item.Path,
                                    }}
                                  />
                                  {/* <Image source={{ uri: item.Path }} style={{ height: height(10), width: width(20), resizeMode: 'contain' }} /> */}
                                  <Text style={{ fontSize: totalSize(1.6), color: 'black' }}>{item.sport_name}</Text>
                                </View>
                                :
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                  <Avatar
                                    size="medium"
                                    // rounded
                                    source={{
                                      uri: item.Path,
                                    }}
                                  />
                                  {/* <Image source={{ uri: item.Path }} style={{ height: height(8), width: width(12), resizeMode: 'contain' }} /> */}
                                  <Text style={{ fontSize: totalSize(1.2), color: 'black' }}>{item.sport_name}</Text>
                                </View>

                            }
                          </TouchableOpacity>
                        );
                      })
                    }
                  </ScrollView>
                </View>
              </View>
            </View>
            <View style={{ height: height(25), alignItems: 'center' }}>
              <View style={{ height: height(25), width: width(90), flexDirection: 'row', backgroundColor: '#ffffff', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: width(45), justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                  <Text style={{ fontSize: totalSize(2), color: '#5c5c7c', marginBottom: 10 }}>Skill Level</Text>
                  <AnimatedCircularProgress
                    size={80}
                    width={5}
                    fill={this.state.skill}
                    tintColor="#00e0ff"
                    backgroundWidth={5}
                    backgroundColor="#cfd8dc">
                    {
                      (fill) => (
                        <Text style={{ fontSize: totalSize(2), color: 'black' }}>
                          {this.state.skill}%
                          </Text>
                      )
                    }
                  </AnimatedCircularProgress>
                </View>
                <View style={{ width: width(45), justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: totalSize(2), color: '#5c5c7c', marginBottom: 10 }}>Fitness</Text>
                  <AnimatedCircularProgress
                    size={80}
                    width={5}
                    fill={fitness}
                    tintColor="#00e0ff"
                    backgroundWidth={5}
                    backgroundColor="#cfd8dc">
                    {
                      (fill) => (
                        <Text style={{ fontSize: totalSize(2), color: 'black' }}>
                          {fitness}%
                          </Text>
                      )
                    }
                  </AnimatedCircularProgress>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  strip: {
    height: height(8),
    width: width(90),
    backgroundColor: '#ffffff',
    borderBottomWidth: 0.3,
    marginBottom: 0.3,
    borderColor: 'gray',
    flexDirection: 'row',
    borderRadius: 5,
    marginTop: 5,
  },
  unSelect: {
    height: height(10),
    width: width(15),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  selected: {
    height: height(14),
    width: width(18),
    borderRadius: 100,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
