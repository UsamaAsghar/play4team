
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,KeyboardAvoidingView
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { observer } from 'mobx-react';
import Store from '../Stores';
import Swipeout from 'react-native-swipeout';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
class LogoTitle extends React.Component {
  render() {
    return (
      <View style={{height:height(9.5),width:width(75),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
        <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
          <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
          <Text style={{fontSize:totalSize(2.5),color:'white'}}> Teams </Text>
        </View>
      </View>
    );
  }
}
export default class Teams extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      teamList: null,
    }
  }
  static navigationOptions = {
      headerTitle: <LogoTitle />,
      headerStyle: {
        backgroundColor:'#1d1d4f'
      },
      headerTintColor:'white',
    }
    componentWillMount = async() => {
        let { orderStore } = Store;
      // let url = 'http://play4team.com/AdminApi/api/gePlayerSports?playerId='+orderStore.playerLoginRes.id;
          this.setState({loading: true})
          await fetch('http://play4team.com/AdminApi/api/playerTeamsList?playerId='+orderStore.playerLoginRes.id, {
           method: 'POST',
            }).then((response) => response.json())
                  .then(async(responseJson) => {
                      console.log('teamList = ',responseJson);
                    if (responseJson.status === 'true') {
                        await this.setState({teamList: responseJson,loading: false})
                    }else {
                      this.setState({loading: false})
                      // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                    }
                  }).catch((error)=>{
                      this.setState({loading: false})
                      // ToastAndroid.show('Please enter your valid email address or password', ToastAndroid.LONG);
                  });
    }
  _member = (item,key) =>{
    return(
    
          <View key={key} style={{height:height(8),width:width(90),borderBottomWidth:0.5,borderColor:'gray',marginBottom:0.5,backgroundColor:'#ffffff',flexDirection:'row'}}>
            <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
              <Image source={item.Path.length>0? {uri: item.Path}:require('../img/friend_img.png')} style={{height:height(6),width:width(12),resizeMode:'contain',borderRadius: 90}} />
            </View>
            <View style={{width:width(70),flexDirection:'row'}}>
              <View style={{width:width(53),justifyContent:'center'}}>
                <View style={{height:height(6),justifyContent:'center',alignItems:'flex-start'}}>
                  <Text style={{fontSize:totalSize(2),color:'black'}}>{item.Team_Name}</Text>
                </View>
              </View>
              <TouchableOpacity style={{ width:width(15),justifyContent:'center',alignItems:'flex-end'}}
                onPress={()=>{
                  this.props.navigation.navigate('TeamDetail',{item: item});
                }}
              >
                <Text style={{ width:width(8),fontSize: 12 , color:'black' }}>View</Text>
                {/* <Image source={require('../img/user_profile.png')} style={{height:height(5),width:width(8),borderWidth:1,borderColor:'black'}} /> */}
              </TouchableOpacity>
            </View>
          </View>
    );
  }
  _header = () => {
      return(
        <View style={{height:height(7),width:width(100),flexDirection:'row',backgroundColor:'#33327e',justifyContent:'center',alignItems:'center'}}>
          <View style={{height:height(5.5),width:width(80),backgroundColor:'white',borderRadius:5,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{height:height(5.5),width:width(15),justifyContent:'center',alignItems:'center'}}
                onPress={()=>{
                  if (this.state.name.length === 0) {
                      ToastAndroid.show('Have you enter any member name ???', ToastAndroid.LONG);
                  }else {
                      this.searchMember()
                  }
                }}
              >
              <Image source={require('../img/search_btn.png')} style={{height:height(4),width:width(7),backgroundColor:'transparent'}}/>
            </TouchableOpacity>
            <View style={{height:height(7),width:width(70)}}>
              <TextInput
                onChangeText={(value) => this.setState({name: value})}
                value={this.state.name}
                placeholder={"Name"}
                placeholderTextColor='gray'
                style={{fontSize:totalSize(2),color:'black',backgroundColor:'white',width:width(65)}}
                underlineColorAndroid="transparent"
                />
            </View>
          </View>
          <TouchableOpacity style={{height:height(5.5),width:width(7),justifyContent:'center',alignItems:'flex-end'}}
            onPress={()=>{ this.clearSearch() }}
          >
            <Image source={require('../img/cross-icon.png')} style={{height:height(3),width:width(5),backgroundColor:'transparent'}}/>
          </TouchableOpacity>
        </View>
      );
  }
  render() {
    let { orderStore } = Store;
      if(this.state.loading == true){
          return(
              <View style={{flex:1,height:height(100),width:width(100),backgroundColor:'rgba(29,37,86,0.6)'}}>
                <OrientationLoadingOverlay
                    visible={true}
                    color="white"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
              </View>
          );
      }
    return (
            <View  style={{flex:1,alignItems:'center'}}>
              {
                  this.state.teamList.data !== [] ?
                    this.state.teamList.data.map((item,key)=>{
                        return(
                            this._member(item,key)
                        );
                    })
                    :
                    <View style={{ flex:1,justifyContent:'center',alignItems:'center' }}>
                        <Text style={{ fontSize: 14,color:'black' }}>Currently you are not a member of any team.</Text>
                    </View>
              }
            </View >
     );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },

});
