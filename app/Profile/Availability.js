
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,ToastAndroid,Alert
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox'
import { observer } from 'mobx-react';
import Store from '../Stores';
class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),width:width(80),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
            <Text style={{fontSize:totalSize(2.5),color:'white'}}> Availability </Text>
          </View>
        </View>
    );
  }
}
@observer export default class TeamAvailability extends Component<Props> {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      avail: false,
      busy : false,
      possible: false,
      status: '',
    }
  }
  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerStyle: {
         backgroundColor:'#1d1d4f'
       },
       headerTintColor:'white',
    }

    alert(){
      if (this.state.status === 1) {
          Alert.alert(
            'Available',
            'You can be found at any time by anyone searching for players in your area',
            [
              {text: 'OK', onPress: () => this.availability()},
            ],

          )
      }else {
          if (this.state.status===0) {
            Alert.alert(
              'Busy',
              'You can not available for any game',
              [
                {text: 'OK', onPress: () => this.availability()},
              ],
              // { cancelable: false }
            )
          }else {
            Alert.alert(
              'Possibly',
              'You are available as per your schedule time',
              [
                {text: 'OK', onPress: () => this.availability()},
              ],
              // { cancelable: false }
            )
          }
      }
    }

    availability(status){
        let { orderStore } = Store;
          // console.warn('id===',orderStore.playerLoginRes.id);
          let url = 'http://play4team.com/AdminApi/api/Players/Available/'+orderStore.playerLoginRes.id+'?' +'&visiblity='+this.state.status;
          // console.log('url=',url);
          this.setState({loading: true})
          fetch('http://play4team.com/AdminApi/api/Team/Available/'+orderStore.playerLoginRes.id+'?' +'&visiblity='+this.state.status, {
             method: 'PUT',
             headers: {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
              }
            }).then((response) => response.json())
                  .then((responseJson) => {
                      this.setState({loading: false})
                    if (responseJson.status === 'True') {
                          orderStore.playerLoginRes.visiblity = this.state.status;
                          console.log('status',responseJson);
                         // ToastAndroid.show('', ToastAndroid.LONG);
                    }else {
                      this.setState({loading: false})
                      ToastAndroid.show('There is some network issue please check your connection and try again', ToastAndroid.LONG);
                    }
                  })
    }
    componentWillMount(){
      let { orderStore } = Store;
      // console.warn('visiblity=',orderStore.playerLoginRes.visiblity);
      if (orderStore.playerLoginRes.visiblity === 1) {
          this.setState({
            avail: true,
            busy: false,
            possible: false
          })
      }else {
          if (orderStore.playerLoginRes.visiblity === 0) {
            this.setState({
              avail: false,
              busy: true,
              possible: false
            })
          }else {
            this.setState({
              avail: false,
              busy: false,
              possible: true
            })
          }
      }
    }
  render() {
    let { orderStore } = Store;

    return (
      <View style={styles.container}>
        <View style={{flex:1}}>
          <TouchableOpacity style={this.state.avail === true? styles.checked : styles.check}
            onPress={()=>{
              this.setState({
                possible: false,
                avail: true,
                busy: false,
                status: 1
              })
              // this.availability(status = 1);
            }}
          >
            <View style={{height:height(7),width:width(15),justifyContent:'center',alignItems:'center'}}>
              <CircleCheckBox
                checked = {this.state.avail}
                outerColor  = {'#5b5b7e'}
                innerColor  = {'#478c5e'}
                />
            </View>
            <View style={{height:height(7),width:width(75),justifyContent:'center'}}>
              <Text style={ this.state.avail === true? styles.text : styles.txt }> Available </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={this.state.busy === true? styles.checked : styles.check}
            onPress={()=>{
              this.setState({
                possible: false,
                avail: false,
                busy: true,
                status: 0
              })
                // this.availability(status = 0);
            }}
            >
            <View style={{height:height(7),width:width(15),justifyContent:'center',alignItems:'center'}}>
              <CircleCheckBox
                checked = {this.state.busy}
                outerColor  = {'#5b5b7e'}
                innerColor  = {'#d21154'}
                />
            </View>
            <View style={{height:height(7),width:width(75),justifyContent:'center'}}>
              <Text style={ this.state.busy === true? styles.text : styles.txt }> Busy </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={this.state.possible === true? styles.checked : styles.check}
            onPress={()=>{
              this.setState({
                avail: false,
                busy: false,
                possible: true,
                status: 2
              })
                // this.availability(status = 0);
            }}
            >
            <View style={{height:height(7),width:width(15),justifyContent:'center',alignItems:'center'}}>
              <CircleCheckBox
                checked = {this.state.possible}
                outerColor  = {'#5b5b7e'}
                innerColor  = {'pink'}
                />
            </View>
            <View style={{height:height(7),width:width(75),justifyContent:'center'}}>
              <Text style={ this.state.possible === true? styles.text : styles.txt }> Possibly </Text>
            </View>
          </TouchableOpacity>
          <View style={{height:height(7),width:width(90),marginTop:20,justifyContent:'center'}}>
            <Text style={{fontSize:totalSize(2),color:'#1d1e4e',fontWeight:'bold'}}> Available </Text>
            <Text style={{fontSize:totalSize(1.5),color:'gray',marginHorizontal: 5}}>You can be found at any time by anyone searching for a player in your area.</Text>
          </View>
          <View style={{height:height(7),width:width(90),justifyContent:'center'}}>
            <Text style={{fontSize:totalSize(2),color:'#1d1e4e',fontWeight:'bold'}}> Busy </Text>
            <Text style={{fontSize:totalSize(1.5),color:'gray',marginHorizontal: 5}}>You are not available at this moment in time.</Text>
          </View>
          <View style={{height:height(7),width:width(90),justifyContent:'center'}}>
            <Text style={{fontSize:totalSize(2),color:'#1d1e4e',fontWeight:'bold'}}> Possibly </Text>
            <Text style={{fontSize:totalSize(1.5),color:'gray',marginHorizontal: 5}}>You are available as per your schedule time.</Text>
          </View>
        </View>
        <TouchableOpacity style={{height:height(7),width:width(100)}}
          onPress={()=>{
            this.alert()
          }}
        >
          <ImageBackground style={{height:height(7),width:width(100),justifyContent:'center',alignItems:'center'}} source={require('../img/bar.png')}>
            <Text style={{fontSize:totalSize(2.2),color:'white'}}>Save</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },
  check: {
    height:height(7),
    width:width(90),
    marginTop:15,
    flexDirection:'row',
    borderRadius:10,
    backgroundColor:'#fefefe'
  },
  checked: {
    height:height(7),
    width:width(90),
    marginTop:15,
    flexDirection:'row',
    borderRadius:10,
    backgroundColor:'#1d1d4f'
  },
  txt: {
    fontSize:totalSize(1.8),
    color:'#1d1d4f'
  },
  text: {
    fontSize:totalSize(1.8),
    color:'white'
  },

});
