
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity,Slider,ToastAndroid
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { Avatar } from 'react-native-elements';
import { observer } from 'mobx-react';
import Store from '../Stores';

class LogoTitle extends React.Component {

  render() {
    return (
        <View style={{height:height(9.5),width:width(75),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(75),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(7),width:width(12)}} />
            <Text style={{fontSize:totalSize(2.5),color:'white'}}> My Sports </Text>
          </View>
        </View>
    );
  }
}
@observer export default class MySport extends Component<Props> {

  constructor(props,context){
    super(props,context);
    this.state = {
      loading: false,
      search: '',
      sports: [],
      sport_id: [],
      skillRate: [],
      removeSport: [],
      selectedSports: [],
      added: false,
    }
  }
  static navigationOptions = {
       headerTitle: <LogoTitle />,
       headerStyle: {
         backgroundColor:'#1d1d4f'
       },
       headerTintColor:'white',
    }
    componentWillMount(){
      let { orderStore } = Store;
      this.setState({loading: true})
      fetch('http://play4team.com/AdminApi/api/Sport', {
       method: 'GET',
        }).then((response) => response.json())
              .then((responseJson) => {
                 console.log('sports=',responseJson);
                  this.setState({loading: false})
                if (responseJson.status === 'True') {
                      orderStore.sports = responseJson.data;
                      for (var i = 0; i < orderStore.sports.length; i++) {
                          orderStore.sports[i].checkStatus = false;
                          orderStore.sports[i].added = false;
                          orderStore.sports[i].skillRate = 5;
                          for (var j = 0; j < orderStore.playerProfile.length; j++) {
                            if (orderStore.sports[i].id === orderStore.playerProfile[j].sport_id) {
                                orderStore.sports[i].checkStatus = true;
                                orderStore.sports[i].added = true;
                                orderStore.sports[i].skillRate = orderStore.playerProfile[j].rate;
                                this.state.sport_id.push(orderStore.sports[i].id);
                                this.state.skillRate.push(orderStore.sports[i].skillRate);
                                // this.state.selectedSports.push(orderStore.sports[i].id +'-'+orderStore.sports[i].skillRate);
                            }
                          }
                          if (orderStore.sports[i].added === false && orderStore.sports[i].checkStatus === false) {
                              this.state.removeSport.push(orderStore.sports[i].id);
                          }
                      }
                      // console.log('sports=',this.state.selectedSports);
                      // console.log('skillRate=',this.state.skillRate);
                      // console.log('new sports==',orderStore.sports);
                      // console.log('fill remove list=',this.state.removeSport);
                      this.setState({ sports: orderStore.sports })
                }else {
                  this.setState({loading: false})
                  ToastAndroid.show('there is a error', ToastAndroid.SHORT);
                }
              }).catch((error)=>{
                  this.setState({loading: false})
                  ToastAndroid.show('error', ToastAndroid.SHORT);
              });
    }
    selectSport(sport_id){
      let { orderStore } = Store;
      var index = this.state.removeSport.indexOf(sport_id);
      this.state.removeSport.splice(index, 1);
      for (var i = 0; i < orderStore.sports.length; i++) {
        if (sport_id === orderStore.sports[i].id) {
            orderStore.sports[i].checkStatus = true;
            orderStore.sports[i].added = true;
            orderStore.sports[i].skillRate = 5;
            this.state.selectedSports.push(orderStore.sports[i].id +'-'+orderStore.sports[i].skillRate);
            this.state.sport_id.push(orderStore.sports[i].id);
            this.state.skillRate.push(orderStore.sports[i].skillRate)
            // orderStore.playerProfile.push(orderStore.sports[i])
            this.setState({
              added: orderStore.sports[i].added
            })
            // console.log('****selected******');
            // console.log('selectedSports====>>>>',this.state.selectedSports)
            // console.log('id are ==',this.state.sport_id);
            // console.log('rates are ==',this.state.skillRate);
            // console.log('new obj ===',orderStore.sports);
            // // console.log('new playerProfile=',orderStore.playerProfile);
            // console.log('index',index);
            // console.log('removeSport=',this.state.removeSport);
        }
      }
    }
    unSelectSport(sport , state ){
      let{orderStore} = Store;
      if (state === false) {
        var index = 0;
        for(var i=0; i< this.state.sport_id.length; i++){
            if(this.state.sport_id[i] === sport.id ){
                index = i;
                break;
            }
      }
      this.state.sport_id.splice(index, 1);
      this.state.skillRate.splice(index, 1);
      if (this.state.removeSport.includes(sport.id)) {
          null
      }else {
        this.state.removeSport.push(sport.id);
      }
      sport.skillRate = 5;
      sport.checkStatus = false;
      sport.added = false;
      this.setState({
        added: orderStore.sports[i].added
      })
      // console.log('****unselected******');
      // console.log('sport unselected',this.state.sport_id);
      // console.log('rates are ==',this.state.skillRate);
      // // console.log('playerProfile=',orderStore.playerProfile);
      // console.log('sport after',orderStore.sports);
      // console.log('remove list=',this.state.removeSport);
    }
  }
  setSkillRate=async(value,sport)=>{
    await this.setState({ skillRate: [], sport_id: [] })
    let { orderStore } = Store;
    let rate = parseInt(value);
    let index = 0;
    for (let i = 0; i < orderStore.sports.length; i++) {
      if (sport.id === orderStore.sports[i].id && sport.added === true ) {
          index = i;
          //this.state.skillRate.splice(index, 1);
          sport.skillRate = rate;
          // this.state.selectedSports.push(orderStore.sports[i].id +'-'+orderStore.sports[i].skillRate);
          for (let j = 0; j < orderStore.sports.length; j++) {
            if (orderStore.sports[j].added && orderStore.sports[j].checkStatus) {
              this.state.skillRate.push(orderStore.sports[j].skillRate); 
              this.state.sport_id.push(orderStore.sports[j].id);
            }
          }

          this.setState({
            added: orderStore.sports[i].added
          })
          // console.log('selectedSports skillRate====>>>',this.state.selectedSports);
          
          console.log('id are  ===',this.state.sport_id);
          console.warn('rate are ==',this.state.skillRate);
      }
    }
  }
  rateSports(){
      let { orderStore } = Store;
    // if ( this.state.sport_id.length === 0 || this.state.skillRate.length === 0 ) {
    //   ToastAndroid.show('Please rate your skills', ToastAndroid.LONG);
    // }
    //   else {
              var url = 'http://play4team.com/AdminApi/api/playerSports?playerId='+orderStore.playerLoginRes.id+'&sportId='+this.state.sport_id+'&rate='+this.state.skillRate+'&removeSportId='+this.state.removeSport;
              console.log('url is ====>>>>',url);
              
              this.setState({loading: true})
              fetch('http://play4team.com/AdminApi/api/playerSports?playerId='+orderStore.playerLoginRes.id+'&sportId='+this.state.sport_id+'&rate='+this.state.skillRate+'&removeSportId='+this.state.removeSport, {
               method: 'POST',
                 // headers: {
                 //   'Content-Type': 'application/x-www-form-urlencoded',
                 //   // 'Accept': 'application/json',
                 //  }
                }).then((response) => response.json())
                      .then((responseJson) => {
                          this.setState({loading: false})
                          orderStore.playerProfile = responseJson.data;
                          console.log('now done =',responseJson);
                        if (responseJson.status === 'True') {
                             ToastAndroid.show('Done', ToastAndroid.LONG);
                        }else {
                          this.setState({loading: false})
                          ToastAndroid.show('please try again there is some network issue', ToastAndroid.LONG);
                        }
                      }).catch((error)=>{
                          this.setState({loading: false})
                          ToastAndroid.show('There is a network issue please,Try again', ToastAndroid.LONG);
                      });
          //}
  }
  searchSports(value){
    let { orderStore } = Store;
    if(value.length < 0){
        ToastAndroid.show('Note: Please Enter Spot Name',ToastAndroid.LONG);
    }else{
        let { orderStore } = Store;
        let sports = orderStore.sports;
        // orderStore.searchResult = [];
        for(var i = 0;i < orderStore.sports.length; i++){
          if(orderStore.sports[i].Name.includes(value)){
            orderStore.searchResult.push(orderStore.sports[i]);
          }
        }
        // console.warn('search sports',orderStore.searchResult);
         // Actions.searchResult({type: 'replace'});
    }
}

  render() {

    let {orderStore} = Store;
    // console.log('name==',orderStore.playerLoginRes.Name);
    // console.log('sports==',orderStore.sports);
    if(this.state.loading === true){
        return(
            <View style={{height: height(100), width: width(100), flex:1}} >
                <OrientationLoadingOverlay
                    visible={true}
                    color="#1d1d4f"
                    indicatorSize="small"
                    messageFontSize={18}
                    message="Loading..."
                />
            </View>
        );
    }

    return (
      <View style={styles.container}>
          <View style={{height:height(5),width:width(100),backgroundColor:'#eaedf2',marginBottom:15,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:totalSize(1.8),color:'#33327e'}}>Most Popular </Text>
          </View>
          <View style={{flex:1}}>
            <ScrollView>
            {
              orderStore.sports.map(( item , key )=>{
                return(
                  <View key={key} style={{flex:1}}>
                    <TouchableOpacity style={styles.strip}
                      onPress={()=>{
                        if (item.added === true) {
                         this.unSelectSport(item,false);
                        }else {
                         this.selectSport(item.id);
                        }

                      }}
                    >
                      <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                        <View style={{height:height(6),width:width(10),justifyContent:'center',alignItems:'center'}}>
                          <Avatar
                            size="medium"
                            // rounded
                            source={{
                            uri: item.Path,
                            }}
                          />
                          {/* <Image source={{uri: item.Path}} style={{height:height(6),width:width(10),resizeMode:'contain'}}/> */}
                        </View>
                      </View>
                      <View style={{width:width(60),justifyContent:'center'}}>
                        <Text style={{fontSize: totalSize(1.8),color:'black'}}>{item.Name}</Text>
                      </View>
                      <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
                        {
                          item.added === true?
                            <Image source={require('../img/tick.png')} />
                            :
                            <Image source={require('../img/unCheck.png')} />
                        }
                      </View>
                    </TouchableOpacity>
                    {
                      item.added === true  && item.checkStatus === true?
                        <View style={{height:height(15),width:width(90),marginBottom:15}}>
                          <View style={{height:height(5),justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize: totalSize(1.5),color:'black'}}>Nice, how do you rate your skill level?</Text>
                          </View>
                          <View style={{height:height(10),backgroundColor:'#ffffff',borderRadius:10}}>
                            <View style={{height:height(6)}}>
                              <Slider
                                value={item.skillRate}
                                onValueChange={ value => this.setSkillRate(value,item)}
                                maximumValue={10}
                                minimumValue={0}
                                thumbTintColor='#29d9a4'
                                maximumTrackTintColor='#393965'
                                minimumTrackTintColor='#1fefd5'
                                style={{backgroundColor:'white',height:height(6)}}
                              />
                            </View>
                            <View style={{height:height(3),justifyContent:'center',alignItems:'flex-end',marginRight:15}}>
                              <Text style={{fontSize: totalSize(1.9),color:'#32325e'}}>Your Rating {parseInt(item.skillRate)} out of 10</Text>
                            </View>
                          </View>
                        </View>
                        :
                        null
                    }
                  </View>
                );
              })
            }
           </ScrollView>
          </View>
          <TouchableOpacity style={{height:height(7),width:width(100)}}
            onPress={()=>{
              this.rateSports()
            }}
          >
            <ImageBackground style={{height:height(7),width:width(100),justifyContent:'center',alignItems:'center'}} source={require('../img/bar.png')}>
              <Text style={{fontSize:totalSize(2.2),color:'white'}}>Save</Text>
            </ImageBackground>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },
  strip: {
    height:height(8),
    width:width(90),
    backgroundColor:'#ffffff',
    marginBottom:0,
    flexDirection:'row',
    borderRadius:5,
  },
});
