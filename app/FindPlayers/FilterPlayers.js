/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import MapView, { Polyline,Marker,Callout,PROVIDER_GOOGLE } from 'react-native-maps';
import Footer from '../Footer/Footer';
class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{height:height(9.5),flexDirection:'row',backgroundColor:'#1d1d4f'}}>
          <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
            <Image source={require('../img/setting.png')}  />
          </View>
          <View style={{width:width(70),justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
            <Image source={require('../img/logo.png')} style={{height:height(9),width:width(15)}} />
            <Text style={{fontSize: totalSize(2.5),color:'white'}}>Home</Text>
          </View>
          <View style={{width:width(15),justifyContent:'center',alignItems:'center'}}>
            <Image source={require('../img/friend.png')}  />
          </View>
        </View>
    );
  }
}
export default class MainScreen extends Component<Props> {
  static navigationOptions = {
       header: null
    }
  render() {
    return (
      <View style={styles.container}>
        <View style={{height:height(88)}}>
          <LogoTitle />
          <View style={{height:height(80)}}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: 31.531184,
                  longitude: 74.352473,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}>
                <MapView.Marker
                  coordinate={
                  { latitude: 31.531184, longitude: 74.352473 }
                  }
                  title={'my location'}
                  description={''}
                  pinColor={'hotpink'}
                  />
                  <Callout>
                    <View style={{height:height(80),width:width(100),backgroundColor:'red'}}>

                    </View>
                  </Callout>
              </MapView>
          </View>
        </View>
        <View style={{height:height(10)}}>
          <Footer nav={this.props.navigation}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
  height:height(80),

},
});
