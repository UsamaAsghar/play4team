import React from 'react';
import { Text, View } from 'react-native';
import {  createBottomTabNavigator, createAppContainer  } from 'react-navigation';

class FindPlayersTab extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Events</Text>
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>People</Text>
      </View>
    );
  }
}


const tabNavigator = createBottomTabNavigator({
  Events: {
      screen: FindPlayersTab,
     
  },
  People: {
      screen: SettingsScreen,
  },
  
},
);

export default createAppContainer(tabNavigator);