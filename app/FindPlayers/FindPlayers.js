/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import Footer from '../Footer/Footer';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { observer } from 'mobx-react';
import Store from '../Stores';
export default class FindPlayers extends Component<Props> {
  static navigationOptions = {
       header: null
    }
  render() {
    let { orderStore } = Store;
    var {params} = this.props.navigation.state;
    return (
      <View style={styles.container}>
        <View style={{height:height(85),backgroundColor:'rgba(29,37,86,0.8)'}}>
          <TouchableOpacity style={{height:height(67),width:width(100)}} onPress={()=>{ this.props._overlayController() }}></TouchableOpacity>
          <View style={{height:height(13),width:width(100),flexDirection:'row', alignItems:'center',justifyContent:'center'}}>
            {/* <TouchableOpacity style={{width:width(33),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                this.props.navigation.navigate('FindPlayer');
                orderStore.search = 'Player';
                orderStore.overlay = false;
              }}
            >
              <Image source={require('../img/players.png')} style={{height:height(7),width:width(28)}} />
            </TouchableOpacity> */}
            <TouchableOpacity style={{width:width(33),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                this.props.navigation.navigate('FindPlayer');
                orderStore.search = 'Team';
                orderStore.overlay = false;
              }}
            >
              <Image source={require('../img/players.png')} style={{height:height(7),width:width(28), resizeMode:'contain'}} />
              {/* <Image source={require('../img/teamSearch.png')} style={{height:height(7),width:width(28)}} /> */}
            </TouchableOpacity>
            <TouchableOpacity style={{width:width(34),justifyContent:'center',alignItems:'center'}}
              onPress={()=>{
                this.props.navigation.navigate('FindPlayer');
                orderStore.search = 'Events';
                orderStore.overlay = false;
              }}
            >
              <Image source={require('../img/event.png')} style={{height:height(7),width:width(28), resizeMode:'contain'}} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    height:height(78)
  },
});
