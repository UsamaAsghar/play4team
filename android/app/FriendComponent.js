/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TextInput,Image,ScrollView,TouchableOpacity
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';

export default class FriendComponent extends Component<Props> {


  static navigationOptions = {
        header:null,

    }
  render() {
    return (

        <View style={{height:height(10),width:width(93),backgroundColor:'#ffffff',flexDirection:'row'}}>
          <View style={{width:width(20),flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <Image source={require('../img/friend_img.png')} style={{height:height(8),width:width(14)}} />
          </View>
          <View style={{width:width(73),flexDirection:'row'}}>
            <View style={{width:width(53),justifyContent:'center'}}>
              <View style={{height:height(4),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(2.2),color:'black'}}>Hamza Butt</Text>
              </View>
              <View style={{height:height(4),justifyContent:'center',alignItems:'flex-start'}}>
                <Text style={{fontSize:totalSize(1.8),color:'gray'}}>Reliability-Skill-Fitness:7</Text>
              </View>
            </View>
            <View style={{width:width(20)}}>
              <View style={{height:height(5),width:width(18),justifyContent:'flex-start',alignItems:'flex-end'}}>
                <Text style={{fontSize:totalSize(1.3),color:'black',marginTop:5}}>Active 9 days</Text>
              </View>
              <View style={{height:height(5),width:width(18),justifyContent:'flex-end',alignItems:'flex-end'}}>
                <Text style={{fontSize:totalSize(1.3),color:'gray',marginBottom:5}}>05:35 pm</Text>
              </View>
            </View>
          </View>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});
