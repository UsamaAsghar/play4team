package com.foodplayers;
import android.content.Intent;
import com.tkporter.sendsms.SendSMSPackage;
import com.facebook.react.ReactActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.facebook.react.ReactActivityDelegate;
 import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
// import android.content.Intent;

public class MainActivity extends ReactActivity  {
    private PermissionListener listener;
    // @Override
    // public void onNewIntent(Intent intent) {
    //     super.onNewIntent(intent);
    //     setIntent(intent);
    // }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //probably some other stuff here
        SendSMSPackage.getInstance().onActivityResult(requestCode, resultCode, data);
    }
    

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
     if (listener != null)
     {
       listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
     }
     super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "FoodPlayers";
    }
    @Override
      protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
          @Override
          protected ReactRootView createRootView() {
           return new RNGestureHandlerEnabledRootView(MainActivity.this);
          }
        };
      }
}
